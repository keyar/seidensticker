//
//  SSShoppingCart.h
//  Seidensticker
//
//  Created by Keyar on 29/09/15.
//  Copyright © 2015 Nandha Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SSAppCommon.h"

@interface SSShoppingCart : NSObject {
    
    
}

+(SSShoppingCart *) shoppingCart;

- (NSString *) getFormattedCartTotalCost;

- (double) getCartTotalCost;

- (NSMutableArray *) getCartItems;

- (void) addToCart:(NSMutableDictionary *) productToAdd
          withSize:(NSString *)productSize
       withSizeIPA:(NSString *)productSizeIPA
       andQuantity:(NSUInteger)quantity
     quantityCount:(NSUInteger)quantityCount
       isCartTable:(BOOL)isCart;

- (void) addCouponToCart:(NSString *)couponCode;
- (void) removeCouponFromCart;
- (NSString *) getCouponCode;

- (void) setTotalForCart:(double)totalCost;
- (void) resetTotalForCart;
- (double) getUpdatedCartTotalCost;

- (NSUInteger) getIndexOfCartItemByProductId:(NSString *)product_id andSize:(NSString *)product_size;

- (NSUInteger) getItemCount;

- (NSString *) getCartProductsInJsonFormat;

- (void) clearCart;

- (void) removeItemForIndex:(NSUInteger) itemIndex;

@end

extern SSShoppingCart *sharedShoppingCart;
#define SHOPPINGCART (sharedShoppingCart? sharedShoppingCart:[SSShoppingCart shoppingCart])