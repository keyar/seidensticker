//
//  SSShoppingCart.m
//  Seidensticker
//
//  Created by Keyar on 29/09/15.
//  Copyright © 2015 Nandha Kumar. All rights reserved.
//

#import "SSShoppingCart.h"
@interface SSShoppingCart ()


@end

@implementation SSShoppingCart

SSShoppingCart *sharedShoppingCart = nil;

+ (SSShoppingCart *) shoppingCart {
    if(!sharedShoppingCart) {
        sharedShoppingCart = [[self alloc] init];
    }
    return sharedShoppingCart;
}

- (id) init {
    return self;
}

#pragma mark- Formatted Cart Total Cost

- (NSString *) getFormattedCartTotalCost {
    
    // Update call amount value
    NSNumber *amount = [[NSNumber alloc] initWithFloat:[self getUpdatedCartTotalCost]];
    
    // Write amount with currency symbols to the textfield
    NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [_currencyFormatter setCurrencyCode:@"EUR"];
    [_currencyFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier: @"fr_FR"]];
    return [_currencyFormatter stringFromNumber:amount];
    
}

#pragma mark- Cart Discount Total

- (void) setTotalForCart:(double)totalCost {
    [[NSUserDefaults standardUserDefaults] setDouble:totalCost forKey:@"CartTotalCost"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void) resetTotalForCart {
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"CartTotalCost"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (double) getUpdatedCartTotalCost {
    return [[[NSUserDefaults standardUserDefaults] valueForKey:@"CartTotalCost"] doubleValue];

}

#pragma mark- Cart Total

- (double) getCartTotalCost
{
    double totalCost = 0.0f ;
    NSArray *cartItems = [self getCartItems];
    for (int i = 0; i<cartItems.count; i++) {
        if ([[cartItems objectAtIndex:i] objectForKey:@"productPrice"] != NULL && ![[[cartItems objectAtIndex:i] objectForKey:@"productPrice"] isEqualToString:@""] && [[cartItems objectAtIndex:i] objectForKey:@"quantity"] != NULL && ![[[cartItems objectAtIndex:i] objectForKey:@"quantity"] isEqualToString:@""])
        {
            NSString *currentItemPrice  = [[[cartItems objectAtIndex:i] objectForKey:@"productPrice"] stringByReplacingOccurrencesOfString:@"\\U20ac" withString:@""];
            currentItemPrice            = [currentItemPrice stringByReplacingOccurrencesOfString:@"€" withString:@""];
            currentItemPrice            = [currentItemPrice stringByReplacingOccurrencesOfString:@" " withString:@""];
            currentItemPrice            = [currentItemPrice stringByReplacingOccurrencesOfString:@"," withString:@"."];
            totalCost                   = totalCost + [currentItemPrice doubleValue] * [[[cartItems objectAtIndex:i] objectForKey:@"quantity"] integerValue];
        }
    }
    return totalCost;
}

#pragma mark-Get Cart Items

- (NSMutableArray *) getCartItems
{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"CartArray"] == NULL) {
        NSMutableArray *myCartArray = [[NSMutableArray alloc] init];
        [[NSUserDefaults standardUserDefaults] setObject:myCartArray forKey:@"CartArray"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"CartArray"];
}

- (void) setCartItems:(NSMutableArray *)cartItems {
    [[NSUserDefaults standardUserDefaults] setObject:cartItems forKey:@"CartArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Coupons

- (void) addCouponToCart:(NSString *)couponCode {
    [[NSUserDefaults standardUserDefaults] setObject:couponCode forKey:@"CouponCode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void) removeCouponFromCart {
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"CouponCode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *) getCouponCode {
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"CouponCode"];
}
- (NSUInteger) getItemCount {
    NSMutableArray *myCartArray = [[self getCartItems] mutableCopy];
    NSLog(@"%@",myCartArray);
    return [myCartArray count];
}

- (void) removeItemForIndex:(NSUInteger) itemIndex {
    NSMutableArray *myCartArray = [[self getCartItems] mutableCopy];
    if (itemIndex < [myCartArray count]) {
        [myCartArray removeObjectAtIndex:itemIndex];
    }
    [self setCartItems:myCartArray];
}
- (void) addToCart:(NSMutableDictionary *) productToAdd
          withSize:(NSString *)productSize
       withSizeIPA:(NSString *)productSizeIPA
       andQuantity:(NSUInteger)quantity
     quantityCount:(NSUInteger)quantityCount
            isCartTable:(BOOL)isCart{
    
    NSMutableArray *myCartArray = [[self getCartItems] mutableCopy];
    NSMutableDictionary *myCartItem;
    
    
    NSUInteger cartItemIndex = [self getIndexOfCartItemByProductId:[[productToAdd objectForKey:@"id"] stringValue]
                                                           andSize:productSize];
    if (cartItemIndex == -1)
    {
        myCartItem = [[NSMutableDictionary alloc] init];
//        [myCartItem setValue:[productToAdd objectForKey:@"id"]              forKey:@"productId"];
        [myCartItem setValue:[productToAdd objectForKey:@"id"]              forKey:@"id"];

        [myCartItem setValue:[NSString stringWithFormat:@"%lu", (unsigned long)quantity]    forKey:@"quantity"];
        [myCartItem setValue:[productToAdd objectForKey:@"name"]            forKey:@"productName"];
        [myCartItem setValue:[productToAdd objectForKey:@"price"]           forKey:@"productPrice"];
        
        if(!isStoreApp){
            if([productToAdd objectForKey:@"special_price"])
            {
                [myCartItem setValue:[productToAdd objectForKey:@"special_price"]   forKey:@"productPrice"];
            }
            else{
                [myCartItem setValue:[productToAdd objectForKey:@"special_price"]   forKey:@"productDiscountPrice"];
            }
            
        }
        
        [myCartItem setValue:productSize    forKey:@"productSize"];
        [myCartItem setValue:productSizeIPA forKey:@"productSizeIPA"];
        [myCartItem setValue:[NSString stringWithFormat:@"%lu",(unsigned long)quantityCount] forKey:@"quantityArrayCount"];

        if ([[productToAdd objectForKey:@"product_image"] isKindOfClass:[NSArray class]]) {
            [myCartItem setValue:[[productToAdd objectForKey:@"product_image"] objectAtIndex:0]     forKey:@"productImage"];
        }
        [myCartArray addObject:myCartItem];
    } else {
//        myCartItem = [[myCartArray objectAtIndex:cartItemIndex] mutableCopy];
        NSUInteger itemCurrentQuantity;
        if(isCart == YES) {
            myCartItem = [productToAdd mutableCopy];
            itemCurrentQuantity = quantity;//[[myCartItem valueForKey:@"quantity"] integerValue] ;
        } else {
            myCartItem = [[myCartArray objectAtIndex:cartItemIndex] mutableCopy];
            itemCurrentQuantity = quantity;//[[myCartItem valueForKey:@"quantity"] integerValue] ;//+ quantity
        }
        [myCartItem setValue:[NSString stringWithFormat:@"%lu",(unsigned long)itemCurrentQuantity] forKey:@"quantity"];
        [myCartItem setValue:[NSString stringWithFormat:@"%lu",(unsigned long)quantityCount] forKey:@"quantityArrayCount"];

        [myCartArray replaceObjectAtIndex:cartItemIndex withObject:myCartItem];
    }
    
    [self setCartItems:myCartArray];
}

- (NSUInteger) getIndexOfCartItemByProductId:(NSString *)product_id
                                     andSize:(NSString *)productSize{
    NSUInteger indexFound = -1;
    NSMutableArray *myCartArray = [self getCartItems];
    for (int i=0; i< myCartArray.count; i++) {
        NSMutableDictionary *currentCartItem = [[myCartArray objectAtIndex:i] mutableCopy];
        if ([[[currentCartItem valueForKey:@"id"] stringValue]   isEqualToString:product_id] &&
            [[currentCartItem valueForKey:@"productSize"]               isEqualToString:productSize]) {
            return i;
            break;
        }// productId changed to id because to increase the count in the Cart table
    }
    return indexFound;
}

#pragma mark-Clear Cart Item

- (void) clearCart {
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"CartArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Get Products Json Format

- (NSString *) getCartProductsInJsonFormat {
    
    NSString *strProducts = @"";
    
    NSMutableArray *myCartArray = [self getCartItems];
    
    NSMutableArray *cartItemsArray = [[NSMutableArray alloc] init];
    
    for (int i=0; i<myCartArray.count ; i++) {
        NSMutableDictionary *myCartCurrentItem = [[myCartArray objectAtIndex:i] mutableCopy];
        NSLog(@"currentCartItem = %@",myCartCurrentItem);
        
        [cartItemsArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   [myCartCurrentItem objectForKey:@"id"],       @"product_id",
                                   [myCartCurrentItem objectForKey:@"productSizeIPA"],  @"ipa",
                                   [myCartCurrentItem objectForKey:@"quantity"],        @"qty",
                                   nil]];
    }//productId changed to id because to increase the count in the Cart table
    if (cartItemsArray != NULL) {
        strProducts = [COMMON getJsonDataFromDictionary:cartItemsArray];
    }
    return strProducts;
}

@end
