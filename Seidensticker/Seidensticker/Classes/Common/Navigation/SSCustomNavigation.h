//
//  SSCustomNavigation.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/18/15.
//  Copyright (c) 2015 Oclocksoftware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSCustomNavigation : UIViewController


@property (nonatomic, strong) IBOutlet UIImageView *bgImageView;

@property(nonatomic,weak) IBOutlet UIButton *navigationLeftBtn;
@property(nonatomic,weak) IBOutlet UIButton *navigationRightBtn;
@property(nonatomic,weak) IBOutlet UILabel  *navigationLeftLbl;
@property(nonatomic,weak) IBOutlet UILabel *navigationRightLbl;
@property(nonatomic,weak) IBOutlet UILabel  *titleLbl;
@property(nonatomic,weak) IBOutlet UIButton *navigationShopBtn;
@property(nonatomic,weak) IBOutlet UILabel *navigationShopLbl;
@property (strong, nonatomic) IBOutlet UIButton *navigationCatergoryShopBtn;
@property (strong, nonatomic) IBOutlet UILabel *navigationCatergoryShopLabel;

@property (strong, nonatomic) IBOutlet UILabel *navigationCategoryFiltreLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightLabelTralingConstraints;


- (void)setLeftHidden:(BOOL)_hide;
- (void)setRightHidden:(BOOL)_hide;
- (void)setTitleHidden:(BOOL)_hide;
- (void)setLeftLabelHidden:(BOOL)_hide;
- (void)setShopBtnHidden:(BOOL)_hide;
- (void)setShopLabelHidden:(BOOL)_hide;
- (void)setCatergoryShopBtnLabelHidden:(BOOL)_hide;



@end
