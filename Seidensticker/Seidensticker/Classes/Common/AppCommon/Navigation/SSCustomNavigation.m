//
//  SSCustomNavigation.m
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/18/15.
//  Copyright (c) 2015 Oclocksoftware. All rights reserved.
//

#import "SSCustomNavigation.h"
#import "SSConfig.h"
#import "SSAppDelegate.h"
#import "SSAppCommon.h"
@interface SSCustomNavigation ()

@end

@implementation SSCustomNavigation
@synthesize bgImageView,navigationLeftBtn,navigationRightBtn,titleLbl;
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setFrame:CGRectMake(0, 0,
                                   [UIScreen mainScreen].bounds.size.width,
                                   CGRectGetHeight(self.view.frame))];
    [titleLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(30)]];
    [titleLbl setTextColor:[UIColor blackColor]];
    titleLbl.textAlignment=NSTextAlignmentCenter;
    
    [self.view setFrame:CGRectMake(0, 0,
                                   [UIScreen mainScreen].bounds.size.width,
                                   CGRectGetHeight(self.view.frame))];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Navigation Methods
- (void)setRightHidden:(BOOL)_hide
{
    
    [navigationRightBtn setHidden:_hide];
}

- (void)setLeftHidden:(BOOL)_hide
{
    
    [navigationLeftBtn setHidden:_hide];
}

- (void)setTitleHidden:(BOOL)_hide
{
    
    [titleLbl setHidden:_hide];
}

@end
