//
//  SSCustomNavigation.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/18/15.
//  Copyright (c) 2015 Oclocksoftware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSCustomNavigation : UIViewController


@property (nonatomic, strong) IBOutlet UIImageView *bgImageView;

@property(nonatomic,weak) IBOutlet UIButton *navigationLeftBtn;
@property(nonatomic,weak) IBOutlet UIButton *navigationRightBtn;
@property(nonatomic,weak) IBOutlet UILabel  *titleLbl;

- (void)setLeftHidden:(BOOL)_hide;
- (void)setRightHidden:(BOOL)_hide;
- (void)setTitleHidden:(BOOL)_hide;


@end
