//
//  AppCommon.m
//
//  Created by ocsdev8 on 04/06/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <CommonCrypto/CommonCrypto.h>
#include <sys/xattr.h>
#import "Reachability.h"
#import "SSAppCommon.h"
#import "SSConfig.h"
#import "SSAppDelegate.h"
#import "NSString+HTML.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GAIFields.h"

@implementation SSAppCommon

SSAppCommon *sharedCommon = nil;

+ (SSAppCommon *)common {
    
    if (!sharedCommon) {
        
        sharedCommon = [[self alloc] init];
    }
    return sharedCommon;
}

- (id)init {
    
    return self;
}


-(void)TrackerWithName:(NSString *)message
{
    
    if ([BUILD_FOR isEqualToString:@"development"])
    {
        
    }
    else
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            // May return nil if a tracker has not already been initialized with a
            // property ID.
            id tracker = [[GAI sharedInstance] defaultTracker];
            
            // This screen name value will remain set on the tracker and sent with
            // hits until it is set to a new value or to nil.
            [tracker set:kGAIScreenName
                   value:message];
            
            // Previous V3 SDK versions
            // [tracker send:[[GAIDictionaryBuilder createAppView] build]];
            
            // New SDK versions
            [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        });
    }

    
    
}

#pragma mark - Get Current Device Info

- (currentDevice)getCurrentDevice
{
    if (([[UIScreen mainScreen] bounds].size.height == 480 && [[UIScreen mainScreen] bounds].size.width == 320) || ([[UIScreen mainScreen] bounds].size.width == 480 && [[UIScreen mainScreen] bounds].size.height == 320)) {
        return iPhone4;
    }
    else if (([[UIScreen mainScreen] bounds].size.height == 568 && [[UIScreen mainScreen] bounds].size.width == 320) || ([[UIScreen mainScreen] bounds].size.width == 568 && [[UIScreen mainScreen] bounds].size.height == 320))
        return iPhone5;
    else if (([[UIScreen mainScreen] bounds].size.height == 667 && [[UIScreen mainScreen] bounds].size.width == 375) || ([[UIScreen mainScreen] bounds].size.width == 667 && [[UIScreen mainScreen] bounds].size.height == 375))
        return iPhone6;
    else if (([[UIScreen mainScreen] bounds].size.height == 736 && [[UIScreen mainScreen] bounds].size.width == 414) || ([[UIScreen mainScreen] bounds].size.width == 736 && [[UIScreen mainScreen] bounds].size.height == 414))
        return iPhone6Plus;
    else if (([[UIScreen mainScreen] bounds].size.height == 768 && [[UIScreen mainScreen] bounds].size.width == 1024) || ([[UIScreen mainScreen] bounds].size.width == 1024 && [[UIScreen mainScreen] bounds].size.height == 768))
        return iPad;
    
    return 0;
}
#pragma mark - check device type

-(float)getDeviceHeight {
    
    return [[UIScreen mainScreen] bounds].size.height;
}

-(float)getOsVersion {
    
    return [[[UIDevice currentDevice] systemVersion] floatValue];
}

- (BOOL) iPhone4{
    
    if ([UIScreen mainScreen].bounds.size.height == 480) {
        
        return YES;
    }
    return NO;
}

- (BOOL) versionIsIOS7{
    
    if ([[[UIDevice currentDevice] systemVersion] integerValue] >= 7) {
        
        return YES;
    }
    return NO;
}

- (BOOL)iPhone5{
    
    if ([[UIScreen mainScreen] bounds].size.height == 568)
        
        return YES;
    else
        return NO;
}

- (BOOL)iPhone6{
    
    if ([[UIScreen mainScreen] bounds].size.height == 667)
        
        return YES;
    else
        return NO;
}
- (BOOL)iPhone6Plus{
    
    if ([[UIScreen mainScreen] bounds].size.height == 736)
        
        return YES;
    else
        return NO;
}

#pragma mark-iPad

- (BOOL) iPad{
    
    if ([[UIScreen mainScreen] bounds].size.height == 768){
        
        return YES;
    }
    return NO;
}

#pragma mark - NibName

- (NSString *)nibName:(NSString *)strNibName{
    
    if([self iPad])
        return   strNibName=[NSString stringWithFormat:@"%@iPad",strNibName];
    else
        return  strNibName=[NSString stringWithFormat:@"%@",strNibName];
}


#pragma mark - COLOR

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

#pragma mark - String Decoding 
- (NSString *) getDecodedString:(NSString *)currentString {
    currentString = [[currentString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] kv_decodeHTMLCharacterEntities];
    currentString = [[currentString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] kv_decodeHTMLCharacterEntities];

    currentString = [currentString stringByReplacingOccurrencesOfString:@"u20ac" withString:@"€"];
    currentString = [currentString stringByReplacingOccurrencesOfString:@"&euro;" withString:@"€"];
    return currentString;
}
#pragma mark - Resizeable Font

- (UIFont *)getResizeableFont:(UIFont *)currentFont {
    
    CGFloat sizeScale = 1;
    
    if (IS_IPHONE_DEVICE)
    {
        
        if ([COMMON getCurrentDevice]==iPhone6Plus )
        {
            sizeScale = 1.3;
        }
        else if ([COMMON getCurrentDevice]== iPhone6)
        {
            sizeScale = 1.2;
        }
    }
    else
    {
        sizeScale = 1.4;
    }
    return [currentFont fontWithSize:(currentFont.pointSize * sizeScale)];
}


void saveContentsToFile (id data, NSString* filename) {
    
    NSArray *namesArray = [filename componentsSeparatedByString:@"/"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileNameToSave;
    
    if ([namesArray count]>1) {
        NSString *dirNameToSave=[documentsDirectory stringByAppendingPathComponent:[namesArray objectAtIndex:0]];
        if (![[NSFileManager defaultManager] fileExistsAtPath:dirNameToSave])
            [[NSFileManager defaultManager] createDirectoryAtPath:dirNameToSave withIntermediateDirectories:YES attributes:nil error:nil];
        
        
        
        for (int i=1; i<[namesArray count]-1; i++) {
            
            dirNameToSave = [dirNameToSave stringByAppendingPathComponent:[namesArray objectAtIndex:i]];
            
            if (![[NSFileManager defaultManager] fileExistsAtPath:dirNameToSave])
                
                [[NSFileManager defaultManager] createDirectoryAtPath:dirNameToSave withIntermediateDirectories:YES attributes:nil error:nil];
            
        }
        
    }
    
    fileNameToSave = [documentsDirectory stringByAppendingPathComponent:filename];
    
    //NSLog(@"fileNameToSave = %@",fileNameToSave);
    
    [data writeToFile:fileNameToSave atomically:YES];
    
    // NSLog(@"data = %@",data);
    
    // NSLog(@"url = %@",[NSURL fileURLWithPath:fileNameToSave]);
    addSkipBackupAttributeToItemAtURL([NSURL fileURLWithPath:fileNameToSave]);
}

bool addSkipBackupAttributeToItemAtURL (NSURL* URL)
{

    NSError *error = nil;
    
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                    
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

#pragma mark - Reachable

-(BOOL) isReachableAlert {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        return YES;
    }
    else {
        [self reachabilityNotReachableAlert];
        return NO;
    }
}

-(void)reachabilityNotReachableAlert{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Message" preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    
    [alertController addAction:ok];
    
//    [self presentViewController:alertController animated:YES completion:nil];
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"]
                                                    message:@"It appears that you have lost network connectivity. Please check your network settings!"
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark - Cache Methods

-(void)removeFileFromLocalCache:(NSMutableDictionary *)configDict {
    
}

#pragma mark - Progress View Methods

#pragma mark - Progress View Methods
-(void)loadIcon:(UIViewController *)controller
{
    [self removeLoad];
    [self removeLoadingIcon];
    
    _progressView =[[M13ProgressViewRing alloc]init];
    _progressView. backgroundColor =[UIColor whiteColor];
    
    viewLoadingView = [[UIView alloc] initWithFrame:CGRectMake(_progressView.frame.origin.x - 5,
                                                               _progressView.frame.origin.y - 5,
                                                               (_progressView.frame.size.width+10),
                                                               (_progressView.frame.size.height+10))];
    [viewLoadingView setBackgroundColor:[UIColor blackColor]];
    [viewLoadingView.layer setCornerRadius:5.0];
    [viewLoadingView.layer setMasksToBounds:YES];

    [controller.view addSubview:viewLoadingView];
    [controller.view addSubview:_progressView];
    [_progressView performAction:M13ProgressViewActionNone animated:YES];
    [_progressView setProgress:0 animated:YES];
    if (_indeterminateSwitch.on){
        _progressSlider.enabled = YES;
        _iconControl.enabled = YES;
        _animateButton.enabled = YES;
        [_progressView setIndeterminate:YES];
    }else{
        _progressSlider.enabled = YES;
        _iconControl.enabled = YES;
        _animateButton.enabled = YES;
        [_progressView setIndeterminate:YES];
    }
}
- (void)removeLoad
{
    _progressView.hidden =YES;
    [_progressView removeFromSuperview];
    [viewLoadingView removeFromSuperview];
    
    [progressHUD hide:YES];
    [progressHUD removeFromSuperview];

}


#pragma mark-Loading

-(void)showLoadingIcon:(UIViewController *)controller
{
    [self removeLoadingIcon];
    [self removeLoad];
//    progressHUD = [[MBProgressHUD alloc] init];
//    progressHUD = [[MBProgressHUD alloc] initWithView:controller.view];
//    [controller.view addSubview:progressHUD];
//    [progressHUD show:YES];
    
    _progressView =[[M13ProgressViewRing alloc]init];
    _progressView. backgroundColor =[UIColor whiteColor];
    
    viewLoadingView = [[UIView alloc] initWithFrame:CGRectMake(_progressView.frame.origin.x - 5,
                                                               _progressView.frame.origin.y - 5,
                                                               (_progressView.frame.size.width+10),
                                                               (_progressView.frame.size.height+10))];
    [viewLoadingView setBackgroundColor:[UIColor blackColor]];
    [viewLoadingView.layer setCornerRadius:5.0];
    [viewLoadingView.layer setMasksToBounds:YES];
    
    [controller.view addSubview:viewLoadingView];
    [controller.view addSubview:_progressView];
    [_progressView performAction:M13ProgressViewActionNone animated:YES];
    [_progressView setProgress:0 animated:YES];
    if (_indeterminateSwitch.on){
        _progressSlider.enabled = YES;
        _iconControl.enabled = YES;
        _animateButton.enabled = YES;
        [_progressView setIndeterminate:YES];
    }else{
        _progressSlider.enabled = YES;
        _iconControl.enabled = YES;
        _animateButton.enabled = YES;
        [_progressView setIndeterminate:YES];
    }
}

-(void)loadIconParticularView:(UIView *)view
{
    [self removeLoadingIcon];
    progressHUD = [[MBProgressHUD alloc] init];
    progressHUD = [[MBProgressHUD alloc] initWithView:view];
    CGRect frameNew =  progressHUD.frame;
    frameNew.origin.y = progressHUD.frame.origin.y - 10;
    progressHUD.frame = frameNew;
    [view addSubview:progressHUD];
    [progressHUD show:YES];
}

-(void)removeLoadingIcon {
    
    [progressHUD hide:YES];
    [progressHUD removeFromSuperview];
    
    _progressView.hidden =YES;
    [_progressView removeFromSuperview];
    [viewLoadingView removeFromSuperview];
}


+(void)showSimpleAlertWithMessage:(NSString *)message{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: APP_TITLE
                                                   message: message
                                                  delegate: nil
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil];
    
    [alert show];
}

#pragma mark Show alert

- (void)showAlert:(NSString *)message {
    
    UIAlertView  *av = [[UIAlertView alloc] initWithTitle:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"]
                                                  message:message
                                                 delegate:nil
                                        cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                        otherButtonTitles:nil];
    [av show];
}

#pragma mark - Remove Nulls 

- (NSMutableDictionary *) removeNullValuesFromDictionary:(NSMutableDictionary *) currentDictionary {
    for (NSString * key in [currentDictionary allKeys])
    {
        if ([[currentDictionary objectForKey:key] isKindOfClass:[NSNull class]])
            [currentDictionary setObject:@"" forKey:key];
    }
    return currentDictionary;
}

#pragma mark - User Details

- (void) storeUserDetails:(NSMutableDictionary *) userDetails {
    userDetails = [self removeNullValuesFromDictionary:[userDetails mutableCopy]];
    [[NSUserDefaults standardUserDefaults] setObject:userDetails forKey:USER_DETAILS];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSDictionary *) getUserDetails{
    return [[NSUserDefaults standardUserDefaults] objectForKey:USER_DETAILS];
}
#pragma mark-removeUserDetails
- (void) removeUserDetails {
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:USER_DETAILS];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL) isUserLoggedIn {
    NSDictionary *userDetails = [self getUserDetails];
    if (userDetails != NULL) {
        return YES;
    }
    return NO;
}

- (NSString *)getCustomerSecureKey {
    NSDictionary *userDetails = [self getUserDetails];
    return [userDetails objectForKey:@"secure_key"];
}
-(void)storeUserAddress:(NSMutableDictionary *) userRegister{
    userRegister = [self removeNullValuesFromDictionary:[userRegister mutableCopy]];
    [[NSUserDefaults standardUserDefaults] setObject:userRegister forKey:USER_REGISTER];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - getUserAccessToken
-(NSString *)getCurrentUserID
{
    NSString *getCurrentUserID =[[NSUserDefaults standardUserDefaults]valueForKey:USER_CURRENT_ID];
    return getCurrentUserID;
}

-(void)setTabBarSelection:(BOOL)tabBool{
    [[NSUserDefaults standardUserDefaults] setBool:tabBool forKey:IS_TAB_BAR_CAN_SELECT];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(BOOL)getTabBarSelection{
   return [[NSUserDefaults standardUserDefaults] boolForKey:IS_TAB_BAR_CAN_SELECT];
}

#pragma mark - Screen size Ratio

+(CGSize)screenSize{
    static CGSize screenSize;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        screenSize = [[UIScreen mainScreen]bounds].size;
    });
    return screenSize;
}
+(CGFloat)widthRatio{
    
    static CGFloat width;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (IS_IPHONE_4||IS_IPHONE_5 || IS_IPAD){
            width =1.0;
        }else{
            width=((float)([SSAppCommon screenSize].width)/320);
        }
    });
    return width;
}
+(CGFloat)heightRatio{
    static CGFloat height;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (IS_IPHONE_4||IS_IPHONE_5|| IS_IPAD){
            height =1.0;
        }else{
            height=((float)([SSAppCommon screenSize].height)/568);
        }
    });
    return height;
}

#pragma mark Get string as JsonStructure Using NSDictionary

- (NSString *)getJsonDataFromDictionary:(NSMutableArray *)array {
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:nil] ;
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}



@end
