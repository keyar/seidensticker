//
//  AppCommon.h
//
//  Created by ocsdev8 on 04/06/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
#import "M13ProgressViewRing.h"

#import "MBProgressHUD.h"

#define CATEGORYVIEW_TAG 101
#define EMPTYCARTLABEL_TAG 1001

#pragma mark - Device model checking
#define IS_IPHONE UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone
#define IS_HEIGHT_GTE_480 ([[UIScreen mainScreen ] bounds].size.height >= 480.0f) && ([[UIScreen mainScreen ] bounds].size.height < 568.0f)
#define IS_HEIGHT_GTE_568 ([[UIScreen mainScreen ] bounds].size.height >= 568.0f) && ([[UIScreen mainScreen ] bounds].size.height < 667.0f)
#define IS_IPHONE_4 ( IS_IPHONE && IS_HEIGHT_GTE_480 )
#define IS_IPHONE_5 ( IS_IPHONE && IS_HEIGHT_GTE_568 )
#define IS_IPHONE_6 ( IS_IPHONE && IS_HEIGHT_GTE_667 )


typedef enum {
    
    iPhone4,
    iPhone5,
    iPhone6,
    iPhone6Plus,
    iPad
}currentDevice ;

@interface SSAppCommon : NSObject
{

    NSArray *permissions;
    MBProgressHUD *progressHUD;
    
    UIView *viewLoadingView;
    
}

+(SSAppCommon *) common;

- (currentDevice)getCurrentDevice;


// ResizeableFont
- (UIFont *)getResizeableFont:(UIFont *)currentFont;


-(BOOL) isReachableAlert;
-(void)reachabilityNotReachableAlert;

- (NSString *) getDecodedString:(NSString *)currentString;

void saveContentsToFile (id data, NSString* filename);

-(UIColor*)colorWithHexString:(NSString*)hex;

//Progress View

- (void)loadIcon:(UIViewController *)controller;
- (void)removeLoad;

-(void)showLoadingIcon:(UIViewController *)controller;
-(void)removeLoadingIcon;

@property (nonatomic, retain)   M13ProgressViewRing *progressView;

@property (nonatomic, retain) IBOutlet UISlider *progressSlider;
@property (nonatomic, retain) IBOutlet UIButton *animateButton;
@property (nonatomic, retain) IBOutlet UISegmentedControl *iconControl;
@property (nonatomic, retain) IBOutlet UISwitch *indeterminateSwitch;

+(void)showSimpleAlertWithMessage:(NSString *)message;
+(CGFloat)widthRatio;
+(CGFloat)heightRatio;

//User Details
- (void) storeUserDetails:(NSDictionary *) userDetails;
- (NSDictionary *) getUserDetails;
- (void) removeUserDetails;
- (BOOL) isUserLoggedIn;
- (NSString *)getCustomerSecureKey;
-(void)storeUserAddress:(NSMutableDictionary *) userRegister;
//- (NSDictionary *) getUserRegisterDetails;
//- (NSString *)getUserSecureKey;

-(NSString *)getCurrentUserID;
- (NSString *)getJsonDataFromDictionary:(NSMutableArray *)array;

- (void)showAlert:(NSString *)message;


-(void)TrackerWithName:(NSString *)message;

//
#pragma mark - TabBar
-(void)setTabBarSelection:(BOOL)tabBool;
-(BOOL)getTabBarSelection;

@end

extern SSAppCommon *sharedCommon;
#define COMMON (sharedCommon? sharedCommon:[SSAppCommon common])
