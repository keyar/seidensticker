//
//  ViewController.m
//  NMBottomTabBarExample
//
//  Created by Prianka Liz Kariat on 04/12/14.
//  Copyright (c) 2014 Prianka Liz. All rights reserved.
//

#import "NMBottomTabBarController.h"
#import "SSAppCommon.h"
#import "SSConfig.h"
#import "SSShopViewController.h"
#import "SSMonPanierAddressViewController.h"
#import "SSAccountViewController.h"
#import "SSMonPanierLoginViewController.h"

@interface NMBottomTabBarController ()
{
    SSMonPanierAddressViewController *monPanierAddressVC;
    SSAccountViewController *accountViewController;
    SSShopViewController *shopVC;
    
    
    SSMonPanierLoginViewController *monPanierLoginVC;
}

@end

@implementation NMBottomTabBarController
@synthesize controllers = _controllers;

-(id)init{
    
    self = [super init];
    if(self){
       
        
    }
    return self;
}
-(void)awakeFromNib{

    self.tabBar = [NMBottomTabBar new];
    selectedIndex = -1;
    self.controllers = [NSArray new];
    [self.view addSubview:self.tabBar];
    containerView = [UIView new];
    [self.view addSubview:containerView];
    [self setUpConstraintsForContainerView];
    [self setUpConstraintsForTabBar];
    [self.tabBar setDelegate:self];
  
}

-(void)setControllers:(NSArray *)controllers{
    
    _controllers = controllers;
     [self.tabBar layoutTabWihNumberOfButtons:self.controllers.count];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)setUpConstraintsForTabBar{
    
    NMBottomTabBar *tempTabBar = self.tabBar;
    if(IS_IPHONE4||IS_IPHONE5 ||IS_IPHONE6 ||IS_IPHONE6_Plus)
    {

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[tempTabBar(==50)]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tempTabBar)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[tempTabBar]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tempTabBar)]];
    [tempTabBar setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view setNeedsLayout];
    }
    else{
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[tempTabBar(==50)]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tempTabBar)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-150-[tempTabBar]-150-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tempTabBar)]];
        [tempTabBar setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.view setNeedsLayout];
    }
    

    
}
-(void)setUpConstraintsForContainerView {
    
    NMBottomTabBar *tempTabBar = self.tabBar;
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[containerView]-0-[tempTabBar]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(tempTabBar,containerView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[containerView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(containerView)]];
    [containerView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view setNeedsLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)selectTabAtIndex:(NSInteger)index{
   
   [self.tabBar setTabSelectedWithIndex:index];
}

- (void) hideTabBar {
    
    [self.tabBar setHidden:YES];
}
-(void) showTabBar
{
    [self.tabBar setHidden:NO];
}

-(void)didSelectTabAtIndex:(NSInteger)index{

    [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
    accountViewController=nil;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        accountViewController = [[SSAccountViewController alloc]initWithNibName:@"SSAccountViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        accountViewController = [[SSAccountViewController alloc]
                            initWithNibName:@"SSAccountViewController"
                            bundle:nil];
    }
    
    monPanierLoginVC = nil;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierLoginVC = [[SSMonPanierLoginViewController alloc]initWithNibName:@"SSMonPanierLoginViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierLoginVC = [[SSMonPanierLoginViewController alloc]
                            initWithNibName:@"SSMonPanierLoginViewController"
                            bundle:nil];
    }
    
    shopVC = nil;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        shopVC = [[SSShopViewController alloc]initWithNibName:@"SSShopViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        shopVC = [[SSShopViewController alloc]
                            initWithNibName:@"SSShopViewController"
                            bundle:nil];
    }
    
     if(selectedIndex == -1){
        UIViewController *destinationController = [self.controllers objectAtIndex:index];
        [self addChildViewController:destinationController];
        [destinationController didMoveToParentViewController:self];
        [containerView addSubview:destinationController.view];
        [self setUpContsraintsForDestinationControllerView:destinationController.view];
        selectedIndex = index;
        
    }
    else if(selectedIndex != index){
        
        UIViewController *sourceController = [self.controllers objectAtIndex:selectedIndex];
        // UIViewController *destinationController = [self.controllers objectAtIndex:index];
        
        
        UINavigationController *destinationController = (UINavigationController *)[self.controllers objectAtIndex:index];
        
        id baseViewController=[destinationController viewControllers][0];
        if([baseViewController isKindOfClass:[SSShopViewController class]])
            destinationController.navigationBarHidden=YES;
        
        if(index==2){
           
            if ([COMMON isUserLoggedIn]) {
                destinationController.viewControllers = [[NSArray alloc]initWithObjects:accountViewController, nil];
            }
            else{
                destinationController.viewControllers=[[NSArray alloc]initWithObjects:monPanierLoginVC, nil];
            }
        }
        else{
             destinationController.viewControllers=[[NSArray alloc]initWithObjects:baseViewController, nil];
        }
       
        [self addChildViewController:destinationController];
        
        [destinationController didMoveToParentViewController:self];
        
        [self transitionFromViewController:sourceController toViewController:destinationController duration:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
        
        } completion:^(BOOL finished) {
            
            [sourceController willMoveToParentViewController:nil];
            
            [sourceController removeFromParentViewController];
           
            [self setUpContsraintsForDestinationControllerView:destinationController.view];
            
            selectedIndex = index;
            
            if([self.delegate respondsToSelector:@selector(didSelectTabAtIndex:)]){
                
                [self.delegate didSelectTabAtIndex:selectedIndex];
            
            }
        }];
        
    }
    
    
    //NEW CODE FOR TAB BY NANDHA
    
    else {
        
        UINavigationController *destinationController = (UINavigationController *)[self.controllers objectAtIndex:index];
        
        if (index == 0 || index == 1 || index ==3) {
            
            [destinationController popToRootViewControllerAnimated:YES];
        }
        else if (index == 2){
            
                if ([COMMON isUserLoggedIn]) {
                    destinationController.viewControllers = [[NSArray alloc]initWithObjects:accountViewController, nil];
                }
                else{
                    [destinationController popToRootViewControllerAnimated:YES];
                }
        }
    }

   
}
-(BOOL)shouldSelectTabAtIndex:(NSInteger)index{
    
    if([self.delegate respondsToSelector:@selector(shouldSelectTabAtIndex:)]){
        return [self.delegate shouldSelectTabAtIndex:index];
    }
    return YES;
    
}
-(void)setUpContsraintsForDestinationControllerView : (UIView *)view {
    
    [containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(view)]];
    [containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(view)]];
    [view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [containerView setNeedsLayout];

}

@end
