//
//  SSAppDelegate.m
//  Seidensticker
//
//  Created by Nandha Kumar on 19/09/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSAppDelegate.h"
#import "SSConfig.h"
#import "SSWebServiceConfig.h"
#import "SSWebService.h"
#import "SSAppCommon.h"
#import <Parse/Parse.h>
#import "GAI.h"
#import "SSStoreLoginViewController.h"


#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#define URL_REQUEST_TYPE    @"seidenstickerios://"

@interface SSAppDelegate () {

    SSWebService *webservice;
    NMBottomTabBarController *tabBarController;
    NSDictionary *notificationPayload;
    BOOL isRedirect;

}
@property (nonatomic, strong) UINavigationController            *navigationController;

@end

@implementation SSAppDelegate

@synthesize shopViewController,searchViewController,monpanierViewController,magasinsViewController,categoryViewController,splash;
@synthesize accountViewController,monPanierLoginVC;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"isStoreApp = %d",isStoreApp);
    
    NSLog(@"Config File = %@",API_CONFIG);
    
    [COMMON setTabBarSelection:YES];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"redirect_categoryid"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"redirect_categoryname"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"redirect_productid"];
   
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        // Optional: automatically send uncaught exceptions to Google Analytics.
        [GAI sharedInstance].trackUncaughtExceptions = YES;
        
        // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
        [GAI sharedInstance].dispatchInterval = 20;
        
        // Optional: set Logger to VERBOSE for debug information.
        [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
        
        // Initialize tracker. Replace with your tracking ID.
        [[GAI sharedInstance] trackerWithTrackingId:GOOGLEANALYTICS_ID];
        
        [COMMON TrackerWithName:@"Application Launch"];

    });
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self initializeParse];
    });

    
    splash = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [splash setImage:[UIImage imageNamed:@"Default.png"]];
    [self.window addSubview:splash];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    
    
    // Register for Push Notitications
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    notificationPayload = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    
    [Fabric with:@[[Crashlytics class]]];

    isRedirect = NO;
    if (notificationPayload != NULL) {
        NSString *categoryId    = [notificationPayload objectForKey:@"c"];
        NSString *productId     = [notificationPayload objectForKey:@"p"];
        NSString *categoryName  = [notificationPayload objectForKey:@"t"];
        if (categoryName == NULL) {
            categoryName = @"";
        }
        if (categoryId != NULL) {
            [self setCategoryViewLaunchForCategoryId:categoryId withCategoryName:categoryName];
        }
        
        if (productId != NULL) {
            [self setProductViewLaunchForProductId:productId];
        }
    }

    
    if([COMMON isReachableAlert]){
        //[self removeFileFromLocalCache];
        [self loadConfigAPI];
    }
    else {
        UIApplicationShortcutItem *shortcutItem = [launchOptions objectForKey:UIApplicationLaunchOptionsShortcutItemKey];
        if(shortcutItem){
            [self handleShortCutItem:shortcutItem];
        } else {
            [self loadHomewView];
        }
    }
    return YES;
}

- (void) setCategoryViewLaunchForCategoryId:(NSString *)categoryID  withCategoryName:categoryName{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"redirect_categoryid"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"redirect_categoryname"];
    
    [[NSUserDefaults standardUserDefaults] setValue:categoryID      forKey:@"redirect_categoryid"];
    [[NSUserDefaults standardUserDefaults] setValue:categoryName    forKey:@"redirect_categoryname"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [shopViewController redirectCategorymethod:categoryID withTitle:categoryName];

}

- (void) setProductViewLaunchForProductId:(NSString *)productID {
    
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"redirect_productid"];
    [[NSUserDefaults standardUserDefaults] setValue:productID forKey:@"redirect_productid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [shopViewController  redirectProduct:productID];
}

- (void) initializeParse {
    [Parse enableLocalDatastore];
    
    // Override point for customization after application launch.
    [Parse setApplicationId:@"i3mFFrykmYz5xYnscz2RjNj0tPVgmEDAdYvwqKVT"
                  clientKey:@"BEXHZxRjTaioYXB3jxzDMvc75D0lGM7aGO5R8nXg"];

}

#pragma mark - Handle Short Cut Item

- (void)handleShortCutItem:(UIApplicationShortcutItem *)shortcutItem  {
    [self loadHomewView];
    if([shortcutItem.type isEqualToString:@"com.oclockapps.seidensticker.ShortCut.Home"]){
        [tabBarController selectTabAtIndex:0];
    }
    if([shortcutItem.type isEqualToString:@"com.oclockapps.seidensticker.ShortCut.Search"]){
        [tabBarController selectTabAtIndex:1];
    }
    if([shortcutItem.type isEqualToString:@"com.oclockapps.seidensticker.ShortCut.Cart"]){
        [tabBarController selectTabAtIndex:2];
    }
    if([shortcutItem.type isEqualToString:@"com.oclockapps.seidensticker.ShortCut.Magasins"]){
        [tabBarController selectTabAtIndex:3];
    }
}

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler {
    NSLog(@"%@", shortcutItem.type);
    [COMMON setTabBarSelection:YES];
    [self handleShortCutItem:shortcutItem];
    
}

#pragma mark - Push Notifications

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    // Get Bundle Info for Remote Registration (handy if you have more than one app)
    NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    
    NSString *currentVersionBuild = [NSString stringWithFormat:@"%@(%@)",version,build];
    
    
    NSLog(@"deviceSystemVersion:  %@" ,currentVersionBuild);
    
    // Check what Notifications the user has turned on.  We registered for all three, but they may have manually disabled some or all of them.
    NSUInteger rntypes = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    
    // Set the defaults to disabled unless we find otherwise...
    NSString *pushBadge = (rntypes & UIRemoteNotificationTypeBadge) ? @"enabled" : @"disabled";
    NSString *pushAlert = (rntypes & UIRemoteNotificationTypeAlert) ? @"enabled" : @"disabled";
    NSString *pushSound = (rntypes & UIRemoteNotificationTypeSound) ? @"enabled" : @"disabled";
    
    NSLog(@"rntypes:  %lu",  (unsigned long)rntypes);
    
    // Get the users Device Model, Display Name, Unique ID, Token & Version Number
    UIDevice *dev = [UIDevice currentDevice];
    NSString *deviceUdid = @"";//[OpenUDID value];
    
    NSString *deviceName = dev.name;
    NSString *deviceModel = dev.model;
    NSString *deviceSystemVersion = dev.systemVersion;
    
    
    
    // NSString *pushSecureKey=[[NSUserDefaults standardUserDefaults]valueForKey:PUSH_SECURE_KEY];
    
    NSString *pushSecureKey = [COMMON getCustomerSecureKey];
    
    NSString *buildFor =  BUILD_FOR;
    
    if((NSString *)[NSNull null] == pushSecureKey || pushSecureKey==nil){
        pushSecureKey=@"";//0
    }
    
    // Prepare the Device Token for Registration (remove spaces and < >)
    NSString *currentDeviceToken = [[[[deviceToken description]
                                      stringByReplacingOccurrencesOfString:@"<"withString:@""]
                                     stringByReplacingOccurrencesOfString:@">" withString:@""]
                                    stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSLog(@"buildFor---%@", buildFor);
    
    NSLog(@"deviceToken---%@", currentDeviceToken);
    
    
    [[NSUserDefaults standardUserDefaults] setValue:deviceUdid forKey:ID_DEVICE];
    [[NSUserDefaults standardUserDefaults] setValue:currentDeviceToken forKey:DEVICETOKEN];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    NSString *urlString = [NSString stringWithFormat:@"/apns.php?task=%@&appname=%@&appversion=%@&deviceuid=%@&devicetoken=%@&devicename=%@&devicemodel=%@&deviceversion=%@&pushbadge=%@&pushalert=%@&pushsound=%@&sessionid=%@&development=%@&type=%lu",@"register", appName,currentVersionBuild, deviceUdid, currentDeviceToken, deviceName, deviceModel, deviceSystemVersion, pushBadge, pushAlert, pushSound,pushSecureKey,buildFor, (unsigned long)rntypes];
    
    NSLog(@"urlString:  %@",  urlString);
    
    // Register the Device Data
    
    [self registerDeviceWithDeviceToken:urlString];

}
-(void)registerDeviceWithDeviceToken:(NSString*)apnsUrl{
    [webservice registerPushNotification:apnsUrl
                                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                     NSLog(@"responseObject:  %@",  responseObject);
                                 }
                                 failure:^(AFHTTPRequestOperation *operation, id error) {
                                     
                                     NSLog(@"ERRORDATA:  %@",  error);
                                 }];
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    NSLog(@"error--> = %@",error);
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    NSLog(@"UserInfo = %@",userInfo);
    
    if (application.applicationState != UIApplicationStateActive ) {
        [self handleRemoteNotification:application userInfo:userInfo];
    }
    
    
    //Parse - Notify Payload
    if (application.applicationState == UIApplicationStateInactive) {
        // The application was just brought from the background to the foreground,
        // so we consider the app as having been "opened by a push notification."
        [PFAnalytics trackAppOpenedWithRemoteNotificationPayload:userInfo];
    }

    [PFPush handlePush:userInfo];
}

- (void)applicationDidFinishLaunching:(NSNotification *)notification {
    // ... other Parse setup logic here
    [PFAnalytics trackAppOpenedWithRemoteNotificationPayload:[notification userInfo]];
}

-(void) handleRemoteNotification:(UIApplication *)application userInfo:(NSDictionary *)userInfo {
    
    NSLog(@"c=%@, p=%@",[userInfo objectForKey:@"c"],[userInfo objectForKey:@"p"]);
    
    NSString *categoryId    = [userInfo objectForKey:@"c"];
    NSString *productId     = [userInfo objectForKey:@"p"];
    NSString *categoryName  = [userInfo objectForKey:@"t"];
    if (categoryName == NULL) {
        categoryName = @"";
    }
    if (categoryId != NULL) {
        [self setCategoryViewLaunchForCategoryId:categoryId withCategoryName:categoryName];
    }
    
    if (productId != NULL) {
        [self setProductViewLaunchForProductId:productId];
    }
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void) loadHomewView
{
   shopViewController = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        shopViewController = [[SSShopViewController alloc]initWithNibName:@"SSShopViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        shopViewController = [[SSShopViewController alloc]
                     initWithNibName:@"SSShopViewController"
                     bundle:nil];
        
    }
    UINavigationController *shopNavigationController=[[UINavigationController alloc]initWithRootViewController:shopViewController];
    
    searchViewController = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        searchViewController = [[SSSearchViewController alloc]initWithNibName:@"SSSearchViewControlleriPad" bundle:[NSBundle mainBundle]];
        
    }
    else {
        searchViewController = [[SSSearchViewController alloc]
                                   initWithNibName:@"SSSearchViewController"
                                   bundle:nil];
        
    }
    UINavigationController *searchNavigationController=[[UINavigationController alloc]initWithRootViewController:searchViewController];
    
    
    accountViewController = nil;
    
    monPanierLoginVC = nil;
    
    
//    accountViewController = [[SSAccountViewController alloc]
//                                 initWithNibName:@"SSAccountViewController"
//                                 bundle:nil];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierLoginVC = [[SSMonPanierLoginViewController alloc]initWithNibName:@"SSMonPanierLoginViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierLoginVC = [[SSMonPanierLoginViewController alloc]
                             initWithNibName:@"SSMonPanierLoginViewController"
                             bundle:nil];
    }
    UINavigationController *monPanierNavigationController=[[UINavigationController alloc]initWithRootViewController:monPanierLoginVC];
       
    magasinsViewController = nil;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        magasinsViewController = [[SSMagasinsViewController alloc]initWithNibName:@"SSMagasinsViewControlleriPad" bundle:[NSBundle mainBundle]];
        
    }
    else {
        magasinsViewController = [[SSMagasinsViewController alloc]
                                initWithNibName:@"SSMagasinsViewController"
                                bundle:nil];
        
    }
    UINavigationController *magasinsNavigationController=[[UINavigationController alloc]initWithRootViewController:magasinsViewController];
    
    tabBarController = (NMBottomTabBarController *)self.window.rootViewController;
    
    tabBarController.controllers = [NSArray arrayWithObjects:shopNavigationController,searchNavigationController,monPanierNavigationController,magasinsNavigationController, nil];
    
    tabBarController.delegate = self;
    
   // [tabBarController willMoveToParentViewController:nil];
    
  //  [tabBarController removeFromParentViewController];

    [tabBarController.tabBar configureTabAtIndex:0
                            andTitleOrientation :kTItleToBottomOfIcon
                   withUnselectedBackgroundImage:nil
                         selectedBackgroundImage:nil
                                       iconImage:[UIImage imageNamed:@"article_active_icon"]
                                         andText:@"ARTICLES"
                                     andTextFont:FuturaStd_Book(10)
                                   andFontColour:[UIColor blackColor]];
    
    
    [tabBarController.tabBar configureTabAtIndex:1
                            andTitleOrientation :kTItleToBottomOfIcon
                   withUnselectedBackgroundImage:nil
                         selectedBackgroundImage:nil
                                       iconImage:[UIImage imageNamed:@"rechercher_inactive_icon"]
                                         andText:@"RECHERCHER"
                                     andTextFont:FuturaStd_Book(10)
                                   andFontColour:[UIColor blackColor]];
    
    
    [tabBarController.tabBar configureTabAtIndex:2
                            andTitleOrientation :kTItleToBottomOfIcon
                   withUnselectedBackgroundImage:nil
                         selectedBackgroundImage:nil
                                       iconImage:[UIImage imageNamed:@"profile_inactive_icon"]
                                         andText:@"MON COMPTE"
                                     andTextFont:FuturaStd_Book(10)
                                   andFontColour:[UIColor blackColor]];

    
    [tabBarController.tabBar configureTabAtIndex:3
                            andTitleOrientation :kTItleToBottomOfIcon
                   withUnselectedBackgroundImage:nil
                         selectedBackgroundImage:nil
                                       iconImage:[UIImage imageNamed:@"home_inactive_icon"]
                                         andText:@"MAGASINS"
                                     andTextFont:FuturaStd_Book(10)
                                   andFontColour:[UIColor blackColor]];

    
    [tabBarController selectTabAtIndex:0];
    
    [self.window makeKeyAndVisible];
}

#pragma mark - Config API

-(void)loadConfigAPI{
    webservice = [[SSWebService alloc]init];
    [webservice getConfigData:API_CONFIG success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSLog(@"Config Data = %@",responseObject);
            [webservice saveLocalConfigData:responseObject];
            [webservice getMenuData:API_KEY_MENU success:^(AFHTTPRequestOperation *operation, id responseObject) {
                [webservice saveLocalMenuData:responseObject];
            } failure:^(AFHTTPRequestOperation *operation, id error) {
                
            }];
            
        } else {
            NSLog(@"ERROR IN config Data");
        }
        
        [self loadHomewView];

        
    }failure:^(AFHTTPRequestOperation *operation, id error) {
        
       
        [self loadHomewView];
        
    }];
    
}

-(void)selectTabAtIndex : (NSInteger)index {
    [tabBarController selectTabAtIndex:index];
}

- (void) hideTabBar {
    [tabBarController hideTabBar];
   
}

-(void) showTabBar
{
    [tabBarController showTabBar];
    
}

-(void)popToHomePage
{
    
}


#pragma mark - Optionally Handle the URL

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    if (!url) {
        return NO;
    }
    NSString *URLString = [url absoluteString];
    NSRange rangeValue = [URLString rangeOfString:URL_REQUEST_TYPE options:NSCaseInsensitiveSearch];
    
    if (rangeValue.length > 0) {
        return [self redirectFromAd:URLString];
    }
    return NO;
}

- (BOOL) redirectFromAd:(NSString *)URLString {
     [tabBarController selectTabAtIndex:0];
    URLString = [URLString stringByReplacingOccurrencesOfString:URL_REQUEST_TYPE withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [URLString length])];
    if (URLString != NULL) {
        NSArray *arrSeperate = [URLString componentsSeparatedByString:@":"];
        if ([arrSeperate count]>1) {
            if ([[arrSeperate objectAtIndex:0] isEqualToString:@"c"]) {
                NSArray *catSeperate = [[arrSeperate objectAtIndex:1] componentsSeparatedByString:@"t="];
                NSString *categoryId;
                NSString *categoryName;
                categoryId      = [catSeperate objectAtIndex:0];
                if ([catSeperate count] > 0) {
                    categoryName    = [catSeperate objectAtIndex:1];
                }
                
                [self setCategoryViewLaunchForCategoryId:categoryId withCategoryName:categoryName];
                return YES;
            } else if ([[arrSeperate objectAtIndex:0] isEqualToString:@"p"]) {
                [self setProductViewLaunchForProductId:[arrSeperate objectAtIndex:1]];
                return YES;
            } else
                return YES;
        } else
            return YES;

    } else
        return YES;
    
    return YES;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    if (!url) {
        return NO;
    }
    
    NSString *URLString = [url absoluteString];
    NSRange rangeValue = [URLString rangeOfString:URL_REQUEST_TYPE options:NSCaseInsensitiveSearch];
    
    if (rangeValue.length > 0) {
        return [self redirectFromAd:URLString];
    }
    return NO;
}


@end
