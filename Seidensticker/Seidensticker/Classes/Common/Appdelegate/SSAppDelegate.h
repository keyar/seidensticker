//
//  SSAppDelegate.h
//  Seidensticker
//
//  Created by Nandha Kumar on 19/09/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NMBottomTabBarController.h"

#import "SSShopViewController.h"

#import "SSSearchViewController.h"

#import "SSMonPanierViewController.h"

#import "SSMagasinsViewController.h" 

#import "SSCategoryViewController.h"

#import "SSPaymentDetailViewController.h"

#import "SSMonPanierViewController.h"

#import "SSPaymentWebViewController.h"

#import "SSAccountViewController.h"

#import "SSMonPanierLoginViewController.h"

#import "SSOrderConfirmationViewController.h"
#import "GAIFields.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"

#import "SSMonPanierAddressDisplayViewController.h"


@interface SSAppDelegate : UIResponder <UIApplicationDelegate,NMBottomTabBarControllerDelegate>


extern int systemIDForDeviceType();

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) NMBottomTabBarController *tabBarController;

@property (nonatomic,retain) SSShopViewController *shopViewController;

@property (nonatomic,retain) SSSearchViewController *searchViewController;

@property (nonatomic,retain) SSMonPanierViewController *monpanierViewController;

@property (nonatomic,retain) SSMagasinsViewController *magasinsViewController;

@property (nonatomic,retain) SSCategoryViewController *categoryViewController;

@property (nonatomic,retain) SSPaymentDetailViewController *paymentDetailViewController;

@property (nonatomic,retain) SSPaymentWebViewController *paymentWebViewController;

@property (nonatomic,retain) SSMonPanierAddressDisplayViewController *addressDisplayViewController;

@property (nonatomic,retain) SSMonPanierViewController *monPanierViewController;

@property (nonatomic,retain) SSOrderConfirmationViewController *orderConfirmationViewController;

@property (nonatomic,retain) SSAccountViewController *accountViewController;

@property (nonatomic,retain) SSMonPanierLoginViewController *monPanierLoginVC;


@property (nonatomic,retain) UIImageView *splash;
-(void)selectTabAtIndex : (NSInteger)index;


-(void) loadHomewView;
- (void) hideTabBar;

- (void) showTabBar;

-(void) popToHomePage;
@end

