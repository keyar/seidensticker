//
//  Config.h
//  Seidensticker
//
//  Created by Nandha Kumar on 19/09/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//


#define GOOGLEANALYTICS_ID               @"UA-72314150-1"

#define APP_NAME [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"]

#define screen_width                    [[UIScreen mainScreen] bounds].size.width
#define screen_height                   [[UIScreen mainScreen] bounds].size.width
//#define  IS_IPAD_DEVICE (([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPhone)?NO:YES)

#define IS_IPHONE_DEVICE                (([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)?NO:YES)

#define IS_IPHONE4                      (([[UIScreen mainScreen] bounds].size.height-480)?NO:YES)

#define IS_IPHONE5                      (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)

#define IS_IPHONE6                      (([[UIScreen mainScreen] bounds].size.height-667)?NO:YES)

#define IS_IPHONE6_Plus                 (([[UIScreen mainScreen] bounds].size.height-736)?NO:YES)

#define IS_IPAD                         (([[UIScreen mainScreen] bounds].size.height-768)?NO:YES)

// OS Versions
#define IS_GREATER_IOS7                 (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)?YES:NO)

#define IS_GREATER_IOS8                 (([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)?YES:NO)

#define IS_GREATER_IOS9                 (([[[UIDevice currentDevice] systemVersion] floatValue] >= 9)?YES:NO)



//Default keys
#define IOS_OLDER_THAN_6                ([[[UIDevice currentDevice] systemVersion]  floatValue] < 6.0 )
#define IS_GREATER_THAN_OR_EQUAL_IOS7   (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)?YES:NO)
#define IS_LESSER_THAN_OR_EQUAL_IOS7    (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)?YES:NO)
#define IS_GREATER_THAN_OR_EQUAL_IOS8   (([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)?YES:NO)
#define IS_LESS_THAN_IOS8               (([[[UIDevice currentDevice] systemVersion] floatValue] < 8)?YES:NO)

#define FuturaStd_Book(FontSize)       [UIFont fontWithName:@"FuturaStd-Book" size:FontSize]

#define VIEW_BG_COLOR                  [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f]
#define ORANGE_COLOR                   [UIColor colorWithRed:253.0f/255.0f green:127.0f/255.0f blue:36.0f/255.0f alpha:1.0f]
#define SKYBLUE_COLOR                  [UIColor colorWithRed:99.0f/255.0f green:177.0f/255.0f blue:204.0f/255.0f alpha:1.0f]
#define BLACK_COLOR                    [UIColor colorWithRed:3.0f/255.0f green:3.0f/255.0f blue:3.0f/255.0f alpha:1.0f]
#define GRAY_COLOR                     [UIColor colorWithRed:143.0f/255.0f green:143.0f/255.0f blue:143.0f/255.0f alpha:1.0f]
#define PINK_COLOR                     [UIColor colorWithRed:229.0f/255.0f green:66.0f/255.0f blue:166.0f/255.0f alpha:1.0f]


#define BANNERTIMING                      2.0

#define APP_TITLE                   @"Seidensticker"


#define IsStoreUserLoggedIn         @"IsStoreUserLoggedIn"

#pragma mark - Defaults Keys

#define USER_DETAILS                @"USER_DETAILS"

#define USER_REGISTER                @"USER_REGISTER"

#define USER_CURRENT_ID               @"USER_CURRENT_ID"

#pragma mark - Error Message - Login Page and Register Page

#define INVALID_EMAIL               @"Adresse e-mail invalide."

#define EMAIL_REQUIRED              @"Adresse e-mail requise."

#define PASSWORD_REQUIRED           @"Mot de passe requis."

#define INVALID_PASSWORD            @"mot de passe non valable"

#define PASSWORD_DOESNOT_MATCH      @"Le mot de passe ne correspond pas au mot de passe de confirmation"

#define PASSWORD_NOT_CORRECT        @"Le mot de passe n'est pas le bon."


#define INVALID_AUTHENTICATION      @"Échec d’authentification"

#define FILL_FIRSTNAME              @"Prénom requis"

#define FILL_SURNAME                @"Nom de famille requis"

#define INVALID_EMAIL_ID            @"Cette adresse e-mail n'est pas valide"

#define INVALID_BIRTHDATE           @"Date invalide de naissance"

#define INVALID_POSTAL_CODE         @"Le code postal est invalide. Il doit  saisi comme suit : 00000"

#define FILL_DETAILS                @"s'il vous plaît remplir tous les détails"

#define INVOICE_REQUIRED            @"Facture Magasin requise."//numéro de facture requis

#define INVALID_INVOICE             @"Facture Invalide."

#define INVOICE_CHARACTERS_CHECK    @"Le numéro de facture magasin ne peut excéder 9 caractères"

#define FranceLatitude          @"47.0000"
#define FranceLongitude         @"2.0000"


#define IS_TAB_BAR_CAN_SELECT       @"IsTabBarCanSelect"

#ifdef DEBUG
#define NSLog(...) NSLog(__VA_ARGS__)
#else
#define NSLog(...)
#endif
