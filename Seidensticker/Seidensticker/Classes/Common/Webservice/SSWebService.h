//
//  SSWebService.h
//  Seidensticker
//
//  Created by Keyar on 21/09/15.
//  Copyright © 2015 Nandha Kumar. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"
#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef void (^WebServiceRequestSuccessHandler)(AFHTTPRequestOperation *operation, id responseObject);

typedef void (^WebServiceRequestFailureHandler)(AFHTTPRequestOperation  *operation, id error);

typedef void (^WebServiceRequestXMLSuccessHandler)(AFHTTPRequestOperation  *operation);
typedef void (^WebServiceRequestXMLFailureHandler)(AFHTTPRequestOperation  *operation, NSError *error);


@interface SSWebService : AFHTTPRequestOperationManager

+ (SSWebService *) service;

- (NSDictionary *) getLocalConfigData;

- (void) saveLocalConfigData:(NSDictionary *)configData;

- (NSString *) getAPIUrl:(NSString *)APIKey;

- (void) getCommandsData:(NSString *)secureKey
                 success:(WebServiceRequestSuccessHandler)success
                 failure:(WebServiceRequestFailureHandler)failure;

- (void) getBannersData:(NSString *) APIBanners
                success:(WebServiceRequestSuccessHandler)success
                failure:(WebServiceRequestFailureHandler)failure;

- (void) getSliderData:(NSString *) APISlider
               success:(WebServiceRequestSuccessHandler)success
               failure:(WebServiceRequestFailureHandler)failure;

- (void)getConfigData:(NSString *)APIConfig
              success:(WebServiceRequestSuccessHandler)success
              failure:(WebServiceRequestFailureHandler)failure;

- (NSDictionary *) getLocalMenuData;
- (void) saveLocalMenuData:(NSDictionary *)configData;

- (void) getMenuData:(NSString *)APIMenu
             success:(WebServiceRequestSuccessHandler)success
             failure:(WebServiceRequestFailureHandler)failure;

- (void) getRequestForURL:(NSString *) APIURL
                  success:(WebServiceRequestSuccessHandler)success
                  failure:(WebServiceRequestFailureHandler)failure;

- (void) userLoginWithEmailAddress:(NSString *)emailAddress
                       andPassword:(NSString *) password
                       deviceToken:(NSString *)deviceToken
                           success:(WebServiceRequestSuccessHandler)success
                           failure:(WebServiceRequestFailureHandler)failure;

- (void) checkLoginWithEmailAddress:(NSString *)emailAddress
                            success:(WebServiceRequestSuccessHandler)success
                            failure:(WebServiceRequestFailureHandler)failure;

- (void) createUserWithEmailAddress:(NSString *) emailAddress
                           password:(NSString *) password
                          firstName:(NSString *) firstname
                           lastName:(NSString *) lastname
                    dateOfBirthYear:(NSString *) dobYear
                   dateOfBirthMonth:(NSString *) dobMonth
                     dateOfBirthDay:(NSString *) dobDay
                        deviceToken:(NSString *)deviceToken
                            success:(WebServiceRequestSuccessHandler) success
                            failure:(WebServiceRequestFailureHandler) failure;

#pragma mark - Profile Updation
- (void) UserUpdationWithSecureKey:(NSString *) secureKey
                      emailAddress:(NSString *) emailAddress
                         firstName:(NSString *) firstname
                          lastName:(NSString *) lastname
                   dateOfBirthYear:(NSString *) dobYear
                  dateOfBirthMonth:(NSString *) dobMonth
                    dateOfBirthDay:(NSString *) dobDay
                        oldPasword:(NSString *) oldPassword
                       newPassword:(NSString *) newPassword
                   confirmPassword:(NSString *) confirmPassword
                           success:(WebServiceRequestSuccessHandler) success
                           failure:(WebServiceRequestFailureHandler) failure;

- (void) getPaymentMethodsForUser:(NSString *) secureKey
                          success:(WebServiceRequestSuccessHandler)success
                          failure:(WebServiceRequestFailureHandler)failure;

- (void) getAddressListForUser:(NSString *) secureKey
                       success:(WebServiceRequestSuccessHandler)success
                       failure:(WebServiceRequestFailureHandler)failure;


- (void) AddAddressForUserWithFirstname:(NSString *) firstname
                               lastname:(NSString *) lastname
                               address1:(NSString *) address1
                               postcode:(NSString *) postcode
                                   city:(NSString *) city
                                  phone:(NSString *) phone
                                  alias:(NSString *) alias
                              addressId:(NSString *) addressId
                       forUserSecureKey:(NSString *) secureKey
                                success:(WebServiceRequestSuccessHandler) success
                                failure:(WebServiceRequestFailureHandler) failure;

- (void) searchProductsForKeyword:(NSString *) keyword
                          success:(WebServiceRequestSuccessHandler)success
                          failure:(WebServiceRequestFailureHandler)failure;

- (void) getListStores:(BOOL) isAll
               success:(WebServiceRequestSuccessHandler)success
               failure:(WebServiceRequestFailureHandler)failure;

- (void) syncCartWithProducts:(NSString *) productsData
                    codePromo:(NSString *) codePromoData
             forUserSecureKey:(NSString *) secureKey
                      success:(WebServiceRequestSuccessHandler) success
                      failure:(WebServiceRequestFailureHandler) failure;

- (void) gotoPaymentWithProducts:(NSString *) productsData
               withPaymentMethod:(NSString *) paymentMethod
                forUserSecureKey:(NSString *) secureKey
           userDeliveryAddressID:(NSString *) deliveryAddressID
                         success:(WebServiceRequestSuccessHandler) success
                         failure:(WebServiceRequestFailureHandler) failure;


#pragma mark - Request for Zero Payment Process
- (void) gotoPaymentWithInvoiceProducts:(NSString *) productsData
                      withPaymentMethod:(NSString *) paymentMethod
                       forUserSecureKey:(NSString *) secureKey
                        userInvoiceCode:(NSString *) invoice
                         withCouponCode:(NSString *) coupon
                  userDeliveryAddressID:(NSString *) deliveryAddressID
                                success:(WebServiceRequestSuccessHandler) success
                                failure:(WebServiceRequestFailureHandler) failure;

- (void) registerPushNotification:(NSString *)apnsUrl
                          success:(WebServiceRequestSuccessHandler)success
                          failure:(WebServiceRequestFailureHandler)failure;

@end
