//
//  SSWebService.m
//  Seidensticker
//
//  Created by Keyar on 21/09/15.
//  Copyright © 2015 Nandha Kumar. All rights reserved.
//

#import "SSWebService.h"

#import "AFNetworking.h"
#import "SSAppCommon.h"
#import "SSWebServiceConfig.h"
#import "SSAppCommon.h"

static const NSString *WebServicePost   = @"POST";
static const NSString *WebServiceGet    = @"GET";
static const NSString *WebServicePut    = @"PUT";
static const NSString *WebServiceDelete = @"DELETE";

static const NSString *WebServiceContentJSON = @"application/json";
static const NSString *WebServiceContentFORM = @"application/x-www-form-urlencoded";
static NSString       *WebServiceMimeType    = @"image/jpeg";

@interface SSWebService (){
    NSString *urlString;
    NSString *baseUrl;
    BOOL isConfigAPI;
}

@end

@implementation SSWebService

- (id)init {
    self = [super init];
    if (self) {
        
        self.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/plain", @"application/json",@"image/png",nil];
    }
    return self;
}

+ (SSWebService *)service {
    return [[SSWebService alloc] init];
}

#pragma mark - Config Data
- (void) getConfigData:(NSString *)APIConfig
               success:(WebServiceRequestSuccessHandler)success
               failure:(WebServiceRequestFailureHandler)failure {
    
    urlString = [NSString stringWithFormat:@"%@/%@",BASE_URL,APIConfig];
    NSLog(@"urlString = %@",urlString);
    isConfigAPI = YES;
    
    [self sendRequestWithURLString:urlString
                     andParameters:nil
                            method:WebServiceGet
             saveContentToFileName:API_CONFIG
           completionSucessHandler:success
          completionFailureHandler:failure];
}

- (NSDictionary *) getLocalConfigData {
    return [[NSUserDefaults standardUserDefaults] objectForKey:DATA_CONFIG];
}

- (void) saveLocalConfigData:(NSDictionary *)configData {
    [[NSUserDefaults standardUserDefaults] setObject:configData forKey:DATA_CONFIG];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *) getAPIUrl:(NSString *)APIKey {
    NSDictionary *dictConfig = [self getLocalConfigData];
    if ([dictConfig objectForKey:@"apis"] != NULL &&
        [[dictConfig objectForKey:@"apis"] objectForKey:APIKey] != NULL &&
        [[[dictConfig objectForKey:@"apis"] objectForKey:APIKey] objectForKey:BUILD_FOR]) {
        return [[[dictConfig objectForKey:@"apis"] objectForKey:APIKey] objectForKey:BUILD_FOR];
    }
    else
        return @"";
}

#pragma mark - Menu

- (NSDictionary *) getLocalMenuData {
    return [[NSUserDefaults standardUserDefaults] objectForKey:DATA_MENU];
}

- (void) saveLocalMenuData:(NSDictionary *)configData {
    [[NSUserDefaults standardUserDefaults] setObject:configData forKey:DATA_MENU];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void) getMenuData:(NSString *)APIMenu
             success:(WebServiceRequestSuccessHandler)success
             failure:(WebServiceRequestFailureHandler)failure {
    urlString = [self getAPIUrl:APIMenu];
    NSLog(@"urlString = %@",urlString);
    
    [self sendRequestWithURLString:urlString
                     andParameters:nil
                            method:WebServiceGet
             saveContentToFileName:API_KEY_MENU
           completionSucessHandler:success
          completionFailureHandler:failure];
    
}

#pragma mark - Banners

- (void) getBannersData:(NSString *) APIBanners
                success:(WebServiceRequestSuccessHandler)success
                failure:(WebServiceRequestFailureHandler)failure {
    urlString = [self getAPIUrl:APIBanners];
    NSLog(@"urlString = %@",urlString);
    
    [self sendRequestWithURLString:urlString
                     andParameters:nil
                            method:WebServiceGet
             saveContentToFileName:API_KEY_BANNERS
           completionSucessHandler:success
          completionFailureHandler:failure];
    
}

#pragma mark - Slider

- (void) getSliderData:(NSString *) APISlider
               success:(WebServiceRequestSuccessHandler)success
               failure:(WebServiceRequestFailureHandler)failure {
    urlString = [self getAPIUrl:APISlider];
    NSLog(@"urlString = %@",urlString);
    
    [self sendRequestWithURLString:urlString
                     andParameters:nil
                            method:WebServiceGet
             saveContentToFileName:API_KEY_SLIDER
           completionSucessHandler:success
          completionFailureHandler:failure];
    
}

#pragma mark - Search

- (void) searchProductsForKeyword:(NSString *) keyword
                          success:(WebServiceRequestSuccessHandler)success
                          failure:(WebServiceRequestFailureHandler)failure {
    urlString = [self getAPIUrl:API_KEY_SEARCH];
    
    urlString = [NSString stringWithFormat:@"%@?q=%@&id_lang=1&orderby=position&orderway=desc",urlString,keyword];
    
    [self sendRequestWithURLString:urlString
                     andParameters:nil
                            method:WebServiceGet
             saveContentToFileName:API_KEY_SEARCH
           completionSucessHandler:success
          completionFailureHandler:failure];
    
}

#pragma mark - List Stores

- (void) getListStores:(BOOL) isAll
               success:(WebServiceRequestSuccessHandler)success
               failure:(WebServiceRequestFailureHandler)failure {
    urlString = [self getAPIUrl:API_KEY_STORES];
    
    if (isAll) {
        urlString = [NSString stringWithFormat:@"%@?all=1",urlString];
    }
    
    [self sendRequestWithURLString:urlString
                     andParameters:nil
                            method:WebServiceGet
             saveContentToFileName:@""
           completionSucessHandler:success
          completionFailureHandler:failure];
}


#pragma mark - Login
- (void) userLoginWithEmailAddress:(NSString *) emailAddress
                       andPassword:(NSString *) password
                       deviceToken:(NSString *) deviceToken
                           success:(WebServiceRequestSuccessHandler) success
                           failure:(WebServiceRequestFailureHandler) failure {
    
    NSMutableDictionary *loginDict = [[NSMutableDictionary alloc] init];
    
    if(emailAddress)   [loginDict setObject:emailAddress forKey:@"email"];
    if(password)       [loginDict setObject:password     forKey:@"passwd"];
    if(deviceToken)    [loginDict setObject:deviceToken     forKey:@"devicetoken"];

    
    [self sendRequestWithURLString:[self getAPIUrl:API_KEY_LOGIN]
                     andParameters:loginDict
                            method:WebServicePost
             saveContentToFileName:API_KEY_LOGIN
           completionSucessHandler:success
          completionFailureHandler:failure];
    
}

#pragma mark - Login Create

- (void) checkLoginWithEmailAddress:(NSString *) emailAddress
                            success:(WebServiceRequestSuccessHandler) success
                            failure:(WebServiceRequestFailureHandler) failure {
    
    NSMutableDictionary *loginDict = [[NSMutableDictionary alloc] init];
    
    if(emailAddress)   [loginDict setObject:emailAddress forKey:@"email_create"];
    [loginDict setObject:@"1" forKey:@"SubmitCreate"];
    
    [self sendRequestWithURLString:[self getAPIUrl:API_KEY_LOGIN]
                     andParameters:loginDict
                            method:WebServicePost
             saveContentToFileName:API_KEY_LOGIN
           completionSucessHandler:success
          completionFailureHandler:failure];
}

#pragma mark - Registration

- (void) createUserWithEmailAddress:(NSString *) emailAddress
                           password:(NSString *) password
                          firstName:(NSString *) firstname
                           lastName:(NSString *) lastname
                    dateOfBirthYear:(NSString *) dobYear
                   dateOfBirthMonth:(NSString *) dobMonth
                     dateOfBirthDay:(NSString *) dobDay
                        deviceToken:(NSString *) deviceToken
                            success:(WebServiceRequestSuccessHandler) success
                            failure:(WebServiceRequestFailureHandler) failure {
    NSMutableDictionary *loginDict = [[NSMutableDictionary alloc] init];
    
    if(emailAddress)    [loginDict setObject:emailAddress forKey:@"email"];
    if(password)        [loginDict setObject:password     forKey:@"passwd"];
    if(firstname)       [loginDict setObject:firstname     forKey:@"firstname"];
    if(lastname)        [loginDict setObject:lastname     forKey:@"lastname"];
    if(dobYear)         [loginDict setObject:dobYear     forKey:@"years"];
    if(dobMonth)        [loginDict setObject:dobMonth     forKey:@"months"];
    if(dobDay)          [loginDict setObject:dobDay     forKey:@"days"];
    if(deviceToken)     [loginDict setObject:deviceToken     forKey:@"devicetoken"];
    
    
    [loginDict setObject:@"0" forKey:@"is_new_customer"];
    
    
    
    [self sendRequestWithURLString:[self getAPIUrl:API_KEY_REGISTER]
                     andParameters:loginDict
                            method:WebServicePost
             saveContentToFileName:API_KEY_REGISTER
           completionSucessHandler:success
          completionFailureHandler:failure];
    
}
#pragma mark - Profile Updation

- (void) UserUpdationWithSecureKey:(NSString *) secureKey
                      emailAddress:(NSString *) emailAddress
                         firstName:(NSString *) firstname
                          lastName:(NSString *) lastname
                   dateOfBirthYear:(NSString *) dobYear
                  dateOfBirthMonth:(NSString *) dobMonth
                    dateOfBirthDay:(NSString *) dobDay
                        oldPasword:(NSString *) oldPassword
                       newPassword:(NSString *) newPassword
                   confirmPassword:(NSString *) confirmPassword
                            success:(WebServiceRequestSuccessHandler) success
                            failure:(WebServiceRequestFailureHandler) failure {
    
    
    urlString = [self getAPIUrl:API_KEY_UPDATE];
    
    urlString = [NSString stringWithFormat:@"%@?secure_key=%@&firstname=%@&lastname=%@&email=%@&old_passwd=%@&passwd=%@&confirmation=%@&years=%@&months=%@&days=%@",urlString,secureKey,firstname,lastname,emailAddress,oldPassword,newPassword,confirmPassword,dobYear,dobMonth,dobDay];
    
    NSLog(@"urlString = %@",urlString);
    
    [self sendRequestWithURLString:urlString
                     andParameters:nil
                            method:WebServiceGet
             saveContentToFileName:API_KEY_UPDATE
           completionSucessHandler:success
          completionFailureHandler:failure];
    
    
}



#pragma mark - Get Payment Methods
- (void) getPaymentMethodsForUser:(NSString *) secureKey
                          success:(WebServiceRequestSuccessHandler)success
                          failure:(WebServiceRequestFailureHandler)failure {
    urlString = [self getAPIUrl:API_KEY_PAYMENTMETHOD];
    
    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
    [userDict setObject:secureKey forKey:@"secure_key"];
    
    [self sendRequestWithURLString:urlString
                     andParameters:userDict
                            method:WebServicePost
             saveContentToFileName:API_KEY_PAYMENTMETHOD
           completionSucessHandler:success
          completionFailureHandler:failure];
    
}


#pragma mark - Address Requests

- (void) getAddressListForUser:(NSString *) secureKey
                       success:(WebServiceRequestSuccessHandler)success
                       failure:(WebServiceRequestFailureHandler)failure {
    urlString = [self getAPIUrl:API_KEY_ADDRESS];
    
    urlString = [NSString stringWithFormat:@"%@?secure_key=%@",urlString,secureKey];
    
    [self sendRequestWithURLString:urlString
                     andParameters:nil
                            method:WebServiceGet
             saveContentToFileName:API_KEY_ADDRESS
           completionSucessHandler:success
          completionFailureHandler:failure];
    
}
#pragma mark - Add Address with field

- (void) AddAddressForUserWithFirstname:(NSString *) firstname
                               lastname:(NSString *) lastname
                               address1:(NSString *) address1
                               postcode:(NSString *) postcode
                                   city:(NSString *) city
                                  phone:(NSString *) phone
                                  alias:(NSString *) alias
                              addressId:(NSString *) addressId
                       forUserSecureKey:(NSString *) secureKey
                                success:(WebServiceRequestSuccessHandler) success
                                failure:(WebServiceRequestFailureHandler) failure {
    NSMutableDictionary *loginDict = [[NSMutableDictionary alloc] init];
    
    if(firstname)           [loginDict setObject:firstname      forKey:@"firstname"];
    if(lastname)            [loginDict setObject:lastname       forKey:@"lastname"];
    if(address1)            [loginDict setObject:address1       forKey:@"address1"];
    if(postcode)            [loginDict setObject:postcode       forKey:@"postcode"];
    if(city)                [loginDict setObject:city           forKey:@"city"];
    if(phone)               [loginDict setObject:phone          forKey:@"phone"];
    if(alias)               [loginDict setObject:alias          forKey:@"alias"];

    if(![addressId isEqualToString:@""]){
        if(addressId)    [loginDict setObject:addressId          forKey:@"id"];
    }
    
    [loginDict setObject:secureKey forKey:@"secure_key"];
//    [loginDict setObject:@"Mon adresse" forKey:@"alias"];
    [loginDict setObject:@"8" forKey:@"id_country"];
    
    [self sendRequestWithURLString:[self getAPIUrl:API_KEY_ADD_ADDRESS]
                     andParameters:loginDict
                            method:WebServicePost
             saveContentToFileName:API_KEY_ADD_ADDRESS
           completionSucessHandler:success
          completionFailureHandler:failure];
    
}

#pragma mark - Cart Actions

- (void) syncCartWithProducts:(NSString *) productsData
                    codePromo:(NSString *) codePromoData
             forUserSecureKey:(NSString *) secureKey
                      success:(WebServiceRequestSuccessHandler) success
                      failure:(WebServiceRequestFailureHandler) failure {
    
    urlString = [self getAPIUrl:API_KEY_CART];
    
    NSLog(@"urlSAtring =%@",urlString);
    
    NSMutableDictionary *cartDictionary = [[NSMutableDictionary alloc] init];
    [cartDictionary setObject:productsData   forKey:@"cartproducts"];
    if (![codePromoData isEqualToString:@""]) {
        [cartDictionary setObject:codePromoData  forKey:@"discount_name"];
    }
    
    if (secureKey != NULL && ![secureKey isEqualToString:@""]) {
        [cartDictionary setObject:secureKey forKey:@"secure_key"];
    }

    
    [self POST:urlString parameters:cartDictionary
       success:^(AFHTTPRequestOperation *operation, id responseDict) {
           
           if (success) success(operation,responseDict);
       }
       failure:^(AFHTTPRequestOperation *operation, NSError *error) {
           if (failure) failure(operation,error);
       }];
}


#pragma mark - Request for Payment [Used isStoreApp Payment Page]

- (void) gotoPaymentWithProducts:(NSString *) productsData
               withPaymentMethod:(NSString *) paymentMethod
                forUserSecureKey:(NSString *) secureKey
           userDeliveryAddressID:(NSString *) deliveryAddressID
                         success:(WebServiceRequestSuccessHandler) success
                         failure:(WebServiceRequestFailureHandler) failure {
    
    urlString = [self getAPIUrl:API_KEY_PAYMENTPAGE];
    
    if(deliveryAddressID==nil){
        deliveryAddressID=@"";
    }
    NSLog(@"GetApi Url= %@",[self getAPIUrl:API_KEY_PAYMENTPAGE]);
    NSMutableDictionary *paymentDictionary = [[NSMutableDictionary alloc] init];
    [paymentDictionary setObject:paymentMethod  forKey:@"paymentmethodname"];
    [paymentDictionary setObject:secureKey      forKey:@"secure_key"];
    [paymentDictionary setObject:productsData   forKey:@"addtocartproducts"];
    [paymentDictionary setObject:deliveryAddressID   forKey:@"id_address_delivery"];
    
    if(isStoreApp){
         [paymentDictionary setObject:@"yes"         forKey:@"confirm"];
    }
    
    [self POST:urlString parameters:paymentDictionary
       success:^(AFHTTPRequestOperation *operation, id responseDict) {
           if (success) success(operation,responseDict);
       }
       failure:^(AFHTTPRequestOperation *operation, NSError *error) {
           if (failure) failure(operation,error);
       }];
}

#pragma mark - Request for Zero Payment Process

- (void) gotoPaymentWithInvoiceProducts:(NSString *) productsData
                      withPaymentMethod:(NSString *) paymentMethod
                       forUserSecureKey:(NSString *) secureKey
                        userInvoiceCode:(NSString *) invoice
                         withCouponCode:(NSString *) coupon
                  userDeliveryAddressID:(NSString *) deliveryAddressID
                                success:(WebServiceRequestSuccessHandler) success
                                failure:(WebServiceRequestFailureHandler) failure {
    
    urlString = [self getAPIUrl:API_KEY_PAYMENTPAGE];
    if(deliveryAddressID==nil){
        deliveryAddressID=@"";
    }
    
    NSLog(@"GetApi Url= %@",[self getAPIUrl:API_KEY_PAYMENTPAGE]);
    NSMutableDictionary *paymentDictionary = [[NSMutableDictionary alloc] init];
    [paymentDictionary setObject:paymentMethod  forKey:@"paymentmethodname"];
    [paymentDictionary setObject:secureKey      forKey:@"secure_key"];
    [paymentDictionary setObject:productsData   forKey:@"addtocartproducts"];
    [paymentDictionary setObject:invoice        forKey:@"invoice"];
    [paymentDictionary setObject:coupon         forKey:@"discount_name"];
                    //changed coupon to discount_name
    [paymentDictionary setObject:@"yes"         forKey:@"confirm"];
    [paymentDictionary setObject:deliveryAddressID   forKey:@"id_address_delivery"];
    
    [self POST:urlString parameters:paymentDictionary
       success:^(AFHTTPRequestOperation *operation, id responseDict) {
           if (success) success(operation,responseDict);
       }
       failure:^(AFHTTPRequestOperation *operation, NSError *error) {
           if (failure) failure(operation,error);
       }];
}


- (void) getCommandsData:(NSString *)secureKey
                 success:(WebServiceRequestSuccessHandler)success
                 failure:(WebServiceRequestFailureHandler)failure {
    
    urlString = [self getAPIUrl:API_KEY_COMMANDES];
    
    urlString = [NSString stringWithFormat:@"%@?secure_key=%@",urlString,secureKey];
    
    NSLog(@"urlString = %@",urlString);
    
    [self sendRequestWithURLString:urlString
                     andParameters:nil
                            method:WebServiceGet
             saveContentToFileName:API_KEY_COMMANDES
           completionSucessHandler:success
          completionFailureHandler:failure];
    
}
- (void) registerPushNotification:(NSString *)apnsUrl
                          success:(WebServiceRequestSuccessHandler)success
                          failure:(WebServiceRequestFailureHandler)failure {
    
    urlString = [NSString stringWithFormat:@"%@/%@",BASE_URL,PUSH_NOTIFICATION];
    
    urlString = [NSString stringWithFormat:@"%@%@",urlString,apnsUrl];
    
    NSLog(@"urlString = %@",urlString);
    
    [self sendRequestWithURLString:urlString
                     andParameters:nil
                            method:WebServiceGet
             saveContentToFileName:PUSH_NOTIFICATION
           completionSucessHandler:success
          completionFailureHandler:failure];
    
}


#pragma mark - Request for URL
- (void) getRequestForURL:(NSString *) APIURL
                  success:(WebServiceRequestSuccessHandler)success
                  failure:(WebServiceRequestFailureHandler)failure {
    
    [self sendRequestWithURLString:APIURL
                     andParameters:nil
                            method:WebServiceGet
             saveContentToFileName:API_KEY_SLIDER
           completionSucessHandler:success
          completionFailureHandler:failure];
}

#pragma mark - Request
- (void)sendRequestWithURLString:(NSString *)url
                   andParameters:(NSDictionary *)parameters
                          method:(const NSString *)method
           saveContentToFileName:(NSString *)fileName
         completionSucessHandler:(WebServiceRequestSuccessHandler)sucesshandler
        completionFailureHandler:(WebServiceRequestFailureHandler)failurehandler {
    
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if ( method == WebServiceGet) {
        
        [self GET:url parameters:nil
          success:^(AFHTTPRequestOperation *operation, id responseDict) {
              if (sucesshandler) sucesshandler(operation,responseDict);
              if (![fileName isEqualToString:@""]) {
                  saveContentsToFile(responseDict,fileName);
              }
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              if (failurehandler) failurehandler(operation, error);
          }];
    }
    else{
        
        [self POST:url parameters:parameters
           success:^(AFHTTPRequestOperation *operation, id responseDict) {
               if (sucesshandler) sucesshandler(operation,responseDict);
           }
           failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               if (failurehandler) failurehandler(operation,error);
           }];
    }
}



@end
