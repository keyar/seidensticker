//
//  SSWebServiceConfig.h
//  Seidensticker
//
//  Created by Keyar on 21/09/15.
//  Copyright © 2015 Nandha Kumar. All rights reserved.
//

#ifndef SSWebServiceConfig_h
#define SSWebServiceConfig_h


#ifdef DEBUG

#define BUILD_FOR                      @"development"

#else

#define BUILD_FOR                      @"production"

#endif


////Production URL
//#define BUILD_FOR           @"production"
//#define BASE_URL            @"http://www.seidensticker.fr"
////Development URL
//#define BUILD_FOR           @"development"
//#define BASE_URL            @"http://stagingsetup.com/other-projects/seidensticker-live.fr"


#define BASE_URL            @"http://www.yourhostingnow.com/jean/seidensticker.fr/"

//#define BASE_URL              @"http://www.seidensticker.fr"  //LIVE URL



/*for store APP
 #define API_CONFIG              @"appconfiguration.php"
 #define API_KEY_LOGIN           @"login-new"
 */


#define API_KEY_PRODUCTS        @"products"
#define API_KEY_PRODUCT_DETAIL  @"productdetail"
#define API_KEY_MENU            @"topmenu"
#define API_KEY_SLIDER          @"slider"
#define API_KEY_BANNERS         @"banners"
#define API_KEY_REGISTER        @"register-new"
#define API_KEY_UPDATE          @"editprofile"
#define API_KEY_ADDRESS         @"listaddress-new"
#define API_KEY_ADD_ADDRESS     @"addaddress-new"
#define API_KEY_PAYMENTMETHOD   @"paymentmethod-new"
#define API_KEY_SEARCH          @"search"
#define API_KEY_STORES          @"liststores"
#define API_KEY_CART            @"addtocart"
#define API_KEY_PAYMENTPAGE     @"paymentintermediate" 
#define API_KEY_CODEPROMO       @"addtocart"
#define API_KEY_COMMANDES       @"ordershistory"

//zero purchase key
#define ZERO_PAY_STORE_PURCHASE @"storepurchase"

//StoreApp
#define STORE_APP_PAYMENTNAME   @"storekeeper"

#define DATA_CONFIG             @"DATA_CONFIG"
#define DATA_MENU               @"DATA_MENU"


#define DATA_KEY_BANNERS        @"banners"

#define DATA_KEY_SLIDER         @"slider"

#define ID_DEVICE               @"deviceUdid"

#define DEVICETOKEN             @"deviceToken"

#define PUSH_NOTIFICATION       @"custom-api/push"



#endif /* SSWebServiceConfig_h */
