//
//  AccountViewController.m
//  Seidensticker
//
//  Created by Admin on 26/07/16.
//  Copyright © 2016 Nandha Kumar. All rights reserved.
//

#import "SSAccountViewController.h"
#import "SSShopViewController.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSCustomNavigation.h"
#import "SSCategoryViewController.h"
#import "SSShopViewCustomCell.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#import "SSMenuCustomView.h"
#import "AsyncImage.h"
#import "SSMenuCustomView.h"
#import "UIImageView+AFNetworking.h"
#import "SSMenuDetailViewController.h"
#import "SSProductPageViewController.h"
#import "SSShoppingCart.h"
#import "SSAppDelegate.h"
#import "SSMonPanierAddressModifierViewController.h"
#import "SSMonPanierAddressViewController.h"
#import "SSCommandsViewController.h"
#import "SSMonPanierRegisterViewController.h"
#import "SSMonPanierAddressDisplayViewController.h"
#import "SSStoreLoginViewController.h"


@interface SSAccountViewController ()<NMBottomTabBarControllerDelegate>
{
    SSWebService *webservice;

    UIImageView *navBarHairlineImageView;
    
    UILabel *qtyCount;
    
    SSAppDelegate *appDelegate;
    
    SSCustomNavigation *navigation;
    
    NSMutableDictionary *commandsDictionary;

}
@property (strong, nonatomic) UIWindow *window;

@end

@implementation SSAccountViewController
@synthesize commandsBtn,addressBtn,informationsBtn,logoutBtn;
@synthesize cartItems;
@synthesize isCommanderClicked;
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    webservice = [[SSWebService alloc] init];
    appDelegate = (SSAppDelegate *) [UIApplication sharedApplication].delegate;
    //[self loadNavigation];
    //[self styleNavBar];
    [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
    [self.navigationController setNavigationBarHidden:NO];
    

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(isStoreApp){
        if ([[NSUserDefaults standardUserDefaults] boolForKey:IsStoreUserLoggedIn]==NO) {
            [self loadStoreLoginPage];
        }
        
    }
    
    [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
    [self.navigationController setNavigationBarHidden:NO];
    
    for (UIView *view in self.navigationController.navigationBar.subviews) {
         [view removeFromSuperview];
    }
     [self loadNewNavigationBar];
    //[self loadNavigation];
    
    [COMMON TrackerWithName:@"Account View"];
    
    [commandsBtn setBackgroundColor:[UIColor clearColor]];
    [addressBtn setBackgroundColor:[UIColor clearColor]];
    [informationsBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];

    
   
    [self setUpInitialBtns];
    
 
    NSDictionary *userDict = [[NSDictionary alloc]init];
    
    userDict = [COMMON getUserDetails];
}

-(void)loadStoreLoginPage{
    
    SSStoreLoginViewController *storeLoginViewController=nil;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        storeLoginViewController = [[SSStoreLoginViewController alloc]initWithNibName:@"SSStoreLoginViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        storeLoginViewController = [[SSStoreLoginViewController alloc]initWithNibName:@"SSStoreLoginViewController" bundle:[NSBundle mainBundle]];
    }
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:storeLoginViewController];
    
    [self.navigationController presentViewController:navigationController animated:NO completion:nil];
    
}
-(void)loadNewNavigationBar{
    

    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    UINavigationBar *newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, screen_width, 64.0)];
    [newNavBar setBackgroundColor:[UIColor clearColor]];
    
    [newNavBar setBarTintColor:[UIColor whiteColor]];
    newNavBar.tag=1001;
    UINavigationItem *newItem = [[UINavigationItem alloc] init];
    newItem.title = @"Mon compte";
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                            NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)]}];
    [newNavBar pushNavigationItem:newItem animated:false];
    
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,64,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    
    [newNavBar addSubview:navBorder];
    
    UILabel *lblLefttitle = [UILabel new];
    [lblLefttitle setText:@"Retour"];
    lblLefttitle.textColor = [UIColor orangeColor];
    [lblLefttitle setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [lblLefttitle setTextAlignment:NSTextAlignmentLeft];
    //UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMenuBtn:)];
    // [lblLefttitle addGestureRecognizer:tap];
    lblLefttitle.userInteractionEnabled = YES;
    [lblLefttitle setBackgroundColor:[UIColor clearColor]];
    [lblLefttitle setFrame:CGRectMake(20,(newNavBar.frame.size.height/2)-5 , newNavBar.frame.size.width/3, 30)];
    //[newNavBar addSubview:lblLefttitle];
    
    cartItems = [SHOPPINGCART getCartItems];
    NSInteger quantity = 0;
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"shop_active_icon"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(commandsCartBtnAction:)forControlEvents:UIControlEventTouchUpInside];
    
    
    [button setFrame:CGRectMake(0, 0, 38, 38)];
    [button setBackgroundColor:[UIColor clearColor]];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(11, 15, 15,15)];
    [label setFont:[COMMON getResizeableFont:FuturaStd_Book(8)]];
    [label setText:[NSString stringWithFormat:@"%ld",(long)quantity]];
    [label setBackgroundColor:[UIColor clearColor]];
    label.textAlignment = NSTextAlignmentCenter;
    [label setTextColor:[UIColor blackColor]];
    
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(newNavBar.frame.size.width-55,(newNavBar.frame.size.height/2) -10 , 40, 40)];
    [backButtonView setBackgroundColor:[UIColor clearColor]];
    [button addSubview:label];
    [backButtonView addSubview:button];
    
    [newNavBar addSubview:backButtonView];
    
    [self.view addSubview:newNavBar];

    
}

#pragma mark- loadNavigation

- (void)loadNavigation
{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController.navigationBar setTranslucent:NO];
    navigation = [[SSCustomNavigation alloc] initWithNibName:@"SSCustomNavigation" bundle:nil];
    navigation.view.tag = 1001;
    [self.navigationController.navigationBar addSubview:navigation.view];
    [navigation setLeftHidden:NO];
    [navigation setTitleHidden:NO];
    [navigation setShopBtnHidden:NO];
    [navigation setShopLabelHidden:NO];
    [navigation setCatergoryShopBtnLabelHidden:YES];
    [navigation.titleLbl setText:NSLocalizedString(@"Mon compte", @"")];
    [navigation.titleLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    
    [self setLabel];
    
}
-(void)setLabel
{
    NSLog(@"cart values %@",[SHOPPINGCART getCartItems]);
    cartItems = [SHOPPINGCART getCartItems];
    
    NSInteger quantity = 0;
    
    
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }
    
    navigation.navigationShopLbl.text = [NSString stringWithFormat:@"%ld",(long)quantity];
    [navigation.navigationShopLbl setBackgroundColor:[UIColor clearColor]];
    navigation.navigationShopLbl.textColor = [UIColor blackColor];
    [navigation.navigationShopLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(10)]];
    [navigation.navigationShopLbl setTextAlignment:NSTextAlignmentCenter];
    
    [navigation.navigationShopBtn addTarget:self action:@selector(commandsCartBtnAction:) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark - Right Menu

-(void)commandsCartBtnAction:(id)sender
{
    SSMonPanierViewController *monpanierViewController;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonpanierViewControlleriPad" bundle:[NSBundle mainBundle]];
        
        
    }
    else {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonPanierViewController" bundle:[NSBundle mainBundle]];
    }
    
    monpanierViewController.isPushFromAccToCart=YES;
    [self.navigationController pushViewController:monpanierViewController animated:YES];
}


#pragma mark-navigation bar creation

- (void)styleNavBar
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    UINavigationBar *newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 64.0)];
    [newNavBar setBackgroundColor:[UIColor grayColor]];
    
    [newNavBar setBarTintColor:[UIColor whiteColor]];
    
    UINavigationItem *newItem = [[UINavigationItem alloc] init];
    newItem.title = @"Mon compte";
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                            NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)]}];
    [newNavBar pushNavigationItem:newItem animated:false];
    
   
   //[self setButton:newNavBar];
    [self.view addSubview:newNavBar];
}

#pragma mark - Right Menu

-(void)cartBtnAction:(id)sender
{
    SSMonPanierViewController *monpanierViewController;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonpanierViewControlleriPad" bundle:[NSBundle mainBundle]];
        
        
    }
    else {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonPanierViewController" bundle:[NSBundle mainBundle]];
    }
    
    [self.navigationController pushViewController:monpanierViewController animated:YES];
}


//#pragma mark - Login View
//
//- (void) LoginView {
//    
//    SSMonPanierLoginViewController *monPanierLoginVC = nil;
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
//    {
//        monPanierLoginVC = [[SSMonPanierLoginViewController alloc]initWithNibName:@"SSMonPanierLoginViewControlleriPad" bundle:[NSBundle mainBundle]];
//        monPanierLoginVC.hidesBottomBarWhenPushed=YES;
//    }
//    else {
//        monPanierLoginVC = [[SSMonPanierLoginViewController alloc]
//                            initWithNibName:@"SSMonPanierLoginViewController"
//                            bundle:nil];
//        
//        monPanierLoginVC.hidesBottomBarWhenPushed=YES;
//    }
//    
//    [self.navigationController pushViewController:monPanierLoginVC animated:YES];
//}

#pragma mark - Button Action

-(void)retourButtonAction:(id)sender
{
    //[appDelegate.tabBarController selectTabAtIndex:0];
}

-(void)logoutBtnAction
{
    [commandsBtn setBackgroundColor:[UIColor clearColor]];
    [addressBtn setBackgroundColor:[UIColor clearColor]];
    [informationsBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setBackgroundColor:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
    
    [COMMON removeUserDetails];
    [SHOPPINGCART clearCart];
    [SHOPPINGCART removeCouponFromCart];
    [SHOPPINGCART resetTotalForCart];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IsStoreUserLoggedIn];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    [appDelegate selectTabAtIndex:0];
    
}

-(void)addressBtnAction
{
    [commandsBtn setBackgroundColor:[UIColor clearColor]];
    [addressBtn setBackgroundColor:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
    [informationsBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];

    
    SSMonPanierAddressViewController *monPanierAddressVC = nil;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierAddressVC = [[SSMonPanierAddressViewController alloc]initWithNibName:@"SSMonPanierAddressViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierAddressVC = [[SSMonPanierAddressViewController alloc]
                              initWithNibName:@"SSMonPanierAddressViewController"
                              bundle:nil];
    }
    monPanierAddressVC.isPushedFromAddressDisplayPage=NO;
    monPanierAddressVC.isPushFromAccountPage =YES;
    monPanierAddressVC.isPushedFromMonAccountPage=YES;
    [self.navigationController pushViewController:monPanierAddressVC animated:YES];
    
    //[self redirectUser];
    
    
}

#pragma mark - User Actions
- (void) redirectUser {
    [webservice getAddressListForUser:[COMMON getCustomerSecureKey]
                              success:^(AFHTTPRequestOperation *operation, id responseObject){
                                  NSLog(@"Response = %@",responseObject);
                                  
                                  if([responseObject objectForKey:@"error"] != NULL){
                                      [self redirectToAddAddressView];
                                      [COMMON removeLoadingIcon];
                                  }
                                  else if ([responseObject objectForKey:@"listaddress"] != NULL) {
                                      [self redirectToAddressViewWithDictionary:[responseObject objectForKey:@"listaddress"]];
                                  }
                              }
                              failure:^(AFHTTPRequestOperation *operation, id error) {
                                  //[SSAppCommon showSimpleAlertWithMessage:@"No Internet Connection available. Kindly check your Internet"];
                                  [COMMON removeLoadingIcon];
                                  
                              }];
}
#pragma mark - AddAdressView
- (void) redirectToAddAddressView {
    
    SSMonPanierAddressModifierViewController *monPanierAddressModifierVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierAddressModifierVC = [[SSMonPanierAddressModifierViewController alloc]initWithNibName:@"SSMonPanierAddressModifierViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierAddressModifierVC = [[SSMonPanierAddressModifierViewController alloc]
                                      initWithNibName:@"SSMonPanierAddressModifierViewController"
                                      bundle:nil];
    }
    
    
    monPanierAddressModifierVC.isPushedFromMonAddresses = NO;
     monPanierAddressModifierVC.isPushedFromRegisterPage=NO;
    
    [self.navigationController pushViewController:monPanierAddressModifierVC animated:YES];
}
- (void) redirectToAddressViewWithDictionary:(NSMutableDictionary *) addressDictionary {
    
    SSMonPanierAddressDisplayViewController *monPanierAddressVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierAddressVC = [[SSMonPanierAddressDisplayViewController alloc]initWithNibName:@"SSMonPanierAddressDisplayViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierAddressVC = [[SSMonPanierAddressDisplayViewController alloc]
                              initWithNibName:@"SSMonPanierAddressDisplayViewController"
                              bundle:nil];
    }
    
    monPanierAddressVC.userAddress = addressDictionary;
    
    [self.navigationController pushViewController:monPanierAddressVC animated:YES];
    
}


-(void)commentBtnAction
{
    [commandsBtn setBackgroundColor:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
    [addressBtn setBackgroundColor:[UIColor clearColor]];
    [informationsBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];

    SSCommandsViewController *commandesVC =  nil;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        commandesVC = [[SSCommandsViewController alloc]initWithNibName:@"SSCommandsViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        commandesVC = [[SSCommandsViewController alloc]initWithNibName:@"SSCommandsViewController" bundle:[NSBundle mainBundle]];
    }
    commandesVC.isPushFromAccountPage =YES;
    [self.navigationController pushViewController:commandesVC animated:YES];
}

-(void)informationBtnAction
{
    [commandsBtn setBackgroundColor:[UIColor clearColor]];
    [addressBtn setBackgroundColor:[UIColor clearColor]];
    [informationsBtn setBackgroundColor:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];
    [self gotoRegisterViewWithEmailAddress:@""];
}
#pragma mark - gotoRegisterView
- (void) gotoRegisterViewWithEmailAddress:(NSString *)strEmailId {
    SSMonPanierRegisterViewController *monPanierRegisterVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierRegisterVC = [[SSMonPanierRegisterViewController alloc]initWithNibName:@"SSMonPanierRegisterViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierRegisterVC = [[SSMonPanierRegisterViewController alloc]
                               initWithNibName:@"SSMonPanierRegisterViewController"
                               bundle:nil];
    }
    monPanierRegisterVC.emailAddressToRegister = strEmailId;
    monPanierRegisterVC.isPushedFromMonInform=YES;
    monPanierRegisterVC.isPushFromAccountPage =YES;
    [self.navigationController pushViewController:monPanierRegisterVC animated:YES];
    [COMMON removeLoadingIcon];
}


#pragma mark - SetUp Initial Btns

- (void)setUpInitialBtns
{
    CGFloat AllBtnXpos;
    CGFloat AllBtnWidth;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        AllBtnXpos = 40;
        AllBtnWidth = screen_width-(AllBtnXpos*2);
    }
    else{
        AllBtnXpos = 20;
        AllBtnWidth = screen_width-(AllBtnXpos*2);
    }

    [commandsBtn removeFromSuperview];
    [addressBtn removeFromSuperview];
    [informationsBtn removeFromSuperview];
    [logoutBtn removeFromSuperview];
    
    commandsBtn = [[UIButton alloc]initWithFrame:CGRectMake(AllBtnXpos, 75, AllBtnWidth, 45)];
    //commandsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //[commandsBtn setTranslatesAutoresizingMaskIntoConstraints:NO];
    [commandsBtn setTitle:NSLocalizedString(@"MES COMMANDES", @"") forState:UIControlStateNormal];
    [commandsBtn.titleLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    [commandsBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    commandsBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [commandsBtn setBackgroundColor:[UIColor clearColor]];
    commandsBtn.layer.borderWidth = 1.0f;
    commandsBtn.layer.borderColor = [UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f blue:182.0f/255.0f alpha:1.0f].CGColor;
    [commandsBtn addTarget:self action:@selector(commentBtnAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    CGFloat addressBtnYpos = commandsBtn.frame.origin.y+commandsBtn.frame.size.height+15;
    
     addressBtn = [[UIButton alloc]initWithFrame:CGRectMake(AllBtnXpos, addressBtnYpos, AllBtnWidth, 45)];
   
    [addressBtn setTitle:NSLocalizedString(@"MES ADRESSES", @"") forState:UIControlStateNormal];
    [addressBtn.titleLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    [addressBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    addressBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [addressBtn setBackgroundColor:[UIColor clearColor]];
    addressBtn.layer.borderWidth = 1.0f;
    addressBtn.layer.borderColor = [UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f blue:182.0f/255.0f alpha:1.0f].CGColor;
    [addressBtn addTarget:self action:@selector(addressBtnAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    CGFloat informationsBtnYpos = addressBtn.frame.origin.y+addressBtn.frame.size.height+15;
    informationsBtn = [[UIButton alloc]initWithFrame:CGRectMake(AllBtnXpos, informationsBtnYpos, AllBtnWidth, 45)];
   
    [informationsBtn setTitle:NSLocalizedString(@"MES INFORMATIONS", @"") forState:UIControlStateNormal];
    [informationsBtn.titleLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    [informationsBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    informationsBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [informationsBtn setBackgroundColor:[UIColor clearColor]];
    informationsBtn.layer.borderWidth = 1.0f;
    [informationsBtn addTarget:self action:@selector(informationBtnAction) forControlEvents:UIControlEventTouchUpInside];
    informationsBtn.layer.borderColor = [UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f blue:182.0f/255.0f alpha:1.0f].CGColor;

    //logoutBtn = [UIButton new];
    
    CGFloat logoutBtnYpos;
    
    if(isStoreApp){
        
        logoutBtnYpos = addressBtn.frame.origin.y+addressBtn.frame.size.height+15;
    }
    else{
        
        logoutBtnYpos = informationsBtn.frame.origin.y+informationsBtn.frame.size.height+15;
    }
    
    logoutBtn = [[UIButton alloc]initWithFrame:CGRectMake(AllBtnXpos, logoutBtnYpos, AllBtnWidth, 45)];
    
    [logoutBtn setTitle:NSLocalizedString(@"SE DÉCONNECTER", @"") forState:UIControlStateNormal];
    [logoutBtn.titleLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    [logoutBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    logoutBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [logoutBtn setBackgroundColor:[UIColor clearColor]];
    logoutBtn.layer.borderWidth = 1.0f;
    logoutBtn.layer.borderColor = [UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f blue:182.0f/255.0f alpha:1.0f].CGColor;
    [logoutBtn addTarget:self action:@selector(logoutBtnAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:commandsBtn];
    [self.view addSubview:addressBtn];
    if(!isStoreApp){
        [self.view addSubview:informationsBtn];
    }
    [self.view addSubview:logoutBtn];

    //[self loadConstraints];
}
-(void)loadConstraints{
    
    NSDictionary *constraintsDictionary = @{@"_commandsBtn":commandsBtn,
                                            @"_addressBtn":addressBtn,
                                            @"_informationsBtn":informationsBtn,
                                            @"_logoutBtn":logoutBtn
                                            };
    
    
    NSDictionary *metricsDict = nil;
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-89-[_commandsBtn(50)]-25-[_addressBtn(_commandsBtn)]-25-[_informationsBtn(_commandsBtn)]-25-[_logoutBtn(_commandsBtn)]"
                                                                      options:0
                                                                      metrics:metricsDict
                                                                        views:constraintsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[_commandsBtn]-10-|"
                                                                      options:0
                                                                      metrics:metricsDict
                                                                        views:constraintsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[_addressBtn]-10-|"
                                                                      options:0
                                                                      metrics:metricsDict
                                                                        views:constraintsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[_informationsBtn]-10-|"
                                                                      options:0
                                                                      metrics:metricsDict
                                                                        views:constraintsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[_logoutBtn]-10-|"
                                                                      options:0
                                                                      metrics:metricsDict
                                                                        views:constraintsDictionary]];
}


@end
