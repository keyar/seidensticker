//
//  SSAccountViewController.h
//  Seidensticker
//
//  Created by Admin on 26/07/16.
//  Copyright © 2016 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSAccountViewController : UIViewController
{
    
}
@property(nonatomic,retain) IBOutlet UIButton *commandsBtn;
@property(nonatomic,retain) IBOutlet UIButton *addressBtn;
@property(nonatomic,retain) IBOutlet UIButton *informationsBtn;
@property(nonatomic,retain) IBOutlet UIButton *logoutBtn;
@property(strong,nonatomic) NSMutableArray *cartItems;

@property (strong, nonatomic) NSMutableDictionary *userAddress;
@property (strong, nonatomic) NSDictionary *addressDictionary;
@property (strong, nonatomic) IBOutlet UITextView *addressTextView;


@property(nonatomic,assign) BOOL isCommanderClicked;

//@property(nonatomic,assign) BOOL isPushFromAccToCart;

@end
