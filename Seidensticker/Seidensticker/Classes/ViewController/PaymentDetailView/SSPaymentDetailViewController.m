//
//  SSPaymentDetailViewController.m
//  Seidensticker
//
//  Created by ocs-mini-7 on 10/9/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSPaymentDetailViewController.h"
#import "SSAppCommon.h"
#import "SSConfig.h"
#import "SSCustomNavigation.h"
#import "SSPaymentDetailCustomViewCell.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#import "SSShoppingCart.h"
#import "AsyncImage.h"
#import "SSAppDelegate.h"
#import "SSTermsAndConditionView.h"
#import "SSPaymentWebViewController.h"
#import "SSShoppingCart.h"
#import "NSString+validations.h"
#import "SSMonPanierAddressViewController.h"
#import "SSMonPanierAddressDisplayViewController.h"
#import "SSMonPanierAddressModifierViewController.h"
#import "UIImageView+AFNetworking.h"


#pragma mark-interfaces

#pragma mark- view states
#pragma mark - Navigation Controller

@interface SSPaymentDetailViewController () <UITextFieldDelegate>{
    
    SSWebService *webservice;
    NSMutableDictionary *paymentImages;
    NSMutableArray *logoArray;
    NSString *logo ;
    NSString *logotitle;
    IBOutlet UINavigationBar  *newNavBar;
    
    UIWindow *windowInfo;
    UITextField *factureTextField;
}
@property(nonatomic,retain) SSTermsAndConditionView *termsConditionInfoView;
@end

#pragma mark-Implementation

@implementation SSPaymentDetailViewController
@synthesize paymentBtn,paymentDetailTableView,paymentTotalPrice,paymentImage,conditionBtn,selectedPayIndex,logoArray;
@synthesize isPushFromLoginPageYES,isPushFromAddressViewPageYES;
@synthesize isPushFromAddressModifierPageYES;
@synthesize currentUserDeliveryId;
#pragma mark - View States
- (void)viewDidLoad {
    [super viewDidLoad];
    
    paymentBtn.hidden = YES;
    
    webservice = [[SSWebService alloc] init];
    paymentImages = [[NSMutableDictionary alloc] init];
    selectedPayIndex = -1;
    SSAppDelegate *appDelegate = (SSAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.paymentDetailViewController=self;
    [paymentDetailTableView setScrollEnabled:NO];
    
    NSString *payment = [SHOPPINGCART getFormattedCartTotalCost];
    NSMutableArray *cartItems = [NSMutableArray new];
    cartItems = [SHOPPINGCART getCartItems];
    NSString *codePromoString = [SHOPPINGCART getCouponCode];
    
    
   
    if(codePromoString!=nil && [payment isEqualToString:@"0,00 €"] && [cartItems count]!=0){
        [paymentDetailTableView setHidden:YES];
        [self loadZeroPaymentView];
        
    }
    
    else{
        [paymentDetailTableView setHidden:NO];
        [paymentDetailTableView setScrollEnabled:YES];
        [self loadPaymentMethods];
    }

    
    
   
    
    [paymentTotalPrice setEnabled:NO];
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:@"Lire nos Conditions Générales des Ventes"];
    [titleString addAttribute:NSUnderlineStyleAttributeName
                        value:[NSNumber numberWithInteger:NSUnderlineStyleSingle]
                        range:NSMakeRange(0, [titleString length])];
    [titleString addAttribute:NSFontAttributeName
                        value:[COMMON getResizeableFont:FuturaStd_Book(12)]
                        range:NSMakeRange(0, [titleString length])];
    [titleString addAttribute:NSForegroundColorAttributeName
                        value:[UIColor blackColor]
                        range:NSMakeRange(0, [titleString length])];
    [conditionBtn setAttributedTitle: titleString forState:UIControlStateNormal];
    
    
}
-(void) loadZeroPaymentView{
    
    CGFloat AllBtnXpos;
    CGFloat AllBtnWidth;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        AllBtnXpos = 50;
        AllBtnWidth = screen_width-(AllBtnXpos*2);
    }
    else{
        AllBtnXpos = 30;
        AllBtnWidth = screen_width-(AllBtnXpos*2);
    }
    
    CGFloat factureBtnYPos = paymentTotalPrice.frame.origin.y+paymentTotalPrice.frame.size.height+15;
    
    factureTextField = [[UITextField alloc]initWithFrame:CGRectMake(AllBtnXpos, factureBtnYPos, AllBtnWidth, 50)];
    [factureTextField setBackgroundColor:[UIColor clearColor]];
    factureTextField.layer.borderWidth = 1.0f;
    factureTextField.layer.borderColor = [UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f blue:182.0f/255.0f alpha:1.0f].CGColor;
    factureTextField.delegate = self;
    [factureTextField setKeyboardType:UIKeyboardTypeDefault];
    factureTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    factureTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    [factureTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    factureTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                            initWithString:@" Numéro de facture magasin..."
                                                            attributes:@{
                                                                         NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                         }];

    
    CGFloat validerBtnYpos = factureTextField.frame.origin.y+factureTextField.frame.size.height+25;
    UIButton *validerBtn = [[UIButton alloc]initWithFrame:CGRectMake(AllBtnXpos, validerBtnYpos, AllBtnWidth, 60)];
    [validerBtn setTitle:NSLocalizedString(@"VALIDER LE PAIEMENT", @"") forState:UIControlStateNormal];
    [validerBtn.titleLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(16)]];
    [validerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    validerBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [validerBtn setBackgroundColor:[UIColor blackColor]];
    [validerBtn addTarget:self action:@selector(ZeroPaymentAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:factureTextField];
    [self.view addSubview:validerBtn];
    
}
-(void)ZeroPaymentAction:(id)sender{
    
    NSString *factureTextStr = factureTextField.text;
    
    if(factureTextStr!=nil){
        [self validation:factureTextStr];
    }
    else{
        [SSAppCommon showSimpleAlertWithMessage:INVOICE_REQUIRED];
    }

}

#define MAX_LENGTH 9
//
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    if (textField.text.length >= MAX_LENGTH && range.length == 0)
//    {
//        NSLog(@"NO");
//    
//        return NO; // return NO to not change text
//    }
//    else
//    {
//         NSLog(@"YES");
//        return YES;
//    }
//}

-(void)validation:(NSString *)invoiceStr{
    
    NSString *trimmedString = [invoiceStr stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if(![NSString isEmpty:trimmedString]){
        
        if(trimmedString.length<=MAX_LENGTH){
            [self gotoPaymentPageForStorePurchase:invoiceStr];
        }
        else{
            [SSAppCommon showSimpleAlertWithMessage:INVOICE_CHARACTERS_CHECK];
            return;
        }
//        if([NSString validateInvoiceNumber:invoiceStr]){
//            NSLog(@"YES");
//            [SSAppCommon showSimpleAlertWithMessage:@"Proceed"];
//            return;
//           //[self gotoPaymentPageForStorePurchase:invoiceStr];
//        }
//        else{
//            [SSAppCommon showSimpleAlertWithMessage:INVOICE_CHARACTERS_CHECK];
//            return;
//        }
    }
    else{
        [SSAppCommon showSimpleAlertWithMessage:INVOICE_REQUIRED];
        return;
    }
    
    

}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [COMMON setTabBarSelection:YES];
    [COMMON TrackerWithName:@"Payment View"];
    [paymentDetailTableView setScrollEnabled:YES];
    [self.navigationItem setHidesBackButton:YES animated:NO];
    [self loadNavigation];
    [self paymentPrice];
    //if(IS_IPHONE6_Plus){
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    [self.navigationController.navigationBar addSubview:navBorder];
    //}

   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Load Payment Methods
- (void) loadPaymentMethods {
    [COMMON showLoadingIcon:self];
    [webservice getPaymentMethodsForUser:[COMMON getCustomerSecureKey]
                                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                     paymentImages = responseObject;
                                     [self paymentImageDetail];
                                     //[paymentDetailTableView reloadData];
                                 } failure:^(AFHTTPRequestOperation *operation, id error) {
                                     
                                 }];
}
#pragma mark - Navigation Controller
- (void)loadNavigation
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
    SSCustomNavigation *navigation = [[SSCustomNavigation alloc] initWithNibName:@"SSCustomNavigation" bundle:nil];
    navigation.view.tag = 1001;
    [self.navigationController.navigationBar addSubview:navigation.view];
    [navigation setLeftHidden:NO];
    [navigation setTitleHidden:NO];
    [navigation setRightHidden:YES];
    
    [navigation setLeftLabelHidden:NO];
    
    [navigation setShopBtnHidden:YES];
    [navigation setShopLabelHidden:YES];
    [navigation setCatergoryShopBtnLabelHidden:YES];

    
    [navigation.titleLbl setText:NSLocalizedString(@"Paiement", @"")];
    [navigation.titleLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(15)]];
    
    [navigation.navigationLeftLbl setText:NSLocalizedString(@"Retour", @"")];
    [navigation.navigationLeftLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [navigation.navigationLeftBtn addTarget:self action:@selector(retourDetailBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //[self setLabel:navigation];
    
}
-(void)setLabel:(SSCustomNavigation *)navigation
{
    NSLog(@"cart values %@",[SHOPPINGCART getCartItems]);
    NSMutableArray *cartItems = [NSMutableArray new];
    cartItems = [SHOPPINGCART getCartItems];
    
    NSInteger quantity = 0;
    
    
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }
    
    navigation.navigationShopLbl.text = [NSString stringWithFormat:@"%ld",(long)quantity];
    [navigation.navigationShopLbl setBackgroundColor:[UIColor clearColor]];
    navigation.navigationShopLbl.textColor = [UIColor blackColor];
    [navigation.navigationShopLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(8)]];
    [navigation.navigationShopLbl setTextAlignment:NSTextAlignmentCenter];
    [navigation.navigationShopBtn addTarget:self action:@selector(cartPaimentBtnAction:) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark - Right Menu

-(void)cartPaimentBtnAction:(id)sender
{
    SSMonPanierViewController *monpanierViewController;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonpanierViewControlleriPad" bundle:[NSBundle mainBundle]];
        
        
    }
    else {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonPanierViewController" bundle:[NSBundle mainBundle]];
    }
    
    [self.navigationController pushViewController:monpanierViewController animated:YES];
}

#pragma mark- paymentPrice
-(void)paymentPrice{
    paymentTotalPrice.text = [COMMON getDecodedString:[NSString stringWithFormat:@"%@",[SHOPPINGCART getFormattedCartTotalCost]]];
    [paymentTotalPrice setFont:[COMMON getResizeableFont:FuturaStd_Book(15)]];
}
#pragma mark- paymentImageDetail
-(void) paymentImageDetail
{
    logoArray =[paymentImages objectForKey:@"paymentmethod"];
    
    //NSLog(@"logoImage==%@",logoArray);
    [paymentDetailTableView reloadData];
    [COMMON removeLoadingIcon];
}
#pragma mark- UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(IS_IPHONE4)
    {
        return 65;
    }
    else if(IS_IPHONE6_Plus||IS_IPHONE6 ||IS_IPHONE5){
        return 90;
    }
    else
    {
        return 170;
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger nCount= 0;
    nCount = logoArray.count;
    if(nCount>=3){
      return 3;
    }
    else{
       return logoArray.count;
    }
   // return logoArray.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier=@"Cell";
    SSPaymentDetailCustomViewCell *cell  = (SSPaymentDetailCustomViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        //old
        /* NSArray *nib;
         nib = [[NSBundle mainBundle] loadNibNamed:@"SSPaymentDetailCustomViewCell" owner:self options:nil];
         cell = paymentImageCell;
         */
        
        //new change
        NSArray *nib;
        nib = [[NSBundle mainBundle] loadNibNamed:@"SSPaymentDetailCustomViewCell_Ipad" owner:self options:nil];
        cell = paymentImageCell;
        
    }
    NSString *currentDevice=@"logo";
    if([self isDeviceIpad]==YES){
        currentDevice=@"ipadlogo";
    }
    else{
         currentDevice=@"logo";
        
        cell.paymentLabelWidthConstraint.constant = 150;
        
        if(IS_IPHONE4||IS_IPHONE5)
        {
             cell.paymentLabelWidthConstraint.constant = 120;
        }
        else if(IS_IPHONE6_Plus||IS_IPHONE6){
             cell.paymentLabelWidthConstraint.constant = 150;
        }       
    }

    NSString * logoImage = [[logoArray objectAtIndex:indexPath.row]objectForKey:currentDevice];

    NSString * logoTitle = [[logoArray objectAtIndex:indexPath.row]objectForKey:@"title"];
    
    if ((NSString *)[NSNull null] == logoImage||logoImage==nil) {
        logoImage=@"";
    }
    if ((NSString *)[NSNull null] == logoTitle||logoTitle==nil) {
        logoTitle=@"";
    }
    
    /*
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:logoImage]];
    cell.paymentCardImageView.image = [UIImage imageWithData:imageData];
    //cell.paymentCardImageView.contentMode = UIViewContentModeCenter;
     */
    //new change
    [cell.paymentCardImageView setImageWithURL:[NSURL URLWithString:logoImage] placeholderImage:nil];

    cell.paymentCardLabel.text = logoTitle;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.layer setBorderColor: [[UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f] CGColor]];
    [cell.layer setBorderWidth: 1.5];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self gotoPayment:indexPath.row];
    /*
    if (selectedPayIndex != -1) {
        NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:selectedPayIndex inSection:0];
        NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
  
        SSPaymentDetailCustomViewCell *cell = [tableView cellForRowAtIndexPath:indexPath1];
        
        [cell.selectionView.layer setBorderColor:[UIColor clearColor].CGColor];
        [cell.selectionView.layer setBorderWidth:0.0f];
        
        [cell.layer setBorderColor: [[UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f] CGColor]];
        [cell.layer setBorderWidth: 1.5];
        
        cell = [tableView cellForRowAtIndexPath:indexPath2];
        
        [cell.selectionView.layer setBorderColor:[UIColor colorWithRed:255.0f/255.0f green:128.0f/255.0f blue:0.0f/255.0f alpha:1.0f].CGColor];
        [cell.selectionView.layer setBorderWidth:2.0f];
        
        [cell.layer setBorderColor: [[UIColor clearColor] CGColor]];
        [cell.layer setBorderWidth: 0.0];
    }
    else{
        SSPaymentDetailCustomViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        [cell.selectionView.layer setBorderColor:[UIColor colorWithRed:255.0f/255.0f green:128.0f/255.0f blue:0.0f/255.0f alpha:1.0f].CGColor];
        [cell.selectionView.layer setBorderWidth:2.0f];
        
        [cell.layer setBorderColor: [[UIColor clearColor] CGColor]];
        [cell.layer setBorderWidth: 0.0];
        
    }
    
    selectedPayIndex = indexPath.row;
    
    NSLog(@"selected %ld row", (long)indexPath.row);
     */
}

#pragma mark - Go To Payment View

-(void)gotoPayment: (NSInteger)selectIndex
{
    
    if(currentUserDeliveryId==nil){
        currentUserDeliveryId=@"";
    }
    SSPaymentWebViewController *paymentWebViewController=nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        paymentWebViewController = [[SSPaymentWebViewController alloc]initWithNibName:@"SSPaymentWebViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else
    {
        paymentWebViewController = [[SSPaymentWebViewController alloc]
                                    initWithNibName:@"SSPaymentWebViewController"
                                    bundle:nil];
        
    }
    paymentWebViewController.isZeroPaymentProcess=NO;
    paymentWebViewController.selectedIndex = selectIndex;
    paymentWebViewController.currentUserDeliveryId = currentUserDeliveryId;
    [self.navigationController pushViewController:paymentWebViewController animated:YES];
}

#pragma mark - Go To Payment Zero Payment Process

-(void)gotoPaymentPageForStorePurchase :(NSString *)invoiceStr
{
    if(currentUserDeliveryId==nil){
        currentUserDeliveryId=@"";
    }
    SSPaymentWebViewController *paymentWebViewController=nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        paymentWebViewController = [[SSPaymentWebViewController alloc]initWithNibName:@"SSPaymentWebViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else
    {
        paymentWebViewController = [[SSPaymentWebViewController alloc]
                                    initWithNibName:@"SSPaymentWebViewController"
                                    bundle:nil];
        
    }
    paymentWebViewController.invoiceNumStr = invoiceStr;
    paymentWebViewController.isZeroPaymentProcess=YES;
    paymentWebViewController.currentUserDeliveryId = currentUserDeliveryId;
    [self.navigationController pushViewController:paymentWebViewController animated:YES];
}


 -(IBAction)gotoPaymentButtonAction:(id)sender{
    if (selectedPayIndex != -1)
    {
        
    SSPaymentWebViewController *paymentWebViewController=nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
        paymentWebViewController = [[SSPaymentWebViewController alloc]initWithNibName:@"SSPaymentWebViewControlleriPad" bundle:[NSBundle mainBundle]];
        }
        else {
            paymentWebViewController = [[SSPaymentWebViewController alloc]
                                  initWithNibName:@"SSPaymentWebViewController"
                                  bundle:nil];
            
        }

    [self.navigationController pushViewController:paymentWebViewController animated:YES];

    }

    else
        [COMMON showAlert:NSLocalizedString(@"Please select any one pay mode", @"")];
}
 

#pragma mark Terms and Condition View creation

-(void)showTermsConditionInfoView
{
    windowInfo = [[[UIApplication sharedApplication] delegate] window];
    
    SSTermsAndConditionView *termsConditionInfoView = [[SSTermsAndConditionView alloc] init];
    
    [windowInfo addSubview:termsConditionInfoView];
    
    [termsConditionInfoView.closeButton addTarget:self action:@selector(closeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self  setTermsConditionInfoView:termsConditionInfoView];
    
    NSDictionary *dictView = @{@"_viewCodePromoInfo":termsConditionInfoView};
    
    [windowInfo addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_viewCodePromoInfo]|"
                                
                                                                       options:0
                                
                                                                       metrics:nil views:dictView]];
    
    [windowInfo addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_viewCodePromoInfo]|"
                                
                                                                       options:0
                                
                                                                       metrics:nil views:dictView]];
}

#pragma mark-Button Action
-(void)retourDetailBtnAction:(id)sender
{
    NSArray *array = [self.navigationController viewControllers];
    
    if(isPushFromLoginPageYES == YES){
        [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];
    }
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        if(isPushFromAddressViewPageYES==YES){
            //Do not forget to import AnOldViewController.h
            if ([controller isKindOfClass:[SSMonPanierAddressViewController class]]) {
                
                [self.navigationController popToViewController:controller
                                                      animated:YES];
                break;
            }
        }
        else if(isPushFromAddressModifierPageYES==YES){
            //Do not forget to import AnOldViewController.h
            if ([controller isKindOfClass:[SSMonPanierAddressModifierViewController class]]) {
                
                [self.navigationController popToViewController:controller
                                                      animated:YES];
                break;
            }
        }
        else{
            if ([controller isKindOfClass:[SSMonPanierAddressDisplayViewController class]]) {
                
                [self.navigationController popToViewController:controller
                                                      animated:YES];
                break;
            }
        }
        
        

    }

//    else{
//        [self.navigationController popToViewController:[array objectAtIndex:0] animated:YES];
//    }
    
    
    [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
    [self.navigationController setNavigationBarHidden:YES];
 
   // [self.navigationController popViewControllerAnimated:YES];
   // [self.navigationController setNavigationBarHidden:YES];
   // [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
    
}

-(IBAction)lirenosconditions:(id)sender
{
    
    [self showTermsConditionInfoView];
    
}

-(IBAction)closeButtonAction:(id)sender
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.termsConditionInfoView removeFromSuperview];
        
    });
}
-(BOOL)isDeviceIpad
{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        
        return YES;
    } else {
        return NO;
    }
}

@end
