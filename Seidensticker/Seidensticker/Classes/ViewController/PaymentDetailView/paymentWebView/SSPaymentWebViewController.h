//
//  SSPaymentWebViewController.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 11/5/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SSPaymentWebViewController : UIViewController
@property(nonatomic,retain)IBOutlet UIWebView *paymentWebView;

@property(nonatomic,retain) NSString *orderNumber;

@property(nonatomic,retain) NSString *currentUserDeliveryId;

@property (nonatomic, assign) NSInteger selectedIndex;

@property(nonatomic,assign) BOOL isZeroPaymentProcess;

@property(nonatomic,retain) NSString *invoiceNumStr;

@end
