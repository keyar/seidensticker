//
//  SSPaymentWebViewController.m
//  Seidensticker
//
//  Created by ocs-mini-7 on 11/5/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSPaymentWebViewController.h"
#import "SSAppCommon.h"
#import "SSConfig.h"
#import "SSCustomNavigation.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#import "SSShoppingCart.h"
#import "SSAppDelegate.h"
#import "SSOrderConfirmationViewController.h"

#pragma mark-interfaces

@interface SSPaymentWebViewController ()
{
    SSWebService *webservice;
    int sendCount;
    
    
}
@end

#pragma mark-Implementation

@implementation SSPaymentWebViewController
@synthesize paymentWebView,orderNumber, selectedIndex;
@synthesize isZeroPaymentProcess,invoiceNumStr,currentUserDeliveryId;


#pragma mark- view states
- (void)viewDidLoad {
    [super viewDidLoad];
    [COMMON setTabBarSelection:NO];
    webservice = [[SSWebService alloc] init];
    SSAppDelegate *appDelegate = (SSAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.paymentWebViewController=self;
    [self setUpConstraints];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationItem setHidesBackButton:YES animated:NO];
    [self loadNavigation];
    
    sendCount = 0;
    SSAppDelegate *appDelegate = (SSAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(isStoreApp){
        [COMMON setTabBarSelection:NO];
        [self gotoPaymentWithMethod:STORE_APP_PAYMENTNAME];
    }
    else{
        if(isZeroPaymentProcess==YES){
            [COMMON setTabBarSelection:NO];
            NSString *codePromoString = [SHOPPINGCART getCouponCode];
            [self storePurchaseAction:codePromoString factureText:invoiceNumStr];
        }
        else{
            [COMMON setTabBarSelection:NO];
            [self gotoPaymentWithMethod:[[[appDelegate paymentDetailViewController].logoArray objectAtIndex:selectedIndex] objectForKey:@"name"]];
        }
  
    }
    
}

#pragma mark - Navigation Controller
- (void)loadNavigation
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
    SSCustomNavigation *navigation = [[SSCustomNavigation alloc] initWithNibName:@"SSCustomNavigation" bundle:nil];
    navigation.view.tag = 1001;
    [self.navigationController.navigationBar addSubview:navigation.view];
    [navigation setLeftHidden:NO];
    [navigation setTitleHidden:NO];
    [navigation setRightHidden:YES];
    [navigation setLeftLabelHidden:NO];
    [navigation setShopBtnHidden:YES];
    [navigation setShopLabelHidden:YES];
    [navigation setCatergoryShopBtnLabelHidden:YES];

    [navigation.titleLbl setText:NSLocalizedString(@"Paiement", @"")];
    [navigation.titleLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(15)]];
    
    [navigation.navigationLeftLbl setText:NSLocalizedString(@"Retour", @"")];
    [navigation.navigationLeftLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [navigation.navigationLeftBtn addTarget:self action:@selector(retourBtnAction:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark- web service function

- (void) gotoPaymentWithMethod:(NSString *) selectedPaymentMethod {
    [COMMON showLoadingIcon:self];
    sendCount++;
    NSString * deliveryAddressID = currentUserDeliveryId;
    if(deliveryAddressID==nil){
        deliveryAddressID=@"";
    }
    
    [webservice gotoPaymentWithProducts:[SHOPPINGCART getCartProductsInJsonFormat]
                      withPaymentMethod:selectedPaymentMethod
                       forUserSecureKey:[COMMON getCustomerSecureKey]
                  userDeliveryAddressID: deliveryAddressID
                                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                    NSLog(@"Payment Response = %@",responseObject);
                                    if ([responseObject isKindOfClass:[NSDictionary class]] && [responseObject objectForKey:@"paymentintermediate"]!= NULL
                                        ) {

                                        NSString *htmlString = [responseObject objectForKey:@"paymentintermediate"];
                                        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\\t" withString:@""];
                                        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                                        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                                        [paymentWebView loadHTMLString:htmlString baseURL:webservice.baseURL];
                                        //[self performSelector:@selector(removeLoaderAfterTwoSec) withObject:self afterDelay:2];
                                    } else {
                                        if ([responseObject isKindOfClass:[NSDictionary class]] && [responseObject objectForKey:@"error"] != NULL) {
                                            [SSAppCommon showSimpleAlertWithMessage:[responseObject objectForKey:@"error"]];
                                            [COMMON removeLoadingIcon];
                                            [COMMON setTabBarSelection:YES];
                                        }
                                    }
                                } failure:^(AFHTTPRequestOperation *operation, id error) {
                                    
                                    [COMMON removeLoadingIcon];
                                    [COMMON setTabBarSelection:YES];
                                }];
}
#pragma mark- web service function For STORE PURCHASE USED TWO API TO AVOID CONFUSION (ZERO PAYMENT PROCESS)

-(void)storePurchaseAction:(NSString *)codePromoString factureText:(NSString *)factureTextStr{
    [COMMON showLoadingIcon:self];
    sendCount++;
    NSString * deliveryAddressID = currentUserDeliveryId;
    if(deliveryAddressID==nil){
        deliveryAddressID=@"";
    }

    
    [webservice gotoPaymentWithInvoiceProducts:[SHOPPINGCART getCartProductsInJsonFormat]
                             withPaymentMethod:ZERO_PAY_STORE_PURCHASE
                              forUserSecureKey:[COMMON getCustomerSecureKey]
                               userInvoiceCode:factureTextStr
                                withCouponCode:codePromoString
                         userDeliveryAddressID:deliveryAddressID
                                       success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                           NSLog(@"Payment Response = %@",responseObject);
                                           if ([responseObject isKindOfClass:[NSDictionary class]] && [responseObject objectForKey:@"paymentintermediate"]!= NULL
                                               ) {
                                               [self oldCode:responseObject];
                                               
                                               //[self newCode:responseObject]; //tested following old code
                                           }
                                           else {
                                               if ([responseObject isKindOfClass:[NSDictionary class]] && [responseObject objectForKey:@"error"] != NULL) {
                                                   [SSAppCommon showSimpleAlertWithMessage:[responseObject objectForKey:@"error"]];
                                                   [COMMON removeLoadingIcon];
                                                   [COMMON setTabBarSelection:YES];
                                               }
                                               else{
                                                   [COMMON removeLoadingIcon];
                                                   [COMMON setTabBarSelection:YES];
                                               }
                                           }
                                           
                                       } failure:^(AFHTTPRequestOperation *operation, id error) {
                                           [COMMON removeLoadingIcon];
                                           [COMMON setTabBarSelection:YES];
                                           
                                       }];
}
#pragma mark - oldCode
-(void)oldCode:(id)responseObject{
       NSString *htmlString = [responseObject objectForKey:@"paymentintermediate"];
       htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\\t" withString:@""];
       htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
       htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
       [paymentWebView loadHTMLString:htmlString baseURL:webservice.baseURL];
       // [self performSelector:@selector(removeLoaderAfterTwoSec) withObject:self afterDelay:3];
}

#pragma mark - removeLoaderAfterTwoSec
-(void)removeLoaderAfterTwoSec{
     [COMMON removeLoadingIcon];
}

#pragma mark - newCode
-(void)newCode:(id)responseObject{
    NSString *mode = [[responseObject objectForKey:@"paymentintermediate"] valueForKey:@"mode"];
    if([mode isEqualToString:@"success"]){
        orderNumber=[NSString stringWithFormat:@"%@",[[responseObject objectForKey:@"paymentintermediate"] valueForKey:@"id_order"]];
        NSLog(@"Order Number==%@",orderNumber);
        [self gotoOrderView];
        [COMMON removeLoadingIcon];
    }
    else{
        [COMMON removeLoadingIcon];
    }
}

#pragma mark - Webview Delegates

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"ERROR");
     [COMMON setTabBarSelection:YES];
    /*if (sendCount < 3) {
        SSAppDelegate *appDelegate = (SSAppDelegate *)[[UIApplication sharedApplication] delegate];
        [self gotoPaymentWithMethod:[[[appDelegate paymentDetailViewController].logoArray objectAtIndex:selectedIndex] objectForKey:@"name"]];
    } else {
        [COMMON showAlert:@"Error in Payment!"];
    }*/
}

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest: (NSURLRequest *) request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSURL *myRequestedUrl= [request URL];
    NSString *urlString = myRequestedUrl.absoluteString;
    
    if (urlString != NULL && [urlString rangeOfString:@"mode=success"].location != NSNotFound) {
        NSLog(@"Started loading");
        NSLog(@"URL String in Start Load = %@",urlString);
        NSLog(@"URL String in Finished Load = %@",urlString);
        
        NSArray *id_order = [urlString componentsSeparatedByString:@"id_order="];
        NSArray *id_cart  = [urlString componentsSeparatedByString:@"id_cart="];
        
        NSString *referenceNumber;
        
        if (id_order!=NULL && [id_order count]>1) {
            referenceNumber = [id_order objectAtIndex:1];
        } else if (id_cart!=NULL && [id_cart count]>1) {
            referenceNumber = [id_cart objectAtIndex:1];
        }
        orderNumber=[[referenceNumber componentsSeparatedByString:@"&"] objectAtIndex:0];
        
        NSLog(@"Order Number==%@",orderNumber);
        
        [self gotoOrderView];
        [webView stopLoading];
        return NO;
    }
    
    if (urlString != NULL && [urlString rangeOfString:@"mode=failure"].location != NSNotFound) {
        [self.navigationController popViewControllerAnimated:YES];
        [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];

        [self.navigationController setNavigationBarHidden:YES];
        [webView stopLoading];
        [COMMON setTabBarSelection:YES];
        return NO;
    }
    
    if (urlString != NULL && ([urlString rangeOfString:BASE_URL].location != NSNotFound || [urlString rangeOfString:@"about:blank"].location != NSNotFound)) {
        [COMMON showLoadingIcon:self];
        [COMMON setTabBarSelection:YES];
    }

    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    /*if (webView.isLoading)
        return;*/
    
}

- (void) gotoOrderView {
    SSOrderConfirmationViewController *orderViewController = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        orderViewController = [[SSOrderConfirmationViewController alloc]
                               initWithNibName:@"SSOrderConfirmationViewControlleriPad"
                               bundle:[NSBundle mainBundle]];
    }
    else {
        orderViewController = [[SSOrderConfirmationViewController alloc]
                               initWithNibName:@"SSOrderConfirmationViewController"
                               bundle:nil];
    }

    [self.navigationController pushViewController:orderViewController animated:YES];
    [COMMON removeLoadingIcon];
    [COMMON setTabBarSelection:YES];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"Finished loading");
    [self performSelector:@selector(removeLoaderAfterTwoSec) withObject:self afterDelay:5];
    //[COMMON removeLoadingIcon];
}

#pragma mark-Button Action
-(void)retourBtnAction:(id)sender
{
//    [self gotoOrderView];
    
    [self.navigationController popViewControllerAnimated:YES];
    [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];

    [self.navigationController setNavigationBarHidden:YES];
    [COMMON setTabBarSelection:YES];
}

#pragma mark-set up Constraints
-(void)setUpConstraints
{
    paymentWebView.translatesAutoresizingMaskIntoConstraints=YES;
    int xPosition = 0,widthPosition,heightPosition;
    if(IS_IPHONE4 || IS_IPHONE5)
    {
        xPosition=27;
        widthPosition=0;
        heightPosition=25;
    }
    else if(IS_IPHONE6){
        xPosition=20;
        widthPosition=40;
        heightPosition=130;
    }
    else if(IS_IPHONE6_Plus){
        xPosition=20;
        widthPosition=79;
        heightPosition=200;
    }
    else
    {
        xPosition=18;
        widthPosition=25;
        heightPosition=700;
    }
    SSAppDelegate *appDelegate = (SSAppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate paymentDetailViewController].selectedPayIndex==1)
    {
        [paymentWebView setFrame:CGRectMake(paymentWebView.frame.origin.x-xPosition, paymentWebView.frame.origin.y, paymentWebView.frame.size.width+widthPosition, paymentWebView.frame.size.height+heightPosition)];
    }
    else{
         [paymentWebView setFrame:CGRectMake(paymentWebView.frame.origin.x-xPosition, paymentWebView.frame.origin.y-26, paymentWebView.frame.size.width+widthPosition, paymentWebView.frame.size.height+heightPosition)];
    }
}

@end
