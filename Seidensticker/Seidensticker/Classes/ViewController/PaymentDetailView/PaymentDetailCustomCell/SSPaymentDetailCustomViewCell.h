//
//  SSPaymentDetailCustomViewCell.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 10/9/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSPaymentDetailCustomViewCell : UITableViewCell

@property(nonatomic,strong) IBOutlet UIImageView *paymentCardImageView;
@property (strong, nonatomic) IBOutlet UILabel *paymentCardLabel;
@property (strong, nonatomic) IBOutlet UIView *selectionView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *paymentLabelWidthConstraint;

@end
