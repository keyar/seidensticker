//
//  SSPaymentDetailCustomViewCell.m
//  Seidensticker
//
//  Created by ocs-mini-7 on 10/9/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSPaymentDetailCustomViewCell.h"
#import "SSConfig.h"
#import "SSAppCommon.h"

@implementation SSPaymentDetailCustomViewCell
@synthesize paymentCardImageView,paymentCardLabel;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.paymentCardLabel.numberOfLines = 3;
    [self.paymentCardLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
}

- (void)setFrame:(CGRect)frame {
    
    frame.origin.y += 5;
    frame.size.height -=6 *3;
    [super setFrame:frame];
}

@end
