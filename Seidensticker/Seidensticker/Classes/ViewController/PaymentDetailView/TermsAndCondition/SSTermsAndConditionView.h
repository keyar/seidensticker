//
//  SSTermsAndConditionView.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 11/3/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSTermsAndConditionView : UIView

@property(nonatomic,weak)  UIImageView *imageviewBackground;
@property(nonatomic,weak)  UIView *viewContainer;
@property(nonatomic,weak)  UIWebView *termsConditionWebView;
@property(nonatomic,weak)  UIButton *closeButton;



@end
