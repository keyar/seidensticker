//
//  SSPaymentDetailViewController.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 10/9/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSPaymentDetailCustomViewCell.h"
@interface SSPaymentDetailViewController : UIViewController<UIWebViewDelegate>
{
    IBOutlet SSPaymentDetailCustomViewCell *paymentImageCell;
    IBOutlet UINavigationBar *navigationBarTitle;
}

@property(nonatomic,retain)IBOutlet UITableView *paymentDetailTableView;

@property(nonatomic,retain)IBOutlet UIButton *paymentBtn;
@property (strong, nonatomic) IBOutlet UITextField *paymentTotalPrice;
@property (strong, nonatomic) IBOutlet UIImageView *paymentImage;
@property (strong, nonatomic) IBOutlet UIButton *conditionBtn;
@property (nonatomic) NSInteger selectedPayIndex;
@property (strong, nonatomic)NSMutableArray *logoArray;

@property (strong, nonatomic)IBOutlet UIView *divideview;

@property(nonatomic,assign) BOOL isPushFromLoginPageYES;

@property(nonatomic,assign) BOOL isPushFromAddressViewPageYES;

@property(nonatomic,assign) BOOL isPushFromAddressModifierPageYES;

@property(nonatomic,retain) NSString *currentUserDeliveryId;

-(void)paymentPrice;
-(IBAction)lirenosconditions:(id)sender;
@end
