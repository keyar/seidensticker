//
//  SSCategoryViewController.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/18/15.
//  Copyright (c) 2015 Oclocksoftware. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SSCategoryViewController : UIViewController
{
    IBOutlet UINavigationBar *navigationBarTitle;
    IBOutlet UIBarButtonItem *barButtonRetourTitle;
    IBOutlet UIBarButtonItem *barButtonFilterTitle;
    NSString *categoryURL;
    BOOL isfilter;
    BOOL isFilterValiderClicked;
    NSString * currentFilterCategoryUrl;
}

@property(nonatomic,retain) id controller;
@property(nonatomic,retain) NSMutableDictionary *currentCategoryItem;
@property(nonatomic,retain) NSString *currentCategoryTitle;
@property(nonatomic,retain) NSString *currentCategoryURL;
@property (weak, nonatomic) IBOutlet UITableView *productsTableView;

- (void) loadTableWithProducts:(NSMutableDictionary *)dataArray;
- (void) loadProductsForFilterQuery:(NSString *) filterQuery;

@property (nonatomic, strong) void (^onCompletion)(id result);

- (void) loadProducts;

@end
