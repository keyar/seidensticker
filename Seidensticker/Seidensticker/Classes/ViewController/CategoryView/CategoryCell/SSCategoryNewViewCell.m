//
//  TableViewCell.m
//  Form
//
//  Created by OCSDEV2 on 23/09/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "SSCategoryNewViewCell.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSCustomNavigation.h"

@implementation SSCategoryNewViewCell
@synthesize leftImageView,rightImageView,leftNameLabel,rightNameLabel,leftPriceLabel,rightPriceLabel,middleButton,middleImageView, middleNameLabel,middlePriceLabel,discountMiddlePriceLabel;

- (void)awakeFromNib {
    // Initialization code

    self.discountRightPercentageLabel.layer.masksToBounds = YES;
    self.discountRightPercentageLabel.layer.cornerRadius = 8.0;
    
    
    self.discountLeftPercentageLabel.layer.masksToBounds = YES;
    self.discountLeftPercentageLabel.layer.cornerRadius = 8.0;
    
    self.discountMiddlePercentageLabel.layer.masksToBounds = YES;
    self.discountMiddlePercentageLabel.layer.cornerRadius = 8.0;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadLeftProductDetails:(NSDictionary *)details {
    
    NSString * imageURL = [NSString stringWithFormat:@"%@",[details valueForKey:@"prod_image"]];
    
    if ([imageURL hasPrefix:@"http://"] || [imageURL hasPrefix:@"https://"]) {
        
        NSMutableURLRequest *userRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageURL]];
        
        [userRequest addValue:@"image/*" forHTTPHeaderField:@"Accept"];
        
        UIActivityIndicatorView *activityIndicator = [self setLoadingImage:leftImageView];
        
        AFImageResponseSerializer *serializer = [AFImageResponseSerializer serializer];
        
        serializer.imageScale = 1.0;
        
        [leftImageView setImageResponseSerializer:serializer];
        
        
        [self.leftImageView setImageWithURLRequest:userRequest placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            
            UIImage     * resizedImage;
            
            resizedImage     = [self resizeImage:image targetSize:leftImageView.frame.size];
                        
            [leftImageView setImage:resizedImage];
                        
            [activityIndicator stopAnimating];
            
            [activityIndicator removeFromSuperview];
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            
            [activityIndicator stopAnimating];
            
            [activityIndicator removeFromSuperview];
        }];
    }
    else {

    }
    
}

- (void)loadLeftRighttDetails:(NSDictionary *)details {
    
    NSString * imageURL = [NSString stringWithFormat:@"%@",[details valueForKey:@"prod_image"]];
    
    if ([imageURL hasPrefix:@"http://"] || [imageURL hasPrefix:@"https://"]) {
        
        NSMutableURLRequest *userRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageURL]];
        
        [userRequest addValue:@"image/*" forHTTPHeaderField:@"Accept"];
        
        UIActivityIndicatorView *activityIndicator = [self setLoadingImage:rightImageView];
        
        AFImageResponseSerializer *serializer = [AFImageResponseSerializer serializer];
        
        serializer.imageScale = 1.0;
        
        [rightImageView setImageResponseSerializer:serializer];
        
        
        [self.rightImageView setImageWithURLRequest:userRequest placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            
            UIImage     * resizedImage;
            
            resizedImage     = [self resizeImage:image targetSize:rightImageView.frame.size];
            
            [rightImageView setImage:resizedImage];
            
            [activityIndicator stopAnimating];
            
            [activityIndicator removeFromSuperview];
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            
            [activityIndicator stopAnimating];
            
            [activityIndicator removeFromSuperview];
        }];
    }
    else {
        
    }
}


- (void)loadLeftMiddleDetails:(NSDictionary *)details {
    
    NSString * imageURL = [NSString stringWithFormat:@"%@",[details valueForKey:@"prod_image"]];
    
    if ([imageURL hasPrefix:@"http://"] || [imageURL hasPrefix:@"https://"]) {
        
        NSMutableURLRequest *userRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageURL]];
        
        [userRequest addValue:@"image/*" forHTTPHeaderField:@"Accept"];
        
        UIActivityIndicatorView *activityIndicator = [self setLoadingImage:middleImageView];
        
        AFImageResponseSerializer *serializer = [AFImageResponseSerializer serializer];
        
        serializer.imageScale = 1.0;
        
        [middleImageView setImageResponseSerializer:serializer];
        
        
        [self.middleImageView setImageWithURLRequest:userRequest placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            
            UIImage     * resizedImage;
            
            resizedImage     = [self resizeImage:image targetSize:middleImageView.frame.size];
            
            [middleImageView setImage:resizedImage];

            [activityIndicator stopAnimating];
            
            [activityIndicator removeFromSuperview];
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            
            [activityIndicator stopAnimating];
            
            [activityIndicator removeFromSuperview];
        }];
    }
    else {
        
    }
}




- (UIImage *)resizeImage:(UIImage *)imageToCrop  targetSize:(CGSize)_targetSize{
    return [imageToCrop imageCroppedToFitSize:_targetSize];
}

- (UIImage *)resizeimageScaled:(UIImage *)imageToCrop  targetSize:(CGSize)_targetSize{
    return [imageToCrop imageScaledToFitSize:_targetSize];
}

#pragma mark Loading the Activity Indicator
- (UIActivityIndicatorView *)setLoadingImage:(UIImageView *)imageView
{
    if(imageView == nil)
        return nil;
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    [activityIndicator setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    activityIndicator.hidesWhenStopped = YES;
    
    [imageView addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
    
    [imageView addConstraint:[NSLayoutConstraint constraintWithItem:activityIndicator
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:imageView
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1
                                                           constant:0]];
    
    [imageView addConstraint:[NSLayoutConstraint constraintWithItem:activityIndicator
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:imageView
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1
                                                           constant:0]];
    
    [imageView addConstraint: [NSLayoutConstraint constraintWithItem: activityIndicator
                                                           attribute: NSLayoutAttributeWidth
                                                           relatedBy: NSLayoutRelationEqual
                                                              toItem: nil
                                                           attribute: NSLayoutAttributeNotAnAttribute
                                                          multiplier: 1
                                                            constant: 20.0f]];
    
    [imageView addConstraint: [NSLayoutConstraint constraintWithItem: activityIndicator
                                                           attribute: NSLayoutAttributeHeight
                                                           relatedBy: NSLayoutRelationEqual
                                                              toItem: nil
                                                           attribute: NSLayoutAttributeNotAnAttribute
                                                          multiplier: 1
                                                            constant: 20.0f]];
    
    return activityIndicator;
}

@end
