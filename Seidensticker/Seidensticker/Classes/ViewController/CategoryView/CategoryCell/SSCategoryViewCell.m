//
//  TableViewCell.m
//  Form
//
//  Created by OCSDEV2 on 23/09/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "SSCategoryViewCell.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSCustomNavigation.h"

@implementation SSCategoryViewCell
@synthesize leftImageView,rightImageView,leftNameLabel,rightNameLabel,leftPriceLabel,rightPriceLabel,middleButton,middleImageView, middleNameLabel,middlePriceLabel;

- (void)awakeFromNib {
    // Initialization code
    
    leftImageView.translatesAutoresizingMaskIntoConstraints = NO;
     rightImageView.translatesAutoresizingMaskIntoConstraints = NO;
     leftNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
     rightNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
     leftPriceLabel.translatesAutoresizingMaskIntoConstraints = NO;
     rightPriceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    middlePriceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    middleNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    middleImageView.translatesAutoresizingMaskIntoConstraints = NO;
    middleButton.translatesAutoresizingMaskIntoConstraints = NO;

    self.discountRightPercentageLabel.layer.masksToBounds = YES;
    self.discountRightPercentageLabel.layer.cornerRadius = 8.0;

    self.discountLeftPercentageLabel.layer.masksToBounds = YES;
    self.discountLeftPercentageLabel.layer.cornerRadius = 8.0;
    
     [self setupConstraints];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void) setupConstraints{
    
    
    if(IS_IPHONE6 || IS_IPHONE6_Plus  || IS_IPHONE5 || IS_IPHONE4)
    {


        // left Name Label
        
        // Width constraint
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftNameLabel
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:0.4
                                                          constant:0]];
        
        // Height constraint
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftNameLabel
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeHeight
                                                        multiplier:0.5
                                                          constant:0]];

        
        
        //Right Label
        
        // Width constraint
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightNameLabel
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:0.4
                                                          constant:0]];
        
        // Height constraint
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightNameLabel
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeHeight
                                                        multiplier:0.5
                                                          constant:0]];
        
        // Right Price Label
        
        // Width constraint
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightPriceLabel
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:0.5
                                                          constant:0]];
        
        // Height constraint
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightPriceLabel
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeHeight
                                                        multiplier:0.5
                                                          constant:0]];
        

    }
    
    
    if(IS_IPHONE6 ||IS_IPHONE6_Plus)
    {
    
        
        
        // Width constraint
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftImageView
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:1.0
                                                          constant:0]];
        
        // Height constraint
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftImageView
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeHeight
                                                        multiplier:1.0
                                                          constant:0]];
        

        
        
        
    // Center horizontally
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftImageView
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:0.85
                                                      constant:0.0]];
    
    
        // Center vertically
     [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftImageView
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0
                                                          constant:0.0]];
        //leftNameLabel
        // Center horizontally
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftNameLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:0.50
                                                          constant:0.0]];
        
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftNameLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.535
                                                          constant:0.0]];
        
        //leftPriceLabel
        
        

        // Center horizontally
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftPriceLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:0.50
                                                          constant:0.0]];
        
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftPriceLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.7
                                                          constant:0.0]];

        
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.discountLeftPriceLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.leftPriceLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.33
                                                          constant:0.0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.discountLeftPriceLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.leftPriceLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0
                                                          constant:0.0]];
        
        
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.adjustLeftLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.leftPriceLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:0.7
                                                          constant:0.0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.adjustLeftLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.leftPriceLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0
                                                          constant:0.0]];

        
        
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.divideViewOne
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.adjustLeftLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.0
                                                          constant:0.0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.divideViewOne
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.adjustLeftLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0
                                                          constant:0.0]];
        

         //right image View
        
        //rightImageView
        
        // Width constraint
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightImageView
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:1.0
                                                          constant:0]];
        
        // Height constraint
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightImageView
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeHeight
                                                        multiplier:0.6
                                                          constant:0]];
        
        
        
        // Center horizontally
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightImageView
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.90
                                                          constant:0.0]];
        
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightImageView
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:0.619
                                                          constant:0.0]];
        
        //rightNameLabel
        
       
        
        // Center horizontally
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightNameLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.53
                                                          constant:0.0]];
        
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightNameLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.53
                                                          constant:0.0]];
        
        //rightPriceLabel
        
      
        
        // Center horizontally
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightPriceLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.53
                                                          constant:0.0]];
        
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightPriceLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.7
                                                          constant:0.0]];
        
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.discountRightPriceLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.rightPriceLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.1
                                                          constant:0.0]];
        
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.discountRightPriceLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.rightPriceLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0
                                                          constant:0.0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.adjustRightLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.rightPriceLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:0.92
                                                          constant:0.0]];
        
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.adjustRightLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.rightPriceLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0
                                                          constant:0.0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.divideViewTwo
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.adjustRightLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.0
                                                          constant:0.0]];
        
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.divideViewTwo
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.adjustRightLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0
                                                          constant:0.0]];
        
        
        
       
    }
    else
    {
        
        // Left image constraints
        // Width constraint
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftImageView
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:0.50
                                                          constant:0]];
        
        // Height constraint
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftImageView
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeHeight
                                                        multiplier:1.05
                                                          constant:0]];
        

        
        
        
        // Center horizontally
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftImageView
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:0.2
                                                          constant:0.0]];
        
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftImageView
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.05
                                                          constant:0.0]];
        
        
        // Constraints for Discount Percentage Label
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.discountLeftPercentageLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.leftImageView
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:4.3
                                                          constant:0.0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.discountLeftPercentageLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.leftImageView
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:0.15
                                                          constant:0.0]];

        
        //leftNameLabel
        
        // Center horizontally
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftNameLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:0.45
                                                          constant:0.0]];
        
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftNameLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.494
                                                          constant:0.0]];
        
        //leftPriceLabel
        
        // Center horizontally
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftPriceLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:0.45
                                                          constant:0.0]];
        
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.leftPriceLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.68
                                                          constant:0.0]];
        
        //Adjust Price label
        
        // Center horizontally
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.adjustLeftLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:0.40
                                                          constant:0.0]];
        
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.adjustLeftLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.62
                                                          constant:0.0]];

        
        
        
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.discountLeftPriceLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.leftPriceLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.7
                                                          constant:0.0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.discountLeftPriceLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.leftPriceLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:0.963
                                                          constant:0.0]];
        
        
        // constraints for divide View
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.divideViewOne
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.adjustLeftLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:0.92
                                                          constant:0.0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.divideViewOne
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.adjustLeftLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0
                                                          constant:0.0]];
      
        //rightImageView
        
        // Width constraint
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightImageView
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:1.0
                                                          constant:0]];
        
        // Height constraint
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightImageView
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeHeight
                                                        multiplier:0.65
                                                          constant:0]];
        
        
        
        
        // Center horizontally
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightImageView
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.82
                                                          constant:0.0]];
        
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightImageView
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:0.672
                                                          constant:0.0]];
        
        
        
        // Constraints for Discount Percentage Label
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.discountRightPercentageLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.rightImageView
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.0
                                                          constant:0.0]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.discountRightPercentageLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.rightImageView
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:0.23
                                                          constant:0.0]];
        

        
        //rightNameLabel
        
        // Center horizontally
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightNameLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.53
                                                          constant:0.0]];
        
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightNameLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.494
                                                          constant:0.0]];
        
        //rightPriceLabel
        
        // Center horizontally
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightPriceLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.53
                                                          constant:0.0]];
        
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.rightPriceLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.68
                                                          constant:0.0]];
        
        
        //Adjust right Label
        
        // Center horizontally
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.adjustRightLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.39
                                                          constant:0.0]];
        
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.adjustRightLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.62
                                                          constant:0.0]];
        
        
        
          // Constraints for  Discount Price Label
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.discountRightPriceLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.rightPriceLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.09
                                                          constant:0.0]];
        
      

        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.discountRightPriceLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.rightPriceLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:0.964
                                                          constant:0.0]];
        
        // Constraints for divide view
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.divideViewTwo
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.adjustRightLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:0.97
                                                          constant:0.0]];
        
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.divideViewTwo
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.adjustRightLabel
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0
                                                          constant:0.0]];
        
        
    }
}

@end
