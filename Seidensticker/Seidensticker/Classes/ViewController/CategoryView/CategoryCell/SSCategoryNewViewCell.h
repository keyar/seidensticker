//
//  TableViewCell.h
//  Form
//
//  Created by OCSDEV2 on 23/09/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIImage+ProportionalFill.h"

#import "AFNetworking.h"

#import "UIImageView+AFNetworking.h"

@interface SSCategoryNewViewCell : UITableViewCell

@property(strong,nonatomic) IBOutlet UIView *divideViewOne,*divideViewTwo, *divideViewThree;

@property (nonatomic, weak) IBOutlet UIImageView *leftImageView;
@property (nonatomic, weak) IBOutlet UIImageView *rightImageView;
@property (nonatomic, weak) IBOutlet UIImageView *middleImageView;
@property (strong, nonatomic) IBOutlet UILabel *discountRightPercentageLabel;
@property (strong, nonatomic) IBOutlet UILabel *discountLeftPercentageLabel;
@property (strong, nonatomic) IBOutlet UILabel *discountMiddlePercentageLabel;
@property (strong, nonatomic) IBOutlet UILabel *leftNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *leftPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *discountLeftPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *rightNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *rightPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *discountRightPriceLabel,*adjustLeftLabel,*adjustRightLabel, *adjustMiddleLabel;

@property (strong, nonatomic) IBOutlet UILabel *discountMiddlePriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *middleNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *middlePriceLabel;
@property (strong, nonatomic) IBOutlet UIButton *leftButton;
@property (strong, nonatomic) IBOutlet UIButton *rightButton;
@property (strong, nonatomic) IBOutlet UIButton *middleButton;


@property(strong,nonatomic) IBOutlet UIView *leftDiscountView;
@property(strong,nonatomic) IBOutlet UIView *rightDiscountView;

- (void)loadLeftProductDetails:(NSDictionary *)details;

- (void)loadLeftRighttDetails:(NSDictionary *)details;

- (void)loadLeftMiddleDetails:(NSDictionary *)details;

@end
