//
//  TableViewCell.h
//  Form
//
//  Created by OCSDEV2 on 23/09/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSCategoryViewCell : UITableViewCell

@property(strong,nonatomic) IBOutlet UIView *divideViewOne,*divideViewTwo;

@property (strong, nonatomic) IBOutlet UIImageView *leftImageView;
@property (strong, nonatomic) IBOutlet UIImageView *rightImageView;
@property (strong, nonatomic) IBOutlet UIImageView *middleImageView;
@property (strong, nonatomic) IBOutlet UILabel *discountRightPercentageLabel;
@property (strong, nonatomic) IBOutlet UILabel *discountLeftPercentageLabel;
@property (strong, nonatomic) IBOutlet UILabel *leftNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *leftPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *discountLeftPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *rightNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *rightPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *discountRightPriceLabel,*adjustLeftLabel,*adjustRightLabel;
@property (strong, nonatomic) IBOutlet UILabel *middleNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *middlePriceLabel;
@property (strong, nonatomic) IBOutlet UIButton *leftButton;
@property (strong, nonatomic) IBOutlet UIButton *rightButton;
@property (strong, nonatomic) IBOutlet UIButton *middleButton;
@end
