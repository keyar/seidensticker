    //
//  SSCategoryViewController.m
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/18/15.
//  Copyright (c) 2015 Oclocksoftware. All rights reserved.
//

#import "SSCategoryViewController.h"
#import "SSCustomNavigation.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#import "SSFilterViewController.h"
#import "NSString+HTML.h"
#import "SSCategoryNewViewCell.h"
#import "AsyncImage.h"
#import "SSProductPageViewController.h"
#import "SSAppDelegate.h"
#import "SSShoppingCart.h"
#import "SSStoreLoginViewController.h"

@interface SSCategoryViewController ()<UITableViewDataSource,UITableViewDelegate> {
    
    NSMutableArray *productsArray;
    NSMutableArray *filtersArray;
    
    SSWebService *webservice;
        
    BOOL isDataLoaded;
    BOOL isFiltered;
    int currentPageProduct;
    NSInteger currentLoadedPage;
    NSInteger totalPages;
    NSString *currentPage;
    SSCustomNavigation *navigation;
    UIRefreshControl *refreshControl;

}
@end

@implementation SSCategoryViewController
@synthesize currentCategoryItem;
@synthesize productsTableView;
@synthesize currentCategoryTitle;
@synthesize currentCategoryURL;


#pragma mark - View States
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setHidesBackButton:YES];
   
    webservice = [[SSWebService alloc] init];
    
    productsArray = [[NSMutableArray alloc] init];
    
    filtersArray  = [[NSMutableArray alloc] init];

    isDataLoaded = NO;
    
    SSAppDelegate *appDelegate = (SSAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.categoryViewController=self;
    
    [self.view setBackgroundColor:VIEW_BG_COLOR];
    
    if ([COMMON isReachableAlert])
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadProductsForFilterQuery:) name:@"FILTER" object:nil];
    currentPageProduct = 1;
    currentPage = @"1";
    currentLoadedPage = 1;
    if(IS_IPHONE6_Plus){
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    [self.navigationController.navigationBar addSubview:navBorder];
    }
    
    // Initialize the refresh control.
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self
                       action:@selector(loadProducts)
             forControlEvents:UIControlEventValueChanged];
    [productsTableView addSubview:refreshControl];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(isStoreApp){
        if ([[NSUserDefaults standardUserDefaults] boolForKey:IsStoreUserLoggedIn]==NO) {
            [self loadStoreLoginPage];
        }
        
    }
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self loadNavigation];
    [self getStrFilter];
    
    if (!isDataLoaded) {
        if ([COMMON isReachableAlert]){
            [self loadProducts];
            isDataLoaded = YES;
        }
    }
   
}
-(void)loadStoreLoginPage{
    
    SSStoreLoginViewController *storeLoginViewController=nil;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        storeLoginViewController = [[SSStoreLoginViewController alloc]initWithNibName:@"SSStoreLoginViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        storeLoginViewController = [[SSStoreLoginViewController alloc]initWithNibName:@"SSStoreLoginViewController" bundle:[NSBundle mainBundle]];
    }
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:storeLoginViewController];
    
    [self.navigationController presentViewController:navigationController animated:NO completion:nil];
    
}

- (void)getStrFilter
{
    NSString *getFilterString = [[NSUserDefaults standardUserDefaults]valueForKey:@"FILTERSTRING"];
    if (getFilterString != NULL && ![getFilterString isKindOfClass:[NSNull class]])
        isFiltered = YES;
    else
        isFiltered = NO;
}


- (void)loadNavigation
{
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    navigation = [[SSCustomNavigation alloc] initWithNibName:@"SSCustomNavigation" bundle:nil];
    navigation.view.tag = 1001;
    [self.navigationController.navigationBar addSubview:navigation.view];
    
    [navigation setLeftHidden:NO];
    
    [navigation setTitleHidden:NO];
    
    if (currentCategoryItem != NULL)
    {
        if ([currentCategoryItem objectForKey:@"html"] != NULL) {
           [navigation.titleLbl setText:[COMMON getDecodedString:[currentCategoryItem objectForKey:@"html"]]];
        }
    }
    else
    {
       currentCategoryTitle = [currentCategoryTitle
                                         stringByReplacingOccurrencesOfString:@"%E9" withString:@"&eacute;"];
       [navigation.titleLbl setText:[COMMON getDecodedString:currentCategoryTitle]];
       // navigation.titleLbl.text=currentCategoryTitle;
        NSLog(@"%@",navigation.titleLbl.text);
        
    }
   // navigation.titleLbl.textAlignment = NSTextAlignmentLeft;

    [navigation.titleLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    
    [navigation.navigationLeftLbl setText:NSLocalizedString(@"Retour", @"")];
    [navigation.navigationLeftLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [navigation.navigationLeftBtn addTarget:self action:@selector(retourBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navigation setCatergoryShopBtnLabelHidden:YES];
    
    [navigation.navigationCatergoryShopBtn setHidden:YES];
    [navigation.navigationCatergoryShopLabel setHidden:YES];
    [navigation.navigationCategoryFiltreLabel setHidden:NO];
    
    [navigation setShopBtnHidden:NO];
    [navigation setShopLabelHidden:NO];
    
    [navigation.navigationRightLbl setHidden:YES];
    
    UITapGestureRecognizer *tapFilterLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(filterBtnAction)];
    [tapFilterLabel setNumberOfTapsRequired:1];
    
    [navigation.navigationCategoryFiltreLabel addGestureRecognizer:tapFilterLabel];
    [navigation.navigationCategoryFiltreLabel setUserInteractionEnabled:YES];
    
    [navigation.navigationCategoryFiltreLabel setText:NSLocalizedString(@"Filtres", @"")];
    [navigation.navigationCategoryFiltreLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
   
    [self setCartIconLabel];
}
-(void)setCartIconLabel
{
    NSMutableArray *cartItems = [NSMutableArray new];
    cartItems = [SHOPPINGCART getCartItems];
    NSLog(@"cart values %@",[SHOPPINGCART getCartItems]);
    cartItems = [SHOPPINGCART getCartItems];
    
    NSInteger quantity = 0;
    
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }
    
    navigation.navigationShopLbl.text = [NSString stringWithFormat:@"%ld",(long)quantity];
    [navigation.navigationShopLbl setBackgroundColor:[UIColor clearColor]];
    navigation.navigationShopLbl.textColor = [UIColor blackColor];
    [navigation.navigationShopLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(8)]];//10
    [navigation.navigationShopLbl setTextAlignment:NSTextAlignmentCenter];
    
    [navigation.navigationShopBtn addTarget:self action:@selector(categoryRightCartBtnAction:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)categoryRightCartBtnAction:(id)sender
{
    SSMonPanierViewController *monpanierViewController;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonpanierViewControlleriPad" bundle:[NSBundle mainBundle]];
        
        
    }
    else {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonPanierViewController" bundle:[NSBundle mainBundle]];
    }
    
    [self.navigationController pushViewController:monpanierViewController animated:YES];
}


#pragma mark - Load Products For Category

- (void) loadProductsForFilterQuery:(NSNotification *)notification
{
    isfilter=YES;
    isFilterValiderClicked=YES; //new change raji 25oct
    
    
    [COMMON showLoadingIcon:self];
    
    NSString *filerStr = [notification object];
   // NSString *filterCategoryURL = [NSString stringWithFormat:@"%@&%@", categoryURL, filerStr];
    
     NSLog(@" URL : %@", [NSString stringWithFormat:@"%@&%@", categoryURL, filerStr]);
    
    //new change raji
    NSString *currentUrl;
    currentLoadedPage=1;
    if (currentCategoryURL != NULL && ![currentCategoryURL isEqualToString:@""]) {
       // currentUrl = [NSString stringWithFormat:@"%@&p=%ld",currentCategoryURL,(long)currentLoadedPage];
        currentUrl = [NSString stringWithFormat:@"%@",currentCategoryURL];
        
    } else {
        currentUrl = [currentCategoryItem objectForKey:@"url"];
    }
    
    
    NSString *filterCategoryURL = [NSString stringWithFormat:@"%@&%@", currentUrl, filerStr];
    
    currentFilterCategoryUrl = filterCategoryURL;
    
    NSLog(@" URL : %@", filterCategoryURL);
    
    [webservice getRequestForURL:filterCategoryURL success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        [productsArray removeAllObjects];
        
        totalPages = [[[responseObject valueForKey:@"categoryproducts"] valueForKey:@"total_pages"] integerValue];
        
        NSLog(@" FilterTotalPages : %ld", (long)totalPages);
        
        [self loadTableWithProducts:responseObject];
        
        
    } failure:^(AFHTTPRequestOperation *operation, id error) {
        //[SSAppCommon showSimpleAlertWithMessage:@"No Internet Connection available. Kindly check your Internet"];
        [COMMON removeLoadingIcon];
        
    }];
}

- (void) loadProducts {
    
    isfilter=NO;
    
    navigation.navigationRightBtn.hidden=YES;
    
    [COMMON showLoadingIcon:self];
    
    //used for filter products loading with pagination work after filter is used
    if(isFilterValiderClicked==YES){
        
        if (currentFilterCategoryUrl != NULL && ![currentFilterCategoryUrl isEqualToString:@""]) {
            categoryURL = [NSString stringWithFormat:@"%@&p=%ld",currentFilterCategoryUrl,(long)currentLoadedPage];
        } else {
            categoryURL = [currentCategoryItem objectForKey:@"url"];
        }
        
        isFilterValiderClicked=NO;
    }
    else{
        if (currentCategoryURL != NULL && ![currentCategoryURL isEqualToString:@""]) {
            categoryURL = [NSString stringWithFormat:@"%@&p=%ld",currentCategoryURL,(long)currentLoadedPage];
        } else {
            categoryURL = [currentCategoryItem objectForKey:@"url"];
        }
    }
    
    //if(isFilterValiderClicked!=YES){
        
        [webservice getRequestForURL:categoryURL success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"%@",responseObject);
            [refreshControl endRefreshing];
            
            if (currentLoadedPage == 1) {
                totalPages = [[[responseObject valueForKey:@"categoryproducts"] valueForKey:@"total_pages"] integerValue];
            }
            [self loadTableWithProducts:responseObject];
            
        } failure:^(AFHTTPRequestOperation *operation, id error) {
            [refreshControl endRefreshing];
            //[SSAppCommon showSimpleAlertWithMessage:@"No Internet Connection available. Kindly check your Internet"];
            [COMMON removeLoadingIcon];
            
        }];
   
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat height = scrollView.frame.size.height;
    
    CGFloat contentYoffset = scrollView.contentOffset.y;
    
    CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
    
    if(distanceFromBottom < height)
    {
        if(isFilterValiderClicked==YES){
            currentLoadedPage=1;
            isFilterValiderClicked=NO;
        }
       
    }
}


- (void) loadTableWithProducts:(NSMutableDictionary *)dataArray {
    
    if (([dataArray objectForKey:@"categoryproducts"] != NULL) &&
        [[dataArray objectForKey:@"categoryproducts"] isKindOfClass:[NSDictionary class]] &&
        [[dataArray objectForKey:@"categoryproducts"] objectForKey:@"products"] != NULL &&
        [[[dataArray objectForKey:@"categoryproducts"] objectForKey:@"products"] isKindOfClass:[NSArray class]]
        ) {
        
        if (currentLoadedPage == 1) {
            productsArray = [[[dataArray objectForKey:@"categoryproducts"] objectForKey:@"products"] mutableCopy];
            filtersArray = [[[dataArray objectForKey:@"categoryproducts"] objectForKey:@"filters"] mutableCopy];
        }
        else{
            for (NSDictionary *dictonaryProducts in [[dataArray objectForKey:@"categoryproducts"] objectForKey:@"products"] ) {
                [productsArray addObject:dictonaryProducts];
            }
        }
         NSLog(@"productsArray count%lu", (unsigned long)[productsArray count]);

        [productsTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    }
    else
    {
        
        productsArray = nil;
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:[dataArray objectForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    [productsTableView reloadData];
    [COMMON removeLoadingIcon];
    navigation.navigationRightBtn.hidden=NO;
}

- (void)alertView: (UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == [alertView cancelButtonIndex])
    {
        if(productsArray!=nil){
            [self navigateToFilter];
        }
        
    }
}

- (int) getProductIndexForObject:(NSMutableDictionary *)currentObject {
    for (int i=0; i<[productsArray count];i++)
    {
        if ([[[productsArray objectAtIndex:i] valueForKey:@"html"] isEqualToString:[currentObject valueForKey:@"html"]]) {
            return i;
        }
    }
    return 0;
}

#pragma mark-Button Action

-(void)retourBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)filterBtnAction
{
    [self navigateToFilter];
}

-(void) navigateToFilter
{
   
    SSFilterViewController *filterVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        filterVC = [[SSFilterViewController alloc]initWithNibName:@"SSFilterViewControlleriPad" bundle:[NSBundle mainBundle]];
        filterVC.controller =self;
        filterVC.currentFilterArray = filtersArray;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:filterVC];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
    else
    {
        filterVC = [[SSFilterViewController alloc]initWithNibName:@"SSFilterViewController" bundle:nil];
        filterVC.controller =self;
        filterVC.currentFilterArray = filtersArray;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:filterVC];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
}

#pragma mark - Table View Delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPHONE6_Plus)
    {
        return 280;
    }
    else if(IS_IPHONE6 ||IS_IPHONE5 ||IS_IPHONE4)
    {
        return 250;
    }
    else
    {
        return 380;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%@", productsArray);
     NSLog(@"productsArray count%lu", (unsigned long)[productsArray count]);
    if(IS_IPHONE6_Plus ||IS_IPHONE6 ||IS_IPHONE5 ||IS_IPHONE4)
    {
        if ([productsArray count] % 2 == 0)
            return ceil([productsArray count]/2);
        else
            return ceil([productsArray count]/2) + 1;
    }
    else
    {
        if ([productsArray count] % 3 == 0)
            return ceil([productsArray count]/3);
        else
            return ceil([productsArray count]/3) + 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    SSCategoryNewViewCell *cell = (SSCategoryNewViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"SSCategoryNewViewCell" owner:self options:nil];
        }
        else
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"SSCategoryNewViewCelliPad" owner:self options:nil];
        }
        cell         = [nib objectAtIndex:0];
    }

    if(IS_IPHONE6_Plus ||IS_IPHONE6 ||IS_IPHONE5 ||IS_IPHONE4 ||[[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        cell.discountLeftPercentageLabel.hidden=YES;
        cell.discountRightPercentageLabel.hidden=YES;
        
        cell.divideViewOne.hidden=YES;
        cell.divideViewTwo.hidden=YES;
        
        cell.adjustLeftLabel.hidden=YES;
        cell.adjustRightLabel.hidden=YES;
        
        cell.leftNameLabel.numberOfLines = 2;
        cell.rightNameLabel.numberOfLines = 2;
        
        [cell.leftNameLabel     setFont:[COMMON getResizeableFont:FuturaStd_Book(11)]];
        [cell.leftPriceLabel    setFont:[COMMON getResizeableFont:FuturaStd_Book(11)]];
        [cell.rightNameLabel    setFont:[COMMON getResizeableFont:FuturaStd_Book(11)]];
        [cell.rightPriceLabel   setFont:[COMMON getResizeableFont:FuturaStd_Book(11)]];
        [cell.discountRightPriceLabel    setFont:[COMMON getResizeableFont:FuturaStd_Book(11)]];
        [cell.discountLeftPriceLabel   setFont:[COMMON getResizeableFont:FuturaStd_Book(11)]];
        [cell.discountRightPercentageLabel    setFont:[COMMON getResizeableFont:FuturaStd_Book(11)]];
        [cell.discountLeftPercentageLabel   setFont:[COMMON getResizeableFont:FuturaStd_Book(11)]];
        [cell.adjustLeftLabel    setFont:[COMMON getResizeableFont:FuturaStd_Book(11)]];
        [cell.adjustRightLabel   setFont:[COMMON getResizeableFont:FuturaStd_Book(11)]];
        
        NSMutableDictionary *productArrayLeft;
        NSMutableDictionary *productArrayRight;
        if (indexPath.row*2 < [productsArray count]) {
            productArrayLeft = [productsArray objectAtIndex:indexPath.row*2];
        }
        if ((indexPath.row*2 + 1) < [productsArray count]) {
           productArrayRight = [productsArray objectAtIndex:indexPath.row*2 + 1];
        }
        
        cell.leftNameLabel.text     = [COMMON getDecodedString:[productArrayLeft objectForKey:@"name"]];
        cell.leftPriceLabel.text    = [COMMON getDecodedString:[productArrayLeft objectForKey:@"price"]];
        cell.rightNameLabel.text    = [COMMON getDecodedString:[productArrayRight objectForKey:@"name"]];
        cell.rightPriceLabel.text   = [COMMON getDecodedString:[productArrayRight objectForKey:@"price"]];
        cell.discountLeftPriceLabel.text=[COMMON getDecodedString:[productArrayLeft objectForKey:@"special_price"]];
        cell.discountRightPriceLabel.text=[COMMON getDecodedString:[productArrayRight objectForKey:@"special_price"]];
        
      if(!isStoreApp){
          if([productArrayLeft objectForKey:@"special_price"] != NULL || [productArrayRight objectForKey:@"special_price"] != NULL )
          {
              
              NSLog(@"special_price %@",cell.leftPriceLabel.text);
              
              NSLog(@"special_price Name %@",cell.leftNameLabel.text);

              
              NSLog(@"special_price %@",cell.rightPriceLabel.text);
              
               NSLog(@"special_price Name %@",cell.rightNameLabel.text);
              
              
              cell.discountRightPercentageLabel.hidden=NO;
              cell.discountLeftPercentageLabel.hidden=NO;
              cell.divideViewOne.hidden=NO;
              cell.divideViewTwo.hidden=NO;
              cell.adjustLeftLabel.hidden=NO;
              cell.adjustRightLabel.hidden=NO;
              cell.leftPriceLabel.hidden=YES;
              cell.rightPriceLabel.hidden=YES;
              cell.adjustLeftLabel.text=[NSString stringWithFormat:@"%@",
                                         cell.leftPriceLabel.text];
              cell.adjustRightLabel.text=[NSString stringWithFormat:@"%@",
                                          cell.rightPriceLabel.text];
              cell.adjustLeftLabel.textColor =[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:2.0f];
              cell.adjustRightLabel.textColor =[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:2.0f];
              float leftPriceValue= [cell.leftPriceLabel.text intValue];
              float rightPricevalue=[cell.rightPriceLabel.text  intValue];
              float leftDiscountPriceValue= [cell.discountLeftPriceLabel.text intValue];
              float rightDiscountPricevalue=[cell.discountRightPriceLabel.text  intValue];
              float leftMinusPriceValue = leftPriceValue -leftDiscountPriceValue;
              float rightMinusPriceValue= rightPricevalue-rightDiscountPricevalue;
              int leftDiscountPerValue= (leftMinusPriceValue*100)/leftPriceValue;
              int rightDiscountPerValue=(rightMinusPriceValue*100)/rightPricevalue;
              
              NSLog(@"Right discount= %d",rightDiscountPerValue);
              
              if(rightDiscountPerValue ==100)
              {
                  cell.rightPriceLabel.hidden=NO;
                  cell.discountRightPercentageLabel.hidden=YES;
                  cell.adjustRightLabel.hidden=NO;
                  cell.divideViewTwo.hidden=YES;
                  cell.adjustRightLabel.textColor =[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:2.0f];
              }
              if(leftDiscountPerValue ==100)
              {
                  cell.leftPriceLabel.hidden=NO;
                  cell.discountLeftPercentageLabel.hidden=YES;
                  cell.adjustLeftLabel.hidden=NO;
                  cell.divideViewOne.hidden=YES;
                  cell.adjustLeftLabel.textColor =[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:2.0f];
              }
              if(rightDiscountPerValue ==-2147483648 )
              {
                  cell.discountRightPercentageLabel.hidden=YES;
                  cell.adjustRightLabel.hidden=YES;
                  cell.divideViewTwo.hidden=YES;
                  cell.rightImageView.hidden=YES;
              }
              if(rightDiscountPerValue ==0 )
              {
                  cell.discountRightPercentageLabel.hidden=YES;
                  cell.adjustRightLabel.hidden=YES;
                  cell.divideViewTwo.hidden=YES;
                  cell.rightImageView.hidden=YES;
              }
              cell.discountLeftPercentageLabel.text  = [NSString stringWithFormat:@" -%d%%",leftDiscountPerValue];
              cell.discountRightPercentageLabel.text = [NSString stringWithFormat:@" -%d%%",rightDiscountPerValue];
          }
      }
        
        
        if(isStoreApp){
            cell.discountLeftPercentageLabel.hidden=YES;
            cell.discountRightPercentageLabel.hidden=YES;
            cell.discountMiddlePercentageLabel.hidden=YES;
            
            cell.divideViewOne.hidden=YES;
            cell.divideViewTwo.hidden=YES;
            cell.divideViewThree.hidden=YES;
            
            cell.adjustLeftLabel.hidden=YES;
            cell.adjustRightLabel.hidden=YES;
            cell.adjustMiddleLabel.hidden=YES;
            
            cell.discountLeftPriceLabel.hidden=YES;
            cell.discountRightPriceLabel.hidden=YES;
            cell.discountMiddlePriceLabel.hidden=YES;
            
            cell.leftDiscountView.hidden=YES;
            cell.rightDiscountView.hidden=YES;
            
            cell.leftPriceLabel.hidden=NO;
            cell.rightPriceLabel.hidden=NO;
            
        }
        
        [cell loadLeftProductDetails:productArrayLeft];
        [cell loadLeftRighttDetails:productArrayRight];
        
        [cell.leftButton setTag:indexPath.row*2];
        [cell.rightButton setTag:indexPath.row*2 + 1];
        
        [cell.leftButton addTarget:self action:@selector(gotoProductDetailView:) forControlEvents:UIControlEventTouchUpInside];
        [cell.rightButton addTarget:self action:@selector(gotoProductDetailView:) forControlEvents:UIControlEventTouchUpInside];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
           return cell;
    }

    else
    {
    cell.discountLeftPercentageLabel.hidden=YES;
    cell.discountRightPercentageLabel.hidden=YES;
    cell.discountMiddlePercentageLabel.hidden=YES;
        
    cell.divideViewOne.hidden=YES;
    cell.divideViewTwo.hidden=YES;
    cell.divideViewThree.hidden=YES;
        
    cell.adjustLeftLabel.hidden=YES;
    cell.adjustRightLabel.hidden=YES;
    cell.adjustMiddleLabel.hidden=YES;
        
    cell.leftNameLabel.numberOfLines = 2;
    [cell.leftNameLabel sizeToFit];
    cell.rightNameLabel.numberOfLines = 2;
    [cell.rightNameLabel sizeToFit];
    cell.middleNameLabel.numberOfLines = 2;
    [cell.middleNameLabel sizeToFit];
        
    cell.leftNameLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.rightNameLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.middleNameLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    [cell.leftNameLabel     setFont:[COMMON getResizeableFont:FuturaStd_Book(11)]];
    [cell.leftPriceLabel    setFont:[COMMON getResizeableFont:FuturaStd_Book(11)]];
    [cell.rightNameLabel    setFont:[COMMON getResizeableFont:FuturaStd_Book(11)]];
    [cell.rightPriceLabel   setFont:[COMMON getResizeableFont:FuturaStd_Book(11)]];
    [cell.middleNameLabel   setFont:[COMMON getResizeableFont:FuturaStd_Book(11)]];
    [cell.middlePriceLabel  setFont:[COMMON getResizeableFont:FuturaStd_Book(11)]];
     
    NSMutableDictionary *productArrayLeft;
    NSMutableDictionary *productArrayRight;
    NSMutableDictionary *productArrayMiddle;
    if (indexPath.row*3 < [productsArray count]) {
        productArrayLeft = [productsArray objectAtIndex:indexPath.row*3];
    }
    if ((indexPath.row*3 + 1) < [productsArray count]) {
        productArrayMiddle = [productsArray objectAtIndex:indexPath.row*3 + 1];
    }
    if ((indexPath.row*3 + 2) < [productsArray count]) {
        productArrayRight = [productsArray objectAtIndex:indexPath.row*3 + 2];
    }
    cell.leftNameLabel.text     = [COMMON getDecodedString:[productArrayLeft objectForKey:@"name"]];
    cell.leftPriceLabel.text    = [COMMON getDecodedString:[productArrayLeft objectForKey:@"price"]];
    cell.rightNameLabel.text    = [COMMON getDecodedString:[productArrayRight objectForKey:@"name"]];
    cell.rightPriceLabel.text   = [COMMON getDecodedString:[productArrayRight objectForKey:@"price"]];
    cell.middleNameLabel.text    = [COMMON getDecodedString:[productArrayMiddle objectForKey:@"name"]];
    cell.middlePriceLabel.text   = [COMMON getDecodedString:[productArrayMiddle objectForKey:@"price"]];
        
    cell.discountLeftPriceLabel.text=[COMMON getDecodedString:[productArrayLeft objectForKey:@"special_price"]];
    cell.discountRightPriceLabel.text=[COMMON getDecodedString:[productArrayRight objectForKey:@"special_price"]];
    cell.discountMiddlePriceLabel.text=[COMMON getDecodedString:[productArrayMiddle objectForKey:@"special_price"]];
        
    if(!isStoreApp){
        if([productArrayLeft objectForKey:@"special_price"] || [productArrayRight objectForKey:@"special_price"] ||[productArrayMiddle objectForKey:@"special_price"] )
        {
            cell.discountRightPercentageLabel.hidden=NO;
            cell.discountLeftPercentageLabel.hidden=NO;
            cell.divideViewOne.hidden=NO;
            cell.divideViewTwo.hidden=NO;
            cell.adjustLeftLabel.hidden=NO;
            cell.adjustRightLabel.hidden=NO;
            cell.discountMiddlePercentageLabel.hidden=NO;
            cell.divideViewThree.hidden=NO;
            cell.adjustMiddleLabel.hidden=NO;
            
            cell.leftPriceLabel.hidden=YES;
            cell.rightPriceLabel.hidden=YES;
            cell.middlePriceLabel.hidden=YES;
            
            cell.adjustLeftLabel.text=[NSString stringWithFormat:@"%@",
                                       cell.leftPriceLabel.text];
            cell.adjustRightLabel.text=[NSString stringWithFormat:@"%@",
                                        cell.rightPriceLabel.text];
            cell.adjustMiddleLabel.text=[NSString stringWithFormat:@"%@",
                                         cell.middlePriceLabel.text];
            
            cell.adjustLeftLabel.textColor =[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:2.0f];
            cell.adjustRightLabel.textColor =[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:2.0f];
            cell.adjustMiddleLabel.textColor =[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:2.0f];
            
            float leftPriceValue= [cell.leftPriceLabel.text intValue];
            float rightPricevalue=[cell.rightPriceLabel.text  intValue];
            float middlePriceValue=[cell.middlePriceLabel.text  intValue];
            
            float leftDiscountPriceValue= [cell.discountLeftPriceLabel.text intValue];
            float rightDiscountPricevalue=[cell.discountRightPriceLabel.text  intValue];
            float middleDiscountPricevalue=[cell.discountMiddlePriceLabel.text  intValue];
            
            float leftMinusPriceValue = leftPriceValue -leftDiscountPriceValue;
            float rightMinusPriceValue= rightPricevalue-rightDiscountPricevalue;
            float middleMinusPriceValue= middlePriceValue-middleDiscountPricevalue;
            
            int leftDiscountPerValue= (leftMinusPriceValue*100)/leftPriceValue;
            int rightDiscountPerValue=(rightMinusPriceValue*100)/rightPricevalue;
            int middleDiscountPerValue=(middleMinusPriceValue*100)/middlePriceValue;
            
            if(rightDiscountPerValue ==-2147483648)
            {
                cell.discountRightPercentageLabel.hidden=YES;
                cell.adjustRightLabel.hidden=YES;
                cell.divideViewTwo.hidden=YES;
                cell.rightImageView.hidden=YES;
            }
            
            if(rightDiscountPerValue ==100)
            {
                cell.rightPriceLabel.hidden=NO;
                cell.discountRightPercentageLabel.hidden=YES;
                cell.adjustRightLabel.hidden=YES;
                cell.divideViewTwo.hidden=YES;
                
            }
            if(leftDiscountPerValue ==100)
            {
                cell.leftPriceLabel.hidden=NO;
                cell.discountLeftPercentageLabel.hidden=YES;
                cell.adjustLeftLabel.hidden=YES;
                cell.divideViewOne.hidden=YES;
                
            }
            cell.discountLeftPercentageLabel.text  = [NSString stringWithFormat:@" -%d%%",leftDiscountPerValue];
            cell.discountRightPercentageLabel.text = [NSString stringWithFormat:@" -%d%%",rightDiscountPerValue];
            cell.discountMiddlePercentageLabel.text = [NSString stringWithFormat:@" -%d%%",middleDiscountPerValue];
        }
    }
   
        if(isStoreApp){
            cell.discountLeftPercentageLabel.hidden=YES;
            cell.discountRightPercentageLabel.hidden=YES;
            cell.discountMiddlePercentageLabel.hidden=YES;
            
            cell.divideViewOne.hidden=YES;
            cell.divideViewTwo.hidden=YES;
            cell.divideViewThree.hidden=YES;
            
            cell.adjustLeftLabel.hidden=YES;
            cell.adjustRightLabel.hidden=YES;
            cell.adjustMiddleLabel.hidden=YES;
            
            cell.discountLeftPriceLabel.hidden=YES;
            cell.discountRightPriceLabel.hidden=YES;
            cell.discountMiddlePriceLabel.hidden=YES;
            
            cell.leftPriceLabel.hidden=NO;
            cell.rightPriceLabel.hidden=NO;
            cell.middlePriceLabel.hidden=NO;
            
         
        }
        
       
        
        /*
         discountRightPercentageLabel
         discountLeftPercentageLabel
         discountMiddlePercentageLabel
         
         divideViewOne  //left
         divideViewTwo  // right
         divideViewThree //mid

         leftPriceLabel //original centre left
         rightPriceLabel //original centre right
         middlePriceLabel //original centre middle
         
         adjustLeftLabel  //strike left original_Price
         adjustMiddleLabel  //strike middle original_Price
         adjustRightLabel //strike right original_Price
         
         discountLeftPriceLabel  // on right of each image special_price
         discountRightPriceLabel  // on right of each image special_price
         discountMiddlePriceLabel  // on right of each image special_price
         
         [cell.rightPriceLabel setBackgroundColor:[UIColor greenColor]];
         [cell.leftPriceLabel setBackgroundColor:[UIColor blueColor]];
         [cell.middlePriceLabel setBackgroundColor:[UIColor yellowColor]];
         
         [cell.discountLeftPriceLabel setBackgroundColor:[UIColor yellowColor]];
         [cell.discountRightPriceLabel setBackgroundColor:[UIColor redColor]];
         [cell.discountMiddlePriceLabel setBackgroundColor:[UIColor blueColor]];
         */

        
    [cell loadLeftProductDetails:productArrayLeft];
    [cell loadLeftRighttDetails:productArrayRight];
    [cell loadLeftMiddleDetails:productArrayMiddle];
        
        
    [cell.leftButton setTag:indexPath.row*3];
    [cell.middleButton setTag:indexPath.row*3 + 1];
    [cell.rightButton setTag:indexPath.row*3 + 2];
    [cell.leftButton addTarget:self action:@selector(gotoProductDetailView:) forControlEvents:UIControlEventTouchUpInside];
        [cell.rightButton addTarget:self action:@selector(gotoProductDetailView:) forControlEvents:UIControlEventTouchUpInside];
    [cell.middleButton addTarget:self action:@selector(gotoProductDetailView:) forControlEvents:UIControlEventTouchUpInside];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
           return cell;
    }

}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        if (([productsArray count]%3) == 0) {
            if (indexPath.row == (([productsArray count]/3)-1) && currentLoadedPage < totalPages) {
                currentLoadedPage++;
                [self loadProducts];
            }
        }
        else{
            if (indexPath.row == (([productsArray count]/3)) && currentLoadedPage < totalPages) {
                currentLoadedPage++;
                [self loadProducts];
            }
        }
        
    }
    else{
        if (([productsArray count]%2) == 0){
            NSLog(@"%ld",(long)([productsArray count]/2)-1);
            NSLog(@" indexPath.row = %ld",(long)indexPath.row);

            if (indexPath.row == (([productsArray count]/2)-1) && currentLoadedPage < totalPages) {
                currentLoadedPage++;
                [self loadProducts];
            }
        }
        else{
            NSLog(@"%ld",(long)([productsArray count]/2));
            NSLog(@" indexPath.row = %ld",(long)indexPath.row);

            if (indexPath.row == (([productsArray count]/2)) && currentLoadedPage < totalPages) {
                currentLoadedPage++;
                [self loadProducts];
            }
        }
    }
    
}


- (void) gotoProductDetailView:(id)sender {
    UIButton *button = (UIButton *)sender;
    int currentIndex = (int)[button tag];

    [COMMON TrackerWithName:[NSString stringWithFormat:@"Product - %@",[[productsArray objectAtIndex:currentIndex] objectForKey:@"name"]]];
    
    SSProductPageViewController *productVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        productVC = [[SSProductPageViewController alloc]initWithNibName:@"SSProductpageViewControlleriPad" bundle:[NSBundle mainBundle]];
        productVC.currentProductItem = [productsArray objectAtIndex:currentIndex];
         productVC.redirectproduct=@"";
        [self.navigationController pushViewController:productVC animated:YES];
    }
    else {
        productVC = [[SSProductPageViewController alloc]
                     initWithNibName:@"SSProductPageViewController"
                     bundle:nil];
        productVC.currentProductItem = [productsArray objectAtIndex:currentIndex];
        productVC.redirectproduct=@"";
        
        [self.navigationController pushViewController:productVC animated:YES];
    }
}

@end
