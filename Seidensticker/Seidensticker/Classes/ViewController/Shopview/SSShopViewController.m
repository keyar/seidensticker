//
//  SSShopViewController.m
//  Seidensticker
//
//  Created by Nandha Kumar on 19/09/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSShopViewController.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSCustomNavigation.h"
#import "SSCategoryViewController.h"
#import "SSShopViewCustomCell.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#import "SSMenuCustomView.h"
#import "AsyncImage.h"
#import "SSMenuCustomView.h"
#import "UIImageView+AFNetworking.h"
#import "SSMenuDetailViewController.h"
#import "SSProductPageViewController.h"
#import "SSAppDelegate.h"
#import "SSShoppingCart.h"
#import "SSStoreLoginViewController.h"

#pragma mark - Interface


@interface SSShopViewController ()<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
{
    int cellCount;
    IBOutlet SSShopViewCustomCell *shopViewCell;
    
    NSMutableArray *bannerArray;
    
    NSMutableArray *sliderArray;
    
    NSMutableArray *menuArray;
    
    NSData *imageData;
    
    SSWebService *webservice;
    
    BOOL isDataLoaded;
    
    UILabel *qtyCount;
}

@end

#pragma mark - Implementation

@implementation SSShopViewController
{
    UIImageView *navBarHairlineImageView;
    //  UIImageView *splash;
}

@synthesize shopLbl,shopScrollLbl,shopScrollView,shopTableView,shopTopScrollImageView,shopBackgroundScrollView;
@synthesize showMenuBtn;
@synthesize cartItems;


#pragma mark - View States
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    _canRemoveCategoryView=TRUE;
    
    bannerArray = [[NSMutableArray alloc] init];
    sliderArray = [[NSMutableArray alloc] init];
    webservice = [[SSWebService alloc] init];
    isDataLoaded = NO;
    
    NSString *redirectCategoryId    = [[NSUserDefaults standardUserDefaults] valueForKey:@"redirect_categoryid"];
    NSString *redirectCategoryName  = [[NSUserDefaults standardUserDefaults] valueForKey:@"redirect_categoryname"];
    if (redirectCategoryId != NULL) {
        NSString *categoryURL = [NSString stringWithFormat:@"%@?id_category_layered=%@",[webservice getAPIUrl:API_KEY_PRODUCTS],redirectCategoryId];
        [self loadCategoryViewForCategoryLink:categoryURL withTitle:redirectCategoryName];
        
    }
    
    NSString *redirectProductId     = [[NSUserDefaults standardUserDefaults] valueForKey:@"redirect_productid"];
    
    if (redirectProductId != NULL) {
        NSString *productURL = [NSString stringWithFormat:@"%@?id_product=%@&controller=product",[webservice getAPIUrl:API_KEY_PRODUCT_DETAIL],redirectProductId];
        [self gotoProductDetailViewForURL:productURL];
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"redirect_categoryid"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"redirect_categoryname"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"redirect_productid"];
    
}

-(void)loadStoreLoginPage{
    
    SSStoreLoginViewController *storeLoginViewController=nil;
        
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        storeLoginViewController = [[SSStoreLoginViewController alloc]initWithNibName:@"SSStoreLoginViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        storeLoginViewController = [[SSStoreLoginViewController alloc]initWithNibName:@"SSStoreLoginViewController" bundle:[NSBundle mainBundle]];
    }
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:storeLoginViewController];
    
    [self.navigationController presentViewController:navigationController animated:NO completion:nil];
    
}


- (void) gotoProductDetailViewForURL:(NSString *)productURL {
    
    NSMutableDictionary *currentProductItem = [[NSMutableDictionary alloc] init];
    
    [currentProductItem setObject:productURL forKey:@"url"];
    
    SSProductPageViewController *productVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        productVC = [[SSProductPageViewController alloc]initWithNibName:@"SSProductpageViewControlleriPad" bundle:[NSBundle mainBundle]];
        productVC.currentProductItem = currentProductItem;
        productVC.redirectproduct=@"YES";
        [self.navigationController pushViewController:productVC animated:YES];
    }
    else {
        productVC = [[SSProductPageViewController alloc]
                     initWithNibName:@"SSProductPageViewController"
                     bundle:nil];
        productVC.currentProductItem = currentProductItem;
        productVC.redirectproduct=@"YES";
        [self.navigationController pushViewController:productVC animated:YES];
    }
}

#pragma mark-navigation bar creation

- (void)styleNavBar
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    UINavigationBar *newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, screen_width, 64.0)];
    [newNavBar setBackgroundColor:[UIColor clearColor]];
    
    [newNavBar setBarTintColor:[UIColor whiteColor]];
    newNavBar.tag=1001;
    UINavigationItem *newItem = [[UINavigationItem alloc] init];
    newItem.title = @"seidensticker";
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                            NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)]}];
    [newNavBar pushNavigationItem:newItem animated:false];
    navBarHairlineImageView = [self findHairlineImageViewUnder:newNavBar];
    navBarHairlineImageView.hidden = NO;
    if(IS_IPHONE6_Plus) {
        UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,64,screen_width,0.8)];
        [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
        
        [newNavBar addSubview:navBorder];
    }
    UILabel *lblLefttitle = [UILabel new];
    [lblLefttitle setText:@"Catégories"];
    lblLefttitle.textColor = [UIColor orangeColor];
    [lblLefttitle setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [lblLefttitle setTextAlignment:NSTextAlignmentLeft];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMenuBtn:)];
    [lblLefttitle addGestureRecognizer:tap];
    lblLefttitle.userInteractionEnabled = YES;
    [lblLefttitle setBackgroundColor:[UIColor clearColor]];
    [lblLefttitle setFrame:CGRectMake(10,(newNavBar.frame.size.height/2)-5 , newNavBar.frame.size.width/3, 30)];
    [newNavBar addSubview:lblLefttitle];
    
    cartItems = [SHOPPINGCART getCartItems];
    NSInteger quantity = 0;
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"shop_active_icon"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(rightSideBtnAction:)forControlEvents:UIControlEventTouchUpInside];
    
    
    [button setFrame:CGRectMake(0, 0, 38, 38)];
    [button setBackgroundColor:[UIColor clearColor]];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(11, 15, 15,15)];
    [label setFont:[COMMON getResizeableFont:FuturaStd_Book(8)]];
    [label setText:[NSString stringWithFormat:@"%ld",(long)quantity]];
    [label setBackgroundColor:[UIColor clearColor]];
    label.textAlignment = NSTextAlignmentCenter;
    [label setTextColor:[UIColor blackColor]];
    
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(newNavBar.frame.size.width-55,(newNavBar.frame.size.height/2) -10 , 40, 40)];
    [backButtonView setBackgroundColor:[UIColor clearColor]];
    [button addSubview:label];
    [backButtonView addSubview:button];
    
    [newNavBar addSubview:backButtonView];
    //[self setLabel:newNavBar];
    [self.view addSubview:newNavBar];
    
}


#pragma mark - Right Menu

-(void)rightSideBtnAction:(id)sender
{
    SSMonPanierViewController *monpanierViewController;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonpanierViewControlleriPad" bundle:[NSBundle mainBundle]];
        
        
    }
    else {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonPanierViewController" bundle:[NSBundle mainBundle]];
    }
    
    [self.navigationController pushViewController:monpanierViewController animated:YES];
}

#pragma mark-Hide navigation line

- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews)
    {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(isStoreApp){
        if ([[NSUserDefaults standardUserDefaults] boolForKey:IsStoreUserLoggedIn]==NO) {
            [self loadStoreLoginPage];
        }
        
    }
    
    [COMMON TrackerWithName:@"Home View"];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    if (!isDataLoaded) {
        [self setUpInitilization];
        [self setUpConstraints];
        [self loadSliders];
        [self loadBanners];
        isDataLoaded = YES;
    }
    
    if(![self.view viewWithTag:CATEGORYVIEW_TAG]){
        
    }
    else {
        [[self.view viewWithTag:CATEGORYVIEW_TAG ] removeFromSuperview];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self styleNavBar];
   // [self setLabel];
    //[self setButton];
    //[self loadNewNavigationBar];
}
-(void)loadNewNavigationBar{
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setHidden:NO];
    
//   UINavigationBar * navBarNew = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, screen_width, 64.0)];
    UIFont *titleFont = [COMMON getResizeableFont:FuturaStd_Book(14)];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor],NSFontAttributeName:titleFont}];
    self.navigationItem.title = @"seidensticker";
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    NSLog(@"cart values %@",[SHOPPINGCART getCartItems]);
    cartItems = [SHOPPINGCART getCartItems];
//    [lblLefttitle setTextAlignment:NSTextAlignmentLeft];
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMenuBtn:)];
//    [lblLefttitle addGestureRecognizer:tap];
    NSInteger quantity = 0;
    
    
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }
    
    NSString *count = [NSString stringWithFormat:@"%ld",(long)quantity];
    
    

    
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"shop_active_icon"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(rightSideBtnAction:)forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat buttonXPos;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        buttonXPos = 29;
    }
    else{
        buttonXPos = 37;
    }
    [button setFrame:CGRectMake(buttonXPos, 0, 38, 38)];
    [button setBackgroundColor:[UIColor clearColor]];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(11, 15, 15,15)];
    [label setFont:[COMMON getResizeableFont:FuturaStd_Book(8)]];
    [label setText:count];
    [label setBackgroundColor:[UIColor clearColor]];
    label.textAlignment = NSTextAlignmentCenter;
    [label setTextColor:[UIColor blackColor]];
    
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, -2, 65, 38)];
    //backButtonView.bounds = CGRectOffset(backButtonView.bounds, -14, -10);
    [backButtonView setBackgroundColor:[UIColor clearColor]];
    
    [button addSubview:label];
    [backButtonView addSubview:button];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    [self.navigationItem setRightBarButtonItem:barButton];
    
    
    UIButton *leftBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setTitle:@"Catégories" forState:UIControlStateNormal];
    [leftBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(showMenuBtn:)forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setFrame:CGRectMake(0, 0, screen_width/3, 38)];
    [leftBtn setBackgroundColor:[UIColor lightGrayColor]];
    
    UIView *leftButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width/3, 38)];
    [leftButtonView setBackgroundColor:[UIColor grayColor]];
    
    [leftButtonView addSubview:leftBtn];
    UIBarButtonItem *barLeftButton = [[UIBarButtonItem alloc] initWithCustomView:leftButtonView];
    self.navigationItem.leftBarButtonItem = barLeftButton;
    
    
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    [self.navigationController.navigationBar addSubview:navBorder];
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    if(_canRemoveCategoryView && [self.view viewWithTag:CATEGORYVIEW_TAG ])
        [self animateAndHideCategoryView:[self.view viewWithTag:CATEGORYVIEW_TAG ]];
    else if([self.view viewWithTag:CATEGORYVIEW_TAG ])
        [self.view bringSubviewToFront:[self.view viewWithTag:CATEGORYVIEW_TAG ]];
    else{
        
    }
    
    _canRemoveCategoryView=TRUE;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Initialization

-(void)setUpInitilization
{
    
    //    background Scroll View Creation
    self.shopBackgroundScrollView = [[UIScrollView alloc] init];
    self.shopBackgroundScrollView.scrollEnabled=YES;
    [self.shopBackgroundScrollView setDelegate:self];
    [self.shopBackgroundScrollView setClipsToBounds:YES];
    [self.shopBackgroundScrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.shopBackgroundScrollView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.shopBackgroundScrollView];
    
    // Scroll View Creation Dynamic
    self.shopScrollView = [[UIScrollView alloc] init];
    self.shopScrollView.pagingEnabled = YES;
    self.shopScrollView.scrollEnabled=YES;
    self.shopScrollView.showsHorizontalScrollIndicator = NO;
    [self.shopScrollView setDelegate:self];
    [self.shopScrollView setClipsToBounds:NO];
    [self.shopScrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.shopScrollView setBackgroundColor:[UIColor whiteColor]];
    [self.shopBackgroundScrollView addSubview:self.shopScrollView];
    
    //image View Creation Dynamic
    self.shopTopScrollImageView=[[UIImageView alloc] init];
    [self.shopTopScrollImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.shopScrollView addSubview: self.shopTopScrollImageView];
    
    //uilabel for scroll view text
    self.shopScrollLbl = [[UILabel alloc] init];
    self.shopScrollLbl.textAlignment=NSTextAlignmentRight;
    [self.shopScrollLbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.shopScrollView addSubview:self.shopScrollLbl];
    
    //UIlabel Creation Dynamic
    self.shopLbl = [[UILabel alloc] init];
    self.shopLbl.backgroundColor = [UIColor clearColor];
    self.shopLbl.textColor=[UIColor blackColor];
    [self.shopLbl setTextAlignment:NSTextAlignmentCenter];
    [self.shopLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(9)]];
    self.shopLbl.text = @"LIVRAISON OFFERTE POUR TOUTE COMMANDE SANS MINIMUM";
    [self.shopLbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.shopBackgroundScrollView addSubview:self.shopLbl];
    
    //Table View Creation Dynamic
    self.shopTableView    = [[UITableView alloc]init];
    [self.shopTableView setDelegate:self];
    [self.shopTableView setDataSource:self];
    self.shopTableView.separatorStyle = NO;
    self.shopTableView.showsHorizontalScrollIndicator = NO;
    self.shopTableView.showsVerticalScrollIndicator   = NO;
    self.shopTableView.bounces = NO;
    self.shopTableView.backgroundColor = VIEW_BG_COLOR;
    self.shopTableView.scrollEnabled = NO;
    [self.shopTableView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.shopBackgroundScrollView addSubview:self.shopTableView];
    
}


-(void)setUpConstraints
{
    
    NSDictionary *constraintsDictionary = @{@"_shopBackgroundScrollView":self.shopBackgroundScrollView,@"_scrollView":self.shopScrollView,@"_shopTopScrollImageView":self.shopTopScrollImageView,@"_shopScrollLbl":self.shopScrollLbl,@"_shopLbl":self.shopLbl, @"_shopTableView":self.shopTableView};
    
    
    NSDictionary *metricsDict;
    if (IS_IPHONE6) {
        metricsDict = @{@"_scrollView":@"0",@"_shopTopScrollImageView":@"0",@"_shopScrollLbl":@"0",@"_shopLbl":@"27",@"_shopTableView":@"0"};
    }
    else if (IS_IPHONE6_Plus)
    {
        metricsDict = @{@"_scrollView":@"0",@"_shopTopScrollImageView":@"0",@"_shopScrollLbl":@"0",@"_shopLbl":@"35",@"_shopTableView":@"0"};
    }
    else if(IS_IPHONE5 || IS_IPHONE4)
    {
        metricsDict = @{@"_scrollView":@"0",@"_shopTopScrollImageView":@"0",@"_shopScrollLbl":@"0",@"_shopLbl":@"27",@"_shopTableView":@"0"};
    }
    else
    {
        metricsDict = @{@"_scrollView":@"0",@"_shopTopScrollImageView":@"0",@"_shopScrollLbl":@"0",@"_shopLbl":@"195",@"_shopTableView":@"0"};
    }
    
    
    //Background scroll view constraints
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[_shopBackgroundScrollView]-(0)-|"
                                                                      options:0
                                                                      metrics:metricsDict
                                                                        views:constraintsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(65)-[_shopBackgroundScrollView]-(0)-|"
                                                                      options:0
                                                                      metrics:metricsDict
                                                                        views:constraintsDictionary]];
    
    //scroll view constraints
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(_scrollView)-[_scrollView]-(_scrollView)-|"
                                                                      options:0
                                                                      metrics:metricsDict
                                                                        views:constraintsDictionary]];
    
    //scroll lbl constraints
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(_shopScrollLbl)-[_shopScrollLbl]-(_shopScrollLbl)-|"
                                                                      options:0
                                                                      metrics:metricsDict
                                                                        views:constraintsDictionary]];
    
    //shop label constraints
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(_shopLbl)-[_shopLbl]-(_shopLbl)-|"
                                                                      options:0
                                                                      metrics:metricsDict
                                                                        views:constraintsDictionary]];
    
    
    
    //shop Table view Constraints
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(_shopTableView)-[_shopTableView]-(_shopTableView)-|"
                                                                      options:0
                                                                      metrics:metricsDict
                                                                        views:constraintsDictionary]];
    
    // constraints for Vertical
    
    if(IS_IPHONE6_Plus)
    {
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[_scrollView(275)]-(185)-[_shopScrollLbl(20)]-82-[_shopLbl(20)]-(1)-[_shopTableView(>=600)]-(0)-|"
                                   
                                                                          options:0
                                                                          metrics:metricsDict
                                                                            views:constraintsDictionary]];
        
    }
    
    else if (IS_IPHONE6)
    {
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[_scrollView(250)]-(165)-[_shopScrollLbl(20)]-72-[_shopLbl(20)]-(1)-[_shopTableView(>=600)]-(0)-|"
                                                                          options:0
                                                                          metrics:metricsDict
                                                                            views:constraintsDictionary]];
        
    }
    
    else if(IS_IPHONE5)
    {
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[_scrollView(200)]-(135)-[_shopScrollLbl(20)]-52-[_shopLbl(20)]-(1)-[_shopTableView(>=600)]-(0)-|"
                                                                          options:0
                                                                          metrics:metricsDict
                                                                            views:constraintsDictionary]];
        
    }
    
    else if (IS_IPHONE4)
    {
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(2)-[_scrollView(170)]-(115)-[_shopScrollLbl(20)]-42-[_shopLbl(20)]-(1)-[_shopTableView(>=600)]-(0)-|"
                                   
                                                                          options:0
                                                                          metrics:metricsDict
                                                                            views:constraintsDictionary]];
        
    }
    else
    {
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(2)-[_scrollView(467)]-(335)-[_shopScrollLbl(20)]-127-[_shopLbl(20)]-(1)-[_shopTableView(>=1000)]-(0)-|"
                                                                          options:0
                                                                          metrics:metricsDict
                                                                            views:constraintsDictionary]];
        
    }
    
    [self.shopScrollView addConstraint:[NSLayoutConstraint constraintWithItem:self.shopTopScrollImageView
                                                                    attribute:NSLayoutAttributeTop
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:self.shopScrollView
                                                                    attribute:NSLayoutAttributeTop
                                                                   multiplier:1
                                                                     constant:0]];
    
    [self.shopScrollView addConstraint:[NSLayoutConstraint constraintWithItem:self.shopTopScrollImageView
                                                                    attribute:NSLayoutAttributeHeight
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:self.shopScrollView
                                                                    attribute:NSLayoutAttributeHeight
                                                                   multiplier:1
                                                                     constant:0]];
    
    
    [self.shopScrollView addConstraint:[NSLayoutConstraint constraintWithItem:self.shopTopScrollImageView
                                                                    attribute:NSLayoutAttributeLeading
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:self.shopScrollView
                                                                    attribute:NSLayoutAttributeLeading
                                                                   multiplier:1
                                                                     constant:0]];
    
    int width;
    
    if (IS_IPHONE6 )
        width = 374;
    else if (IS_IPHONE6_Plus)
        width = 414;
    else if (IS_IPHONE5 || IS_IPHONE4)
        width = 320;
    else
        width=782;
    
    [self.shopScrollView addConstraint:[NSLayoutConstraint constraintWithItem:self.shopTopScrollImageView
                                                                    attribute:NSLayoutAttributeWidth
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:nil
                                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                                   multiplier:1
                                                                     constant:width]];
    
}


#pragma mark - Load API With Images

-(void)loadAPIWithImages:(NSMutableDictionary *)resultData{
    
    bannerArray   = [resultData mutableCopy];
    
    menuArray    = [[NSMutableArray alloc]init];
    
    cellCount    = 0;
    
    NSString *imageType;
    
    NSString *menuSizeType;
    for(int i=0;i<[bannerArray count];i++) {
        
        NSMutableDictionary *imageDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"",@"Big",@"",@"SmallLeft",@"",@"SmallRight", nil];
        imageType = [[bannerArray objectAtIndex:i] valueForKey:@"image_type"];
        
        if ([imageType isEqualToString:@"big"])
        {
            
            [imageDictionary setValue:[bannerArray objectAtIndex:i] forKey:@"Big"];
            
            [menuArray addObject:imageDictionary];
            
            cellCount++;
            
        }
        else if([imageType isEqualToString:@"small"]) {
            
            if (i<[bannerArray count]) {
                
                [imageDictionary setValue:[bannerArray objectAtIndex:i] forKey:@"SmallLeft"];
                
                cellCount++;
                
                if (i != [bannerArray count]-1) {
                    
                    menuSizeType = [[bannerArray objectAtIndex:i+1] valueForKey:@"image_type"];
                    if ([menuSizeType isEqualToString:@"small"]) {
                        
                        [imageDictionary setValue:[bannerArray objectAtIndex:i+1] forKey:@"SmallRight"];
                        
                        i++;
                    }
                }
                [menuArray addObject:imageDictionary];
                NSLog(@"%@",menuArray);
            }
        }
    }
    [shopTableView reloadData];
    [self updateScrollViewContentSize];
}

- (void) loadAPIScrollImages:(NSMutableArray *)sliderImageArr
{
    
    if ([sliderImageArr count] > 0)
    {
        sliderArray = sliderImageArr;
        for (int k=0; k<sliderImageArr.count; k++)
            
        {
            AsyncImage *asyncImage = [[AsyncImage alloc]init];
            
            NSLog(@"Image = %@",[[sliderImageArr objectAtIndex:k] valueForKey:@"image"]);
            [asyncImage setLoadingImage];
            [asyncImage loadImageFromURL:[NSURL URLWithString:[[sliderImageArr objectAtIndex:k] valueForKey:@"image"]] type:AsyncImageResizeTypeCrop isCache:YES];
            [shopTopScrollImageView addSubview:asyncImage];
            [asyncImage setTranslatesAutoresizingMaskIntoConstraints:NO];
            NSDictionary *imageDictionary = @{@"_asyncImageview":asyncImage};
            
            [shopTopScrollImageView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_asyncImageview]|"
                                                                                           options:0
                                                                                           metrics:nil
                                                                                             views:imageDictionary]];
            
            [shopTopScrollImageView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_asyncImageview]|"
                                                                                           options:0
                                                                                           metrics:nil
                                                                                             views:imageDictionary]];
            
            [self setTitle:[NSString stringWithFormat:@"%@",[[sliderImageArr objectAtIndex:k]objectForKey:@"layer_caption"]]
                  forLabel:shopScrollLbl];
            
            UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
            imageButton.frame = CGRectMake(shopTopScrollImageView.frame.origin.x, shopTopScrollImageView.frame.origin.y, shopTopScrollImageView.frame.size.width, shopTopScrollImageView.frame.size.height);
            imageButton.adjustsImageWhenHighlighted = NO;
            [imageButton setTag:k];
            [imageButton addTarget:self action:@selector(gotoCategoryViewFromSlider:) forControlEvents:UIControlEventTouchUpInside];
            [shopTopScrollImageView.superview addSubview:imageButton];
        }
    }
}

- (void) loadCategoryViewForCategoryLink:(NSString *) categoryLink withTitle:(NSString *) categoryTitle {
    [COMMON TrackerWithName:[NSString stringWithFormat:@"Home View - %@",categoryTitle]];
    SSCategoryViewController *categoryVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        categoryVC = [[SSCategoryViewController alloc]initWithNibName:@"SSCategoryViewControlleriPad" bundle:[NSBundle mainBundle]];
        categoryVC.currentCategoryTitle = categoryTitle;
        categoryVC.currentCategoryURL   = categoryLink;
        [self.navigationController pushViewController:categoryVC animated:YES];
    }
    else {
        categoryVC = [[SSCategoryViewController alloc]
                      initWithNibName:@"SSCategoryViewController"
                      bundle:nil];
        
        categoryVC.currentCategoryTitle = categoryTitle;
        categoryVC.currentCategoryURL   = categoryLink;
        [self.navigationController pushViewController:categoryVC animated:YES];
    }
}

- (void) gotoCategoryViewFromSlider:(id)sender {
    UIButton *button = (UIButton *)sender;
    int currentIndex = (int)[button tag];
    [self loadCategoryViewForCategoryLink:[[sliderArray objectAtIndex:currentIndex] objectForKey:@"link"]
                                withTitle:[[sliderArray objectAtIndex:currentIndex] objectForKey:@"layer_caption"]];
    
}

#pragma mark - Label Property

- (void) setTitle:(NSString *) labelText forLabel:(UILabel *) currentLabel
{
    
    labelText = [COMMON getDecodedString:labelText];
    [currentLabel setText           :[NSString stringWithFormat:@"  %@  ",[labelText uppercaseString]]];
    [currentLabel setBackgroundColor:[[UIColor whiteColor]colorWithAlphaComponent:0.8f]];
    [currentLabel setTextColor      :[UIColor blackColor]];
    [currentLabel setTextAlignment  :NSTextAlignmentLeft];
    [currentLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(10)]];
    
    CGRect rect = currentLabel.frame;
    rect.size = [currentLabel.text sizeWithAttributes:@{NSFontAttributeName :
                                                            [UIFont fontWithName:currentLabel.font.fontName
                                                                            size:currentLabel.font.pointSize]}];
    rect.size.height = 20;
    currentLabel.frame = rect;
    
    if([labelText isEqualToString:@""]){
        [currentLabel setBackgroundColor:[[UIColor clearColor]colorWithAlphaComponent:0.8f]];
    }
    
}

#pragma mark - TableView Delegates & Methods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return cellCount;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPHONE6_Plus ||IS_IPHONE6 ||IS_IPHONE5 ||IS_IPHONE4)
    {
        return 195;
    }
    else
    {
        return 400;
    }
}

- (void) gotoCategoryView:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    int currentIndex = (int)[button tag];
    
    SSCategoryViewController *categoryVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        categoryVC = [[SSCategoryViewController alloc]initWithNibName:@"SSCategoryViewControlleriPad" bundle:[NSBundle mainBundle]];
        categoryVC.currentCategoryItem = [bannerArray objectAtIndex:currentIndex];
        
        [self.navigationController pushViewController:categoryVC animated:YES];
    }
    else {
        categoryVC = [[SSCategoryViewController alloc]
                      initWithNibName:@"SSCategoryViewController"
                      bundle:nil];
        
        categoryVC.currentCategoryItem = [bannerArray objectAtIndex:currentIndex];
        [self.navigationController pushViewController:categoryVC animated:YES];
    }
    
}

- (SSShopViewCustomCell *) getCustomCell:(SSShopViewCustomCell *)currentCell
                         withCurrentItem:(NSMutableDictionary *)selectedItem
                                withType:(NSString *)currentType {
    
    UIImageView *Image;
    
    UILabel *Label;
    
    int currentIndex = [self getBannerIndexForObject:selectedItem];
    
    if([currentType isEqualToString:@"SmallLeft"]){
        
        Image = currentCell.smallImageLeft;
        
        Label = currentCell.SmallImageLeftLabel;
        
    }
    else if([currentType isEqualToString:@"SmallRight"])
    {
        
        Image = currentCell.smallImageRight;
        
        Label = currentCell.SmallImageRightLabel;
        NSLog(@"Current Image= %@",currentCell.smallImageRight);
        
    }
    else
    {
        Image = currentCell.LargeImage;
        
        Label = currentCell.BigImageLabel;
        
    }
    
    //[self setTitle:[NSString stringWithFormat:@"%@",Label]];
    
    UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    CGSize buttonSize = CGSizeMake(Image.frame.size.width, Image.frame.size.height);
    imageButton.frame = CGRectMake(Image.frame.origin.x, Image.frame.origin.y, buttonSize.width, buttonSize.height);
    //imageButton.backgroundColor=[UIColor redColor];
    imageButton.adjustsImageWhenHighlighted = NO;
    [imageButton setTag:currentIndex];
    [imageButton addTarget:self action:@selector(gotoCategoryView:) forControlEvents:UIControlEventTouchUpInside];
    [Image.superview addSubview:imageButton];
    
    AsyncImage *asyncImage = [[AsyncImage alloc]init];
    [asyncImage loadImageFromURL:[NSURL URLWithString:[selectedItem valueForKey:@"image"]] type:AsyncImageResizeTypeCrop isCache:YES];
    [Image addSubview:asyncImage];
    [asyncImage setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSDictionary *imageDictionary = @{@"_asyncImageview":asyncImage};
    [Image addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_asyncImageview]|"
                                                                  options:0
                                                                  metrics:nil
                                                                    views:imageDictionary]];
    
    [Image addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_asyncImageview]|"
                                                                  options:0
                                                                  metrics:nil
                                                                    views:imageDictionary]];
    
    [self setTitle:[selectedItem valueForKey:@"html"] forLabel:Label];
    
    return currentCell;
    
}

- (int) getBannerIndexForObject:(NSMutableDictionary *)currentObject {
    for (int i=0; i<[bannerArray count];i++) {
        if ([[[bannerArray objectAtIndex:i] valueForKey:@"html"] isEqualToString:[currentObject valueForKey:@"html"]]) {
            return i;
        }
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"ShopCustomCell";
    SSShopViewCustomCell *cell  = (SSShopViewCustomCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"SSShopViewCustomCell" owner:self options:nil];
        }
        else
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"SSShopViewCustomCelliPad" owner:self options:nil];
        }
        cell         = [nib objectAtIndex:0];
    }
    
    
    NSMutableDictionary *bannerDictionary = [[menuArray objectAtIndex:indexPath.row] copy];
    
    if([[bannerDictionary valueForKey:@"SmallLeft"] isKindOfClass:[NSDictionary class]]) {
        
        cell = [self getCustomCell:cell
                   withCurrentItem:[bannerDictionary valueForKey:@"SmallLeft"]
                          withType:@"SmallLeft"];
        
        if ([[bannerDictionary valueForKey:@"SmallRight"] isKindOfClass:[NSDictionary class]]) {
            
            cell = [self getCustomCell:cell
                       withCurrentItem:[bannerDictionary valueForKey:@"SmallRight"]
                              withType:@"SmallRight"];
            
        }
        cell.selectionStyle  = UITableViewCellSeparatorStyleNone;
    }
    else if ([[bannerDictionary valueForKey:@"Big"] isKindOfClass:[NSDictionary class]]) {
        
        cell = [self getCustomCell:cell
                   withCurrentItem:[bannerDictionary valueForKey:@"Big"]
                          withType:@"Big"];
        
    }
    return cell;
}


#pragma mark - Left Menu
-(void)showMenuBtn:(id)sender
{
    
    SSMenuCustomView *categoryView = [SSMenuCustomView loadView];
    NSDictionary *menuData = [webservice getLocalMenuData];
    categoryView.menuCategories = [[menuData objectForKey:@"topmenu"] objectForKey:@"main_menu"];
    categoryView.tag=CATEGORYVIEW_TAG;
    
    categoryView.itemSelected=^(id itemsForSelectedCategory){
        
        if (itemsForSelectedCategory != NULL && [itemsForSelectedCategory isKindOfClass:[NSArray class]]) {
            
            SSMenuDetailViewController *categoryDetailView=[[SSMenuDetailViewController alloc]
                                                            initWithNibName:@"SSMenuDetailViewController"
                                                            bundle:nil];
            
            categoryDetailView.menuTitleString = categoryView.menuTitle;
            
            [COMMON TrackerWithName:[NSString stringWithFormat:@"Category - %@",categoryDetailView.menuTitleString]];
            
            
            _canRemoveCategoryView=FALSE;
            categoryDetailView.items=itemsForSelectedCategory;
            categoryDetailView.catSelected=^(id selectedCategory) {
                SSCategoryViewController *categoryVC = [[SSCategoryViewController alloc]
                                                        initWithNibName:@"SSCategoryViewController"
                                                        bundle:nil];
                [COMMON TrackerWithName:[NSString stringWithFormat:@"Category - %@",[selectedCategory objectForKey:@"title"]]];
                
                
                categoryVC.currentCategoryTitle = [selectedCategory objectForKey:@"title"];
                categoryVC.currentCategoryURL   = [selectedCategory objectForKey:@"url"];
                [self.navigationController pushViewController:categoryVC animated:YES];
            };
            
            [self.navigationController pushViewController:categoryDetailView animated:YES];
        }
    };
    
    categoryView.catSelected=^(id selectedCategory) {
        SSCategoryViewController *categoryVC = [[SSCategoryViewController alloc]
                                                initWithNibName:@"SSCategoryViewController"
                                                bundle:nil];
        [COMMON TrackerWithName:[NSString stringWithFormat:@"Category - %@",[selectedCategory objectForKey:@"title"]]];
        
        categoryVC.currentCategoryTitle = [selectedCategory objectForKey:@"title"];
        categoryVC.currentCategoryURL   = [selectedCategory objectForKey:@"url"];
        [self.navigationController pushViewController:categoryVC animated:YES];
        
    };
    
    __weak typeof(SSMenuCustomView) *weakView= categoryView;
    categoryView.didcancel=^(){
        [self animateAndHideCategoryView:weakView];
    };
    
    
    [self.view addSubview:categoryView];
    
    categoryView.frame = CGRectMake(-self.view.bounds.size.width, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    [UIView animateWithDuration:0.45f delay:0 options:UIViewAnimationOptionTransitionNone animations:^{
        categoryView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        
    }];
    
}


#pragma mark - Data Methods

- (void) loadSliders {
    
    [webservice getSliderData:API_KEY_SLIDER success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject objectForKey:DATA_KEY_SLIDER] !=NULL)
        {
            [self loadAPIScrollImages:[responseObject objectForKey:DATA_KEY_SLIDER]];
        }
        else
        {
            
        }
    } failure:^(AFHTTPRequestOperation *operation, id error) {
        
        
    }];
}

- (void) loadBanners {
    
    [webservice getBannersData:API_KEY_BANNERS success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"Shop Response = %@",responseObject);
        
        if ([responseObject objectForKey:DATA_KEY_BANNERS] != NULL) {
            
            [self loadAPIWithImages:[responseObject objectForKey:DATA_KEY_BANNERS]];
            
        } else
        {
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, id error) {
        
    }];
}

#pragma mark - Update ScrollView Content Size

- (void) updateScrollViewContentSize {
    
    if(IS_IPHONE4 ||IS_IPHONE5 ||IS_IPHONE6 )
        
    {
        self.shopTableView.contentSize = CGSizeMake(self.view.frame.size.width, cellCount * 175);
        self.shopBackgroundScrollView.contentSize = CGSizeMake(self.view.frame.size.width,
                                                               self.shopScrollView.frame.size.height +
                                                               self.shopLbl.frame.size.height +
                                                               (cellCount * 175) + 42);
    }
    else if(IS_IPHONE6_Plus)
    {
        self.shopTableView.contentSize = CGSizeMake(self.view.frame.size.width, cellCount * 178);
        self.shopBackgroundScrollView.contentSize = CGSizeMake(self.view.frame.size.width,
                                                               self.shopScrollView.frame.size.height +
                                                               self.shopLbl.frame.size.height +
                                                               (cellCount * 178) + 42);
    }
    else{
        self.shopTableView.contentSize = CGSizeMake(self.view.frame.size.width, cellCount * 392);
        self.shopBackgroundScrollView.contentSize = CGSizeMake(self.view.frame.size.width,
                                                               self.shopScrollView.frame.size.height +
                                                               self.shopLbl.frame.size.height +
                                                               (cellCount * 392) + 12);
        
    }
    
}

#pragma mark - Animation Hide Work

-(void)animateAndHideCategoryView:(UIView *)categoryView{
    [UIView animateWithDuration:0.45f delay:0 options:UIViewAnimationOptionTransitionNone animations:^{
        categoryView.frame = CGRectMake(-self.view.bounds.size.width, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    } completion:^(BOOL finished) {
        [categoryView removeFromSuperview];
    }];
}

-(void)redirectCategorymethod:(NSString *) redirectCategoryId withTitle:(NSString *)redirectCategorName
{
    NSString *CategoryId    = [[NSUserDefaults standardUserDefaults] valueForKey:@"redirect_categoryid"];
    NSString *redirectCategoryName  = [[NSUserDefaults standardUserDefaults] valueForKey:@"redirect_categoryname"];
    if (CategoryId != NULL) {
        NSString *categoryURL = [NSString stringWithFormat:@"%@?id_category_layered=%@",[webservice getAPIUrl:API_KEY_PRODUCTS],CategoryId];
        [self loadCategoryViewForCategoryLink:categoryURL withTitle:redirectCategoryName];
        
    }
    
}

-(void)redirectProduct :(NSString *) redirectproductID
{
    NSString *redirectProductId     = [[NSUserDefaults standardUserDefaults] valueForKey:@"redirect_productid"];
    
    if (redirectProductId != NULL) {
        NSString *productURL = [NSString stringWithFormat:@"%@?id_product=%@&controller=product",[webservice getAPIUrl:API_KEY_PRODUCT_DETAIL],redirectProductId];
        [self gotoProductDetailViewForURL:productURL];
    }
    
}

@end

