//
//  SSShopViewCustomCell.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/21/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSShopViewCustomCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *LargeImage;

@property (strong, nonatomic) IBOutlet UIImageView *smallImageLeft;

@property (strong, nonatomic) IBOutlet UIImageView *smallImageRight;

@property (strong, nonatomic) IBOutlet UILabel *SmallImageLeftLabel;

@property (strong, nonatomic) IBOutlet UILabel *SmallImageRightLabel;

@property (strong, nonatomic) IBOutlet UILabel *BigImageLabel;

@property(nonatomic,strong) IBOutlet UIView *divideView;


@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *newsFeedImage;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;




@end
