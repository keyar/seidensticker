//
//  SSShopViewCustomCell.m
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/21/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSShopViewCustomCell.h"
#import "SSAppCommon.h"
#import "SSConfig.h"
@implementation SSShopViewCustomCell
@synthesize LargeImage,smallImageLeft,smallImageRight,SmallImageLeftLabel,SmallImageRightLabel,BigImageLabel;
- (void)awakeFromNib
{
    // Initialization code
    [self setupConstraints];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(void) setupConstraints{
   
    // Height constraint
   
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.smallImageLeft
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeHeight
                                                        multiplier:1.05
                                                          constant:0]];
            // Center horizontally
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.smallImageLeft
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:0.5
                                                      constant:0.0]];
    
    
    // Center vertically
  
   [self addConstraint:[NSLayoutConstraint constraintWithItem:self.smallImageLeft
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.05
                                                          constant:0.0]];
   
    if(IS_IPHONE6 || IS_IPHONE6_Plus )
    {
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.smallImageLeft
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:0.488
                                                          constant:0]];
        
    }
    
    else {
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.smallImageLeft
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:0.5
                                                          constant:0]];
        
    }
    
    
    //smallImageRight
    
    // Width constraint
    
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.smallImageRight
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:0.51
                                                          constant:0]];

    // Height constraint
   
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.smallImageRight
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeHeight
                                                        multiplier:1.05
                                                          constant:0]];
    
    // Center horizontally
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.smallImageRight
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.5
                                                      constant:0.0]];
    
    
    // Center vertically
 
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.smallImageRight
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.05
                                                          constant:0.0]];
   
  
    
    
    // Center vertically for Label
    
     if(IS_IPHONE6_Plus||IS_IPHONE6|| IS_IPHONE5 || IS_IPHONE4 ){
         
         [self addConstraint:[NSLayoutConstraint constraintWithItem:self.SmallImageRightLabel
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:0.44
                                                           constant:0]];
         
         [self addConstraint:[NSLayoutConstraint constraintWithItem:self.SmallImageRightLabel
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.43
                                                           constant:0.0]];
    }
    // constraints for large image
    
    if (IS_IPHONE6_Plus) {
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.LargeImage
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:0.99
                                                          constant:0]];
    }
    
}

- (void)setFrame:(CGRect)frame {
    frame.origin.y += 5;
    frame.size.height -=5 *1.5;
    [super setFrame:frame];
  
}


@end
