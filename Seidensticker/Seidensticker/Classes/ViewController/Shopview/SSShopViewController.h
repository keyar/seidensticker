//
//  SSShopViewController.h
//  Seidensticker
//
//  Created by Nandha Kumar on 19/09/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSShopViewCustomCell.h"
@interface SSShopViewController : UIViewController
{
    //IBOutlet UINavigationBar  *newNavBar;

}

@property(nonatomic,retain)IBOutlet UITableView *shopTableView;
@property(nonatomic,retain)IBOutlet  UIScrollView *shopScrollView;
@property(nonatomic,retain)IBOutlet  UIScrollView *shopBackgroundScrollView;
@property(nonatomic,retain)IBOutlet UILabel *shopLbl,*shopScrollLbl;;
@property(nonatomic,weak) IBOutlet UIButton *showMenuBtn;
@property(nonatomic,retain)IBOutlet UIImageView *shopTopScrollImageView;
@property(nonatomic,assign)BOOL canRemoveCategoryView;
@property(strong,nonatomic) NSMutableArray *cartItems;


- (void) loadAPIScrollImages:(NSArray *)sliderImageArr;
- (void) loadAPIWithImages:(NSMutableDictionary *)resultData;
- (SSShopViewCustomCell *) getCustomCell:(SSShopViewCustomCell *)currentCell
                         withCurrentItem:(NSMutableDictionary *)selectedItem
                                withType:(NSString *)currentType;

- (void) loadSliders;
- (void) loadBanners;
- (void) gotoCategoryView:(id)sender;
-(void)redirectCategorymethod:(NSString *) redirectCategoryId withTitle:(NSString *)redirectCategorName;
- (void) loadCategoryViewForCategoryLink:(NSString *) categoryLink withTitle:(NSString *) categoryTitle;
-(void)redirectProduct :(NSString *) redirectproductID;
@end
