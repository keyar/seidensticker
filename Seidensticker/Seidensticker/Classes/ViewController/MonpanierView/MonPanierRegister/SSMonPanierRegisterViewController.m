//
//  SSMonPanierRegisterViewController.m
//  Seidensticker
//
//  Created by OCS iOS Developer on 06/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSMonPanierRegisterViewController.h"
#import "SSMonPanierLoginViewController.h"
#import "SSMonPanierViewController.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSCustomNavigation.h"
#import "SSMonPanierAddressViewController.h"
#import "SSMonPanierRegisterViewCustomCell.h"
#import "SSMonPanierAddressModifierViewController.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#import "NSString+validations.h"
#import "SSAppDelegate.h"
#import "SSShoppingCart.h"
#import "IQUIView+IQKeyboardToolbar.h"
#import "IQKeyboardManager.h"

#pragma mark - interface
@interface SSMonPanierRegisterViewController ()<BSKeyboardControlsDelegate,UITextFieldDelegate ,UITableViewDelegate,UITableViewDataSource>
{
    BOOL                    iskeyboard;
    //Keyboard return elements
    UIBarButtonItem         *doneBarItem;
    UIToolbar               *keyboardToolbar;
    UIBarButtonItem         *spaceBarItem;
    UIBarButtonItem         *previousBarItem;
    UIBarButtonItem         *nextBarItem;
    NSInteger               URLTag;
    //UIToolbar
    UIToolbar               *pickerToolbar;
    UIToolbar               *pickerViewToolbar;
    UIDatePicker            *MonthPicker;
    UIDatePicker            *DatePicker;
    UIDatePicker            *YearPicker;
    NSInteger count;
    
    NSString                *dobText;
    SSWebService            *webService;
    NSDictionary            *registerDetails;
    
    NSString    *deviceUDID;
    NSString    *deviceToken;
    NSArray     *jsonArray;
    CGFloat     tableHeight;
    
}
@property (strong, nonatomic) IBOutlet UIButton *suivantBtn;
//@property (strong, nonatomic) UITableView *tableView;
//@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end
#pragma mark - implementation
@implementation SSMonPanierRegisterViewController
@synthesize emailAddressToRegister;
@synthesize isPushedFromMonInform;
@synthesize isPushFromAccountPage;
@synthesize isCommanderClicked;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // [self hideTabbar];
    webService = [[SSWebService alloc] init];
    if(IS_IPHONE6_Plus){
        UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,screen_width,0.8)];
        [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
        [self.navigationController.navigationBar addSubview:navBorder];
    }
    
    
    [[IQKeyboardManager sharedManager] considerToolbarPreviousNextInViewClass:[self.tableView class]];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:YES];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    registerDetails = [[NSDictionary alloc] init];
    [super viewWillAppear:animated];
    deviceUDID =[[NSUserDefaults standardUserDefaults]valueForKey:ID_DEVICE];
    deviceToken=[[NSUserDefaults standardUserDefaults]valueForKey:DEVICETOKEN];
    
    [COMMON TrackerWithName:@"Register View"];
    int cellHeight;
    
    if(IS_IPHONE6_Plus||IS_IPHONE6 ||IS_IPHONE5)
    {
        cellHeight = 66;
    }
    else if(IS_IPHONE4)
    {
        cellHeight = 50;
    }
    else{
        cellHeight = 100;
    }
    
    int cellCount = 0;
    if(isPushedFromMonInform==YES){
        cellCount=8;
    }
    else{
        cellCount=6;
    }
    
    tableHeight = ((cellCount *cellHeight)+cellHeight);
    [self loadNavigation];
    registerDetails = [COMMON getUserDetails];
    [_tableView setScrollEnabled:NO];
    [_tableView reloadData];
    
    
//    [_tableView setFrame:CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, screen_width, tableHeight)];
    
    
    
    //_tableView.contentSize = CGSizeMake(_tableView.frame.size.width, tableHeight);
    
    [_tableView setScrollEnabled:NO];
    [_scrollView setScrollEnabled:YES];
   // [_scrollView setContentSize:CGSizeMake(0,tableHeight)];
    
    //_scrollView.frame.size.width,tableHeight
}
-(void)viewDidLayoutSubviews{
    //if(isPushedFromMonInform==YES){
    //}
    _tableHeightConstraint.constant = _tableView.contentSize.height;
    [self.view layoutIfNeeded];
}
- (void)viewDidAppear:(BOOL)animated
{
    [self datePickerToolBar];
    [self loadDatePicker];
    [self loadArray];
}
-(void)loadArray{
    jsonArray= [NSArray new];
    jsonArray = @[ @{@"headertitle":@"Vos informations personnelles",
                    @"fields"     :@[@{@"title":@"FirstName",@"placeholder":@""
                                       },
                                     @{@"title" :@"LastName",@"placeholder":@""
                                       },
                                     @{@"title" :@"Email",@"placeholder":@""
                                       },
                                     @{@"title" :@"Email",@"placeholder":@""
                                           }]
                    },
                  @{@"headertitle":@"Date de naissance",
                    @"fields"     :@[@{@"title":@[@{@"Datetitle":@"Date"
                                                    },
                                                  @{@"Datetitle":@"Month"
                                                    },
                                                  @{@"Datetitle":@"Year"
                                                    }]
                                       }]
                    
                    },
                  @{@"headertitle":@"",
                    @"fields"     :@[@{@"title":@"Mot de passe actue..."
                                       },
                                     @{@"title":@"Nouveau mot de passe..."
                                       },
                                     @{@"title":@"Confirmation..."
                                       }]
                    
                    }];
    
    NSLog(@"jsonArray%@-->",jsonArray);

}
#pragma mark - loadNavigation
- (void)loadNavigation
{
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController.navigationBar setTranslucent:NO];
    SSCustomNavigation *navigation = [[SSCustomNavigation alloc] initWithNibName:@"SSCustomNavigation" bundle:nil];
    navigation.view.tag = 1001;
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    
    [navigation.view addSubview:navBorder];
    [self.navigationController.navigationBar addSubview:navigation.view];
    [navigation setLeftHidden:NO];
    [navigation setTitleHidden:NO];
    [navigation setShopBtnHidden:NO];
    [navigation setShopLabelHidden:NO];
    [navigation setCatergoryShopBtnLabelHidden:YES];

    [navigation.titleLbl setText:NSLocalizedString(@"S'inscrire", @"")];
    [navigation.titleLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    [navigation.navigationLeftLbl setText:NSLocalizedString(@"Retour", @"")];
    [navigation.navigationLeftLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [navigation.navigationLeftBtn addTarget:self
                                     action:@selector(registeRetour:)
                           forControlEvents:UIControlEventTouchUpInside];
    
    [self setLabel:navigation];
   // [_tableView removeFromSuperview];
   // [_scrollView removeFromSuperview];
    //_tableView = [[UITableView alloc] init];
   // _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    
    //_tableView.translatesAutoresizingMaskIntoConstraints = YES;
   
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor clearColor];
    [_tableView setScrollEnabled:NO];
    _tableView.bounces = NO;
    //[_tableView setShowsHorizontalScrollIndicator:NO];
    //[_tableView setShowsVerticalScrollIndicator:NO];
    //_scrollView = [[UIScrollView alloc] init];
    //_scrollView.translatesAutoresizingMaskIntoConstraints = YES;
     _scrollView.backgroundColor = [UIColor clearColor];
    
    [_scrollView setScrollEnabled:YES];
    [_tableView setScrollEnabled:NO];
    _scrollView.bounces = NO;
    
    CGRect scrollViewFrame = _scrollView.frame;
    scrollViewFrame.origin.x =10;
    scrollViewFrame.size.width = screen_width-40;
    [_scrollView setFrame:scrollViewFrame];
    
    CGRect tableFrame = _tableView.frame;
    tableFrame.origin.x =0;
    tableFrame.size.width = _scrollView.frame.size.width;
    [_tableView setFrame:tableFrame];
    
   // [self.view addSubview:_scrollView];
    //[_scrollView addSubview:_tableView];
    
    // [_scrollView setFrame:CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height-(_suivantBtn.frame.size.height*4))];
    // [_tableView setFrame:CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, _scrollView.frame.size.height)];
    
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(hideKeyBoard)];
    
    [self.view addGestureRecognizer:tapGesture];
    //[self setupConstraints];
   
    
    
    
}
-(void)setLabel:(SSCustomNavigation *)navigation
{
    NSLog(@"cart values %@",[SHOPPINGCART getCartItems]);
    NSMutableArray * cartItems =[NSMutableArray new];
    cartItems = [SHOPPINGCART getCartItems];
    
    NSInteger quantity = 0;
    
    
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }
    
    navigation.navigationShopLbl.text = [NSString stringWithFormat:@"%ld",(long)quantity];
    [navigation.navigationShopLbl setBackgroundColor:[UIColor clearColor]];
    navigation.navigationShopLbl.textColor = [UIColor blackColor];
    [navigation.navigationShopLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(9)]];
    [navigation.navigationShopLbl setTextAlignment:NSTextAlignmentCenter];

    [navigation.navigationShopBtn addTarget:self action:@selector(cartRegisterBtnAction:) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark - Right Menu

-(void)cartRegisterBtnAction:(id)sender
{
    SSMonPanierViewController *monpanierViewController;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonpanierViewControlleriPad" bundle:[NSBundle mainBundle]];
        
        
    }
    else {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonPanierViewController" bundle:[NSBundle mainBundle]];
    }
    
    [self.navigationController pushViewController:monpanierViewController animated:YES];
}

- (NSString *) getEmail {
    NSString *emailAddress;
    if(isPushedFromMonInform!=YES){
        if (emailAddressToRegister == NULL) {
            emailAddressToRegister = @"";
        }
        emailAddress = emailAddressToRegister;
        NSLog(@"email = %@",emailAddress);
        
    }
    else{
        emailAddress = [registerDetails valueForKey:@"email"];
        if (emailAddress == NULL||emailAddress==nil||[emailAddress isEqualToString:@""]) {
            emailAddress = @"";
        }
    }
    return emailAddress;
}
- (NSString *) getFirstName {
    NSString *firstName;
    firstName = [registerDetails valueForKey:@"firstname"];
    if (firstName == NULL||firstName==nil||[firstName isEqualToString:@""]) {
            firstName = @"";
    }
   
    return firstName;
}
- (NSString *) getLastName {
    NSString *lastName;
    lastName = [registerDetails valueForKey:@"lastname"];
    if (lastName == NULL||lastName==nil||[lastName isEqualToString:@""]) {
        lastName = @"";
    }
    
    return lastName;
}

- (NSString *) getDayField {
    NSString *days;
    days = [registerDetails valueForKey:@"days"];
    if (days == NULL||days==nil||[days isEqualToString:@""]) {
        days = @"";
    }
    
    return days;
}

- (NSString *) getMonthField {
    NSString *months;
    months = [registerDetails valueForKey:@"months"];
    if (months == NULL||months==nil||[months isEqualToString:@""]) {
        months = @"";
    }
    
    return months;
}

- (NSString *) getYearField {
    NSString *years;
    years = [registerDetails valueForKey:@"years"];
    if (years == NULL||years==nil||[years isEqualToString:@""]) {
        years = @"";
    }
    
    return years;
}


#pragma mark - hideKeyBoard
-(void)hideKeyBoard {
    [dateField resignFirstResponder];
    [monthField resignFirstResponder];
    [yearField resignFirstResponder];
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //hides keyboard when another part of layout was touched
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - datePickerToolBar
-(void)datePickerToolBar
{
    pickerToolbar                    = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 325, 320, 38)];
    pickerToolbar.barStyle           = UIBarStyleBlackTranslucent;
    
    NSMutableArray *barItems         = [[NSMutableArray alloc] init];
    UIBarButtonItem *spaceBarItems     =  [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem  *doneBarItems     =  [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Valider", @"")
                                                                           style:UIBarButtonItemStyleDone
                                                                          target:self
                                                                          action:@selector(ValiderDateBtnAction)];
    [doneBarItems setTitleTextAttributes:@{
                                           NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(16)],
                                           NSForegroundColorAttributeName:[UIColor colorWithRed:255.0f/255.0f green:126.0f/255.0f blue:0.0f/255.0f alpha:1.0f]
                                           } forState:UIControlStateNormal];
    [barItems      addObject:spaceBarItems];
    [barItems      addObject:doneBarItems];
    [pickerToolbar setItems:barItems animated:YES];
    pickerToolbar.autoresizingMask   = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
    [monthField  setInputAccessoryView:pickerToolbar];
    [dateField setInputAccessoryView:pickerToolbar];
    [yearField setInputAccessoryView:pickerToolbar];
    [MonthPicker addSubview:pickerToolbar];
    [DatePicker addSubview:pickerToolbar];
    [YearPicker addSubview:pickerToolbar];
    
}
#pragma mark - ValiderDateBtnAction
-(void)ValiderDateBtnAction
{
    if (dateField.tag==1) {
        NSDateFormatter *MonthFormat= [[NSDateFormatter alloc] init];
        [MonthFormat setDateFormat:@"MM"];
        monthField.text = [NSString stringWithFormat:@"%@",
                           [MonthFormat stringFromDate:MonthPicker.date]];
        
        NSDateFormatter *DateFormat= [[NSDateFormatter alloc] init];
        [DateFormat setDateFormat:@"dd"];
        dateField.text = [NSString stringWithFormat:@"%@",
                          [DateFormat stringFromDate:MonthPicker.date]];
        NSDateFormatter *YearFormat= [[NSDateFormatter alloc] init];
        [YearFormat setDateFormat:@"yyyy"];
        yearField.text = [NSString stringWithFormat:@"%@",
                          [YearFormat stringFromDate:MonthPicker.date]];
    }
    else{
    }
    dobText=[NSString stringWithFormat:@"%@-%@-%@",yearField.text,monthField.text,dateField.text];
    [monthField resignFirstResponder];
    [dateField resignFirstResponder];
    [yearField resignFirstResponder];
}
-(void)CancelBtnAction
{
    monthField.text = @"";
    dateField.text = @"";
    monthField.text = @"";
    
}
- (void)LabelChange:(id)sender
{
    NSDateFormatter *MonthFormat= [[NSDateFormatter alloc] init];
    [MonthFormat setDateFormat:@"MM"];
    monthField.text = [NSString stringWithFormat:@"%@",
                       [MonthFormat stringFromDate:MonthPicker.date]];
    
    NSDateFormatter *DateFormat= [[NSDateFormatter alloc] init];
    [DateFormat setDateFormat:@"dd"];
    dateField.text = [NSString stringWithFormat:@"%@",
                      [DateFormat stringFromDate:MonthPicker.date]];
    
    NSDateFormatter *yearFormat= [[NSDateFormatter alloc] init];
    [yearFormat setDateFormat:@"yyyy"];
    yearField.text = [NSString stringWithFormat:@"%@",
                      [yearFormat stringFromDate:MonthPicker.date]];
}
- (void)LabelChange1:(id)sender
{
    NSDateFormatter *MonthFormat= [[NSDateFormatter alloc] init];
    [MonthFormat setDateFormat:@"MM"];
    monthField.text = [NSString stringWithFormat:@"%@",
                       [MonthFormat stringFromDate:DatePicker.date]];
    
    NSDateFormatter *DateFormat= [[NSDateFormatter alloc] init];
    [DateFormat setDateFormat:@"dd"];
    dateField.text = [NSString stringWithFormat:@"%@",
                      [DateFormat stringFromDate:DatePicker.date]];
    
    NSDateFormatter *yearFormat= [[NSDateFormatter alloc] init];
    [yearFormat setDateFormat:@"yyyy"];
    yearField.text = [NSString stringWithFormat:@"%@",
                      [yearFormat stringFromDate:DatePicker.date]];
}
- (void)LabelChange2:(id)sender
{
    NSDateFormatter *MonthFormat= [[NSDateFormatter alloc] init];
    [MonthFormat setDateFormat:@"MM"];
    monthField.text = [NSString stringWithFormat:@"%@",
                       [MonthFormat stringFromDate:YearPicker.date]];
    
    NSDateFormatter *DateFormat= [[NSDateFormatter alloc] init];
    [DateFormat setDateFormat:@"dd"];
    dateField.text = [NSString stringWithFormat:@"%@",
                      [DateFormat stringFromDate:YearPicker.date]];
    
    NSDateFormatter *yearFormat= [[NSDateFormatter alloc] init];
    [yearFormat setDateFormat:@"yyyy"];
    yearField.text = [NSString stringWithFormat:@"%@",
                      [yearFormat stringFromDate:YearPicker.date]];
}
#pragma mark - loadDatePicker
- (void)loadDatePicker
{
    //Month
    MonthPicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
    //    [MonthPicker setLocale:[NSLocale currentLocale]];
    MonthPicker.datePickerMode = UIDatePickerModeDate;
    [MonthPicker setBackgroundColor:[UIColor whiteColor]];
    [MonthPicker addTarget:self
                    action:@selector(LabelChange:)
          forControlEvents:UIControlEventValueChanged];
    monthField.inputView   =  MonthPicker;
    [self datePickerToolBar];
    
    //date
    DatePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
    //    [DatePicker setLocale:[NSLocale currentLocale]];
    DatePicker.datePickerMode = UIDatePickerModeDate;
    [DatePicker setBackgroundColor:[UIColor whiteColor]];
    [DatePicker addTarget:self
                   action:@selector(LabelChange:)
         forControlEvents:UIControlEventValueChanged];
    dateField.inputView  =  MonthPicker;
    [self datePickerToolBar];
    
    //Year
    YearPicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
    //    [YearPicker setLocale:[NSLocale currentLocale]];
    YearPicker.datePickerMode = UIDatePickerModeDate;
    [YearPicker setBackgroundColor:[UIColor whiteColor]];
    [YearPicker addTarget:self
                   action:@selector(LabelChange:)
         forControlEvents:UIControlEventValueChanged];
    yearField.inputView = MonthPicker;
    [self datePickerToolBar];
}
- (void)animateView:(NSUInteger)tag
{
    //[self checkBarButton:tag];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView commitAnimations];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark- UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(IS_IPHONE6_Plus||IS_IPHONE6 ||IS_IPHONE5)
    {
        return 66;
    }
    else if(IS_IPHONE4)
    {
        return 50;
    }
    else{
        return 100;
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(isPushedFromMonInform!=YES){
        if(section==0){
            return 4;
        }
        if(section==1){
            return 1;
        }
        return 1;
       
    }
    else{
        if(section==0){
            return 3;
        }
        if(section==1){
           return 4;
        }
         return 1;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(IS_IPHONE4){
        return 25;
    }
    else if(IS_IPHONE5){
        return 30;
    }
    else {
        return 45;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    int Height = 0 ;
    if(IS_IPHONE4){
         Height = 25;
    }
    else if(IS_IPHONE5){
         Height = 30;
    }
    else {
         Height = 45;
    }
    UIView  * headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, Height)];
    NSString *sectionName;
    if(section==0){
        sectionName = NSLocalizedString(@"Vos informations personnelles", @"Vos informations personnelles");
    }
    else {
        sectionName = NSLocalizedString(@"Date de naissance", @"Date de naissance");
    }

    
    UILabel *bottomBorderLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, Height)];
    bottomBorderLabel.backgroundColor = [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:0.5];
    bottomBorderLabel.text= sectionName;
    [bottomBorderLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
       [bottomBorderLabel setTextColor:[UIColor blackColor]];
    [headerView addSubview:bottomBorderLabel];
    
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    SSMonPanierRegisterViewCustomCell *cell = (SSMonPanierRegisterViewCustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[SSMonPanierRegisterViewCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.registerTextField.delegate = self;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
   
    
    if(indexPath.section == 0){
        if (indexPath.row == 0){
            [cell.registerTextField setKeyboardType:UIKeyboardTypeDefault];
            cell.registerTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            cell.registerTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
            [cell.registerTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
            cell.registerTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                            initWithString:@"*Prénom..."
                                                            attributes:@{
                                                                         NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                         }];
            cell.registerTextField.tag = 101;
            if(isPushedFromMonInform==YES){
                cell.registerTextField.text = [self getFirstName];
            }
            else{
                if(strFirstName==nil){
                    strFirstName=@"";
                }
                cell.registerTextField.text = strFirstName;
            }
             [self addBorderToCell:cell];
        }
        if (indexPath.row == 1){
            [cell.registerTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
            cell.registerTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            cell.registerTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
            cell.registerTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                            initWithString:@"*Nom..."
                                                            attributes:@{
                                                                         NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                         }];
            cell.registerTextField.tag = 102;
            if(isPushedFromMonInform==YES){
                cell.registerTextField.text = [self getLastName];
            }
            else{
                if(strLastName==nil){
                    strLastName=@"";
                }
                cell.registerTextField.text = strLastName;
            }
             [self addBorderToCell:cell];
        }
        if (indexPath.row == 2){
            [cell.registerTextField setKeyboardType:UIKeyboardTypeEmailAddress];
            cell.registerTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            [cell.registerTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
            //[cell.registerTextField setEnabled:NO];
            cell.registerTextField.text = [self getEmail];
            cell.registerTextField.tag = 103;
             [self addBorderToCell:cell];
        }
        if(isPushedFromMonInform!=YES){
            if (indexPath.row == 3)
            {
                cell.registerTextField.secureTextEntry =YES;
                cell.registerTextField.autocorrectionType = UITextAutocorrectionTypeNo;
                cell.registerTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
                [cell.registerTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
                cell.registerTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                                initWithString:@"Mot de passe..."
                                                                attributes:@{
                                                                             NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                             }];
                cell.registerTextField.tag = 104;
                if(strRegisterPassword==nil){
                    strRegisterPassword=@"";
                }
                cell.registerTextField.text = strRegisterPassword;
                 [self addBorderToCell:cell];
            }
   
        }
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.backgroundColor =[UIColor clearColor];
        cell.backgroundColor =[UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        [cell.layer setBorderColor: [[UIColor colorWithRed:151.0f/255.0f
//                                                     green:151.0f/255.0f
//                                                      blue:151.0f/255.0f
//                                                     alpha:1.0f] CGColor]];
        //[cell.layer setBorderWidth: 1.0];
        //[self addBorderToCell:cell];
       [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    else
    {
        if(indexPath.section == 1) {
             if(isPushedFromMonInform!=YES){
                 if (indexPath.row == 0){
                     if(IS_IPHONE5){
                         dateField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 75, 51)];
                     }
                     else if(IS_IPHONE4){
                         dateField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 75, 40)];
                     }
                     
                     else {
                         dateField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 85, 51)];
                     }
                     
                     dateField.attributedPlaceholder = [[NSAttributedString alloc]
                                                        initWithString:@"Jour"
                                                        attributes:@{
                                                                     NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                     }];
                     UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 0)];
                     dateField.leftView = paddingView;
                     dateField.leftViewMode = UITextFieldViewModeAlways;
                     dateField.background = [UIImage imageNamed:@"dropdown.png"];
                     dateField.layer.masksToBounds=YES;
                     //dateField.layer.borderColor=[[UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f] CGColor];
                     //dateField.layer.borderWidth= 1.0f;
                     [self addBorderToDOBTextField:dateField];
                     if(isPushedFromMonInform==YES){
                         dateField.text = [self getDayField];
                     }
                     [cell.contentView addSubview:dateField];
                     if(IS_IPHONE5){
                         monthField = [[UITextField alloc] initWithFrame:CGRectMake(97, 0, 88, 51)];
                     }
                     else if(IS_IPHONE4){
                         monthField = [[UITextField alloc] initWithFrame:CGRectMake(97, 0, 88, 40)];
                     }
                     
                     else{
                         monthField = [[UITextField alloc] initWithFrame:CGRectMake(110, 0, 110, 51)];
                     }
                     monthField.attributedPlaceholder = [[NSAttributedString alloc]
                                                         initWithString:@"Mois"
                                                         attributes:@{
                                                                      NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                      }];
                     UIView *monthPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 0)];
                     monthField.leftView = monthPadding;
                     monthField.leftViewMode = UITextFieldViewModeAlways;
                     monthField.background = [UIImage imageNamed:@"dropdown.png"];
                     monthField.layer.masksToBounds=YES;
                     //monthField.layer.borderColor=[[UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f] CGColor];
                     //monthField.layer.borderWidth= 1.0f;
                     [self addBorderToDOBTextField:monthField];
                     if(isPushedFromMonInform==YES){
                         monthField.text = [self getMonthField];
                     }
                     [cell.contentView addSubview:monthField];
                     
                     if(IS_IPHONE5){
                         yearField = [[UITextField alloc] initWithFrame:CGRectMake(207, 0, 75, 51)];
                     }
                     else if(IS_IPHONE4){
                         yearField = [[UITextField alloc] initWithFrame:CGRectMake(207, 0, 75, 40)];
                     }
                     else if (IS_IPHONE6){
                         yearField = [[UITextField alloc] initWithFrame:CGRectMake(240, 0, 85, 51)];//85
                     }
                     else{
                         yearField = [[UITextField alloc] initWithFrame:CGRectMake(245, 0, 92, 51)];//85
                     }
                     yearField.attributedPlaceholder = [[NSAttributedString alloc]
                                                        initWithString:@"Année"
                                                        attributes:@{
                                                                     NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                     }];
                     UIView *yearPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 12, 0)];
                     yearField.leftView = yearPadding;
                     yearField.leftViewMode = UITextFieldViewModeAlways;
                     yearField.background = [UIImage imageNamed:@"dropdown.png"];
                     yearField.layer.masksToBounds=YES;
                     //yearField.layer.borderColor=[[UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f] CGColor];
                     // yearField.layer.borderWidth= 1.0f;
                     [self addBorderToDOBTextField:yearField];
                     if(isPushedFromMonInform==YES){
                         yearField.text = [self getYearField];
                     }
                     [cell.contentView addSubview:yearField];
                     
                     cell.backgroundColor =[UIColor clearColor];
                 }
             }
             else{
                 if (indexPath.row == 0){
                     if(IS_IPHONE5){
                         dateField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 75, 51)];
                     }
                     else if(IS_IPHONE4){
                         dateField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 75, 40)];
                     }
                     
                     else {
                         dateField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 85, 51)];
                     }
                     
                     dateField.attributedPlaceholder = [[NSAttributedString alloc]
                                                        initWithString:@"Jour"
                                                        attributes:@{
                                                                     NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                     }];
                     UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 0)];
                     dateField.leftView = paddingView;
                     dateField.leftViewMode = UITextFieldViewModeAlways;
                     dateField.background = [UIImage imageNamed:@"dropdown.png"];
                     dateField.layer.masksToBounds=YES;
                     //dateField.layer.borderColor=[[UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f] CGColor];
                     //dateField.layer.borderWidth= 1.0f;
                     [self addBorderToDOBTextField:dateField];
                     if(isPushedFromMonInform==YES){
                         dateField.text = [self getDayField];
                     }
                     [cell.contentView addSubview:dateField];
                     if(IS_IPHONE5){
                         monthField = [[UITextField alloc] initWithFrame:CGRectMake(97, 0, 88, 51)];
                     }
                     else if(IS_IPHONE4){
                         monthField = [[UITextField alloc] initWithFrame:CGRectMake(97, 0, 88, 40)];
                     }
                     
                     else{
                         monthField = [[UITextField alloc] initWithFrame:CGRectMake(110, 0, 110, 51)];
                     }
                     monthField.attributedPlaceholder = [[NSAttributedString alloc]
                                                         initWithString:@"Mois"
                                                         attributes:@{
                                                                      NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                      }];
                     UIView *monthPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 0)];
                     monthField.leftView = monthPadding;
                     monthField.leftViewMode = UITextFieldViewModeAlways;
                     monthField.background = [UIImage imageNamed:@"dropdown.png"];
                     monthField.layer.masksToBounds=YES;
                     //monthField.layer.borderColor=[[UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f] CGColor];
                     //monthField.layer.borderWidth= 1.0f;
                     [self addBorderToDOBTextField:monthField];
                     if(isPushedFromMonInform==YES){
                         monthField.text = [self getMonthField];
                     }
                     [cell.contentView addSubview:monthField];
                     
                     if(IS_IPHONE5){
                         yearField = [[UITextField alloc] initWithFrame:CGRectMake(207, 0, 75, 51)];
                     }
                     else if(IS_IPHONE4){
                         yearField = [[UITextField alloc] initWithFrame:CGRectMake(207, 0, 75, 40)];
                     }
                     else if (IS_IPHONE6){
                         yearField = [[UITextField alloc] initWithFrame:CGRectMake(240, 0, 85, 51)];//85
                     }
                     else{
                         yearField = [[UITextField alloc] initWithFrame:CGRectMake(245, 0, 92, 51)];//85
                     }
                     yearField.attributedPlaceholder = [[NSAttributedString alloc]
                                                        initWithString:@"Année"
                                                        attributes:@{
                                                                     NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                     }];
                     UIView *yearPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 12, 0)];
                     yearField.leftView = yearPadding;
                     yearField.leftViewMode = UITextFieldViewModeAlways;
                     yearField.background = [UIImage imageNamed:@"dropdown.png"];
                     yearField.layer.masksToBounds=YES;
                     //yearField.layer.borderColor=[[UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f] CGColor];
                     // yearField.layer.borderWidth= 1.0f;
                     [self addBorderToDOBTextField:yearField];
                     if(isPushedFromMonInform==YES){
                         yearField.text = [self getYearField];
                     }
                     [cell.contentView addSubview:yearField];
                     
                     cell.backgroundColor =[UIColor clearColor];
                 }

                     if (indexPath.row == 1)
                     {
                         cell.registerTextField.tag=201;
                         cell.registerTextField.secureTextEntry =YES;
                         cell.registerTextField.autocorrectionType = UITextAutocorrectionTypeNo;
                         cell.registerTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
                         [cell.registerTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
                         cell.registerTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                                         initWithString:@"Mot de passe actue..."
                                                                         attributes:@{
                                                                                      NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                                      }];
                         if(strPassword==nil){
                             strPassword=@"";
                         }
                         cell.registerTextField.text = strPassword;
                         [self addBorderToCell:cell];
                         
                     }
                     if (indexPath.row == 2)
                     {   cell.registerTextField.tag=202;
                         cell.registerTextField.secureTextEntry =YES;
                         cell.registerTextField.autocorrectionType = UITextAutocorrectionTypeNo;
                         cell.registerTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
                         [cell.registerTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
                         cell.registerTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                                         initWithString:@"Nouveau mot de passe..."
                                                                         attributes:@{
                                                                                      NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                                      }];
                         if(strNewPassword==nil){
                             strNewPassword=@"";
                         }
                         cell.registerTextField.text = strNewPassword;
                         [self addBorderToCell:cell];
                     }
                     
                     if (indexPath.row == 3)
                     {
                         cell.registerTextField.tag=203;
                         cell.registerTextField.secureTextEntry =YES;
                         cell.registerTextField.autocorrectionType = UITextAutocorrectionTypeNo;
                         cell.registerTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
                         [cell.registerTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
                         cell.registerTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                                         initWithString:@"Confirmation..."
                                                                         attributes:@{
                                                                                      NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                                      }];
                         if(strConfirmPassword==nil){
                             strConfirmPassword=@"";
                         }
                         cell.registerTextField.text = strConfirmPassword;
                         [self addBorderToCell:cell];
                     }
                 
             }
            
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    
    
}
#pragma mark - addBorderToCell
-(void)addBorderToCell:(UITableViewCell*)cell{
    UIView *leftBorder;
    UIView *rightBorder;
    UIView *topBorder;
    UIView *bottomBorder;
    leftBorder = [[UIView alloc] initWithFrame:CGRectMake(1, 0, 1.5, cell.frame.size.height)];
    rightBorder = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.size.width-2.0f, 0, 1.5, cell.frame.size.height)];
    topBorder = [[UIView alloc] initWithFrame:CGRectMake(1, 0,cell.frame.size.width,1.5)];
    bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height-2.0f, cell.frame.size.width, 1.5)];
    [cell addSubview:leftBorder];
    [cell addSubview:topBorder];
    [cell addSubview:rightBorder];
    [cell addSubview:bottomBorder];
    [leftBorder setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin];
    [rightBorder setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin];
    [topBorder setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
    [bottomBorder setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
    leftBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
    rightBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
    topBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
    bottomBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];

}
#pragma mark - addBorderToDOBTextField
-(void)addBorderToDOBTextField:(UITextField *)currentTextField{
    UIView *leftBorder;
    UIView *rightBorder;
    UIView *topBorder;
    UIView *bottomBorder;
    leftBorder = [[UIView alloc] initWithFrame:CGRectMake(1, 0, 1.5, currentTextField.frame.size.height)];
    rightBorder = [[UIView alloc] initWithFrame:CGRectMake(currentTextField.frame.size.width-2.0f, 0, 1.5, currentTextField.frame.size.height)];
    topBorder = [[UIView alloc] initWithFrame:CGRectMake(1, 0,currentTextField.frame.size.width,1.5)];
    bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, currentTextField.frame.size.height-2.0f, currentTextField.frame.size.width, 1.5)];
    [currentTextField addSubview:leftBorder];
    [currentTextField addSubview:topBorder];
    [currentTextField addSubview:rightBorder];
    [currentTextField addSubview:bottomBorder];
    [leftBorder setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin];
    [rightBorder setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin];
    [topBorder setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
    [bottomBorder setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
    leftBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
    rightBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
    topBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
    bottomBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];

}

#pragma mark-show and hide TabBar Method

- (void) showTabBar {
    SSAppDelegate *appDelegate = (SSAppDelegate *) [UIApplication sharedApplication].delegate;
    [appDelegate showTabBar];
}

- (void) hideTabbar {
    SSAppDelegate *appDelegate = (SSAppDelegate *) [UIApplication sharedApplication].delegate;
    [appDelegate hideTabBar];
}


#pragma mark - Button Actions
-(void)registeRetour:(id)sender
{
    //    [self.navigationController popViewControllerAnimated:YES];
    //    [self.navigationController setNavigationBarHidden:YES];
    
    //NSArray *array = [self.navigationController viewControllers];
    
    [self.navigationController popViewControllerAnimated:YES];
    [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
    //[self.navigationController popToViewController:[array objectAtIndex:0] animated:YES];
    //[self.navigationController setNavigationBarHidden:YES];
    
   // [self.navigationController popViewControllerAnimated:YES];
    if(isPushFromAccountPage==YES){
        [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
        [self.navigationController setNavigationBarHidden:NO];
    }
    else{
        [self.navigationController setNavigationBarHidden:YES];
    }
    
}
#pragma mark - gotoAddressView
- (void) gotoAddressView{
    
    SSMonPanierAddressModifierViewController *monPanierAddressModifierVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierAddressModifierVC = [[SSMonPanierAddressModifierViewController alloc]initWithNibName:@"SSMonPanierAddressModifierViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierAddressModifierVC = [[SSMonPanierAddressModifierViewController alloc]
                                      initWithNibName:@"SSMonPanierAddressModifierViewController"
                                      bundle:nil];
    }
    if(isCommanderClicked==YES){
        monPanierAddressModifierVC.isCommanderClicked=YES;
    }
    else{
        monPanierAddressModifierVC.isCommanderClicked=NO;
    }
    monPanierAddressModifierVC.isPushedFromMonAddresses=NO;
    monPanierAddressModifierVC.isPushedFromRegisterPage=YES;
    [self.navigationController pushViewController:monPanierAddressModifierVC animated:YES];
    [COMMON removeLoadingIcon];
}
#pragma mark - registerAPI
-(void) registerAPI{
    [COMMON showLoadingIcon:self];
    [webService createUserWithEmailAddress:strEmail
                                  password:strPassword
                                 firstName:strFirstName
                                  lastName:strLastName
                           dateOfBirthYear:Year
                          dateOfBirthMonth:Month
                            dateOfBirthDay:Day
                               deviceToken:deviceToken
                                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                       if ([responseObject objectForKey:@"register"] != NULL) {
                                           
                                           
                                           [COMMON storeUserDetails:[responseObject objectForKey:@"register"]];
                                           [COMMON removeLoadingIcon];
                                           [self gotoAddressView];
                                       } else if([responseObject objectForKey:@"error"] != NULL){
                                           [SSAppCommon showSimpleAlertWithMessage:[responseObject objectForKey:@"error"]];
                                       }
                                       [COMMON removeLoadingIcon];
                                       
                                   }
                                   failure:^(AFHTTPRequestOperation *operation, id error) {
                                       [COMMON removeLoadingIcon];
                                   }];
}
-(void)userProfileUpdateAPI{
     [COMMON showLoadingIcon:self];
    [webService UserUpdationWithSecureKey:[COMMON getCustomerSecureKey]
                             emailAddress:strEmail
                                firstName:strFirstName
                                 lastName:strLastName
                          dateOfBirthYear:Year
                         dateOfBirthMonth:Month
                           dateOfBirthDay:Day
                               oldPasword:strPassword
                              newPassword:strNewPassword
                          confirmPassword:strConfirmPassword
                                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                      NSLog(@"RESPONSE-->%@",responseObject);
                                      if([responseObject valueForKey:@"error"]){
                                          [SSAppCommon showSimpleAlertWithMessage:[responseObject valueForKey:@"error"]];
                                      }
                                      else{
                                          
                                          [COMMON storeUserDetails:[responseObject objectForKey:@"editprofile"]];
                                          [self.navigationController popViewControllerAnimated:YES];
                                          
                                          [self.navigationController setNavigationBarHidden:YES];
                                      }
                                       [COMMON removeLoadingIcon];
                                  } failure:^(AFHTTPRequestOperation *operation, id error) {
                                      [SSAppCommon showSimpleAlertWithMessage:@"Error"];
                                      NSLog(@"ERROR--->%@",error);
                                       [COMMON removeLoadingIcon];
                                  }];
}
#pragma mark - gotoRegister
- (IBAction)gotoRegister:(id)sender {
    if(isPushedFromMonInform!=YES){
        [self getRegistration];
    }
    else{
        [self getUpdation];
    }
}
-(void)textFieldDidEndEditing:(UITextField*)textField
{
    id textFieldSuper = textField;
    
    while (![textFieldSuper isKindOfClass:[UITableViewCell class]]) {
        
        textFieldSuper = [textFieldSuper superview];
        
    }
    NSIndexPath *indexPath ;
    
    SSMonPanierRegisterViewCustomCell *cell;
    
    indexPath = [self.tableView indexPathForCell:(UITableViewCell *)textFieldSuper];
    
    cell = (SSMonPanierRegisterViewCustomCell *) [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section]];
    
    if(cell.registerTextField.text==nil){
        cell.registerTextField.text =@"";
    }
    if(cell.registerTextField.tag ==101){
        strFirstName= cell.registerTextField.text;
        NSLog(@"registerDetails%@-->",registerDetails);
    }
    if(cell.registerTextField.tag ==102){
        strLastName= cell.registerTextField.text;
    }
    if(cell.registerTextField.tag ==103){
        strEmail= cell.registerTextField.text;
    }
   
    if(cell.registerTextField.tag ==201){
         strPassword= cell.registerTextField.text;
    }
    if(cell.registerTextField.tag ==202){
         strNewPassword= cell.registerTextField.text;
    
    }
    if(cell.registerTextField.tag ==202){
         strConfirmPassword= cell.registerTextField.text;

    }
    if(cell.registerTextField.tag == 104){
        strRegisterPassword = cell.registerTextField.text;
        //[self getRegistration];
    }
    if(strPassword!=nil){
        //[self getUpdation];
    }
//    [_tableView setScrollEnabled:NO];
//    [_scrollView setScrollEnabled:YES];
//    [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width,tableHeight)];

}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    id textFieldSuper = textField;
    
    while (![textFieldSuper isKindOfClass:[UITableViewCell class]]) {
        
        textFieldSuper = [textFieldSuper superview];
        
    }
    NSIndexPath *indexPath ;
    
    SSMonPanierRegisterViewCustomCell *cell;
    
    indexPath = [self.tableView indexPathForCell:(UITableViewCell *)textFieldSuper];
    
    cell = (SSMonPanierRegisterViewCustomCell *) [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section]];
    
    if(cell.registerTextField.tag ==201){
         strPassword= cell.registerTextField.text;
        
    }
    if(cell.registerTextField.tag ==202){
         strNewPassword= cell.registerTextField.text;
    }
    if(cell.registerTextField.tag ==202){
         strConfirmPassword= cell.registerTextField.text;
    }
    NSLog(@"strPassword-->  %@  %@  %@",strPassword,strNewPassword,strConfirmPassword);
    return YES;
}

-(void)getUpdation{
    firstNameCell  =(SSMonPanierRegisterViewCustomCell *)[_tableView visibleCells][0];
    lastNameCell   =(SSMonPanierRegisterViewCustomCell *)[_tableView visibleCells][1];
    emailCell      =(SSMonPanierRegisterViewCustomCell *)[_tableView visibleCells][2];
    
    //oldPasswordCell   =(SSMonPanierRegisterViewCustomCell *)[_tableView visibleCells][4];
   // newPasswordCell   =(SSMonPanierRegisterViewCustomCell *)[_tableView visibleCells][5];
    //confirmPasswordCell   =(SSMonPanierRegisterViewCustomCell *)[_tableView visibleCells][6];
    
    strFirstName   = firstNameCell.registerTextField.text;
    strLastName    = lastNameCell.registerTextField.text;
    strEmail       = emailCell.registerTextField.text;
    
    if(strPassword==nil){
        strPassword=@"";
    }
    if(strNewPassword==nil){
        strNewPassword=@"";
    }
    if(strConfirmPassword==nil){
        strConfirmPassword=@"";
    }
    NSLog(@"strPassword-->  %@  %@  %@",strPassword,strNewPassword,strConfirmPassword);
    
    Year           = yearField.text;
    Month          = monthField.text;
    Day            = dateField.text;
    
   // [self userProfileUpdateAPI];
    if(![NSString isEmpty:strEmail] && ![NSString isEmpty:strPassword] && ![NSString isEmpty:strFirstName] && ![NSString isEmpty:strLastName] )
    {
        if(![NSString isEmpty:strNewPassword]){
            
            if(![strPassword isEqualToString:strNewPassword]){
                [SSAppCommon showSimpleAlertWithMessage:PASSWORD_DOESNOT_MATCH];
                [COMMON removeLoadingIcon];
                return;
            }
            else{
                if(![strNewPassword isEqualToString:strConfirmPassword]){
                    [SSAppCommon showSimpleAlertWithMessage:PASSWORD_DOESNOT_MATCH];
                    [COMMON removeLoadingIcon];
                    return;
                }
                else{
                    //[SSAppCommon showSimpleAlertWithMessage:@"userProfileUpdateAPI"];
                    [self userProfileUpdateAPI];
                }
            }
            
        }
        else{
            //[SSAppCommon showSimpleAlertWithMessage:@"userProfileUpdateAPI"];
            [self userProfileUpdateAPI];
        }
        
    }
    else{
        
        [self validateFields];
        [COMMON removeLoadingIcon];
        return;
    }
    
    
}
-(void)getRegistration{
    firstNameCell  =(SSMonPanierRegisterViewCustomCell *)[_tableView visibleCells][0];
    lastNameCell   =(SSMonPanierRegisterViewCustomCell *)[_tableView visibleCells][1];
    emailCell      =(SSMonPanierRegisterViewCustomCell *)[_tableView visibleCells][2];
    passwordCell   =(SSMonPanierRegisterViewCustomCell *)[_tableView visibleCells][3];
    
    strEmail       = emailCell.registerTextField.text;
    strPassword    = passwordCell.registerTextField.text;
    strFirstName   = firstNameCell.registerTextField.text;
    strLastName    = lastNameCell.registerTextField.text;
    Year           = yearField.text;
    Month          = monthField.text;
    Day            = dateField.text;
    
    
    //&& ![NSString isEmpty:Year]&& ![NSString isEmpty:Month] && ![NSString isEmpty:Day] DOB Validation removed
    
    if(![NSString isEmpty:strEmail] && ![NSString isEmpty:strPassword] && ![NSString isEmpty:strFirstName] && ![NSString isEmpty:strLastName] )
    {
        [self registerAPI];
    }
    
    else{
        
        if([NSString isEmpty:strEmail] && ![NSString isEmpty:strPassword] && ![NSString isEmpty:strFirstName] && ![NSString isEmpty:strLastName]){
            [SSAppCommon showSimpleAlertWithMessage:INVALID_EMAIL_ID];
            [COMMON removeLoadingIcon];
            return;
        }
        if(![NSString isEmpty:strEmail] && [NSString isEmpty:strPassword] && ![NSString isEmpty:strFirstName] && ![NSString isEmpty:strLastName]){
            [SSAppCommon showSimpleAlertWithMessage:INVALID_PASSWORD];
            [COMMON removeLoadingIcon];
            return;
        }
        if(![NSString isEmpty:strEmail] && ![NSString isEmpty:strPassword] && [NSString isEmpty:strFirstName] && ![NSString isEmpty:strLastName]){
            [SSAppCommon showSimpleAlertWithMessage:FILL_FIRSTNAME];
            [COMMON removeLoadingIcon];
            return;
        }
        if(![NSString isEmpty:strEmail] && ![NSString isEmpty:strPassword] && ![NSString isEmpty:strFirstName] && [NSString isEmpty:strLastName]){
            [SSAppCommon showSimpleAlertWithMessage:FILL_SURNAME];
            [COMMON removeLoadingIcon];
            return;
        }

        // DOB Validation removed
        //        if(![NSString isEmpty:strEmail] && ![NSString isEmpty:strPassword] && ![NSString isEmpty:strFirstName] && ![NSString isEmpty:strLastName]  && [NSString isEmpty:Year]&& [NSString isEmpty:Month] && [NSString isEmpty:Day] ){
        //            [SSAppCommon showSimpleAlertWithMessage:INVALID_BIRTHDATE];
        //            [COMMON removeLoadingIcon];
        //            return;
        //        }
        [SSAppCommon showSimpleAlertWithMessage:INVALID_EMAIL_ID];
        [COMMON removeLoadingIcon];
        return;
    }

}
-(void)validateFields{
    if([NSString isEmpty:strEmail] && ![NSString isEmpty:strPassword] && ![NSString isEmpty:strFirstName] && ![NSString isEmpty:strLastName]){
        [SSAppCommon showSimpleAlertWithMessage:INVALID_EMAIL_ID];
        [COMMON removeLoadingIcon];
        return;
    }
    if(![NSString isEmpty:strEmail] && [NSString isEmpty:strPassword] && ![NSString isEmpty:strFirstName] && ![NSString isEmpty:strLastName]){
        [SSAppCommon showSimpleAlertWithMessage:INVALID_PASSWORD];
        [COMMON removeLoadingIcon];
        return;
    }
    if(![NSString isEmpty:strEmail] && ![NSString isEmpty:strPassword] && [NSString isEmpty:strFirstName] && ![NSString isEmpty:strLastName]){
        [SSAppCommon showSimpleAlertWithMessage:FILL_FIRSTNAME];
        [COMMON removeLoadingIcon];
        return;
    }
    if(![NSString isEmpty:strEmail] && ![NSString isEmpty:strPassword] && ![NSString isEmpty:strFirstName] && [NSString isEmpty:strLastName]){
        [SSAppCommon showSimpleAlertWithMessage:FILL_SURNAME];
        [COMMON removeLoadingIcon];
        return;
    }

}
@end
