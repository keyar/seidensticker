//
//  SSMonPanierRegisterViewCustomCell.m
//  Seidensticker
//
//  Created by OCS iOS Developer on 07/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSMonPanierRegisterViewCustomCell.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSCustomNavigation.h"
#pragma mark- implementation
@implementation SSMonPanierRegisterViewCustomCell
- (void)awakeFromNib {
    // Initialization code
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
#pragma mark- UITableViewCellStyle
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.registerTextField = [[UITextField alloc] init];
        self.registerTextField.translatesAutoresizingMaskIntoConstraints = NO;
        self.registerTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        [self.registerTextField setFont:[COMMON getResizeableFont:FuturaStd_Book(15)]];
        [self.registerTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
        [self.registerTextField setKeyboardAppearance:UIKeyboardAppearanceLight];
        [self.contentView addSubview:self.registerTextField];
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.registerTextField
                                         attribute:NSLayoutAttributeHeight
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeHeight
                                         multiplier:1.0
                                         constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.registerTextField
                                         attribute:NSLayoutAttributeWidth
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeWidth
                                         multiplier:0.96
                                         constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.registerTextField
                                         attribute:NSLayoutAttributeCenterX
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeCenterX
                                         multiplier:1.06
                                         constant:0]];
        
        [self.contentView addConstraint:[NSLayoutConstraint
                                         constraintWithItem:self.registerTextField
                                         attribute:NSLayoutAttributeCenterY
                                         relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                         attribute:NSLayoutAttributeCenterY
                                         multiplier:1.0
                                         constant:0]];
    }
    return self;
}
#pragma mark- setFrame
- (void)setFrame:(CGRect)frame {
    
    frame.origin.y += 5;
    frame.size.height -=5 *3;
    [super setFrame:frame];
}
@end
