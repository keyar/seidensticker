//
//  SSMonPanierRegisterViewCustomCell.h
//  Seidensticker
//
//  Created by OCS iOS Developer on 07/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#pragma mark- interface
@interface SSMonPanierRegisterViewCustomCell : UITableViewCell <UITextFieldDelegate>
{
    UIView *cellView;
}
@property (strong, nonatomic) UITextField *registerTextField;
@end
