//
//  SSMonPanierRegisterViewController.h
//  Seidensticker
//
//  Created by OCS iOS Developer on 06/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSMonPanierRegisterViewCustomCell.h"
#import "BSKeyboardControls.h"
#import "IQKeyboardManagerConstants.h"
#import "IQKeyboardManager.h"
#pragma mark - interface
@interface SSMonPanierRegisterViewController : UIViewController{
    IBOutlet UITextField *dateField;
    IBOutlet UITextField *monthField;
    IBOutlet UITextField *yearField;
    NSString *strEmail;
    
    NSString *userFirstName;
    NSString *userLastName;
    NSString *userEmailAddress;
    NSString *strRegisterPassword;
    NSString *strPassword;
    NSString *strNewPassword;
    NSString *strConfirmPassword;
    NSString *strFirstName;
    NSString *strLastName;
    NSString *Year;
    NSString *Month;
    NSString *Day;
    
    SSMonPanierRegisterViewCustomCell  *firstNameCell;
    SSMonPanierRegisterViewCustomCell  *lastNameCell;
    SSMonPanierRegisterViewCustomCell  *emailCell;
    SSMonPanierRegisterViewCustomCell  *passwordCell;
    SSMonPanierRegisterViewCustomCell  *oldPasswordCell;
    SSMonPanierRegisterViewCustomCell  *newPasswordCell;
    SSMonPanierRegisterViewCustomCell  *confirmPasswordCell;

}
@property(nonatomic,assign)BOOL isPushedFromMonInform;

@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@property (nonatomic, retain) NSString *emailAddressToRegister;
@property(nonatomic,retain)id controller;

@property(nonatomic,assign) BOOL isPushFromAccountPage;

@property(nonatomic,assign) BOOL isCommanderClicked;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableBottomConstraint;

- (IBAction)gotoRegister:(id)sender;
-(void) registerAPI;
@end
