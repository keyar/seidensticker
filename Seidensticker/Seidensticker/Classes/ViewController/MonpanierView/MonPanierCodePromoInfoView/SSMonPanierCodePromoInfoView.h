//
//  SSMonPanierCodePromoInfoView.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 10/22/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSMonPanierCodePromoInfoView : UIView


@property (strong, nonatomic)  UIImageView *imageviewBackground;
@property (strong, nonatomic)  UIView *viewContainer;
@property (strong, nonatomic)  UIButton *codePromoOkBtn;
@property (strong, nonatomic)  UILabel *CodePromoTitleLabel;
@property (strong, nonatomic)  UITextField *codePromoTextField;
@property (strong, nonatomic)  NSString *subString;

@end
