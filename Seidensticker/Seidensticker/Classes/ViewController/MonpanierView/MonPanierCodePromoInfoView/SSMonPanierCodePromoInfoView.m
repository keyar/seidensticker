//
//  SSMonPanierCodePromoInfoView.m
//  Seidensticker
//
//  Created by ocs-mini-7 on 10/22/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSMonPanierCodePromoInfoView.h"
#import "SSAppCommon.h"
#import "SSConfig.h"
@interface SSMonPanierCodePromoInfoView()<UITextFieldDelegate>
{
    BOOL isUpdateConstraint;
    
}

@end


@implementation SSMonPanierCodePromoInfoView
@synthesize subString;

-(instancetype)init{
    self = [super init];
    
    if (self) {
        [self setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self setBackgroundColor:[UIColor clearColor]];
        [self configureElements];
    }
    
    return self;
}


#pragma mark - configure elements

-(void)singleTapClose:(UITapGestureRecognizer *)gesture{
    [self removeGestureRecognizer:gesture];
    //[gesture setCancelsTouchesInView:NO];
    gesture.cancelsTouchesInView=NO;
    [self removeFromSuperview];
    
}

-(void)configureElements{
    UIImageView *imagviewContainerBg = [[UIImageView alloc] init];
    [imagviewContainerBg setTranslatesAutoresizingMaskIntoConstraints:NO];
    [imagviewContainerBg setBackgroundColor:[UIColor whiteColor]];
    [imagviewContainerBg setAlpha:0.6];
    [self addSubview:imagviewContainerBg];
    [self setImageviewBackground:imagviewContainerBg];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapClose:)];
    [tapGesture setNumberOfTapsRequired:1];
    [self addGestureRecognizer:tapGesture];
    
    UIView *viewContainer = [[UIView alloc] init];
    [viewContainer setTranslatesAutoresizingMaskIntoConstraints:NO];
    [viewContainer setBackgroundColor:[UIColor whiteColor]];
    [viewContainer.layer setBorderWidth:1.0];
    [viewContainer.layer setBorderColor:[[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f] CGColor]];
    [self addSubview:viewContainer];
    [self setViewContainer:viewContainer];
    
    UILabel *CodePromoTitleLabel = [[UILabel alloc] init];
    [CodePromoTitleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [CodePromoTitleLabel setTextColor:BLACK_COLOR];
    [CodePromoTitleLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [CodePromoTitleLabel setText:NSLocalizedString(@"Code de réduction", @"")];
    [CodePromoTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [CodePromoTitleLabel setBackgroundColor:[UIColor clearColor]];
    [self.viewContainer addSubview:CodePromoTitleLabel];
    [self setCodePromoTitleLabel:CodePromoTitleLabel];
    
    UIButton *codePromoOkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [codePromoOkBtn setTranslatesAutoresizingMaskIntoConstraints:NO];
    [codePromoOkBtn setBackgroundColor:[UIColor whiteColor]];
    codePromoOkBtn.layer.borderWidth = 1;
    codePromoOkBtn.layer.borderColor = [[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f] CGColor];
    [codePromoOkBtn setTitle:@"ok" forState:UIControlStateNormal];
    [codePromoOkBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    codePromoOkBtn.titleLabel.font  = [COMMON getResizeableFont:FuturaStd_Book(12)];
    [self.viewContainer addSubview:codePromoOkBtn];
    [self setCodePromoOkBtn:codePromoOkBtn];
    
    UITextField *codePromoTextField = [[UITextField alloc] init];
    [codePromoTextField setTranslatesAutoresizingMaskIntoConstraints:NO];
    [codePromoTextField setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    codePromoTextField.layer.borderWidth = 1;
    codePromoTextField.layer.borderColor = [[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f] CGColor];
    codePromoTextField.backgroundColor  =[UIColor whiteColor];
    codePromoTextField.delegate = self;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    codePromoTextField.leftView = paddingView;
    codePromoTextField.leftViewMode = UITextFieldViewModeAlways;
    
    [self.viewContainer addSubview:codePromoTextField];
    [self setCodePromoTextField:codePromoTextField];
    
    
}

-(void)setUpConstraints
{
    NSDictionary *viewDictionary = @{@"_imagviewContainerBg":_imageviewBackground,
                                     @"_viewContainer":_viewContainer,
                                     @"_codePromoOkBtn":_codePromoOkBtn,
                                     @"_CodePromoTitleLabel":_CodePromoTitleLabel,
                                     @"_codePromoTextField":_codePromoTextField};
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_imagviewContainerBg]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:viewDictionary]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_imagviewContainerBg]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:viewDictionary]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.viewContainer
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:0.8
                                                      constant:0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.viewContainer
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                    multiplier:1
                                                      constant:280]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.viewContainer
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                    multiplier:1
                                                      constant:100]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.viewContainer
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1
                                                      constant:0]];
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(==5)-[_CodePromoTitleLabel]-135-|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_codePromoTextField]-90-|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-195-[_codePromoOkBtn]-30-|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    

    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(==0)-[_CodePromoTitleLabel(==40)]-(0)-|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(==40)-[_codePromoTextField(==40)]-(0)-|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(==40)-[_codePromoOkBtn(==40)]-(0)-|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    
}

-(void)updateConstraints{
    [super updateConstraints];
    if (!isUpdateConstraint) {
        [self setUpConstraints];
        isUpdateConstraint = YES;
    }
}

+(BOOL)requiresConstraintBasedLayout{
    return YES;
}


#pragma mark- text field delegae

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.layer.borderColor=[[UIColor colorWithRed:113.0f/255.0f green:184.0f/255.0f blue:236.0f/255.0f alpha:1.0f] CGColor];
    textField.layer.borderWidth= 1.0f;
    return YES;
}


@end
