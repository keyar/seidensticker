//
//  SSMonPanierLoginViewController.m
//  Seidensticker
//
//  Created by OCS iOS Developer on 06/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSAppDelegate.h"
#import "SSMonPanierLoginViewController.h"
#import "SSMonPanierViewController.h"
#import "SSMonPanierRegisterViewController.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSCustomNavigation.h"
#import "SSMonPanierLoginViewCustomCell.h"
#import "SSMonPanierAddressViewController.h"
#import "SSPaymentDetailViewController.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#import "NSString+validations.h"
#import "SSMonPanierAddressModifierViewController.h"
#import "NMBottomTabBarController.h"
#import "BSKeyboardControls.h"
#import "SSShoppingCart.h"
#import "SSMonPanierAddressDisplayViewController.h"
//#import "IQKeyboardManager.h"
//#import "IQUIView+IQKeyboardToolbar.h"

#pragma mark- interface
@interface SSMonPanierLoginViewController ()<NMBottomTabBarDelegate,NMBottomTabBarControllerDelegate>
{
    SSWebService                *webservice;
    
    SSCustomNavigation          *navigation;
    BOOL            iskeyboard;
    //Keyboard return elements
    UIBarButtonItem         *doneBarItem;
    UIToolbar               *keyboardToolbar;
    UIBarButtonItem         *spaceBarItem;
    UIBarButtonItem         *previousBarItem;
    UIBarButtonItem         *nextBarItem;
    NSInteger               URLTag;
    UILabel                 *qtyCount;
    
    NSString    *deviceUDID;
    NSString    *deviceToken;
}
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@property (strong, nonatomic) UIButton *customButton;

@end

#pragma mark- implementation
@implementation SSMonPanierLoginViewController
@synthesize cartItems,isPushedFromTab;
@synthesize isCommanderClicked;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    webservice = [[SSWebService alloc] init];
    
    //[self setKeyboardControls:[[BSKeyboardControls alloc] init]];
    //[self.keyboardControls setDelegate:self];
//    if(IS_IPHONE6_Plus)
//    {
//    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,415,0.8)];
//    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
//    [self.navigationController.navigationBar addSubview:navBorder];
//    }

    //[self hideTabbar];
//     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didloadAccountView:) name:@"didloadAccountView" object:nil];
    [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
    [self.navigationController setNavigationBarHidden:NO];

   // [self styleNavBar];
    [self.view setUserInteractionEnabled:YES];
    
    
}
- (void) hideTabbar {
    SSAppDelegate *appDelegate = (SSAppDelegate *) [UIApplication sharedApplication].delegate;
    [appDelegate hideTabBar];
}
- (void)viewDidAppear:(BOOL)animated
{
    //[self loadNewNavigationBar];
    [self initializeKeyboardToolBar];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    [self.view setUserInteractionEnabled:YES];

    deviceUDID =[[NSUserDefaults standardUserDefaults]valueForKey:ID_DEVICE];
    deviceToken=[[NSUserDefaults standardUserDefaults]valueForKey:DEVICETOKEN];
    
     //cell.loginTextField.text=@""
    
    //[self.navigationController setNavigationBarHidden:NO animated:NO];
    
    [self styleNavBar];
    
    [COMMON TrackerWithName:@"Login View"];

}


-(void) initializeKeyboardToolBar
{
    if (keyboardToolbar == nil)
    {
        keyboardToolbar            = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 38.0f)];
        keyboardToolbar.barStyle   = UIBarStyleBlackTranslucent;
        spaceBarItem    = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                         target:nil
                                                                         action:nil];
        
        doneBarItem     = [[UIBarButtonItem alloc]  initWithTitle:NSLocalizedString(@"Suivant", @"")
                                                            style:UIBarButtonItemStyleDone
                                                           target:self
                                                           action:@selector(gotoLogin:)];
        
        [keyboardToolbar setItems:[NSArray     arrayWithObjects:
                                   spaceBarItem,
                                   doneBarItem, nil]];

    }
}

#pragma mark - BSKeyboard Control Delegate

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls{
    [self gotoLogin:nil];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    
    NSLog(@"TAG-->%ld",(long)textField.tag);
    SSMonPanierLoginViewCustomCell *emailCell=[self.firstloginTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    SSMonPanierLoginViewCustomCell *passwdCell=[self.firstloginTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    
    SSMonPanierLoginViewCustomCell *newEmailCell=[self.secondsignUpTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    strSignUpEmail  = newEmailCell.loginTextField.text;
    strEmailAddress = emailCell.loginTextField.text;
    strPassword     = passwdCell.loginTextField.text;

    if(textField.tag==101 || textField.tag==102){
        newEmailCell.loginTextField.text=@"";
        strSignUpEmail =@"";
    }
    if(textField.tag==103){
        emailCell.loginTextField.text =@"";
        passwdCell.loginTextField.text=@"";
        strEmailAddress = @"";
        strPassword     = @"";
    }
    
}
-(void)textFieldDidEndEditing:(UITextField*)textField
{
    id textFieldSuper = textField;
    
    while (![textFieldSuper isKindOfClass:[UITableViewCell class]]) {
        
        textFieldSuper = [textFieldSuper superview];
        
    }

    NSIndexPath *indexPath ;

    SSMonPanierLoginViewCustomCell *cell;
    indexPath = [self.firstloginTable indexPathForCell:(UITableViewCell *)textFieldSuper];
    
    cell = (SSMonPanierLoginViewCustomCell *) [self.firstloginTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section]];
    
    
}

#pragma mark- loadNavigation
- (void)loadNavigation
{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController.navigationBar setTranslucent:NO];
    navigation = [[SSCustomNavigation alloc] initWithNibName:@"SSCustomNavigation" bundle:nil];
    navigation.view.tag = 1001;
    [self.navigationController.navigationBar addSubview:navigation.view];
    if(isPushedFromTab==YES){
        [navigation setLeftHidden:NO];
        [navigation.navigationLeftLbl setText:NSLocalizedString(@"Retour", @"")];
        [navigation.navigationLeftLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
        [navigation.navigationLeftBtn addTarget:self action:@selector(retourAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    else{
        [navigation setLeftHidden:YES];
    }
    
    [navigation setTitleHidden:NO];
    [navigation setShopBtnHidden:NO];
    [navigation setShopLabelHidden:NO];
    [navigation setCatergoryShopBtnLabelHidden:YES];

    [navigation.titleLbl setText:NSLocalizedString(@"Connexion", @"")];
    [navigation.titleLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    [self setLabel];
}
-(void)setLabel
{
    NSLog(@"cart values %@",[SHOPPINGCART getCartItems]);
    cartItems = [SHOPPINGCART getCartItems];
    
    NSInteger quantity = 0;
    
    
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }
    
    navigation.navigationShopLbl.text = [NSString stringWithFormat:@"%ld",(long)quantity];
    [navigation.navigationShopLbl setBackgroundColor:[UIColor clearColor]];
    navigation.navigationShopLbl.textColor = [UIColor blackColor];
    [navigation.navigationShopLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(10)]];
    [navigation.navigationShopLbl setTextAlignment:NSTextAlignmentCenter];
    
    [navigation.navigationShopBtn addTarget:self action:@selector(cartBtnAction:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)loadNewNavigationBar{
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setHidden:NO];
    UIFont *titleFont = [COMMON getResizeableFont:FuturaStd_Book(14)];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor],NSFontAttributeName:titleFont}];
    self.navigationItem.title = @"Connexion";
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    NSLog(@"cart values %@",[SHOPPINGCART getCartItems]);
    cartItems = [SHOPPINGCART getCartItems];
    
    NSInteger quantity = 0;
    
    
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }
    
    NSString *count = [NSString stringWithFormat:@"%ld",(long)quantity];
    
    
    self.navigationItem.leftBarButtonItem=nil;
    self.navigationItem.hidesBackButton=YES;
    
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"shop_active_icon"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(cartBtnAction:)forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat buttonXPos;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        buttonXPos = 29;
    }
    else{
        buttonXPos = 37;
    }
    [button setFrame:CGRectMake(buttonXPos, 0, 38, 38)];
    [button setBackgroundColor:[UIColor clearColor]];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(11, 15, 15,15)];
    [label setFont:[COMMON getResizeableFont:FuturaStd_Book(8)]];
    [label setText:count];
    [label setBackgroundColor:[UIColor clearColor]];
    label.textAlignment = NSTextAlignmentCenter;
    [label setTextColor:[UIColor blackColor]];
    
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, -2, 65, 38)];
    //backButtonView.bounds = CGRectOffset(backButtonView.bounds, -14, -10);
    [backButtonView setBackgroundColor:[UIColor clearColor]];
    
    [button addSubview:label];
    [backButtonView addSubview:button];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    //[barButton setBackgroundVerticalPositionAdjustment:-50.0f forBarMetrics:UIBarMetricsDefault];
    [self.navigationItem setRightBarButtonItem:barButton];
    
    
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    [self.navigationController.navigationBar addSubview:navBorder];
    
}
#pragma mark-navigation bar creation

- (void)styleNavBar
{
//    [self.navigationController setNavigationBarHidden:YES animated:NO];
//    UINavigationBar *newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 64.0)];
//    [newNavBar setBackgroundColor:[UIColor clearColor]];
//    
//    [newNavBar setBarTintColor:[UIColor whiteColor]];
//    
//    UINavigationItem *newItem = [[UINavigationItem alloc] init];
//    newItem.title = @"Connexion";
//    
//    [[UINavigationBar appearance] setTitleTextAttributes: @{
//                                                            NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)]}];
//    [newNavBar pushNavigationItem:newItem animated:false];
//   
//    //[self setLabelNew:newNavBar];
//    [self setButton:newNavBar];
//    [self.view addSubview:newNavBar];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    UINavigationBar *newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, screen_width, 64.0)];
    [newNavBar setBackgroundColor:[UIColor clearColor]];
    
    [newNavBar setBarTintColor:[UIColor whiteColor]];
    newNavBar.tag=1001;
    UINavigationItem *newItem = [[UINavigationItem alloc] init];
    newItem.title = @"Connexion";
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                            NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)]}];
    [newNavBar pushNavigationItem:newItem animated:false];
    
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,64,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
        
    [newNavBar addSubview:navBorder];
    
    UILabel *lblLefttitle = [UILabel new];
    [lblLefttitle setText:@"Retour"];
    lblLefttitle.textColor = [UIColor orangeColor];
    [lblLefttitle setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [lblLefttitle setTextAlignment:NSTextAlignmentLeft];
    //UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMenuBtn:)];
   // [lblLefttitle addGestureRecognizer:tap];
    lblLefttitle.userInteractionEnabled = YES;
    [lblLefttitle setBackgroundColor:[UIColor clearColor]];
    [lblLefttitle setFrame:CGRectMake(20,(newNavBar.frame.size.height/2)-5 , newNavBar.frame.size.width/3, 30)];
    //[newNavBar addSubview:lblLefttitle];
    
    cartItems = [SHOPPINGCART getCartItems];
    NSInteger quantity = 0;
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"shop_active_icon"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(cartBtnAction:)forControlEvents:UIControlEventTouchUpInside];
    
    
    [button setFrame:CGRectMake(0, 0, 38, 38)];
    [button setBackgroundColor:[UIColor clearColor]];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(11, 15, 15,15)];
    [label setFont:[COMMON getResizeableFont:FuturaStd_Book(8)]];
    [label setText:[NSString stringWithFormat:@"%ld",(long)quantity]];
    [label setBackgroundColor:[UIColor clearColor]];
    label.textAlignment = NSTextAlignmentCenter;
    [label setTextColor:[UIColor blackColor]];
    
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(newNavBar.frame.size.width-55,(newNavBar.frame.size.height/2) -10 , 40, 40)];
    [backButtonView setBackgroundColor:[UIColor clearColor]];
    [button addSubview:label];
    [backButtonView addSubview:button];
    
    [newNavBar addSubview:backButtonView];
    
    [self.view addSubview:newNavBar];
}



#pragma mark - Right Menu

-(void)cartBtnAction:(id)sender
{
    SSMonPanierViewController *monpanierViewController;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonpanierViewControlleriPad" bundle:[NSBundle mainBundle]];
        
        
    }
    else {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonPanierViewController" bundle:[NSBundle mainBundle]];
    }
    
    [self.navigationController pushViewController:monpanierViewController animated:YES];
}


#pragma mark- UITableview Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(IS_IPHONE4 ||IS_IPHONE5 ||IS_IPHONE6 ||IS_IPHONE6_Plus)
    {
       return 65;
    }
    else{
        return 90;
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(tableView == _firstloginTable)
    {
        return 1;
    }
    else
    {
        return 1;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == _firstloginTable)
    {
        return 2;
    }
    else
    {
        return 1;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier=@"Cell";
    SSMonPanierLoginViewCustomCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
   if (cell == nil)
    {
        NSArray *nib;
        nib = [[NSBundle mainBundle] loadNibNamed:@"SSMonPanierLoginViewCustomCell" owner:self options:nil];

        cell= [nib objectAtIndex:0];
    }

   if(tableView == _firstloginTable)
   {
        cell.loginTextField.delegate = self;
       if (indexPath.row == 0){
           cell.loginTextField.tag = 101;
           [cell.loginTextField setKeyboardType:UIKeyboardTypeEmailAddress];
            cell.loginTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                    initWithString:@"Adresse email..."
                                                        attributes:@{
                                               NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                 }];
       }
       if (indexPath.row == 1){
           cell.loginTextField.tag = 102;
            cell.loginTextField.secureTextEntry =YES;
            cell.loginTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                    initWithString:@"Mot de passe..."
                                                        attributes:@{
                                               NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                 }];
       }
       loginTable.separatorStyle = UITableViewCellSeparatorStyleNone;
       cell.selectionStyle = UITableViewCellSelectionStyleNone;
//       [cell.layer setBorderColor: [[UIColor colorWithRed:151.0f/255.0f
//                                                         green:151.0f/255.0f
//                                                          blue:151.0f/255.0f
//                                                         alpha:1.0f] CGColor]];
//       [cell.layer setBorderWidth: 1.5];
       [self addBorderToCell:cell];
       return cell;
   }
   else{
       cell.loginTextField.delegate = self;

       if (indexPath.row == 0){
           cell.loginTextField.tag = 103;
           [cell.loginTextField setKeyboardType:UIKeyboardTypeEmailAddress];
           cell.loginTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                initWithString:@"Adresse email..."
                                                    attributes:@{
                                           NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                             }];
       }
       
       signUpTable.separatorStyle = UITableViewCellSeparatorStyleNone;
       signUpTable.backgroundColor = [UIColor clearColor];
       cell.selectionStyle = UITableViewCellSelectionStyleNone;
      // [cell.layer setBorderColor: [[UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f] CGColor]];
       //[cell.layer setBorderWidth: 1.5];
        [self addBorderToCell:cell];
       return cell;
   }
}

#pragma mark - addBorderToCell
-(void)addBorderToCell:(UITableViewCell*)cell{
    UIView *leftBorder;
    UIView *rightBorder;
    UIView *topBorder;
    UIView *bottomBorder;
    leftBorder = [[UIView alloc] initWithFrame:CGRectMake(1, 0, 1.5, cell.frame.size.height)];
    rightBorder = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.size.width-2.0f, 0, 1.5, cell.frame.size.height)];
    topBorder = [[UIView alloc] initWithFrame:CGRectMake(1, 0,cell.frame.size.width,1.5)];
    bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height-2.0f, cell.frame.size.width, 1.5)];
    [cell addSubview:leftBorder];
    [cell addSubview:topBorder];
    [cell addSubview:rightBorder];
    [cell addSubview:bottomBorder];
    [leftBorder setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin];
    [rightBorder setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin];
    [topBorder setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
    [bottomBorder setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
    leftBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
    rightBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
    topBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
    bottomBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
    
}

#pragma mark- hide keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //hides keyboard when another part of layout was touched
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
}
#pragma mark- tabbar method
- (void) showTabBar {
    SSAppDelegate *appDelegate = (SSAppDelegate *) [UIApplication sharedApplication].delegate;
    [appDelegate showTabBar];
}
#pragma mark-Button Actions
-(void)retourAction:(id)sender
{
    [self showTabBar];
//    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    [COMMON removeLoadingIcon];
    SSAppDelegate *appDelegate = (SSAppDelegate *)[[UIApplication sharedApplication] delegate];
    [[appDelegate monPanierViewController].monPanierTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - gotoAddressBookView
- (void) gotoAddressBookView {
    
    [webservice getAddressListForUser:[COMMON getCustomerSecureKey]
                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  if([responseObject objectForKey:@"error"] != NULL){
                                      [self gotoAddressAddView];
                                      
                                  }
                                  else if ([responseObject objectForKey:@"listaddress"] != NULL) {
                                      [self gotoAddressListView:[responseObject objectForKey:@"listaddress"]];
                                     
                                  } else {
                                      [self gotoAddressAddView];
                                     
                                  }
                                  [COMMON removeLoadingIcon];
                              }
                              failure:^(AFHTTPRequestOperation *operation, id error) {
                                  [COMMON removeLoadingIcon];
                              }];
}
#pragma mark - gotoAddressListView
- (void) gotoAddressListView:(NSMutableDictionary *) userAddress {
//    SSMonPanierAddressViewController *monPanierAddressVC = nil;
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
//    {
//        monPanierAddressVC = [[SSMonPanierAddressViewController alloc]initWithNibName:@"SSMonPanierAddressViewControlleriPad" bundle:[NSBundle mainBundle]];
//    }
//    else {
//        monPanierAddressVC = [[SSMonPanierAddressViewController alloc]
//                              initWithNibName:@"SSMonPanierAddressViewController"
//                              bundle:nil];
//    }
   
    
    SSMonPanierAddressDisplayViewController *monPanierAddressVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierAddressVC = [[SSMonPanierAddressDisplayViewController alloc]initWithNibName:@"SSMonPanierAddressDisplayViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierAddressVC = [[SSMonPanierAddressDisplayViewController alloc]
                              initWithNibName:@"SSMonPanierAddressDisplayViewController"
                              bundle:nil];
    }
    if(isCommanderClicked==YES){
        monPanierAddressVC.isCommanderClicked=YES;
    }
    else{
        monPanierAddressVC.isCommanderClicked=NO;
    }

    monPanierAddressVC.userAddress = userAddress;
    monPanierAddressVC.isPushFirstTimeLoginPage =YES;
    [self.navigationController pushViewController:monPanierAddressVC animated:YES];
    [COMMON removeLoadingIcon];
}
#pragma mark - gotoAddressAddView
- (void) gotoAddressAddView {
    SSMonPanierAddressModifierViewController *monPanierAddressModifierVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierAddressModifierVC = [[SSMonPanierAddressModifierViewController alloc]initWithNibName:@"SSMonPanierAddressModifierViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierAddressModifierVC = [[SSMonPanierAddressModifierViewController alloc]
                                      initWithNibName:@"SSMonPanierAddressModifierViewController"
                                      bundle:nil];
    }
    if(isCommanderClicked==YES){
        monPanierAddressModifierVC.isCommanderClicked=YES;
    }
    else{
        monPanierAddressModifierVC.isCommanderClicked=NO;
    }

    monPanierAddressModifierVC.isPushedFromMonAddresses = NO;
    monPanierAddressModifierVC.isPushedFromRegisterPage=NO;
    monPanierAddressModifierVC.isPushFromLoginPage=YES;
    [self.navigationController pushViewController:monPanierAddressModifierVC animated:YES];
    [COMMON removeLoadingIcon];
    
}
#pragma mark - gotoRegisterView
- (void) gotoRegisterViewWithEmailAddress:(NSString *)strEmailId {
    SSMonPanierRegisterViewController *monPanierRegisterVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierRegisterVC = [[SSMonPanierRegisterViewController alloc]initWithNibName:@"SSMonPanierRegisterViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierRegisterVC = [[SSMonPanierRegisterViewController alloc]
                               initWithNibName:@"SSMonPanierRegisterViewController"
                               bundle:nil];
    }
    if(isCommanderClicked==YES){
        monPanierRegisterVC.isCommanderClicked=YES;
    }
    else{
        monPanierRegisterVC.isCommanderClicked=NO;
    }
    monPanierRegisterVC.emailAddressToRegister = strEmailId;
    monPanierRegisterVC.isPushedFromMonInform=NO;
    [self.navigationController pushViewController:monPanierRegisterVC animated:YES];
    [COMMON removeLoadingIcon];
}
-(void)loginFieldValues {
    
//    SSMonPanierLoginViewCustomCell *emailCell=[loginTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
//    
//    SSMonPanierLoginViewCustomCell *passwdCell=[loginTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
//    
//    emailCell.loginTextField.text = @"";
//    
//    passwdCell.loginTextField.text = @"";
    
}


-(void)gotoAccountView {
    
    SSAccountViewController *accountViewController = nil;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        accountViewController = [[SSAccountViewController alloc]
                                 initWithNibName:@"SSAccountViewControlleriPad"
                                 bundle:[NSBundle mainBundle]];
    }
    else {
        accountViewController = [[SSAccountViewController alloc]
                                 initWithNibName:@"SSAccountViewController"
                                 bundle:nil];
    }
    accountViewController.isCommanderClicked=NO;
    
    [self.navigationController pushViewController:accountViewController animated:YES];
    
    //[self.navigationController setNavigationBarHidden:NO];
//    SSMonPanierAddressViewController *monPanierAddressVC = nil;
//    
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
//    {
//        monPanierAddressVC = [[SSMonPanierAddressViewController alloc]initWithNibName:@"SSMonPanierAddressViewControlleriPad" bundle:[NSBundle mainBundle]];
//    }
//    else {
//        monPanierAddressVC = [[SSMonPanierAddressViewController alloc]
//                              initWithNibName:@"SSMonPanierAddressViewController"
//                              bundle:nil];
//    }
//    monPanierAddressVC.isPushFromAccountPage =NO;
//    monPanierAddressVC.isPushFromLoginPage =YES;
//    [self.navigationController pushViewController:monPanierAddressVC animated:YES];
    
}

#pragma mark - loginEmailAPI
-(void) loginEmailAPI{
    [COMMON showLoadingIcon:self];
    [webservice userLoginWithEmailAddress:strEmailAddress
                              andPassword:strPassword
                              deviceToken:deviceToken
                                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                      if ([responseObject objectForKey:@"customer"] != NULL) {
                                          [self.view setUserInteractionEnabled:YES];
                                          [COMMON storeUserDetails:[responseObject objectForKey:@"customer"]];
                                                                                                          
                                                                                    
                                          //[self loginFieldValues];
                                          
                                          NSLog(@"cart values %@",[SHOPPINGCART getCartItems]);
                                          cartItems = [SHOPPINGCART getCartItems];
                                          if([cartItems count]!=0){
                                              [self gotoAddressBookView];
                                             
                                          }
                                          else{
                                               [self gotoAccountView];
                                          }
                                          
                                      }
                                      else if([responseObject objectForKey:@"error"] != NULL){
                                          [SSAppCommon showSimpleAlertWithMessage:[responseObject objectForKey:@"error"]];
                                          [COMMON removeLoadingIcon];
                                          [self.view setUserInteractionEnabled:YES];
                                      }
                                      else {
                                          [SSAppCommon showSimpleAlertWithMessage:INVALID_AUTHENTICATION];
                                          [COMMON removeLoadingIcon];
                                      }
                                      
                                      [self.view setUserInteractionEnabled:YES];
                                      
                                  }
                                  failure:^(AFHTTPRequestOperation *operation, id error) {
                                      [COMMON removeLoadingIcon];
                                      [self.view setUserInteractionEnabled:YES];
                                  }];
}
#pragma mark - checkEmailAPI
-(void) checkEmailAPI{
    [COMMON showLoadingIcon:self];
    [webservice checkLoginWithEmailAddress:strSignUpEmail
                                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                      
                                       if([responseObject objectForKey:@"error"] != NULL){
                                           [self.view setUserInteractionEnabled:YES];
                                           [SSAppCommon showSimpleAlertWithMessage:[responseObject objectForKey:@"error"]];
                                       }
                                       else {
                                           [self.view setUserInteractionEnabled:YES];
                                           [self gotoRegisterViewWithEmailAddress:strSignUpEmail];
                                       }
                                       [COMMON removeLoadingIcon];
                                   }
                                   failure:^(AFHTTPRequestOperation *operation, id error) {
                                       [self.view setUserInteractionEnabled:YES];
                                       [COMMON removeLoadingIcon];
                                   }];
}
#pragma mark - gotoLogin
- (IBAction)gotoLogin:(id)sender {
    signUpCell=(SSMonPanierLoginViewCustomCell *)[_secondsignUpTable visibleCells][0];
    usernameCell=(SSMonPanierLoginViewCustomCell *)[_firstloginTable visibleCells][0];
    passwordCell=(SSMonPanierLoginViewCustomCell *)[_firstloginTable visibleCells][1];
    strSignUpEmail  = signUpCell.loginTextField.text;
    strEmailAddress = usernameCell.loginTextField.text;
    strPassword     = passwordCell.loginTextField.text;
    [COMMON showLoadingIcon:self];
    [self.view setUserInteractionEnabled:NO];
    if([NSString isEmpty:strSignUpEmail]){
        if([NSString isEmpty:strEmailAddress] && [NSString isEmpty:strPassword]){
            [SSAppCommon showSimpleAlertWithMessage:EMAIL_REQUIRED];
            [COMMON removeLoadingIcon];
            [self.view setUserInteractionEnabled:YES];
            return;
        }
        if(![NSString isEmpty:strEmailAddress] && [NSString isEmpty:strPassword]){
            [SSAppCommon showSimpleAlertWithMessage:INVALID_PASSWORD];
            [COMMON removeLoadingIcon];
            [self.view setUserInteractionEnabled:YES];
            return;
        }
        if([NSString isEmpty:strEmailAddress] && ![NSString isEmpty:strPassword]){
            [SSAppCommon showSimpleAlertWithMessage:EMAIL_REQUIRED];
            [COMMON removeLoadingIcon];
            [self.view setUserInteractionEnabled:YES];
            return;
        }
        if(![NSString isEmpty:strEmailAddress] && ![NSString isEmpty:strPassword]){
            if(![NSString validateEmail:strEmailAddress]){
                [SSAppCommon showSimpleAlertWithMessage:INVALID_EMAIL];
                [COMMON removeLoadingIcon];
                [self.view setUserInteractionEnabled:YES];
                return;
            }
            [self loginEmailAPI];
        }
    }
    else{
        if(![NSString isEmpty:strSignUpEmail] && [NSString isEmpty:strPassword] && ![NSString isEmpty:strEmailAddress]){
            [SSAppCommon showSimpleAlertWithMessage:INVALID_PASSWORD];
            [COMMON removeLoadingIcon];
            [self.view setUserInteractionEnabled:YES];
             return;
        }
        if(![NSString isEmpty:strEmailAddress] && ![NSString isEmpty:strPassword] && ![NSString isEmpty:strSignUpEmail]){
            if(![NSString validateEmail:strEmailAddress]){
                [SSAppCommon showSimpleAlertWithMessage:INVALID_EMAIL];
                [COMMON removeLoadingIcon];
                [self.view setUserInteractionEnabled:YES];
                return;
            }
            [self loginEmailAPI];
        }
        if([NSString isEmpty:strEmailAddress] && [NSString isEmpty:strPassword] && ![NSString isEmpty:strSignUpEmail]){
            if(![NSString validateEmail:strSignUpEmail]){
                [SSAppCommon showSimpleAlertWithMessage:INVALID_EMAIL];
                [COMMON removeLoadingIcon];
                [self.view setUserInteractionEnabled:YES];
                return;
            }
            [self checkEmailAPI];
        }
        if([NSString isEmpty:strEmailAddress] && ![NSString isEmpty:strPassword] && ![NSString isEmpty:strSignUpEmail]){
            if(![NSString validateEmail:strSignUpEmail]){
                [SSAppCommon showSimpleAlertWithMessage:INVALID_EMAIL];
                [COMMON removeLoadingIcon];
                [self.view setUserInteractionEnabled:YES];
                return;
            }
            [self checkEmailAPI];
        }
//        else{
//            //[self.view setUserInteractionEnabled:YES];
//            //[COMMON removeLoadingIcon];
//        }
    }
}
@end
