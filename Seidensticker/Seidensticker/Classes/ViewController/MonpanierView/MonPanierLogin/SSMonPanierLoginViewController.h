//
//  SSMonPanierLoginViewController.h
//  Seidensticker
//
//  Created by OCS iOS Developer on 06/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"
#import "SSMonPanierLoginViewCustomCell.h"
//#import "IQKeyboardManagerConstants.h"
//#import "IQKeyboardManager.h"
#pragma mark - interface
@interface SSMonPanierLoginViewController : UIViewController<BSKeyboardControlsDelegate,UITextFieldDelegate>
{
    IBOutlet UITableView *loginTable;
    IBOutlet UITableView *signUpTable;
    NSString *strSignUpEmail;
    NSString *strEmailAddress;
    NSString *strPassword;
    SSMonPanierLoginViewCustomCell  *signUpCell;
    SSMonPanierLoginViewCustomCell  *usernameCell;
    SSMonPanierLoginViewCustomCell  *passwordCell;
    
    
   
    
}
@property(nonatomic,retain)id controller;
@property (strong, nonatomic) IBOutlet UILabel *loginLabel;
@property (strong, nonatomic) IBOutlet UILabel *signupLabel;
@property (strong, nonatomic) IBOutlet UITableView *firstloginTable;
@property (strong, nonatomic) IBOutlet UITableView *secondsignUpTable;
@property (strong, nonatomic) IBOutlet UIView *userListView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *suivantBottomConstraint;

//NEW

@property(strong,nonatomic) NSMutableArray *cartItems;
@property(nonatomic,assign)BOOL isPushedFromTab;

@property(nonatomic,assign) BOOL isCommanderClicked;

- (IBAction)gotoLogin:(id)sender;
- (void) gotoRegisterViewWithEmailAddress:(NSString *)strEmailId;
-(void) loginEmailAPI;
-(void) checkEmailAPI;
@end
