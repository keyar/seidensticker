//
//  SSMonPanierLoginViewCustomCell.m
//  Seidensticker
//
//  Created by OCS iOS Developer on 06/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSMonPanierLoginViewCustomCell.h"
#import "SSMonPanierLoginViewController.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSCustomNavigation.h"
#pragma mark - implementation
@implementation SSMonPanierLoginViewCustomCell
@synthesize loginTextField;
- (void)awakeFromNib {
    self.loginTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.loginTextField setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    [self.loginTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
    [self.loginTextField setKeyboardAppearance:UIKeyboardAppearanceLight];

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
#pragma mark - setFrame
- (void)setFrame:(CGRect)frame {
    
    frame.origin.y += 5;
    frame.size.height -=5 *3;
    [super setFrame:frame];
}
@end
