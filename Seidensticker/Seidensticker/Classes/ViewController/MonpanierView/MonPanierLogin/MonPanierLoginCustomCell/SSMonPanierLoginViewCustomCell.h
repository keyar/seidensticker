//
//  SSMonPanierLoginViewCustomCell.h
//  Seidensticker
//
//  Created by OCS iOS Developer on 06/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"
#import "IQKeyboardManagerConstants.h"
#import "IQKeyboardManager.h"
#pragma mark - interface
@interface SSMonPanierLoginViewCustomCell : UITableViewCell <UITextFieldDelegate>
{
    UIView *cellView;
 }
@property (strong, nonatomic)     IBOutlet UITextField *loginTextField;;

@end
