//
//  SSMonPanierAddressViewController.m
//  Seidensticker
//
//  Created by OCS iOS Developer on 07/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSAppDelegate.h"
#import "SSMonPanierAddressViewController.h"
#import "SSMonPanierAddressModifierViewController.h"
#import "SSAppCommon.h"
#import "SSConfig.h"
#import "SSCustomNavigation.h"
#import "SSPaymentDetailViewController.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#import "NSString+validations.h"
#import "NMBottomTabBarController.h"
#import "SSMonPanierAddressCustomCell.h"
#import "SSShoppingCart.h"
#import "SSMonPanierAddressDisplayViewController.h"



@interface SSMonPanierAddressViewController ()<UITextViewDelegate, UITableViewDelegate, UITableViewDataSource,UIWebViewDelegate>
{
    SSWebService                *webService;
    NMBottomTabBarController    *bottomTabBar;
    NSMutableArray              *userArray;
    SSAppDelegate               *appDelegate;
    BOOL            isFirstTime;
    
    UIRefreshControl *refreshControl;
    UILabel *noItemsLabel;
    NSString *currentUserDeliveryId;
}
@property (strong, nonatomic) IBOutlet UILabel *noRecordsLabel;

@end

#pragma mark- implementation

@implementation SSMonPanierAddressViewController
@synthesize addressDictionary, addresslist;
@synthesize userAddress;
@synthesize addressTableView;
@synthesize isPushFromAccountPage,isPushFromLoginPage,isPushedFromRegisterPage;
@synthesize isPushedFromAddressDisplayPage;

@synthesize isCommanderClicked,isPushedFromMonAccountPage;

@synthesize orderNumber;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    isFirstTime=YES;
    appDelegate = (SSAppDelegate *) [UIApplication sharedApplication].delegate;

    webService = [[SSWebService alloc] init];
    
    _addressArray = [NSArray new];
    
    // [self pushToHomePage];
    [COMMON showLoadingIcon:self];
    
    if(IS_IPHONE6_Plus){
        
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    [self.navigationController.navigationBar addSubview:navBorder];
        
    }
    // Initialize the refresh control.
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self
                       action:@selector(getUserAddressList)
             forControlEvents:UIControlEventValueChanged];
    [addressTableView addSubview:refreshControl];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self showTabBar];
   
    _noRecordsLabel.hidden =YES;
     [COMMON TrackerWithName:@"Address View"];
    
//    noItemsLabel=[[UILabel alloc]init];
//    noItemsLabel.hidden=YES;
    [noItemsLabel removeFromSuperview];
    
    [self setUpInitialTable];
    
    [self loadNavigation];
    
    addressTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if(isFirstTime==YES){
         [addressTableView setHidden:YES];
         isFirstTime =NO;
    }
    else{
         [addressTableView setHidden:NO];
    }
   
   
    [self getUserAddressList];
    
   // [addressTableView reloadData];
    
//    userArray = [[NSMutableArray alloc]init];
//    
//    NSDictionary *userDict = [[NSDictionary alloc]init];
//    
//    userDict = [COMMON getUserDetails];
//    
//    if (userAddress == NULL) {
//        [self getUserListAddress];
//    } else {
//        [self getUserDetails];
//    }
}


- (void)setUpInitialTable
{
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
//        [addressTableView registerNib:[UINib nibWithNibName:@"SSMonPanierAddressCustomCelliPad" bundle:nil] forCellReuseIdentifier:@"SSMonPanierAddressCustomCelliPad"];
//    else
        [addressTableView registerNib:[UINib nibWithNibName:@"SSMonPanierAddressCustomCell" bundle:nil] forCellReuseIdentifier:@"SSMonPanierAddressCustomCell"];
}

#pragma mark- loadNavigation

- (void)loadNavigation
{

    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController.navigationBar setTranslucent:NO];
    SSCustomNavigation *navigation = [[SSCustomNavigation alloc] initWithNibName:@"SSCustomNavigation" bundle:nil];
    navigation.view.tag=1001;
    [self.navigationController.navigationBar addSubview:navigation.view];
    
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    
    [navigation.view addSubview:navBorder];
    
    [navigation setRightHidden:NO];
    [navigation setLeftHidden:NO];
    [navigation setTitleHidden:NO];
    [navigation setCatergoryShopBtnLabelHidden:YES];

    
    [navigation.titleLbl setText:NSLocalizedString(@"Mes adresse", @"")];
    [navigation.titleLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    
    if(isPushedFromRegisterPage==YES){
        [navigation.navigationLeftLbl setHidden:YES];
        [navigation.navigationLeftBtn setHidden:YES];
    }
    else{
        [navigation.navigationLeftLbl setText:NSLocalizedString(@"Retour", @"")];
        [navigation.navigationLeftLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
        [navigation.navigationLeftBtn addTarget:self
                                         action:@selector(addressRetour:)
                               forControlEvents:UIControlEventTouchUpInside];
    }
    
   
    
    UIImage *image = [[UIImage imageNamed:@"plusImage"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [navigation.navigationRightBtn setImage:image forState:UIControlStateNormal];
    navigation.navigationRightBtn.tintColor = [UIColor blackColor];
    [navigation.navigationRightBtn addTarget:self action:@selector(addressModifierAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if(isPushFromLoginPage==YES){
        [navigation.navigationLeftLbl setHidden:YES];
        [navigation.navigationLeftBtn setHidden:YES];
        
    }
//    if(isPushedFromRegisterPage==YES){
//        [navigation.navigationLeftLbl setHidden:YES];
//        [navigation.navigationLeftBtn setHidden:YES];
//    }

}


#pragma mark - User Actions

- (void) getUserAddressList {
    
    [webService getAddressListForUser:[COMMON getCustomerSecureKey]
                              success:^(AFHTTPRequestOperation *operation, id responseObject){
                                  [refreshControl endRefreshing];
                                  NSLog(@"Response = %@",responseObject);
                                  
                                  if ([responseObject count] == 0) {
                                      
                                      [COMMON removeLoadingIcon];
                                      [self loadEmptyLabel];
                                      
                                      
                                  }
                                  
                                  if([responseObject objectForKey:@"error"] != NULL){
                                      
                                      [COMMON removeLoadingIcon];
                                      [self loadEmptyLabel];
                                      
                                      
                                  }
                                  else if ([responseObject objectForKey:@"listaddress"] != NULL) {
                                      
                                     [noItemsLabel setHidden:YES];
                                     [noItemsLabel removeFromSuperview];
                                     
                                      [COMMON removeLoadingIcon];
                                      
                                      NSDictionary * addressDict;
                                      
                                      addressDict = [responseObject mutableCopy];
                                      
                                      _addressArray = [addressDict valueForKey:@"listaddress"];
                                      [addressTableView setBackgroundColor:[UIColor whiteColor]];
                                      [addressTableView setHidden:NO];
                                      [addressTableView reloadData];
                                    }
                              }
                              failure:^(AFHTTPRequestOperation *operation, id error) {
                                  [refreshControl endRefreshing];
                                  //[SSAppCommon showSimpleAlertWithMessage:@"No Internet Connection available. Kindly check your Internet"];
                                  [COMMON removeLoadingIcon];
                                  
                              }];
}

-(void)loadEmptyLabel{
    [noItemsLabel removeFromSuperview];
    noItemsLabel=[[UILabel alloc]init];
    CGFloat XPos = 20;
    CGFloat YPos = screen_height/2;
    noItemsLabel.frame=CGRectMake(XPos ,(YPos-40) ,screen_width-(XPos*2),40);
    
    noItemsLabel.textAlignment=NSTextAlignmentCenter;
    [noItemsLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(18)]];
    noItemsLabel.tag=EMPTYCARTLABEL_TAG;
    noItemsLabel.text=@"Votre adresse est vide";
    [noItemsLabel setBackgroundColor:[UIColor clearColor]];
    [noItemsLabel setHidden:NO];
    addressTableView.hidden=YES;
    [self.view addSubview:noItemsLabel];
}


#pragma mark - Login View

- (void) LoginView {
    
    SSMonPanierLoginViewController *monPanierLoginVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierLoginVC = [[SSMonPanierLoginViewController alloc]initWithNibName:@"SSMonPanierLoginViewControlleriPad" bundle:[NSBundle mainBundle]];
        monPanierLoginVC.hidesBottomBarWhenPushed=YES;
    }
    else {
        monPanierLoginVC = [[SSMonPanierLoginViewController alloc]
                            initWithNibName:@"SSMonPanierLoginViewController"
                            bundle:nil];
        
        monPanierLoginVC.hidesBottomBarWhenPushed=YES;
    }
    
    [self.navigationController pushViewController:monPanierLoginVC animated:YES];
}

#pragma mark - User  Details

- (NSString *) getFormattedAddress:(id)dictionary {
    
    NSString *strAddress = @"";
    
    NSString *firstName = [dictionary valueForKey:@"firstname"];
    
    NSString *lastName = [dictionary valueForKey:@"lastname"];

    NSString *addressOne = [dictionary valueForKey:@"address1"];

    NSString *addressTwo = [dictionary valueForKey:@"address2"];

    NSString *postalCode = [dictionary valueForKey:@"postcode"];

    NSString *city = [dictionary valueForKey:@"city"];

    NSString *countryName = [dictionary valueForKey:@"country_name"];

    NSString *phone = [dictionary valueForKey:@"phone"];

    NSString *alias = [dictionary valueForKey:@"alias"];


    if (firstName != NULL && ![firstName isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@%@",strAddress,firstName];
    }
    if (lastName != NULL && ![lastName isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@ %@",strAddress,lastName];
    }
    if (addressOne != NULL && ![addressOne isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@\n%@",strAddress,addressOne];
    }
    if (addressTwo != NULL && ![addressTwo isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@\n%@",strAddress,addressTwo];
    }
    if (postalCode != NULL && ![postalCode isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@\n%@",strAddress,postalCode];
    }
    if (city != NULL && ![city isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@ %@",strAddress,city];
    }
    if (countryName != NULL && ![countryName isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@\n%@",strAddress,countryName];
    }
    if (phone != NULL && ![phone isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@\n%@",strAddress,phone];
    }
    if (alias != NULL && ![alias isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@\n%@",strAddress,alias];
    }

    return strAddress;
}

- (void) getUserDetails{
    
    SSMonPanierAddressCustomCell *cell = [addressTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    cell.addressTextView.text = [self getFormattedAddress:userAddress];
      
    [cell.addressTextView setFont:[COMMON getResizeableFont:FuturaStd_Book(17)]];
}

#pragma mark- addAddressView

- (void) gotoAddAddressView:(id ) addressModifyDictionary{
     [noItemsLabel removeFromSuperview];
    
    SSMonPanierAddressModifierViewController *monPanierAddressModifierVC = nil;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierAddressModifierVC = [[SSMonPanierAddressModifierViewController alloc]initWithNibName:@"SSMonPanierAddressModifierViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierAddressModifierVC = [[SSMonPanierAddressModifierViewController alloc]
                                      initWithNibName:@"SSMonPanierAddressModifierViewController"
                                      bundle:nil];
    }
    if(isPushFromLoginPage==YES){
        monPanierAddressModifierVC.isPushFromLoginPage=YES;
    }
    else{
        monPanierAddressModifierVC.isPushFromLoginPage=NO;
    }
    monPanierAddressModifierVC.isPushedFromMonAddresses=YES;
    monPanierAddressModifierVC.isPushedFromRegisterPage=NO;
    monPanierAddressModifierVC.userModifyAddress = addressModifyDictionary;
    
    [self.navigationController pushViewController:monPanierAddressModifierVC animated:YES];
}


#pragma mark - gotoPaymentView

- (void)gotoPaymentView{
    
    SSPaymentDetailViewController *paymentDetailVC = nil;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        paymentDetailVC = [[SSPaymentDetailViewController alloc]initWithNibName:@"SSPaymentDetailViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        paymentDetailVC = [[SSPaymentDetailViewController alloc]
                           initWithNibName:@"SSPaymentDetailViewController"
                           bundle:nil];
    }
    if(isPushFromLoginPage==YES){
        paymentDetailVC.isPushFromLoginPageYES=YES;
    }
    else{
        paymentDetailVC.isPushFromLoginPageYES=NO;
        paymentDetailVC.isPushFromAddressViewPageYES=YES;
    }
    paymentDetailVC.currentUserDeliveryId = currentUserDeliveryId;
    [self.navigationController pushViewController:paymentDetailVC animated:YES];
    [COMMON removeLoadingIcon];
}


#pragma mark - Button Actions

- (void)addressVCModifier:(UIButton *)sender {
    
    NSUInteger index = sender.tag;
    NSMutableDictionary *tempDict=[NSMutableDictionary new];
    tempDict=[_addressArray objectAtIndex:index];
    
    //[self getUserListAddress];//:tempDict];
   
    currentUserDeliveryId =[tempDict valueForKey:@"id"];
    if(currentUserDeliveryId==nil){
        currentUserDeliveryId=@"";
    }
    if(isPushedFromMonAccountPage==YES){
        [self gotoAddAddressView:tempDict];
    }
    else{
//        if(isCommanderClicked==YES){
//            [self checkForPaymentProcess];
//        }
        NSMutableArray *cartItems = [NSMutableArray new];
        cartItems = [SHOPPINGCART getCartItems];
        if([cartItems count]!=0){
            [self checkForPaymentProcess:tempDict];
        }
    }
    
    
    
    
    //    if([cartItems count]!=0){
//        [COMMON showLoadingIcon:self];
//        [self gotoPaymentView];
//    }

}
-(void)checkForPaymentProcess:(NSMutableDictionary *) currectSelectDict{
    
    NSMutableArray *cartItems = [NSMutableArray new];
    cartItems = [SHOPPINGCART getCartItems];
    NSString *payment = [SHOPPINGCART getFormattedCartTotalCost];
    NSString *codePromoString = [SHOPPINGCART getCouponCode];
    cartItems = [SHOPPINGCART getCartItems];
    
    if([cartItems count]!=0){
        if(isStoreApp){
            [COMMON showLoadingIcon:self];
             [self redirectToAddressDisplayPageForStorApp:[currectSelectDict mutableCopy]];
            /*
            if(isPushedFromAddressDisplayPage==YES){
                [COMMON setTabBarSelection:NO];
                [self gotoPaymentForStoreAppWithMethod:STORE_APP_PAYMENTNAME];
            }
            else{
                 [self redirectToAddressDisplayPageForStorApp:[currectSelectDict mutableCopy]];
            }*/
        }
        else{
            if(![payment isEqualToString:@"0,00 €"]){
                [COMMON showLoadingIcon:self];
                [self gotoPaymentView];
            }
            if(codePromoString!=nil && [payment isEqualToString:@"0,00 €"]){
                [COMMON showLoadingIcon:self];
                [self gotoPaymentView];
            }
        }
        
        
    }

}

- (void)gotoPaymentAction:(UITapGestureRecognizer *)tap {
    UITextView *currentView  = (UITextView *) tap.view;
    
    NSInteger selectedIndex = currentView.tag;

    NSMutableDictionary *tempDict=[NSMutableDictionary new];
    tempDict=[_addressArray objectAtIndex:selectedIndex];
    
    currentUserDeliveryId =[tempDict valueForKey:@"id"];
    if(currentUserDeliveryId==nil){
        currentUserDeliveryId=@"";
    }
//    if(isCommanderClicked==YES){
//        [self checkForPaymentProcess];
//    }
    NSMutableArray *cartItems = [NSMutableArray new];
    cartItems = [SHOPPINGCART getCartItems];
    if([cartItems count]!=0){
        [self checkForPaymentProcess:tempDict];
    }
    
}
- (void)tableSeletedActionToPayment{
//    if(isCommanderClicked==YES){
//        [self checkForPaymentProcess];
//    }

//    if([cartItems count]!=0){
//        [COMMON showLoadingIcon:self];
//        [self gotoPaymentView];
//    }
}


#pragma mark- show and hide TabBar

- (void) showTabBar
{
    [appDelegate showTabBar];
}
- (void) hideTabbar {
    
    [appDelegate hideTabBar];
}
-(void)addressRetour:(id)sender
{    
//    //[appDelegate.tabBarController willMoveToParentViewController:nil];
//    
//    //[appDelegate.tabBarController removeFromParentViewController];
//    
//    if ([_loginString isEqualToString:@"loginString"]) {
//
//        //[appDelegate.tabBarController selectTabAtIndex:0];
//    }
//    else {
    
        //[self showTabBar];
    
    if(isPushFromAccountPage==YES){
        [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
        [self.navigationController popViewControllerAnimated:YES];
        [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
        [self.navigationController setNavigationBarHidden:NO];
    }
//    if(isPushedFromRegisterPage==YES){
//        [self gotoUserAccountPage];
//    }
    else{
        [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
        [self.navigationController popViewControllerAnimated:YES];
        [self.navigationController setNavigationBarHidden:YES];
    }
   // }
}
-(void)gotoUserAccountPage{
    
    SSAccountViewController *accountViewController = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        accountViewController = [[SSAccountViewController alloc]initWithNibName:@"SSAccountViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        accountViewController = [[SSAccountViewController alloc]
                                 initWithNibName:@"SSAccountViewController"
                                 bundle:nil];
    }
    [self.navigationController pushViewController:accountViewController animated:NO];
    
}


-(void)addressModifierAction:(id)sender
{
    [noItemsLabel removeFromSuperview];
    SSMonPanierAddressModifierViewController *monPanierAddressModifierVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierAddressModifierVC = [[SSMonPanierAddressModifierViewController alloc]initWithNibName:@"SSMonPanierAddressModifierViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierAddressModifierVC = [[SSMonPanierAddressModifierViewController alloc]
                                      initWithNibName:@"SSMonPanierAddressModifierViewController"
                                      bundle:nil];
    }
    
    monPanierAddressModifierVC.addressArray = _addressArray;
    monPanierAddressModifierVC.isPushedFromMonAddresses = YES;
    
    [self.navigationController pushViewController:monPanierAddressModifierVC animated:YES];
}

//- (IBAction)addAddress:(id)sender {
//    
//    [COMMON showLoadingIcon:self];
//    
//    if(isCommanderClicked==YES){
//        [self checkForPaymentProcess];
//    }
//}



#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _addressArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
        
    return 300;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier;
    
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
//        CellIdentifier = @"SSMonPanierAddressCustomCelliPad";
//    else
        CellIdentifier = @"SSMonPanierAddressCustomCell";
    
    
    SSMonPanierAddressCustomCell *cell = (SSMonPanierAddressCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    userAddress = [_addressArray objectAtIndex:indexPath.row];
    
    NSString *daterString;
    
    daterString = [userAddress valueForKey:@"alias"];
    
    cell.titleLabel.text = daterString;
    
    NSString *addressStr = [userAddress valueForKey:@"address"];
    addressStr = [addressStr stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    cell.addressTextView.text = addressStr;
    
    cell.addressTextView.tag = indexPath.row;
    [cell.addressTextView setFont:[COMMON getResizeableFont:FuturaStd_Book(17)]];
    UITapGestureRecognizer *addressTextView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoPaymentAction:)];
    [addressTextView setNumberOfTapsRequired:1];
    [cell.addressTextView addGestureRecognizer:addressTextView];
    
    //ADD BUTTON
    if(isPushedFromMonAccountPage==YES){
        [cell.addButton setTitle:NSLocalizedString(@"Mettre à jour", @"") forState:UIControlStateNormal];
    }
    else{
        [cell.addButton setTitle:NSLocalizedString(@"Choisir cette adresse", @"") forState:UIControlStateNormal];
    }
    
    cell.addButton.tag = indexPath.row;
    [cell.addButton addTarget:self action:@selector(addressVCModifier:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   // [self addressVCModifier:nil];
   
    NSMutableDictionary *tempDict=[NSMutableDictionary new];
    tempDict=[_addressArray objectAtIndex:indexPath.row];
    
    currentUserDeliveryId =[tempDict valueForKey:@"id"];
    if(currentUserDeliveryId==nil){
        currentUserDeliveryId=@"";
    }
    
    //[self tableSeletedActionToPayment];

}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}
-(void)redirectToAddressDisplayPageForStorApp:(NSMutableDictionary *)currentSelectDict{
    
    [COMMON removeLoadingIcon];
    
    id userAddressArray = [NSMutableArray new];
    [userAddressArray addObject:[currentSelectDict mutableCopy]];
    
    SSMonPanierAddressDisplayViewController *monPanierDisplayAddressVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierDisplayAddressVC = [[SSMonPanierAddressDisplayViewController alloc]initWithNibName:@"SSMonPanierAddressDisplayViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierDisplayAddressVC = [[SSMonPanierAddressDisplayViewController alloc]
                              initWithNibName:@"SSMonPanierAddressDisplayViewController"
                              bundle:nil];
    }
    
    monPanierDisplayAddressVC.userAddress = [userAddressArray mutableCopy];
    monPanierDisplayAddressVC.isCommanderClicked =NO;
    monPanierDisplayAddressVC.isSelectedFromAddressListTable =YES;
    
    if(isPushedFromMonAccountPage==YES){
        [self.navigationController pushViewController:monPanierDisplayAddressVC animated:YES];
    }
    else{
        [[NSNotificationCenter defaultCenter]postNotificationName:@"updateAddress" object:[userAddressArray mutableCopy]];
        [self.navigationController popViewControllerAnimated:YES];

    }
        
    [COMMON removeLoadingIcon];
   
}

#pragma mark- web service function
- (void) gotoPaymentForStoreAppWithMethod:(NSString *) selectedPaymentMethod {
    [self.view setUserInteractionEnabled:NO];
    //sendCount++;
    NSString * deliveryAddressID = currentUserDeliveryId;
    if(deliveryAddressID==nil){
        deliveryAddressID=@"";
    }
    
    [webService gotoPaymentWithProducts:[SHOPPINGCART getCartProductsInJsonFormat]
                      withPaymentMethod:selectedPaymentMethod
                       forUserSecureKey:[COMMON getCustomerSecureKey]
                  userDeliveryAddressID: deliveryAddressID
                                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                    NSLog(@"Payment Response = %@",responseObject);
                                    if ([responseObject isKindOfClass:[NSDictionary class]] && [responseObject objectForKey:@"paymentintermediate"]!= NULL
                                        ) {
                                        
                                        NSString *htmlString = [responseObject objectForKey:@"paymentintermediate"];
                                        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\\t" withString:@""];
                                        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                                        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                                        
                                        [self loadWebViewForStoreApp:htmlString];
                                        
                                        
                                    } else {
                                        if ([responseObject isKindOfClass:[NSDictionary class]] && [responseObject objectForKey:@"error"] != NULL) {
                                            [SSAppCommon showSimpleAlertWithMessage:[responseObject objectForKey:@"error"]];
                                            [COMMON removeLoadingIcon];
                                            [self.view setUserInteractionEnabled:YES];
                                            [COMMON setTabBarSelection:YES];
                                        }
                                    }
                                } failure:^(AFHTTPRequestOperation *operation, id error) {
                                    
                                    [COMMON removeLoadingIcon];
                                    [self.view setUserInteractionEnabled:YES];
                                    [COMMON setTabBarSelection:YES];
                                }];
}

-(void) loadWebViewForStoreApp:(NSString*) htmlString{
    
    UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    webview.delegate= self;
    [webview setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:webview];
    [webview setHidden:YES];
    [webview loadHTMLString:htmlString baseURL:webService.baseURL];
}

#pragma mark - Webview Delegates

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"ERROR");
    [COMMON setTabBarSelection:YES];
}

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest: (NSURLRequest *) request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSURL *myRequestedUrl= [request URL];
    NSString *urlString = myRequestedUrl.absoluteString;
    
    if (urlString != NULL && [urlString rangeOfString:@"mode=success"].location != NSNotFound) {
        NSLog(@"Started loading");
        NSLog(@"URL String in Start Load = %@",urlString);
        NSLog(@"URL String in Finished Load = %@",urlString);
        
        NSArray *id_order = [urlString componentsSeparatedByString:@"id_order="];
        NSArray *id_cart  = [urlString componentsSeparatedByString:@"id_cart="];
        
        NSString *referenceNumber;
        
        if (id_order!=NULL && [id_order count]>1) {
            referenceNumber = [id_order objectAtIndex:1];
        } else if (id_cart!=NULL && [id_cart count]>1) {
            referenceNumber = [id_cart objectAtIndex:1];
        }
        orderNumber=[[referenceNumber componentsSeparatedByString:@"&"] objectAtIndex:0];
        
        NSLog(@"Order Number==%@",orderNumber);
        
        [self gotoOrderView];
        [webView stopLoading];
        return NO;
    }
    
    if (urlString != NULL && [urlString rangeOfString:@"mode=failure"].location != NSNotFound) {
        [self.navigationController popViewControllerAnimated:YES];
        [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
        
        [self.navigationController setNavigationBarHidden:YES];
        [webView stopLoading];
        [COMMON setTabBarSelection:YES];
        return NO;
    }
    
    if (urlString != NULL && ([urlString rangeOfString:BASE_URL].location != NSNotFound || [urlString rangeOfString:@"about:blank"].location != NSNotFound)) {
        [COMMON showLoadingIcon:self];
        [COMMON setTabBarSelection:YES];
    }
    
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    /*if (webView.isLoading)
     return;*/
    
}

- (void) gotoOrderView {
    [self.view setUserInteractionEnabled:YES];
    SSOrderConfirmationViewController *orderViewController = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        orderViewController = [[SSOrderConfirmationViewController alloc]
                               initWithNibName:@"SSOrderConfirmationViewControlleriPad"
                               bundle:[NSBundle mainBundle]];
    }
    else {
        orderViewController = [[SSOrderConfirmationViewController alloc]
                               initWithNibName:@"SSOrderConfirmationViewController"
                               bundle:nil];
    }
    
    [self.navigationController pushViewController:orderViewController animated:YES];
    [COMMON removeLoadingIcon];
    [COMMON setTabBarSelection:YES];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"Finished loading");
    [self performSelector:@selector(removeLoaderAfterTwoSec) withObject:self afterDelay:8];
    //[COMMON removeLoadingIcon];
}
#pragma mark - removeLoaderAfterTwoSec
-(void)removeLoaderAfterTwoSec{
    [COMMON removeLoadingIcon];
}


@end
