//
//  SSMonPanierAddressCustomCell.m
//  Seidensticker
//
//  Created by OCS iOS Developer on 27/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSMonPanierAddressCustomCell.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSCustomNavigation.h"

@implementation SSMonPanierAddressCustomCell

@synthesize titleLabel,addButton,addressTextView,sepratorLabel;

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self setUpInitialValues];
}

- (void)setUpInitialValues {
    
    
    self.mainVIew.layer.borderWidth = 1.0f;
    
    self.mainVIew.layer.borderColor = [UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f blue:182.0f/255.0f alpha:1.0f].CGColor;
    
    
    [titleLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    [titleLabel setTextColor:[UIColor blackColor]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
    
    sepratorLabel.backgroundColor = [UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f blue:182.0f/255.0f alpha:1.0f];
    
    
    [addButton.titleLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    [addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    addButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [addButton setBackgroundColor:[UIColor blackColor]];
    
}


@end
