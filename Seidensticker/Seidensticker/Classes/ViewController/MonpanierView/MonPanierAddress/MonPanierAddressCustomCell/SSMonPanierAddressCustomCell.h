//
//  SSMonPanierAddressCustomCell.h
//  Seidensticker
//
//  Created by Admin on 27/07/16.
//  Copyright © 2016 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSMonPanierAddressCustomCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *mainVIew;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) IBOutlet UILabel *sepratorLabel;

@property (strong, nonatomic) IBOutlet UITextView *addressTextView;

@property (strong, nonatomic) IBOutlet UIButton *addButton;

@end
