//
//  SSMonPanierAddressViewController.h
//  Seidensticker
//
//  Created by OCS iOS Developer on 07/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SSMonPanierAddressViewController : UIViewController
{
    SSMonPanierAddressViewController *monPanierAddressVC;
}

@property (strong, nonatomic) NSDictionary *userAddress;

@property (strong, nonatomic) NSMutableDictionary *addressDictionary;

@property (strong, nonatomic)  IBOutlet UIButton *addressButton, *suivantButton;

@property (strong, nonatomic) IBOutlet UIView *mainView;

@property (strong, nonatomic) IBOutlet UILabel *addressLabel;

@property (strong, nonatomic) IBOutlet UILabel *sepLabel;

@property (strong, nonatomic) IBOutlet UITextView *addressTextView;

@property (strong, nonatomic) IBOutlet UITableView *addressTableView;

//@property(nonatomic,assign) BOOL isPresent;

@property(nonatomic,retain)id controller;

@property(nonatomic,retain) NSMutableDictionary *addresslist;

@property(nonatomic,retain) NSString *loginString;

@property(nonatomic, strong) NSArray *addressArray;

@property(nonatomic,assign) BOOL isPushFromAccountPage;
@property(nonatomic,assign) BOOL isPushFromLoginPage;
@property(nonatomic,assign)BOOL isPushedFromRegisterPage;

@property(nonatomic,assign) BOOL isCommanderClicked;

@property(nonatomic,assign) BOOL isPushedFromMonAccountPage;

@property(nonatomic,assign)BOOL isPushedFromAddressDisplayPage;

@property(nonatomic,retain) NSString *orderNumber;

- (IBAction)modifier:(id)sender;

//- (IBAction)gotoPayment:(id)sender;

//@property (strong, nonatomic) IBOutlet UIScrollView *addressScrollView;
//
//@property (strong, nonatomic) IBOutlet UITableView *addressTableView;
// 
//@property (strong, nonatomic) IBOutlet UILabel *addAddressLabel;
//- (IBAction)addAddress:(id)sender;

//@property (strong, nonatomic) IBOutlet UIView *addAddressView;
//
//@property (strong, nonatomic) IBOutlet UIView *addressModifierView;
//
////@property (strong, nonatomic)  IBOutlet UIButton *addAddressButton;
//@property (strong, nonatomic) IBOutlet UIButton *addAddress;
//@property (strong, nonatomic) IBOutlet UIButton *addAddressButton;

@end
