 //
//  SSMonPanierAddressDisplayViewController.m
//  Seidensticker
//
//  Created by OCS iOS Developer on 07/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSAppDelegate.h"
#import "SSMonPanierAddressDisplayViewController.h"
#import "SSMonPanierAddressViewController.h"
#import "SSMonPanierAddressModifierViewController.h"
#import "SSAppCommon.h"
#import "SSConfig.h"
#import "SSCustomNavigation.h"
#import "SSPaymentDetailViewController.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#import "NSString+validations.h"
#import "NMBottomTabBarController.h"
#import "SSShoppingCart.h"


#pragma mark- interface
@interface SSMonPanierAddressDisplayViewController ()<UITextFieldDelegate,UIWebViewDelegate>
{
    SSWebService                *webService;
    NMBottomTabBarController    *bottomTabBar;
    NSMutableArray              *userArray;
    NSString *currentUserDeliveryId;
    int sendCount;
   NSString *currentOrderNumber;
}
@property(nonatomic,retain) NSString *orderNumber;


@end
#pragma mark- implementation

@implementation SSMonPanierAddressDisplayViewController
@synthesize addressLabel,addressTextView,addressButton;
@synthesize addressDictionary, addresslist;
@synthesize userAddress;
@synthesize isPushFirstTimeLoginPage;
@synthesize orderNumber;

@synthesize isCommanderClicked,isSelectedFromAddressListTable;

- (void)viewDidLoad {
    [super viewDidLoad];
   
    webService = [[SSWebService alloc] init];
    self.addressButton.layer.borderWidth=1;
    self.addressTextView.layer.borderWidth=1;
    UITapGestureRecognizer *textBox = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoPaymentActionTapGesture:)];
    [textBox setNumberOfTapsRequired:1];
    [addressTextView addGestureRecognizer:textBox];
    [addressTextView setUserInteractionEnabled:YES];
    [self.addressButton.titleLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(13)]];
    [self.addressLabel setText:NSLocalizedString(@"Votre adresse de livraison", nil)];
    //[self hideTabbar];
    if(IS_IPHONE6_Plus){
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    [self.navigationController.navigationBar addSubview:navBorder];
    }

    if(isStoreApp){
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            _addressLabelYPosition.constant  = 80;
        }
        else{
            _addressLabelYPosition.constant  = 70;
        }
        [self paymentPrice];
    }
    else{
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            _addressLabelYPosition.constant  = 50;
        }
    }
    
    userArray = [[NSMutableArray alloc]init];
}
#pragma mark- paymentPrice
-(void)paymentPrice{
    
    CGFloat Ypos = 20;
    
    CGFloat xpos = addressLabel.frame.origin.x;
    
    UILabel *paymentTotalPrice = [[UILabel alloc]initWithFrame:CGRectMake(xpos, Ypos, (self.view.frame.size.width - (xpos * 2)), 40)];
    paymentTotalPrice.text = [COMMON getDecodedString:[NSString stringWithFormat:@"Total à régler :%@",[SHOPPINGCART getFormattedCartTotalCost]]];
    //paymentTotalPrice.text = @"Total à régler :";
    [paymentTotalPrice setBackgroundColor:[UIColor clearColor]];
    [paymentTotalPrice setTintColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f]];
    [paymentTotalPrice setFont:[COMMON getResizeableFont:FuturaStd_Book(15)]];
    
    [self.view addSubview:paymentTotalPrice];
}
-(void)updateAddress:(NSNotification *)notification{
    
     NSDictionary *cellData = [notification object];
    
     NSLog(@"cellData%@",cellData);
     userAddress = [[NSMutableDictionary alloc]init];
     userAddress = [(NSMutableDictionary*)cellData mutableCopy];
     isCommanderClicked = NO;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateAddress:) name:@"updateAddress" object:nil];

    [super viewWillAppear:animated];
    [COMMON TrackerWithName:@"Address View"];
    [self loadNavigation];
    
    NSDictionary *userDict = [[NSDictionary alloc]init];
    
    /*if(isStoreApp){
        if(isSelectedFromAddressListTable==YES){
           // [addressButton setHidden:YES];
        }
    }*/
    
    userDict = [COMMON getUserDetails];
    
    userArray = [[NSMutableArray alloc]init];
    if (userAddress == NULL) {
        [self getUserAddress];
    } else {
        [self getUserDetails];
    }
    [_suivantButton setHidden:YES];
    
    //NSString *payment = [SHOPPINGCART getFormattedCartTotalCost];
    NSMutableArray *cartItems = [NSMutableArray new];
    cartItems = [SHOPPINGCART getCartItems];
    if([cartItems count]!=0){
        [_suivantButton setHidden:NO];
    }
    
    [COMMON removeLoadingIcon];
    
}

#pragma mark- loadNavigation
- (void)loadNavigation
{
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController.navigationBar setTranslucent:NO];
    SSCustomNavigation *navigation = [[SSCustomNavigation alloc] initWithNibName:@"SSCustomNavigation" bundle:nil];
    navigation.view.tag=1001;
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    
    [navigation.view addSubview:navBorder];
    [self.navigationController.navigationBar addSubview:navigation.view];
    
    [navigation setTitleHidden:NO];
    [navigation setShopBtnHidden:NO];
    [navigation setShopLabelHidden:NO];
    [navigation setCatergoryShopBtnLabelHidden:YES];

    [navigation.titleLbl setText:NSLocalizedString(@"Adresse de livraison", @"")];
    [navigation.titleLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    
    if(isPushFirstTimeLoginPage==YES){
        [navigation setLeftHidden:YES];
        [navigation.navigationLeftLbl setHidden:YES];
    }
    else{
        [navigation setLeftHidden:NO];
        [navigation.navigationLeftLbl setHidden:NO];
        [navigation.navigationLeftLbl setText:NSLocalizedString(@"Retour", @"")];
        [navigation.navigationLeftLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
        [navigation.navigationLeftBtn addTarget:self
                                         action:@selector(retour:)
                               forControlEvents:UIControlEventTouchUpInside];
    }
    
    [self setLabel:navigation];
    
}
-(void)setLabel:(SSCustomNavigation*)navigation
{
    NSMutableArray *cartItems = [NSMutableArray new];
    cartItems = [SHOPPINGCART getCartItems];
    NSLog(@"cart values %@",[SHOPPINGCART getCartItems]);
    cartItems = [SHOPPINGCART getCartItems];
    
    NSInteger quantity = 0;
    
    
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }
    
    navigation.navigationShopLbl.text = [NSString stringWithFormat:@"%ld",(long)quantity];
    [navigation.navigationShopLbl setBackgroundColor:[UIColor clearColor]];
    navigation.navigationShopLbl.textColor = [UIColor blackColor];
    [navigation.navigationShopLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(8)]];
    [navigation.navigationShopLbl setTextAlignment:NSTextAlignmentCenter];
    
    [navigation.navigationShopBtn addTarget:self action:@selector(addressRightSideBtnAction:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)newStyleNArBar{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    UINavigationBar *newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, screen_width, 64.0)];
    [newNavBar setBackgroundColor:[UIColor clearColor]];
    
    [newNavBar setBarTintColor:[UIColor whiteColor]];
    newNavBar.tag=1001;
    UINavigationItem *newItem = [[UINavigationItem alloc] init];
    newItem.title = @"Adresse de livraison";
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                            NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)]}];
    [newNavBar pushNavigationItem:newItem animated:false];
    
    //if(IS_IPHONE6_Plus) {
        UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,64,screen_width,0.8)];
        [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
        
        [newNavBar addSubview:navBorder];
    //}
    UILabel *lblLefttitle = [UILabel new];
    [lblLefttitle setText:@"Retour"];
    lblLefttitle.textColor = [UIColor orangeColor];
    [lblLefttitle setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [lblLefttitle setTextAlignment:NSTextAlignmentLeft];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(retour:)];
    [lblLefttitle addGestureRecognizer:tap];
    lblLefttitle.userInteractionEnabled = YES;
    [lblLefttitle setBackgroundColor:[UIColor clearColor]];
    [lblLefttitle setFrame:CGRectMake(10,(newNavBar.frame.size.height/2)-5 , newNavBar.frame.size.width/3, 30)];
    [newNavBar addSubview:lblLefttitle];
    NSMutableArray *cartItems = [NSMutableArray new];
    cartItems = [SHOPPINGCART getCartItems];
    cartItems = [SHOPPINGCART getCartItems];
    NSInteger quantity = 0;
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"shop_active_icon"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(addressRightSideBtnAction:)forControlEvents:UIControlEventTouchUpInside];
    
    
    [button setFrame:CGRectMake(0, 0, 38, 38)];
    [button setBackgroundColor:[UIColor clearColor]];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(11, 15, 15,15)];
    [label setFont:[COMMON getResizeableFont:FuturaStd_Book(8)]];
    [label setText:[NSString stringWithFormat:@"%ld",(long)quantity]];
    [label setBackgroundColor:[UIColor clearColor]];
    label.textAlignment = NSTextAlignmentCenter;
    [label setTextColor:[UIColor blackColor]];
    
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(newNavBar.frame.size.width-55,(newNavBar.frame.size.height/2) -10 , 40, 40)];
    [backButtonView setBackgroundColor:[UIColor clearColor]];
    [button addSubview:label];
    [backButtonView addSubview:button];
    
    [newNavBar addSubview:backButtonView];
    //[self setLabel:newNavBar];
    [self.view addSubview:newNavBar];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark - Right Menu

-(void)addressRightSideBtnAction:(id)sender
{
    SSMonPanierViewController *monpanierViewController;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonpanierViewControlleriPad" bundle:[NSBundle mainBundle]];
        
        
    }
    else {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonPanierViewController" bundle:[NSBundle mainBundle]];
    }
    
    [self.navigationController pushViewController:monpanierViewController animated:YES];
}


#pragma mark - User  Details

- (NSString *) getFormattedAddress :(id) userAddressDict {
    NSString *strAddress = @"";
    
    NSString * firstNameString = [self checkNullValueInString:[userAddressDict objectForKey:@"firstname"]];
    if (firstNameString != NULL && ![firstNameString isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@%@",strAddress,firstNameString];
    }
    NSString * lastNameStr = [self checkNullValueInString:[userAddressDict objectForKey:@"lastname"]];
    if (lastNameStr != NULL && ![lastNameStr isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@ %@",strAddress,lastNameStr];
    }
    
    NSString * address1Str = [self checkNullValueInString:[userAddressDict objectForKey:@"address1"]];
    if (address1Str != NULL && ![address1Str isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@\n%@",strAddress,address1Str];
    }
    
    NSString * address2Str = [self checkNullValueInString:[userAddressDict objectForKey:@"address2"]];
    if (address2Str != NULL && ![address2Str isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@\n%@",strAddress,address2Str];
    }
    
    NSString * postCodeStr = [self checkNullValueInString:[userAddressDict objectForKey:@"postcode"]];
    if (postCodeStr != NULL && ![postCodeStr isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@\n%@",strAddress,postCodeStr];
    }
    
    NSString * cityStr =[self checkNullValueInString:[userAddressDict objectForKey:@"city"]];
    if (cityStr != NULL && ![cityStr isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@ %@",strAddress,cityStr];
    }
    
    NSString * countryNameStr = [self checkNullValueInString:[userAddressDict objectForKey:@"country_name"]];
    if (countryNameStr != NULL && ![countryNameStr isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@\n%@",strAddress,countryNameStr];
    }

    NSString * phoneStr = [self checkNullValueInString:[userAddressDict objectForKey:@"phone"]];
    if (phoneStr != NULL && ![phoneStr isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@\n%@",strAddress,phoneStr];
    }

    return strAddress;
}
#pragma mark - checkNullValueInString
-(NSString *)checkNullValueInString:(NSString *)currentString{
    
//    currentString ; //[NSString stringWithFormat:@"%@",string];
    if ((NSString *)[NSNull null] == currentString||currentString == nil) {
        currentString=@"";
    }
    return currentString;
}

-(void) getUserDetails{
      
      NSMutableArray * userAddressArray=[NSMutableArray new];
      userAddressArray = (NSMutableArray*)[userAddress mutableCopy];
      if([userAddressArray count]>=1){
          NSMutableDictionary * userAddressDict=[NSMutableDictionary new];
          userAddressDict = [[userAddressArray objectAtIndex:0] mutableCopy];
          currentUserDeliveryId =[userAddressDict valueForKey:@"id"];
          if(currentUserDeliveryId==nil){
              currentUserDeliveryId=@"";
          }
          self.addressTextView.text = [self getFormattedAddress:userAddressDict];
      }
      else{
          self.addressTextView.text =@"";
      }
    [self.addressTextView setEditable:NO];
     [self.addressTextView setSelectable:NO];
     [self.addressTextView setUserInteractionEnabled:YES];
      
    
      [self.addressTextView setFont:[COMMON getResizeableFont:FuturaStd_Book(17)]];
}

- (void) getUserAddress {
    [self gotoAddAddressView:userAddress];
}
- (void) getCurrentUserAddressByList{
    [self gotoListAddressView:userAddress];
}
#pragma mark- addAddressView
- (void) gotoAddAddressView:(NSMutableDictionary *) addressModifyDictionary{
    
    SSMonPanierAddressModifierViewController *monPanierAddressModifierVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierAddressModifierVC = [[SSMonPanierAddressModifierViewController alloc]initWithNibName:@"SSMonPanierAddressModifierViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierAddressModifierVC = [[SSMonPanierAddressModifierViewController alloc]
                                      initWithNibName:@"SSMonPanierAddressModifierViewController"
                                      bundle:nil];
    }
    if(isCommanderClicked==YES){
        monPanierAddressModifierVC.isCommanderClicked=YES;
    }
    else{
        monPanierAddressModifierVC.isCommanderClicked=NO;

    }
   
    monPanierAddressModifierVC.userModifyAddress = addressModifyDictionary;
    monPanierAddressModifierVC.isPushedFromRegisterPage=NO;
    monPanierAddressModifierVC.isPushedFromMonAddresses=NO;
    [self.navigationController pushViewController:monPanierAddressModifierVC animated:YES];
     [COMMON removeLoadingIcon];
    
    
    
}
-(void)gotoListAddressView:(NSMutableDictionary *) addressModifyDictionary{
    
    SSMonPanierAddressViewController *monPanierAddressVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierAddressVC = [[SSMonPanierAddressViewController alloc]initWithNibName:@"SSMonPanierAddressViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierAddressVC = [[SSMonPanierAddressViewController alloc]
                              initWithNibName:@"SSMonPanierAddressViewController"
                              bundle:nil];
    }
    if(isCommanderClicked==YES){
        monPanierAddressVC.isCommanderClicked=YES;
    }
    else{
        monPanierAddressVC.isCommanderClicked=NO;
    }
    if(isStoreApp){
        monPanierAddressVC.isPushedFromAddressDisplayPage=YES;
    }
    monPanierAddressVC.userAddress = userAddress;
    
    [self.navigationController pushViewController:monPanierAddressVC animated:YES];
}
#pragma mark - gotoPaymentView
- (void)gotoPaymentView{

    if(currentUserDeliveryId==nil){
        currentUserDeliveryId=@"";
    }
    SSPaymentDetailViewController *paymentDetailVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        paymentDetailVC = [[SSPaymentDetailViewController alloc]initWithNibName:@"SSPaymentDetailViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        paymentDetailVC = [[SSPaymentDetailViewController alloc]
                           initWithNibName:@"SSPaymentDetailViewController"
                           bundle:nil];
    }
    paymentDetailVC.isPushFromLoginPageYES=NO;
    paymentDetailVC.isPushFromAddressViewPageYES=NO;
    paymentDetailVC.currentUserDeliveryId = currentUserDeliveryId;
    [self.navigationController pushViewController:paymentDetailVC animated:YES];
    [COMMON removeLoadingIcon];
    
}

#pragma mark - Button Actions
- (IBAction)modifier:(id)sender {
    [COMMON showLoadingIcon:self];
    [self getCurrentUserAddressByList];
}
- (IBAction)gotoPayment:(id)sender {

    NSMutableArray *cartItems = [NSMutableArray new];
    cartItems = [SHOPPINGCART getCartItems];
    if([cartItems count]!=0){
         [self checkForPaymentMethodProcess];
    }
  
//    if(isCommanderClicked==YES){
//        [self checkForPaymentMethodProcess];
//    }
    
    
}
- (void)gotoPaymentActionTapGesture:(UITapGestureRecognizer *)tap {

//    if(isCommanderClicked==YES){
//        [self checkForPaymentMethodProcess];
//    }
    NSMutableArray *cartItems = [NSMutableArray new];
    cartItems = [SHOPPINGCART getCartItems];
    if([cartItems count]!=0){
        [self checkForPaymentMethodProcess];
    }

    
}
-(void)checkForPaymentMethodProcess{
    NSMutableArray *cartItems = [NSMutableArray new];
    NSString *payment = [SHOPPINGCART getFormattedCartTotalCost];
    NSString *codePromoString = [SHOPPINGCART getCouponCode];
    cartItems = [SHOPPINGCART getCartItems];
    if([cartItems count]!=0){
        if(isStoreApp){
            [COMMON setTabBarSelection:NO];
            [COMMON showLoadingIcon:self];
            [self gotoPaymentForStoreAppWithMethod:STORE_APP_PAYMENTNAME];
        }
        else{
            if(![payment isEqualToString:@"0,00 €"]){
                [COMMON showLoadingIcon:self];
                [self gotoPaymentView];
            }
            if(codePromoString!=nil && [payment isEqualToString:@"0,00 €"]){
                [COMMON showLoadingIcon:self];
                [self gotoPaymentView];
            }
        }
        
        
    }

}


#pragma mark- show and hide TabBar
- (void) showTabBar
{
    SSAppDelegate *appDelegate = (SSAppDelegate *) [UIApplication sharedApplication].delegate;
    [appDelegate showTabBar];
}
- (void) hideTabbar {
    SSAppDelegate *appDelegate = (SSAppDelegate *) [UIApplication sharedApplication].delegate;
    [appDelegate hideTabBar];
 
}
-(void)retour:(id)sender
{
//    SSAppDelegate *appDelegate = (SSAppDelegate *)[[UIApplication sharedApplication] delegate];
//    [[appDelegate monPanierViewController].monPanierTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
//    if (self.isPresent) {
//        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//    }
//    else{
//        [self showTabBar];
//        NSArray *array = [self.navigationController viewControllers];
//        [self.navigationController popToViewController:[array objectAtIndex:0] animated:YES];
//        [self.navigationController setNavigationBarHidden:YES];
//    }
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
    [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];

}

//- (IBAction)addAddress:(id)sender {
//    [COMMON showLoadingIcon:self];
//    [self gotoPaymentView];
//}

#pragma mark- web service function

- (void) gotoPaymentForStoreAppWithMethod:(NSString *) selectedPaymentMethod {
     [self.view setUserInteractionEnabled:NO];
    //sendCount++;
    NSString * deliveryAddressID = currentUserDeliveryId;
    if(deliveryAddressID==nil){
        deliveryAddressID=@"";
    }
    
    [webService gotoPaymentWithProducts:[SHOPPINGCART getCartProductsInJsonFormat]
                      withPaymentMethod:selectedPaymentMethod
                       forUserSecureKey:[COMMON getCustomerSecureKey]
                  userDeliveryAddressID: deliveryAddressID
                                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                    NSLog(@"Payment Response = %@",responseObject);
                                    if ([responseObject isKindOfClass:[NSDictionary class]] && [responseObject objectForKey:@"paymentintermediate"]!= NULL
                                        ) {
                                        
                                        NSString *htmlString = [responseObject objectForKey:@"paymentintermediate"];
                                        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\\t" withString:@""];
                                        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                                        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                                        [self loadWebViewForStoreApp:htmlString];
                                        
                                    } else {
                                        if ([responseObject isKindOfClass:[NSDictionary class]] && [responseObject objectForKey:@"error"] != NULL) {
                                            [SSAppCommon showSimpleAlertWithMessage:[responseObject objectForKey:@"error"]];
                                            [COMMON removeLoadingIcon];
                                            [COMMON setTabBarSelection:YES];
                                            [self.view setUserInteractionEnabled:YES];
                                        }
                                    }
                                } failure:^(AFHTTPRequestOperation *operation, id error) {
                                    [COMMON setTabBarSelection:YES];
                                    [COMMON removeLoadingIcon];
                                     [self.view setUserInteractionEnabled:YES];
                                }];
}

-(void) loadWebViewForStoreApp:(NSString*) htmlString{
    
    UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    webview.delegate= self;
    [webview setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:webview];
    [webview setHidden:YES];
    [webview loadHTMLString:htmlString baseURL:webService.baseURL];
}

#pragma mark - Webview Delegates

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"ERROR");
    [COMMON setTabBarSelection:YES];
}

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest: (NSURLRequest *) request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSURL *myRequestedUrl= [request URL];
    NSString *urlString = myRequestedUrl.absoluteString;
    
    if (urlString != NULL && [urlString rangeOfString:@"mode=success"].location != NSNotFound) {
        NSLog(@"Started loading");
        NSLog(@"URL String in Start Load = %@",urlString);
        NSLog(@"URL String in Finished Load = %@",urlString);
        
        NSArray *id_order = [urlString componentsSeparatedByString:@"id_order="];
        NSArray *id_cart  = [urlString componentsSeparatedByString:@"id_cart="];
        
        NSString *referenceNumber;
        
        if (id_order!=NULL && [id_order count]>1) {
            referenceNumber = [id_order objectAtIndex:1];
        } else if (id_cart!=NULL && [id_cart count]>1) {
            referenceNumber = [id_cart objectAtIndex:1];
        }
        orderNumber=[[referenceNumber componentsSeparatedByString:@"&"] objectAtIndex:0];
        
        NSLog(@"Order Number==%@",orderNumber);
        
        [self gotoOrderView];
        [webView stopLoading];
        return NO;
    }
    
    if (urlString != NULL && [urlString rangeOfString:@"mode=failure"].location != NSNotFound) {
        [self.navigationController popViewControllerAnimated:YES];
        [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
        
        [self.navigationController setNavigationBarHidden:YES];
        [webView stopLoading];
        return NO;
    }
    
    if (urlString != NULL && ([urlString rangeOfString:BASE_URL].location != NSNotFound || [urlString rangeOfString:@"about:blank"].location != NSNotFound)) {
        [COMMON showLoadingIcon:self];
    }
    
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    /*if (webView.isLoading)
     return;*/
    
}

- (void) gotoOrderView {
    [self.view setUserInteractionEnabled:YES];
    SSOrderConfirmationViewController *orderViewController = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        orderViewController = [[SSOrderConfirmationViewController alloc]
                               initWithNibName:@"SSOrderConfirmationViewControlleriPad"
                               bundle:[NSBundle mainBundle]];
    }
    else {
        orderViewController = [[SSOrderConfirmationViewController alloc]
                               initWithNibName:@"SSOrderConfirmationViewController"
                               bundle:nil];
    }
    orderViewController.addressDisplayOrderNumber = orderNumber;
    [self.navigationController pushViewController:orderViewController animated:YES];
    [COMMON removeLoadingIcon];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"Finished loading");
    [self performSelector:@selector(removeLoaderAfterTwoSec) withObject:self afterDelay:8];
    //[COMMON removeLoadingIcon];
}
#pragma mark - removeLoaderAfterTwoSec
-(void)removeLoaderAfterTwoSec{
    [COMMON removeLoadingIcon];
}

@end
