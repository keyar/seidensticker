//
//  SSMonPanierAddressDisplayViewController.h
//  Seidensticker
//
//  Created by OCS iOS Developer on 07/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSMonPanierAddressCustomCell.h"
#pragma mark- interface
@interface SSMonPanierAddressDisplayViewController : UIViewController

{
    SSMonPanierAddressDisplayViewController *monPanierDisplayAddressVC;
    
}

@property(nonatomic,assign)BOOL isPushFirstTimeLoginPage; 

@property (strong, nonatomic) NSMutableDictionary *userAddress;

@property (strong, nonatomic) NSDictionary *addressDictionary;

@property (strong, nonatomic)  IBOutlet UIButton *addressButton;

@property (strong, nonatomic)  IBOutlet UIButton *suivantButton;

@property (strong, nonatomic) IBOutlet UILabel *addressLabel;

@property (strong, nonatomic) IBOutlet UITextView *addressTextView;

@property(nonatomic,assign) BOOL isPresent;

@property(nonatomic,assign) BOOL isCommanderClicked;

@property(nonatomic,assign) BOOL isSelectedFromAddressListTable;


@property(nonatomic,retain)id controller;

@property(nonatomic,retain) NSMutableDictionary *addresslist;

- (IBAction)modifier:(id)sender;

- (IBAction)gotoPayment:(id)sender;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *addressLabelYPosition;
@property (strong, nonatomic) IBOutlet UILabel *paymentPriceLabel;
@property (strong, nonatomic) IBOutlet UIButton *modifierButton;


//@property (strong, nonatomic) IBOutlet UIScrollView *addressScrollView;
//
//@property (strong, nonatomic) IBOutlet UITableView *addressTableView;
// 
//@property (strong, nonatomic) IBOutlet UILabel *addAddressLabel;
//- (IBAction)addAddress:(id)sender;

//@property (strong, nonatomic) IBOutlet UIView *addAddressView;
//
//@property (strong, nonatomic) IBOutlet UIView *addressModifierView;
//
////@property (strong, nonatomic)  IBOutlet UIButton *addAddressButton;
//@property (strong, nonatomic) IBOutlet UIButton *addAddress;
//@property (strong, nonatomic) IBOutlet UIButton *addAddressButton;

@end
