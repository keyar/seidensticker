//
//  SSMonPanierAddressModifierViewController.h
//  Seidensticker
//
//  Created by OCS iOS Developer on 07/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSMonPanierAddressModifierViewCustomCell.h"
#import "BSKeyboardControls.h"
#pragma mark- interface
@interface SSMonPanierAddressModifierViewController : UIViewController<UITextFieldDelegate>
{
     NSMutableArray *userArray;
    IBOutlet SSMonPanierAddressModifierViewCustomCell *addressCell;
}
@property(nonatomic,retain)id controller;
@property (strong, nonatomic) IBOutlet UIScrollView *addressScrollView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)addAddress:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;

@property (strong, nonatomic) NSMutableDictionary *userModifyAddress;

@property (strong, nonatomic) NSDictionary *addressModifyDictionary;

@property (strong, nonatomic) NSArray *addressArray;

//@property(nonatomic,assign)BOOL isPresent;

@property(nonatomic,assign)BOOL isPushedFromMonAddresses;

@property(nonatomic,assign)BOOL isPushedFromRegisterPage;

@property(nonatomic,assign)BOOL isPushedFromMonPanierPage;

@property(nonatomic,assign)BOOL isPushedFromLoginPage;



@property(nonatomic,assign) BOOL isCommanderClicked;

@property (nonatomic,strong) IBOutlet UIButton * suiventButton;

@property(strong,nonatomic) NSMutableArray *cartItems;
@property(nonatomic,assign) BOOL isPushFromAccountPage;
@property(nonatomic,assign) BOOL isPushFromLoginPage;
@end
