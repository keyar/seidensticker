//
//  SSMonPanierAddressModifierViewCustomCell.h
//  Seidensticker
//
//  Created by OCS iOS Developer on 07/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#pragma mark- interface
@interface SSMonPanierAddressModifierViewCustomCell : UITableViewCell <UITextFieldDelegate>
{
    UIView *cellView;
}
@property (nonatomic,retain) IBOutlet UITextField *addressTextField;

@property (strong, nonatomic) UITextField *textField;


@end
