//
//  SSMonPanierAddressModifierViewCustomCell.m
//  Seidensticker
//
//  Created by OCS iOS Developer on 07/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSMonPanierAddressModifierViewCustomCell.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSCustomNavigation.h"
#pragma mark- implementation
@implementation SSMonPanierAddressModifierViewCustomCell

- (void)awakeFromNib {
    // Initialization code
    self.addressTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                   initWithString:@""
                                                   attributes:@{
                                                                NSFontAttributeName:[COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                                                               }];
   

}
#pragma mark- Frame
- (void)setFrame:(CGRect)frame {
    
    frame.origin.y += 5;
    frame.size.height -=5 *3;
    [super setFrame:frame];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
