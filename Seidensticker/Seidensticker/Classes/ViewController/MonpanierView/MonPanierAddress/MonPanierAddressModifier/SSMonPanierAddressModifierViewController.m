//
//  SSMonPanierAddressModifierViewController.m
//  Seidensticker
//
//  Created by OCS iOS Developer on 07/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSMonPanierAddressModifierViewController.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSCustomNavigation.h"
#import "SSPaymentDetailViewController.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#import "NSString+validations.h"
#import "SSAppDelegate.h"
#import "SSShoppingCart.h"
#import "SSAccountViewController.h"
#import "SSMonPanierAddressViewController.h"
#import "SSMonPanierAddressDisplayViewController.h"


#pragma mark - interface
@interface SSMonPanierAddressModifierViewController ()<BSKeyboardControlsDelegate,UITextFieldDelegate ,UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate>
{
    SSWebService            *webService;
    NSString                *strFirstName;
    NSString                *strLastName;
    NSString                *strAddress;
    NSString                *strPostCode;
    NSString                *strVille;
    NSString                *strTelephone;
    NSString                *strAlias;
    BOOL                    is_maodify;
    NSString *currentUserDeliveryId;

}
@property(nonatomic,retain) NSString *orderNumber;

@end
#pragma mark - implementation
@implementation SSMonPanierAddressModifierViewController
@synthesize addressModifyDictionary;
@synthesize userModifyAddress;
@synthesize cartItems,isPushedFromMonAddresses;
@synthesize isPushedFromRegisterPage;
@synthesize isPushedFromMonPanierPage;
@synthesize isPushFromAccountPage,isPushFromLoginPage;
@synthesize isCommanderClicked;
@synthesize orderNumber;

- (void)viewDidLoad {
    [super viewDidLoad];
    
   // [self hideTabbar];
    webService = [[SSWebService alloc] init];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [COMMON TrackerWithName:@"Address Modifier View"];
    [self loadNavigation];
    [COMMON showLoadingIcon:self];
    
    if(userModifyAddress == NULL){
        
        [self initializeRegisterArray];
        
        [COMMON removeLoadingIcon];
        
    }
    else{
        [self initializeAddressArray];
        
        [COMMON removeLoadingIcon];
    }
    
    if(IS_IPHONE6_Plus){
        
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    [self.navigationController.navigationBar addSubview:navBorder];
        
    }
    [_tableView setScrollEnabled:YES];
    
//    [_suiventButton setHidden:NO];
//    
//    NSMutableArray *cartProItems = [NSMutableArray new];
//    cartProItems = [SHOPPINGCART getCartItems];
//    if([cartProItems count]==0){
//        [_suiventButton setHidden:YES];
//        
//    }
   
}

#pragma mark - loadNavigation

- (void)loadNavigation
{
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController.navigationBar setTranslucent:NO];
    SSCustomNavigation *navigation = [[SSCustomNavigation alloc] initWithNibName:@"SSCustomNavigation" bundle:nil];
    navigation.view.tag = 1001;
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    
    [navigation.view addSubview:navBorder];
    [self.navigationController.navigationBar addSubview:navigation.view];
    [navigation setLeftHidden:NO];
    [navigation setTitleHidden:NO];
    [navigation setShopBtnHidden:NO];
    [navigation setShopLabelHidden:NO];
    [navigation setCatergoryShopBtnLabelHidden:YES];

    [navigation.titleLbl setText:NSLocalizedString(@"Adresse de livraison", @"")];
    [navigation.titleLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    
    [navigation.navigationLeftLbl setText:NSLocalizedString(@"Retour", @"")];
    [navigation.navigationLeftLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [navigation.navigationLeftBtn addTarget:self
                                     action:@selector(addModifierRetour:)
                           forControlEvents:UIControlEventTouchUpInside];
    [self setLabel:navigation];
    
}
-(void)setLabel:(SSCustomNavigation*)navigation
{
    NSLog(@"cart values %@",[SHOPPINGCART getCartItems]);
    cartItems = [SHOPPINGCART getCartItems];
    
    NSInteger quantity = 0;
    
    
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }
    
    navigation.navigationShopLbl.text = [NSString stringWithFormat:@"%ld",(long)quantity];
    [navigation.navigationShopLbl setBackgroundColor:[UIColor clearColor]];
    navigation.navigationShopLbl.textColor = [UIColor blackColor];
    [navigation.navigationShopLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(8)]];
    [navigation.navigationShopLbl setTextAlignment:NSTextAlignmentCenter];
    
    [navigation.navigationShopBtn addTarget:self action:@selector(addressModiCartBtnAction:) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark - Right Menu

-(void)addressModiCartBtnAction:(id)sender
{
    SSMonPanierViewController *monpanierViewController;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonpanierViewControlleriPad" bundle:[NSBundle mainBundle]];
        
        
    }
    else {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonPanierViewController" bundle:[NSBundle mainBundle]];
    }
    
    [self.navigationController pushViewController:monpanierViewController animated:YES];
}


#pragma mark - initialize the array for Register

-(void)initializeRegisterArray{
    
    NSDictionary *userDict = [[NSDictionary alloc]init];
    
    userDict = [COMMON getUserDetails];
    currentUserDeliveryId = [userDict valueForKey:@"id"];
    if(currentUserDeliveryId==nil){
        currentUserDeliveryId=@"";
    }
    
    
    strFirstName = @"";
    strLastName  = @"";
    strAddress   = @"";
    strPostCode  = @"";
    strVille     = @"";
    strTelephone = @"";
    strAlias     = @"";

    if ([userDict valueForKey:@"firstname"] != NULL) {
        strFirstName = [userDict valueForKey:@"firstname"];
    }
    if ([userDict valueForKey:@"lastname"] != NULL) {
        strLastName = [userDict valueForKey:@"lastname"];
    }
    if ([userDict valueForKey:@"address1"] != NULL) {
        strAddress = [userDict valueForKey:@"address1"];
    }
    if ([userDict valueForKey:@"postcode"] != NULL) {
        strPostCode = [userDict valueForKey:@"postcode"];
    }
    if ([userDict valueForKey:@"city"] != NULL) {
        strVille = [userDict valueForKey:@"city"];
    }
    if ([userDict valueForKey:@"phone"] != NULL) {
        strTelephone = [userDict valueForKey:@"phone"];
    }
    //new change
    
//    if ([userDict valueForKey:@"alias"] != NULL) {
//        strAlias = [userDict valueForKey:@"alias"];
//    }

    [self initializeUserArray];
    
}

#pragma mark - initialize the array for Address List

-(void)initializeAddressArray{
    
    strFirstName = @"";
    strLastName  = @"";
    strAddress   = @"";
    strPostCode  = @"";
    strVille     = @"";
    strTelephone = @"";
    strAlias     = @"";

    
    if(userModifyAddress!=nil){
        currentUserDeliveryId = [NSString stringWithFormat:@"%@",[userModifyAddress valueForKey:@"id"]];
    }
    if(currentUserDeliveryId==nil){
        currentUserDeliveryId=@"";
    }
    if ([userModifyAddress valueForKey:@"firstname"] != NULL) {
        strFirstName = [userModifyAddress valueForKey:@"firstname"];
    }
    if ([userModifyAddress valueForKey:@"lastname"] != NULL) {
        strLastName = [userModifyAddress valueForKey:@"lastname"];
    }
    if ([userModifyAddress valueForKey:@"address1"] != NULL) {
        strAddress = [userModifyAddress valueForKey:@"address1"];
    }
    if ([userModifyAddress valueForKey:@"postcode"] != NULL) {
        strPostCode = [userModifyAddress valueForKey:@"postcode"];
    }
    if ([userModifyAddress valueForKey:@"city"] != NULL) {
        strVille = [userModifyAddress valueForKey:@"city"];
    }
    if ([userModifyAddress valueForKey:@"phone"] != NULL) {
        strTelephone = [userModifyAddress valueForKey:@"phone"];
    }
    //new
//    if ([userModifyAddress valueForKey:@"alias"] != NULL) {
//        strAlias = [userModifyAddress valueForKey:@"alias"];
//    }

    [self initializeUserArray];
    
}
#pragma mark - initializeUserArray for Register and Address List

-(void) initializeUserArray{
    
    userArray = [[NSMutableArray alloc]init];
    
    [userArray insertObject:[[NSMutableArray alloc]initWithObjects:
                             [NSMutableDictionary dictionaryWithObjectsAndKeys:strFirstName,@"title",@"*Prénom...",@"placeholder", nil],
                             [NSMutableDictionary dictionaryWithObjectsAndKeys:strLastName,@"title",@"*Nom...",@"placeholder", nil],
                             [NSMutableDictionary dictionaryWithObjectsAndKeys:strAddress,@"title",@"*Adresse...",@"placeholder", nil],
                             [NSMutableDictionary
                              dictionaryWithObjectsAndKeys:strPostCode,@"title",@"*Code postal...",@"placeholder", nil],
                             [NSMutableDictionary dictionaryWithObjectsAndKeys:strVille,@"title",@"*Ville...",@"placeholder", nil],
                             [NSMutableDictionary dictionaryWithObjectsAndKeys:@"France",@"title",@"*Country",@"placeholder", nil],
                             [NSMutableDictionary dictionaryWithObjectsAndKeys:strTelephone,@"title",@"Téléphone...",@"placeholder", nil],
                             nil] atIndex:0];
    
    //new alias removed
    // [NSMutableDictionary dictionaryWithObjectsAndKeys:strAlias,@"title",@"*Alias...",@"placeholder", nil],
    
    NSLog(@"personalArray=%@",userArray);
    
    [_tableView reloadData];
    
    [COMMON removeLoadingIcon];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(IS_IPHONE6 ||IS_IPHONE6_Plus){
    return 65;}
    else if(IS_IPHONE4 ||IS_IPHONE5 ){
        return 50;}
    else{
        return 90;}
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 7; //alias row removed return 8;
}

-(void)textFieldDidEndEditing:(UITextField*)textField
{
    id textFieldSuper = textField;
    
    while (![textFieldSuper isKindOfClass:[UITableViewCell class]]) {
        
        textFieldSuper = [textFieldSuper superview];
        
    }
    NSIndexPath *indexPath ;
    
    SSMonPanierAddressModifierViewCustomCell *cell;
    
    indexPath = [self.tableView indexPathForCell:(UITableViewCell *)textFieldSuper];
    
    cell = (SSMonPanierAddressModifierViewCustomCell *) [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section]];
    
    if(cell.addressTextField.text==nil){
        cell.addressTextField.text =@"";
    }
    
    [[[userArray objectAtIndex:0] objectAtIndex:indexPath.row] setObject:cell.addressTextField.text forKey:@"title"];
//    self.suiventButton.userInteractionEnabled = NO;
    
    //[self addAddress:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier=@"Cell";
    
    SSMonPanierAddressModifierViewCustomCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"SSMonPanierAddressModifierViewCustomCell" owner:self  options:nil];
        cell   = addressCell;
    }
    
    NSString *titleText;
    NSString *placeHolderText;
    titleText = [[[userArray valueForKey:@"title"]objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    placeHolderText =  [[[userArray valueForKey:@"placeholder"]objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    
    if([titleText isEqualToString:@""] || titleText == nil || titleText == NULL)
        cell.addressTextField.placeholder = placeHolderText;
    else
        cell.addressTextField.text = titleText;
    
    
        cell.addressTextField.delegate = self;
    
    if (indexPath.row == 0){
        [cell.addressTextField setKeyboardType:UIKeyboardTypeDefault];
        [cell.addressTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
        cell.addressTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        cell.addressTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    }
    if (indexPath.row == 1){
        [cell.addressTextField setKeyboardType:UIKeyboardTypeDefault];
        [cell.addressTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
        cell.addressTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        cell.addressTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;

                            }
    if (indexPath.row == 2){
        [cell.addressTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
        cell.addressTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        cell.addressTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        [cell.addressTextField setKeyboardType:UIKeyboardTypeDefault];
        
                            }
    if (indexPath.row == 3){
        
        [cell.addressTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
        cell.addressTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        cell.addressTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        cell.addressTextField.keyboardType = UIKeyboardTypeNumberPad;
                                }
    if (indexPath.row == 4){
        [cell.addressTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
        cell.addressTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        cell.addressTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
                            }
    if (indexPath.row == 5){
        [cell.addressTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
        cell.addressTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        cell.addressTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        [cell.addressTextField setTextColor:[UIColor blackColor]];
        [cell.addressTextField setEnabled:NO];
           }
    if (indexPath.row == 6){
       [cell.addressTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
        cell.addressTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        cell.addressTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        cell.addressTextField.keyboardType = UIKeyboardTypeNumberPad;
    }
    
    //new change alias row removing
//    if (indexPath.row == 7){
//        [cell.addressTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
//        cell.addressTextField.autocorrectionType = UITextAutocorrectionTypeNo;
//        cell.addressTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
//        cell.addressTextField.keyboardType = UIKeyboardTypeDefault;
//    }

    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
//    [cell.layer setBorderColor: [[UIColor colorWithRed:151.0f/255.0f
//                                                 green:151.0f/255.0f
//                                                  blue:151.0f/255.0f
//                                                 alpha:1.0f] CGColor]];
//    [cell.layer setBorderWidth: 1.0];
    
    UIView *leftBorder;
    UIView *rightBorder;
    UIView *topBorder;
    UIView *bottomBorder;
    leftBorder = [[UIView alloc] initWithFrame:CGRectMake(1, 0, 1.5, cell.frame.size.height)];
    rightBorder = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.size.width-2.0f, 0, 1.5, cell.frame.size.height)];
    topBorder = [[UIView alloc] initWithFrame:CGRectMake(1, 0,cell.frame.size.width,1.5)];
    bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height-2.0f, cell.frame.size.width, 1.5)];
    [cell addSubview:leftBorder];
    [cell addSubview:topBorder];
    [cell addSubview:rightBorder];
    [cell addSubview:bottomBorder];
    [leftBorder setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin];
    [rightBorder setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin];
    [topBorder setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
    [bottomBorder setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
    leftBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f
                                                  green:151.0f/255.0f
                                                   blue:151.0f/255.0f
                                                  alpha:1.0f];
    rightBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f
                                                  green:151.0f/255.0f
                                                   blue:151.0f/255.0f
                                                  alpha:1.0f];
    topBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f
                                                green:151.0f/255.0f
                                                 blue:151.0f/255.0f
                                                alpha:1.0f];
    bottomBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f
                                                   green:151.0f/255.0f
                                                    blue:151.0f/255.0f
                                                   alpha:1.0f];



    return cell;
}
#pragma mark- hide keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //hides keyboard when another part of layout was touched
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
}
#pragma mark - gotoPaymentView
- (void)gotoPaymentView{
    
    SSPaymentDetailViewController *paymentDetailVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        paymentDetailVC = [[SSPaymentDetailViewController alloc]initWithNibName:@"SSPaymentDetailViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        paymentDetailVC = [[SSPaymentDetailViewController alloc]
                           initWithNibName:@"SSPaymentDetailViewController"
                           bundle:nil];
    }
    if(isPushFromLoginPage==YES){
        paymentDetailVC.isPushFromLoginPageYES =YES;
    }
    else{
        paymentDetailVC.isPushFromLoginPageYES =NO;
        paymentDetailVC.isPushFromAddressViewPageYES =NO;
        paymentDetailVC.isPushFromAddressModifierPageYES =YES;
    }
    
    paymentDetailVC.currentUserDeliveryId = currentUserDeliveryId;
    [self.navigationController pushViewController:paymentDetailVC animated:YES];
     self.suiventButton.userInteractionEnabled =YES;
}

#pragma mark- Show and hide TabBar Method

- (void) showTabBar {
    
    SSAppDelegate *appDelegate = (SSAppDelegate *) [UIApplication sharedApplication].delegate;
    
    [appDelegate showTabBar];
}

- (void) hideTabbar {
    
    SSAppDelegate *appDelegate = (SSAppDelegate *) [UIApplication sharedApplication].delegate;
    
    [appDelegate hideTabBar];
}

#pragma mark - Retour Button Actions

-(void)addModifierRetour:(id)sender{
    
        //[self showTabBar];
    [self.navigationController popViewControllerAnimated:YES];
    [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
    [self.navigationController setNavigationBarHidden:YES];
    
}

#pragma mark - AddAddress Button Actions

- (IBAction)addAddress:(id)sender{
    
    NSArray *postPerArray = [[userArray objectAtIndex:0]valueForKey:@"title"];

    //NSArray *aliasArray = [_addressArray valueForKey:@"alias"];
    
    //NSString *aliasStr = [postPerArray objectAtIndex:7];
    
    //BOOL isValidAliasName = ![NSString isEmpty:[postPerArray objectAtIndex:7]];
    
    NSString *addressId;
    if(userModifyAddress!=nil){
        addressId = [NSString stringWithFormat:@"%@",[userModifyAddress valueForKey:@"id"]];
    }
    else
        addressId = @"";
    
    NSString *phoneNumber = [postPerArray objectAtIndex:6];
    
    if ((NSString *)[NSNull null] == phoneNumber||phoneNumber == nil) {
        phoneNumber=@"";
    }
   
    
    //new change removing phone number validation ![NSString isEmpty:[postPerArray objectAtIndex:6]]  && ![NSString isEmpty:[postPerArray objectAtIndex:7]]
    //And removing alias row
    
    
    if(![NSString isEmpty:[postPerArray objectAtIndex:0]] && ![NSString isEmpty:[postPerArray objectAtIndex:1]] && ![NSString isEmpty:[postPerArray objectAtIndex:2]] && ![NSString isEmpty:[postPerArray objectAtIndex:3]] && ![NSString isEmpty:[postPerArray objectAtIndex:4]]){
        
        if(![NSString validatePostalCode:[postPerArray objectAtIndex:3]]){
            [SSAppCommon showSimpleAlertWithMessage:INVALID_POSTAL_CODE];
            return;
        }
//        else if([aliasArray containsObject:aliasStr]) {
//           // [SSAppCommon showSimpleAlertWithMessage:INVALID_ALIAS_NAME];
//            return;
//        }
        
        if ([COMMON isReachableAlert]) {
            [COMMON showLoadingIcon:self];
            
            
        [webService AddAddressForUserWithFirstname:[postPerArray objectAtIndex:0]
                                          lastname:[postPerArray objectAtIndex:1]
                                          address1:[postPerArray objectAtIndex:2]
                                          postcode:[postPerArray objectAtIndex:3]
                                              city:[postPerArray objectAtIndex:4]
                                             phone:phoneNumber
                                             alias:@""//[postPerArray objectAtIndex:7]
                                         addressId:addressId
                                  forUserSecureKey:[COMMON getCustomerSecureKey]
                                           success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                               
                                               NSLog(@"Addaddress Response = %@",responseObject);
                                               
                                               if ([responseObject objectForKey:@"addaddress"] != NULL) {
                                                   [self addAddressResponse:responseObject];
                                                   [COMMON removeLoadingIcon];
                                               
                                               } else if([responseObject objectForKey:@"error"] != NULL){
                                                   
                                                   [COMMON removeLoadingIcon];
                                                   [SSAppCommon showSimpleAlertWithMessage:[responseObject objectForKey:@"error"]];
                                               }
                                            }
                                           failure:^(AFHTTPRequestOperation *operation, id error) {
                                               //[SSAppCommon showSimpleAlertWithMessage:@"Not Updated"];
                                               [COMMON removeLoadingIcon];
                                           }];
        }
   }
    else
        [SSAppCommon showSimpleAlertWithMessage:FILL_DETAILS];
    
    return;
}
#pragma addAddressResponse
-(void)addAddressResponse:(id)responseObject{
    if([[responseObject objectForKey:@"addaddress"]valueForKey:@"error"]){
        [SSAppCommon showSimpleAlertWithMessage:[[responseObject objectForKey:@"addaddress"]valueForKey:@"error"]];
    }
    else{
        if(currentUserDeliveryId==nil){
            currentUserDeliveryId=@"";
        }
        
        if(isPushedFromRegisterPage==YES){
            //[self gotoUserAccountPage];
             [self goToloadAddressPage];
        }
        else if(isPushedFromMonAddresses==YES){
            [self.navigationController popViewControllerAnimated:YES];
            [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
        }
        else if(isPushedFromMonPanierPage==YES){
            
            cartItems = [SHOPPINGCART getCartItems];
            if([cartItems count]!=0){
                [self checkForPaymentMethodProcess:responseObject];
            }
        }
//        else if(isPushFromLoginPage==YES){
//             [self gotoPaymentView];
//        }
        else{
            [self gotoUserAccountPage];
        }

        
    }

}
-(void)checkForPaymentMethodProcess:(id)addressResponse{
    NSString *payment = [SHOPPINGCART getFormattedCartTotalCost];
    NSString *codePromoString = [SHOPPINGCART getCouponCode];
    cartItems = [SHOPPINGCART getCartItems];
    if([cartItems count]!=0){
        if(isStoreApp){
             //[COMMON setTabBarSelection:NO];
             //[self gotoPaymentForStoreAppWithMethod:STORE_APP_PAYMENTNAME];
             [COMMON showLoadingIcon:self];
             [self redirectToAddressDisplayPageForStorApp:addressResponse];
        }
        else{
            if(![payment isEqualToString:@"0,00 €"]){
                [COMMON showLoadingIcon:self];
                [self gotoPaymentView];
            }
            if(codePromoString!=nil && [payment isEqualToString:@"0,00 €"]){
                [COMMON showLoadingIcon:self];
                [self gotoPaymentView];
            }
        }
       
        
    }
}
-(void)redirectToAddressDisplayPageForStorApp:(NSMutableDictionary *)currentSelectDict{
    
    currentSelectDict = [currentSelectDict objectForKey:@"addaddress"];
    [COMMON removeLoadingIcon];
    
    id userAddressArray = [NSMutableArray new];
    [userAddressArray addObject:[currentSelectDict mutableCopy]];
    
    SSMonPanierAddressDisplayViewController *monPanierDisplayAddressVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierDisplayAddressVC = [[SSMonPanierAddressDisplayViewController alloc]initWithNibName:@"SSMonPanierAddressDisplayViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierDisplayAddressVC = [[SSMonPanierAddressDisplayViewController alloc]
                                     initWithNibName:@"SSMonPanierAddressDisplayViewController"
                                     bundle:nil];
    }
    
    monPanierDisplayAddressVC.userAddress = [userAddressArray mutableCopy];
    monPanierDisplayAddressVC.isCommanderClicked =NO;
    monPanierDisplayAddressVC.isSelectedFromAddressListTable =NO;
    [self.navigationController pushViewController:monPanierDisplayAddressVC animated:YES];
    [COMMON removeLoadingIcon];
    
}

-(void)goToloadAddressPage{
    SSMonPanierAddressViewController *monPanierAddressVC = nil;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierAddressVC = [[SSMonPanierAddressViewController alloc]initWithNibName:@"SSMonPanierAddressViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierAddressVC = [[SSMonPanierAddressViewController alloc]
                              initWithNibName:@"SSMonPanierAddressViewController"
                              bundle:nil];
    }
    if(isCommanderClicked==YES){
        monPanierAddressVC.isCommanderClicked=YES;
    }
    else{
        monPanierAddressVC.isCommanderClicked=NO;
    }
    monPanierAddressVC.isPushedFromAddressDisplayPage=NO;
    monPanierAddressVC.isPushFromAccountPage =NO;
    monPanierAddressVC.isPushedFromRegisterPage=YES;
    
    [self.navigationController pushViewController:monPanierAddressVC animated:YES];
}

-(void)gotoUserAccountPage{
    
     SSAccountViewController *accountViewController = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        accountViewController = [[SSAccountViewController alloc]initWithNibName:@"SSAccountViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        accountViewController = [[SSAccountViewController alloc]
                                 initWithNibName:@"SSAccountViewController"
                                 bundle:nil];
    }
    if(isCommanderClicked==YES){
        accountViewController.isCommanderClicked=YES;
    }
    else{
        accountViewController.isCommanderClicked=NO;
    }
    
    [self.navigationController pushViewController:accountViewController animated:YES];

}

#pragma mark - BSKeyboard Control Delegate

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls{
    [self addAddress:nil];
}


@end
