//
//  SSMonPanierViewController.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/24/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSMonpanierCustomViewCell.h"
#import "SSMonPanierCodePromoInfoView.h"

#pragma mark - interface
@interface SSMonPanierViewController : UIViewController
{
    
    IBOutlet SSMonpanierCustomViewCell *imageShowCell;

    IBOutlet SSMonpanierCustomViewCell *promoCell;
    
    UIWindow *windowInfo;
    
    SSMonPanierCodePromoInfoView *viewCodePromoInfo;

}

@property(nonatomic,weak)  IBOutlet UITableView *monPanierTableView;
@property (strong, nonatomic) IBOutlet UIButton *commanderButton;
@property(nonatomic,assign)BOOL isPushView;
@property(strong,nonatomic)NSMutableArray *cartItems;
//@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic,assign) BOOL isPushFromAccToCart;
- (IBAction)commander:(id)sender;

-(void) syncCartData;

@end
