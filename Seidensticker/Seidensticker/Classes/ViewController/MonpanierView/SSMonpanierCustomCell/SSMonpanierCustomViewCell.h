//
//  SSMonpanierCustomViewCell.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/30/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#pragma mark - interface
@interface SSMonpanierCustomViewCell : UITableViewCell<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UILabel *quantityValueLabel;
@property (strong, nonatomic) IBOutlet UIButton *addQuantityBtn;
@property (strong, nonatomic) IBOutlet UIButton *lessQuantityBtn;

@property (strong, nonatomic) IBOutlet UIImageView *monPanierImage;

@property (strong, nonatomic) IBOutlet UILabel *monPanierTitleLabel;

@property (strong, nonatomic) IBOutlet UILabel *monPanierSizeLabel;

@property (strong, nonatomic) IBOutlet UILabel *monPanierQuantityLabel;

@property (strong, nonatomic) IBOutlet UILabel *monPanierPriceLabel;

@property (strong, nonatomic) IBOutlet UILabel *monPanierCodePromoLabel;

@property (strong, nonatomic) IBOutlet UILabel *monPanierDiscountValueLabel;

@property (strong, nonatomic) IBOutlet UIButton *monPanierCodePromoAddBtn;

@property (strong, nonatomic) IBOutlet UIButton *monPanierCodePromoMinusBtn;

@property (strong, nonatomic) IBOutlet UIButton *monPanierCodePromoRemoveBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *removeBtnWithConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *removeBtnHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *removeBottomConstraint;

@end
