//
//  SSMonpanierCustomViewCell.m
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/30/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSCustomNavigation.h"
#import "SSMonpanierCustomViewCell.h"
#import "SSMonPanierViewController.h"
#import "SSAppDelegate.h"
#import "SSMonPanierCodePromoInfoView.h"
#import "SSCustomNavigation.h"
#import "SSShoppingCart.h"

#pragma mark - interface
@interface SSMonpanierCustomViewCell ()
{
    
    SSWebService *webservice;
   
   
}


@end
#pragma mark - implementation
@implementation SSMonpanierCustomViewCell

@synthesize monPanierImage;

@synthesize monPanierPriceLabel;

@synthesize monPanierQuantityLabel;

@synthesize monPanierSizeLabel;

@synthesize monPanierTitleLabel;

@synthesize monPanierCodePromoAddBtn;

@synthesize monPanierCodePromoLabel;

@synthesize monPanierDiscountValueLabel;

@synthesize monPanierCodePromoMinusBtn;

@synthesize monPanierCodePromoRemoveBtn;

@synthesize removeBtnWithConstraint,removeBtnHeightConstraint;


- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    webservice = [[SSWebService alloc] init];
    //self.monPanierCodePromoAddBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
   // self.monPanierCodePromoAddBtn.translatesAutoresizingMaskIntoConstraints = NO;
    //[self.monPanierCodePromoAddBtn setFrame:CGRectMake(monPanierDiscountValueLabel.frame.origin.x, monPanierDiscountValueLabel.frame.origin.y-2, monPanierCodePromoAddBtn.frame.size.width, monPanierCodePromoAddBtn.frame.size.height)];
    
//    self.monPanierCodePromoAddBtn.layer.cornerRadius = 15;//half of the width
//    self.monPanierCodePromoAddBtn.layer.borderColor=[UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f].CGColor;
//    self.monPanierCodePromoAddBtn.layer.borderWidth=1.0f;
//    [self.monPanierCodePromoAddBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [self.monPanierCodePromoAddBtn setTitle:@"+" forState:UIControlStateNormal];
//    [self.monPanierCodePromoAddBtn.titleLabel setTextAlignment: NSTextAlignmentCenter];
    
//    [self.monPanierCodePromoAddBtn addTarget:self action:@selector(addButton:) forControlEvents:UIControlEventTouchUpInside];
  //  [self addSubview:self.monPanierCodePromoAddBtn];
    //[self setupConstraints];
    
    
    //half of the width
    //monPanierCodePromoRemoveBtn.layer.cornerRadius = monPanierCodePromoRemoveBtn.frame.size.height/2.0f;
    
    //monPanierCodePromoRemoveBtn.layer.cornerRadius = 16;
   // monPanierCodePromoRemoveBtn.clipsToBounds = YES;
//    monPanierCodePromoRemoveBtn.layer.borderColor=[UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f].CGColor;
//    monPanierCodePromoRemoveBtn.layer.borderWidth=1.0f;
//    [monPanierCodePromoRemoveBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [monPanierCodePromoRemoveBtn setTitle:@"x" forState:UIControlStateNormal];
//    [monPanierCodePromoRemoveBtn.titleLabel setTextAlignment: NSTextAlignmentCenter];
    
    [monPanierCodePromoAddBtn setBackgroundImage:[UIImage imageNamed:@"plus_Icon"] forState:UIControlStateNormal];

    
    [monPanierCodePromoRemoveBtn setBackgroundImage:[UIImage imageNamed:@"remove_Icon"] forState:UIControlStateNormal];
    [monPanierCodePromoLabel setBackgroundColor:[UIColor clearColor]];
    [monPanierDiscountValueLabel setBackgroundColor:[UIColor clearColor]];
    [monPanierCodePromoRemoveBtn setBackgroundColor:[UIColor clearColor]];

    if([self isDeviceIpad]==YES){
        //removeBtnHeightConstraint.constant = 40;
       // removeBtnWithConstraint.constant = 40;
        _removeBottomConstraint.constant = 13;
    }
    else{
        _removeBottomConstraint.constant = 8;
    }
    
}
-(BOOL)isDeviceIpad
{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        
        return YES;
    } else {
        return NO;
    }
}

//- (void)updateConstraints {
//    
//   // [super updateConstraints];
//    
//}

#pragma mark - Constraints
-(void) setupConstraints{
//     if (IS_IPHONE6 ) {
//    // Center horizontally
//    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.monPanierCodePromoAddBtn
//                                                     attribute:NSLayoutAttributeCenterX
//                                                     relatedBy:NSLayoutRelationEqual
//                                                        toItem:self
//                                                     attribute:NSLayoutAttributeCenterX
//                                                    multiplier:1.42
//                                                      constant:0.0]];
//     }
//    else if(IS_IPHONE6_Plus)
//    {
//        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.monPanierCodePromoAddBtn
//                                                         attribute:NSLayoutAttributeCenterX
//                                                         relatedBy:NSLayoutRelationEqual
//                                                            toItem:self
//                                                         attribute:NSLayoutAttributeCenterX
//                                                        multiplier:1.48
//                                                          constant:0.0]];
//    }
//    else if (IS_IPHONE5 || IS_IPHONE4) {
//        // Center horizontally
//        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.monPanierCodePromoAddBtn
//                                                         attribute:NSLayoutAttributeCenterX
//                                                         relatedBy:NSLayoutRelationEqual
//                                                            toItem:self
//                                                         attribute:NSLayoutAttributeCenterX
//                                                        multiplier:1.33
//                                                          constant:0.0]];
//    }
//    else{
//        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.monPanierCodePromoAddBtn
//                                                         attribute:NSLayoutAttributeCenterX
//                                                         relatedBy:NSLayoutRelationEqual
//                                                            toItem:self
//                                                         attribute:NSLayoutAttributeCenterX
//                                                        multiplier:1.72
//                                                          constant:0.0]];
//   
//
//    }
//        // Center vertically
//    if(IS_IPHONE4 ||IS_IPHONE5 ||IS_IPHONE6 ||IS_IPHONE6_Plus)
//    {
//    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.monPanierCodePromoAddBtn
//                                                         attribute:NSLayoutAttributeCenterY
//                                                         relatedBy:NSLayoutRelationEqual
//                                                            toItem:self
//                                                         attribute:NSLayoutAttributeCenterY
//                                                        multiplier:0.95
//                                                          constant:0.0]];
//    }
//    else{
//        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.monPanierCodePromoAddBtn
//                                                         attribute:NSLayoutAttributeCenterY
//                                                         relatedBy:NSLayoutRelationEqual
//                                                            toItem:self
//                                                         attribute:NSLayoutAttributeCenterY
//                                                        multiplier:0.95
//                                                          constant:0.0]];
//    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
