//
//  SSMonPanierViewController.m
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/24/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSMonPanierViewController.h"
#import "SSAppCommon.h"
#import "SSConfig.h"
#import "SSCustomNavigation.h"
#import "SSMonpanierCustomViewCell.h"
#import "SSShoppingCart.h"
#import "AsyncImage.h"
#import "SSMonPanierLoginViewController.h"
#import "SSMonPanierAddressViewController.h"
#import "SSMonPanierAddressDisplayViewController.h"
#import "SSMonPanierAddressModifierViewController.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#import "NMBottomTabBar.h"
#import "NMBottomTabBarController.h"
#import "SSAppDelegate.h"
#import "M13BadgeView.h"
#import "SSAppDelegate.h"
#import "SSMonPanierCodePromoInfoView.h"
#import "SSShopViewController.h"

#pragma mark - interface
@interface SSMonPanierViewController ()<UITextFieldDelegate, NMBottomTabBarControllerDelegate> {
    
    SSWebService *webservice;
    NMBottomTabBarController *bottomTabBar;
    NMBottomTabBar *nmTabBar;
    M13BadgeView *badgeViewObj;
    NSMutableArray *cartDiscountArray;
    NSString *codePromoString;
    BOOL isErrorCart;
    BOOL isErrorInCartItems;
    SSCustomNavigation * navigation;
     UIRefreshControl *refreshControl;
    BOOL isCommanderClicked;

}

@property(nonatomic,retain) SSMonPanierCodePromoInfoView *viewCodePromoInfo;
@end
#pragma mark - implementation
@implementation SSMonPanierViewController

@synthesize monPanierTableView,cartItems,viewCodePromoInfo,isPushView;
@synthesize isPushFromAccToCart;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self showTableViewContentBottom];
    [self.navigationItem setHidesBackButton:YES];
//    [self.navigationController setNavigationBarHidden:YES animated:YES];
    SSAppDelegate *appDelegate = (SSAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.monPanierViewController=self;
    webservice = [[SSWebService alloc] init];
    // Initialize the refresh control.
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self
                       action:@selector(syncCartData)
             forControlEvents:UIControlEventValueChanged];
    [monPanierTableView addSubview:refreshControl];

}
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
     [COMMON TrackerWithName:@"Cart View"];
    [_commanderButton setHidden:YES];
//   if(isPushView)
//   {
//      // [self.navigationController popToRootViewControllerAnimated:NO];
//       SSShopViewController *shopVC=[[SSShopViewController alloc]init];
//       [self.navigationController pushViewController:shopVC animated:NO];
//       isPushView=NO;
//   }
    
    cartItems = [SHOPPINGCART getCartItems];
    codePromoString = [SHOPPINGCART getCouponCode];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self loadNavigationMonPanier];
   
    //[self styleNavBar];
    
    [self checkForCartItems];
    
}
- (void)loadNavigationMonPanier
{
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    navigation = [[SSCustomNavigation alloc] initWithNibName:@"SSCustomNavigation" bundle:nil];
    navigation.view.tag = 1001;
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    
    [navigation.view addSubview:navBorder];    [self.navigationController.navigationBar addSubview:navigation.view];
    [navigation setLeftHidden:NO];
    [navigation setTitleHidden:NO];
    [navigation setCatergoryShopBtnLabelHidden:YES];

    [navigation.titleLbl setText:NSLocalizedString(@"Mon panier", @"")];
    [navigation.titleLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    [navigation.navigationLeftLbl setText:NSLocalizedString(@"Retour", @"")];
    [navigation.navigationLeftLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [navigation.navigationLeftBtn addTarget:self action:@selector(retourMonPanierAction:) forControlEvents:UIControlEventTouchUpInside];
    [navigation.navigationRightLbl setText:NSLocalizedString(@"Tout effacer", @"")];
    [navigation.navigationRightLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [navigation.navigationRightBtn addTarget:self action:@selector(clearCartItem:) forControlEvents:UIControlEventTouchUpInside];
    if([cartItems count]==0){
        [navigation.navigationRightLbl setHidden:YES];
        [navigation.navigationRightBtn setHidden:YES];
    }
    else{
        [navigation.navigationRightLbl setHidden:NO];
        [navigation.navigationRightBtn setHidden:NO];
    }
    
    
}

-(void)clearCartItem:(id)sender{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                              @"Seidensticker" message:@"Tout effacer?" delegate:self
                                             cancelButtonTitle:@"Annuler" otherButtonTitles:@"D'accord", nil];
    alertView.tag =100;
    [alertView show];
}

#pragma mark - Alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==100){
        switch (buttonIndex) {
            case 0:
                NSLog(@"Cancel button clicked");
                break;
            case 1:
                [self setUpClearScreen];
                NSLog(@"OK button clicked");
                break;
                
            default:
                break;
        }
    }
    else{
        
        if (buttonIndex == 0) {
            NSLog(@"Cancel button clicked");
        }
        else{
            codePromoString=@"";
            [self syncCartData];
            [SHOPPINGCART removeCouponFromCart];
            NSLog(@"OK button clicked");
        }
        
    }
   
}
-(void)setUpClearScreen{
    [SHOPPINGCART clearCart];
    [SHOPPINGCART removeCouponFromCart];
    cartItems=nil;
    [navigation.navigationRightLbl setHidden:YES];
    [navigation.navigationRightBtn setHidden:YES];
    [monPanierTableView reloadData];
    [_commanderButton setHidden:YES];
    UILabel *noItemsLabel=[UILabel new];
    CGFloat XPos = 20;
    CGFloat YPos = screen_height/2;
    noItemsLabel.frame=CGRectMake(XPos ,(YPos-40) ,screen_width-(XPos*2),40);
    noItemsLabel.textAlignment=NSTextAlignmentCenter;
    [noItemsLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(18)]];
    noItemsLabel.tag=EMPTYCARTLABEL_TAG;
    noItemsLabel.text=@"Votre panier est vide";
    [noItemsLabel setBackgroundColor:[UIColor clearColor]];
    monPanierTableView.hidden=YES;
    [self.view addSubview:noItemsLabel];

}

#pragma mark-Add Cart Data Web Services

-(void) syncCartData {
    [COMMON showLoadingIcon:self];
    if(codePromoString == NULL)
        codePromoString = @"";
    [webservice syncCartWithProducts:[SHOPPINGCART getCartProductsInJsonFormat]
                           codePromo:codePromoString
                    forUserSecureKey:[COMMON getCustomerSecureKey]
                             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                 [refreshControl endRefreshing];
                                 [self setCartDetails:responseObject];
                             }
                             failure:^(AFHTTPRequestOperation *operation, id error) {
                                 [refreshControl endRefreshing];
                                 //[SSAppCommon showSimpleAlertWithMessage:@"No Internet Connection available. Kindly check your Internet"];
                                 [COMMON removeLoadingIcon];
  
                             }];
}


- (void) setCartDetails:(id) responseObject {

    NSLog(@"Response Object = %@",responseObject);
    
    
    if (cartDiscountArray == NULL) {
        cartDiscountArray = [[NSMutableArray alloc] init];
        _commanderButton.hidden = NO;
    }
    else
        [cartDiscountArray removeAllObjects];
    
     isErrorInCartItems=NO;
    NSMutableDictionary *cartDiscountDict;
    if (responseObject != NULL) {
        BOOL isPromoCode = NO;
        if (![codePromoString isEqualToString:@""]) {
            isPromoCode = YES;
        }
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if ([responseObject valueForKey:@"addtocart"] != NULL) {
                responseObject = [responseObject valueForKey:@"addtocart"];
                isPromoCode = YES;
            }
        }
        
        for(NSString *key in responseObject) {
            if ([key isEqualToString:@"error"] ||[key isEqualToString:@"error_coupon"]) {
                [COMMON showAlert:[responseObject objectForKey:key]];
                
                if([key isEqualToString:@"error"])
                {
                    self.commanderButton.hidden=YES;
                    //[COMMON showAlert:[responseObject objectForKey:key]];
                    isErrorCart=YES;
                    isErrorInCartItems=YES;
                    
                }
                
                if ([key isEqualToString:@"error_coupon"])
                {
                    self.commanderButton.hidden=NO;
                    isPromoCode = NO;
                    [SHOPPINGCART removeCouponFromCart];
                    codePromoString = @"";
                    isErrorCart=NO;
                    isErrorInCartItems=NO;
                }
                
            }
            else {
                if(isErrorCart==YES)
                {
                    self.commanderButton.hidden=YES;
                    isErrorCart=NO;
                    
                }
                else{
                     self.commanderButton.hidden=NO;
                }
                
                cartDiscountDict = [[NSMutableDictionary alloc] init];
                [cartDiscountDict setObject:key  forKey:@"key"]  ;
                [cartDiscountDict setObject:[responseObject objectForKey:key] forKey:@"value"];
                [cartDiscountArray addObject:cartDiscountDict];
                cartDiscountArray=[[[cartDiscountArray reverseObjectEnumerator] allObjects] mutableCopy];
                NSLog(@"Cart Discount Array reverse = %@",cartDiscountArray);
                
            }
        }
        if(isPromoCode && ![codePromoString isEqualToString:@""]) {
            [SHOPPINGCART addCouponToCart:codePromoString];
        } else {
            [SHOPPINGCART removeCouponFromCart];
        }
        if(!isStoreApp){
            if(isErrorInCartItems!=YES){
                cartDiscountDict = [[NSMutableDictionary alloc] init];
                [cartDiscountDict setObject:@"CODE PROMO"   forKey:@"key"];
                [cartDiscountDict setObject:codePromoString forKey:@"value"];
                [cartDiscountArray insertObject:cartDiscountDict atIndex:0];
            }

        }
        
        NSLog(@"Cart Discount Array = %@",cartDiscountArray);
        [self getCartDisccountValue:responseObject];
        [monPanierTableView reloadData];
        [COMMON removeLoadingIcon];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


-(void)getCartDisccountValue:(id)responseObject
{
    double totalCost;
    NSString *totalCostString;
    totalCostString            = [responseObject objectForKey:@"TOTAL"];
    totalCostString            = [totalCostString stringByReplacingOccurrencesOfString:@"€" withString:@""];
    totalCostString            = [totalCostString stringByReplacingOccurrencesOfString:@" " withString:@""];
    totalCostString            = [totalCostString stringByReplacingOccurrencesOfString:@"," withString:@"."];
    totalCost=[totalCostString doubleValue];
    NSLog(@"Total Cost Value=%f",totalCost);
    [SHOPPINGCART setTotalForCart:totalCost];
    
}



#pragma mark - Button Actions
-(void)retourMonPanierAction:(id)sender
{
   // [self.navigationController.navigationBar removeFromSuperview];
    [navigation removeFromParentViewController];
    [self.navigationController popViewControllerAnimated:YES];
    
    if(isPushFromAccToCart==YES){
        [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
         [self.navigationController setNavigationBarHidden:NO];
    }
    else{
        [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
        [self.navigationController setNavigationBarHidden:YES];
    }
   
    
    
//    NSArray *array = [self.navigationController viewControllers];
//    [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];
//    [self.navigationController setNavigationBarHidden:YES];
    
}

#pragma mark- UITableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [cartItems count];
    } else {
        return [cartDiscountArray count];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        if(IS_IPHONE4 ||IS_IPHONE5 ||IS_IPHONE6 ||IS_IPHONE6_Plus){
            return 150;
        }
        else{
            return 320;
        }
    }
    else {
        if(IS_IPHONE4 ||IS_IPHONE5 ||IS_IPHONE6 ||IS_IPHONE6_Plus)
        {
        return 42;
        }
        else
        {
            return 60;
        }
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *simpleTableIdentifier = @"MonpanierCustomCell";
    SSMonpanierCustomViewCell *cell  = (SSMonpanierCustomViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (indexPath.section == 0) {

        if (cell == nil)
            {
                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
                {
                    NSArray *nib;
                    nib     = [[NSBundle mainBundle] loadNibNamed:@"SSMonpanierCustomViewCell" owner:self options:nil];
                    cell    = imageShowCell;
                    cell.monPanierCodePromoAddBtn.hidden=YES;
                }
                else
                {
                    NSArray *nib;
                    nib     = [[NSBundle mainBundle] loadNibNamed:@"SSMonpanierCustomViewCelliPad" owner:self options:nil];
                    cell    = imageShowCell;
                    cell.monPanierCodePromoAddBtn.hidden=YES;
                }
            }
            NSDictionary *currentCartItem = [cartItems objectAtIndex:indexPath.row];
            NSString *photoURLString = [currentCartItem objectForKey:@"productImage"];
            AsyncImage *asyncImage = [[AsyncImage alloc]initWithFrame:CGRectMake(0, 0, cell.monPanierImage.frame.size.width, cell.monPanierImage.frame.size.height)];
            [asyncImage setLoadingImage];
            [asyncImage loadImageFromURL:[NSURL URLWithString:photoURLString]
                                    type:AsyncImageResizeTypeCrop
                                 isCache:YES];
            
            [cell.monPanierImage addSubview:asyncImage];
            cell.monPanierTitleLabel.text       = [COMMON getDecodedString:[currentCartItem objectForKey:@"productName"]];
            if([[currentCartItem objectForKey:@"productSize"] isEqualToString:@"TailleUnique"])
            {
                cell.monPanierSizeLabel.hidden     =YES;
                cell.monPanierQuantityLabel.translatesAutoresizingMaskIntoConstraints = YES;
                int yposition;
                if(IS_IPHONE4 ||IS_IPHONE5 ||IS_IPHONE6 ||IS_IPHONE6_Plus)
                {
                    yposition=25;
                    
                }
                else{
                    yposition=50;
                }
                [cell.monPanierQuantityLabel setFrame:CGRectMake(cell.monPanierQuantityLabel.frame.origin.x+5, cell.monPanierQuantityLabel.frame.origin.y-yposition, cell.monPanierQuantityLabel.frame.size.width, cell.monPanierQuantityLabel.frame.size.height)];
            }
            else
            {
                cell.monPanierSizeLabel.text        = [NSString stringWithFormat:@"Taille: %@",[COMMON getDecodedString:[currentCartItem objectForKey:@"productSize"]]];
            }
            
            NSLog(@"size==%@",cell.monPanierSizeLabel);
            
       // cell.monPanierQuantityLabel.text  = @"Quantité";
        [cell.monPanierQuantityLabel setTextColor:[UIColor blackColor]];
        cell.quantityValueLabel.text =[NSString stringWithFormat:@"%@",[COMMON getDecodedString:[currentCartItem objectForKey:@"quantity"]]];
        NSLog(@"%@",cell.monPanierQuantityLabel);
            
        cell.monPanierPriceLabel.text       = [COMMON getDecodedString:[currentCartItem objectForKey:@"productPrice"]];
        NSInteger maxQuantityCount;
         maxQuantityCount = [[currentCartItem objectForKey:@"quantityArrayCount"] integerValue];
        
        cell.monPanierQuantityLabel.text    = [NSString stringWithFormat:@"Quantité: %@",[COMMON getDecodedString:[currentCartItem objectForKey:@"quantity"]]];
        
        [cell.quantityValueLabel setHidden:YES];
        [cell.addQuantityBtn setHidden:YES];
        [cell.lessQuantityBtn setHidden:YES];
        
//        [cell.addQuantityBtn setTag:indexPath.row];
//        [cell.addQuantityBtn addTarget:self action:@selector(addQuantityAction:) forControlEvents:UIControlEventTouchUpInside];
//        [[cell.addQuantityBtn layer] setBorderWidth:1.0f];
//        [[cell.addQuantityBtn layer] setBorderColor:[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f].CGColor];
//        [cell.lessQuantityBtn setTag:indexPath.row];
//        [cell.lessQuantityBtn addTarget:self action:@selector(lessQuantityAction:) forControlEvents:UIControlEventTouchUpInside];
//        [[cell.lessQuantityBtn layer] setBorderWidth:1.0f];
//        [[cell.lessQuantityBtn layer] setBorderColor:[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f].CGColor];
    }
    else {
    
        if (cell == nil)
        {
            NSArray *nib;
            nib     = [[NSBundle mainBundle] loadNibNamed:@"SSMonpanierCustomViewCell" owner:self options:nil];
            cell    = promoCell;
            [cell.monPanierCodePromoAddBtn addTarget:self action:@selector(addButton:) forControlEvents:UIControlEventTouchUpInside];
            [cell.monPanierCodePromoRemoveBtn addTarget:self action:@selector(promoRemoveAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        cell.monPanierCodePromoRemoveBtn.hidden=YES;
        
        if(IS_IPHONE6_Plus ||IS_IPHONE6 ||IS_IPHONE_5 ||IS_IPHONE_4)
        {
            [cell.monPanierDiscountValueLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
            [cell.monPanierCodePromoLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
        }
        else{
            [cell.monPanierDiscountValueLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(15)]];
            [cell.monPanierCodePromoLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(15)]];
        }
        if([cartItems count]<1 )
        {
            cell.monPanierCodePromoAddBtn.hidden=YES;
            cell.monPanierCodePromoLabel.hidden=YES;
            cell.monPanierDiscountValueLabel.hidden=YES;
            promoCell.hidden=YES;
        }
        
        NSDictionary *currentItemDict = [cartDiscountArray objectAtIndex:indexPath.row];
        cell.monPanierCodePromoLabel.text       = [currentItemDict objectForKey:@"key"];
        cell.monPanierDiscountValueLabel.text   = [currentItemDict objectForKey:@"value"];
        if(isErrorInCartItems==YES){
            if (indexPath.row ==0 )  {
                cell.monPanierDiscountValueLabel.hidden=NO;
                cell.monPanierCodePromoAddBtn.hidden=YES;
                cell.monPanierCodePromoMinusBtn.hidden=YES;
                 cell.monPanierCodePromoRemoveBtn.hidden=YES;
            }
            
        }
        else{
            if (indexPath.row ==0 ) {
                if ([[currentItemDict objectForKey:@"value"] isEqualToString:@""]) {
                    cell.monPanierDiscountValueLabel.hidden=YES;
                    cell.monPanierCodePromoAddBtn.hidden=NO;
                    cell.monPanierCodePromoRemoveBtn.hidden=YES;
                }
                else {
                    cell.monPanierCodePromoAddBtn.hidden=YES;
                    cell.monPanierCodePromoRemoveBtn.hidden=NO;
                    
                }
                
            } else {
                cell.monPanierDiscountValueLabel.hidden=NO;
                cell.monPanierCodePromoAddBtn.hidden=YES;
                cell.monPanierCodePromoMinusBtn.hidden=YES;
                cell.monPanierCodePromoRemoveBtn.hidden=YES;
            }
        }
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}



-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    if(indexPath.section == 0)
        return YES;
    else
        return NO;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [SHOPPINGCART removeItemForIndex:indexPath.row];
        cartItems = [SHOPPINGCART getCartItems];
        [self checkForCartItems];
        [tableView reloadData];
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"Supprimer";
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 1 && indexPath.row == 0 && ![codePromoString isEqualToString:@""]) {
        [self showCodePromoView];
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Remove seperator inset
    
    self.monPanierTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    if ([self.monPanierTableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.monPanierTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        
        [cell setPreservesSuperviewLayoutMargins:NO];
        
    }

    // Explictly set your cell's layout margins
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        
        [cell setLayoutMargins:UIEdgeInsetsZero];
        
    }
    
}


#pragma mark - User Actions
- (void) redirectUser {
    [webservice getAddressListForUser:[COMMON getCustomerSecureKey]
                              success:^(AFHTTPRequestOperation *operation, id responseObject){
                                  NSLog(@"Response = %@",responseObject);
                                  
                                  if([responseObject objectForKey:@"error"] != NULL){
                                      [self redirectToAddAddressView];
                                      [COMMON removeLoadingIcon];
                                  }
                                  else if ([responseObject objectForKey:@"listaddress"] != NULL) {
                                      [self redirectToAddressViewWithDictionary:[responseObject objectForKey:@"listaddress"]];
                                  }
                                }
                              failure:^(AFHTTPRequestOperation *operation, id error) {
                                  //[SSAppCommon showSimpleAlertWithMessage:@"No Internet Connection available. Kindly check your Internet"];
                                  [COMMON removeLoadingIcon];

                            }];
}
#pragma mark - AddAdressView
- (void) redirectToAddAddressView {
    
    SSMonPanierAddressModifierViewController *monPanierAddressModifierVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierAddressModifierVC = [[SSMonPanierAddressModifierViewController alloc]initWithNibName:@"SSMonPanierAddressModifierViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        monPanierAddressModifierVC = [[SSMonPanierAddressModifierViewController alloc]
                                      initWithNibName:@"SSMonPanierAddressModifierViewController"
                                      bundle:nil];
    }
    
   
    monPanierAddressModifierVC.isPushedFromMonAddresses = NO;
    monPanierAddressModifierVC.isPushedFromRegisterPage=NO;
    monPanierAddressModifierVC.isPushedFromMonPanierPage=YES;
    monPanierAddressModifierVC.isCommanderClicked =YES;
    
  //  UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:monPanierAddressModifierVC];
    
   // [self.navigationController presentViewController:navigationController animated:YES completion:nil];

    [self.navigationController pushViewController:monPanierAddressModifierVC animated:YES];
}
- (void) redirectToAddressViewWithDictionary:(NSMutableDictionary *) addressDictionary {
    
    SSMonPanierAddressDisplayViewController *monPanierAddressVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierAddressVC = [[SSMonPanierAddressDisplayViewController alloc]initWithNibName:@"SSMonPanierAddressDisplayViewControlleriPad" bundle:[NSBundle mainBundle]];
        }
    else {
        monPanierAddressVC = [[SSMonPanierAddressDisplayViewController alloc]
                              initWithNibName:@"SSMonPanierAddressDisplayViewController"
                              bundle:nil];
    }
    
    monPanierAddressVC.userAddress = addressDictionary;
    monPanierAddressVC.isCommanderClicked =YES;
    
//    SSMonPanierAddressViewController *monPanierAddressVC = nil;
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
//    {
//        monPanierAddressVC = [[SSMonPanierAddressViewController alloc]initWithNibName:@"SSMonPanierAddressViewControlleriPad" bundle:[NSBundle mainBundle]];
//    }
//    else {
//        monPanierAddressVC = [[SSMonPanierAddressViewController alloc]
//                              initWithNibName:@"SSMonPanierAddressViewController"
//                              bundle:nil];
//    }
//    
//    monPanierAddressVC.userAddress = addressDictionary;
   
    
   // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:monPanierAddressVC];
    
    //[self.navigationController presentViewController:navigationController animated:YES completion:nil];
    
    
    [self.navigationController pushViewController:monPanierAddressVC animated:YES];

}
#pragma mark - Login View
- (void) LoginView {

    SSMonPanierLoginViewController *monPanierLoginVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monPanierLoginVC = [[SSMonPanierLoginViewController alloc]initWithNibName:@"SSMonPanierLoginViewControlleriPad" bundle:[NSBundle mainBundle]];
        monPanierLoginVC.hidesBottomBarWhenPushed=YES;
            }
    else {
        monPanierLoginVC = [[SSMonPanierLoginViewController alloc]
                            initWithNibName:@"SSMonPanierLoginViewController"
                            bundle:nil];
        
        monPanierLoginVC.hidesBottomBarWhenPushed=YES;
    }
    
    monPanierLoginVC.isCommanderClicked=YES;
    monPanierLoginVC.isPushedFromTab = YES;
    [self.navigationController pushViewController:monPanierLoginVC animated:YES];
    
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:monPanierLoginVC];
    
    //[self.navigationController presentViewController:navigationController animated:YES completion:nil];
    
}

-(void) pushToHomePage
{
    NSArray *array = [self.navigationController viewControllers];
    [self.navigationController popToViewController:[array objectAtIndex:0] animated:YES];
    [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];

    [self.navigationController setNavigationBarHidden:YES];
//    [self.navigationController popViewControllerAnimated:YES];
//    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark- Button Actions
- (IBAction)commander:(id)sender
{
   // [self pushToHomePage];
    isCommanderClicked=YES;
    [COMMON showLoadingIcon:self];
    if ([COMMON isUserLoggedIn]) {
        [self redirectUser];
    }
    else {
        
        [self LoginView];
    }
}

- (IBAction)addQuantityAction:(UIButton *)sender {
    NSString * str;
    SSMonpanierCustomViewCell* cell = (SSMonpanierCustomViewCell*)[sender superview].superview;
    NSMutableDictionary *currentCartItem = [NSMutableDictionary new];
    currentCartItem = [[cartItems objectAtIndex:sender.tag] mutableCopy];
    
    NSUInteger x = [[currentCartItem valueForKey:@"quantity"] integerValue];
    NSUInteger maxCount = [[currentCartItem valueForKey:@"quantityArrayCount"] integerValue];
    NSLog(@"AddmaxCount%lu",(unsigned long)maxCount);
    NSLog(@"AddmaxCount%lu",(unsigned long)x);
    x = x + 1;
    if(x<=maxCount){
        str = [NSString stringWithFormat:@"%lu",(unsigned long)x];
        
        [SHOPPINGCART addToCart:[currentCartItem mutableCopy] withSize:[currentCartItem valueForKey:@"productSize"] withSizeIPA:[currentCartItem valueForKey:@"productSizeIPA"] andQuantity:x quantityCount:[[currentCartItem valueForKey:@"quantityArrayCount"] integerValue] isCartTable:YES];
        cartItems = [SHOPPINGCART getCartItems];
    }
    
    else{
        str = [currentCartItem valueForKey:@"quantity"];
    }
    
    cell.quantityValueLabel.text = [NSString stringWithFormat:@"%@",[COMMON getDecodedString:str]] ;
    [currentCartItem setObject:str
                        forKey:@"quantity"];
    
    
   // [self syncCartData];

}

- (IBAction)lessQuantityAction:(UIButton *)sender {
    NSString * str;

    SSMonpanierCustomViewCell* cell = (SSMonpanierCustomViewCell*)[sender superview].superview;
    NSMutableDictionary *currentCartItem = [NSMutableDictionary new];
    currentCartItem = [[cartItems objectAtIndex:sender.tag] mutableCopy];
    
    NSUInteger x = [[currentCartItem valueForKey:@"quantity"] integerValue];
//    NSUInteger maxCount = [[currentCartItem valueForKey:@"quantityArrayCount"] integerValue];
    
    NSLog(@"lessmaxCount%lu",(unsigned long)x);
    x = x -1;
    if(x>=1)
      str = [NSString stringWithFormat:@"%lu",(unsigned long)x];
    else
        str = [currentCartItem valueForKey:@"quantity"];
 
    cell.quantityValueLabel.text = [NSString stringWithFormat:@"%@",[COMMON getDecodedString:str]];
    [currentCartItem setObject:str forKey:@"quantity"];
    [SHOPPINGCART addToCart:[currentCartItem mutableCopy] withSize:[currentCartItem valueForKey:@"productSize"] withSizeIPA:[currentCartItem valueForKey:@"productSizeIPA"] andQuantity:x quantityCount:[[currentCartItem valueForKey:@"quantityArrayCount"] integerValue] isCartTable:YES];
    cartItems = [SHOPPINGCART getCartItems];

    // [self syncCartData];
}

#pragma mark - Disable Commander Button

-(void)checkForCartItems{
    
    for(UIView *removeView in [self.view subviews]) {
        if ([removeView isKindOfClass:[UILabel class]] ) {
            [removeView removeFromSuperview];
        }
    }
    if(![cartItems count]){
        _commanderButton.enabled=YES;
        _commanderButton.alpha=1.0f;
        _commanderButton.hidden = YES;
        UILabel *noItemsLabel=[UILabel new];
        CGFloat XPos = 20;
        CGFloat YPos = screen_height/2;
        noItemsLabel.frame=CGRectMake(XPos ,(YPos-40) ,screen_width-(XPos*2),40);
        noItemsLabel.textAlignment=NSTextAlignmentCenter;
        [noItemsLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(18)]];
        noItemsLabel.tag=EMPTYCARTLABEL_TAG;
        noItemsLabel.text=@"Votre panier est vide";
        [noItemsLabel setBackgroundColor:[UIColor clearColor]];
        monPanierTableView.hidden=YES;
        [self.view addSubview:noItemsLabel];
    }
     else if(cartItems.count){
         _commanderButton.enabled=YES;
         _commanderButton.alpha=1.0f;
         //_commanderButton.hidden = NO;
         monPanierTableView.hidden=NO;
         if([self.view viewWithTag:EMPTYCARTLABEL_TAG]){
             [[self.view viewWithTag:EMPTYCARTLABEL_TAG]removeFromSuperview];
         }
          [self syncCartData];
     }
   
}

#pragma mark-Show the Table view Bottom

-(void)showTableViewContentBottom
{
    int bottomValue;
    
    if(IS_IPHONE4)
    {
        bottomValue=275;
    }
    else if (IS_IPHONE5)
    {
        bottomValue=160;
    }
    else if(IS_IPHONE6 )
    {
        bottomValue=75;
    }
    else if(IS_IPHONE6_Plus)
    {
        bottomValue=30;
    }
    else{
        bottomValue=90;
    }
    
    self.monPanierTableView.contentInset =UIEdgeInsetsMake(0, 0, bottomValue, 0);
}

#pragma mark-Shoe code Promo View

-(void)showCodePromoView
{
    windowInfo = [[[UIApplication sharedApplication] delegate] window];
    viewCodePromoInfo = [[SSMonPanierCodePromoInfoView alloc] init];
    [windowInfo addSubview:viewCodePromoInfo];
    
    [self setViewCodePromoInfo:viewCodePromoInfo];
    [viewCodePromoInfo.codePromoTextField setText:codePromoString];
    
    [viewCodePromoInfo.codePromoOkBtn addTarget:self action:@selector(OkButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    //    [viewCodePromoInfo.codePromoOkBtn setBackgroundColor:[UIColor orangeColor]];
    
    NSDictionary *dictView = @{@"_viewCodePromoInfo":viewCodePromoInfo};
    
    [windowInfo addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_viewCodePromoInfo]|"
                                
                                                                       options:0
                                
                                                                       metrics:nil views:dictView]];
    
    [windowInfo addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_viewCodePromoInfo]|"
                                
                                                                       options:0
                                
                                                                       metrics:nil views:dictView]];
}

#pragma mark-Ok Button Action

-(IBAction)OkButtonAction:(id)sender
{
    
    codePromoString = viewCodePromoInfo.codePromoTextField.text;
    NSLog(@"Code Promo String=%@",codePromoString);
    
    if (![codePromoString isEqualToString:@""]) {
        [self syncCartData];
        [self.viewCodePromoInfo.codePromoOkBtn setBackgroundColor:[UIColor orangeColor]];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.viewCodePromoInfo removeFromSuperview];
            
        });

    }
    else{
        [self syncCartData];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.viewCodePromoInfo removeFromSuperview];
            
        });

    }
    
}


#pragma mark-code promo button Action

-(void)addButton:(id)sender
{
    
    [self showCodePromoView];
}
-(void)promoRemoveAction:(id)sender
{
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                              @"Seidensticker" message:@"Are you sure you want to remove coupon code?" delegate:self
                                             cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alertView.tag =101;
    [alertView show];
}


@end
