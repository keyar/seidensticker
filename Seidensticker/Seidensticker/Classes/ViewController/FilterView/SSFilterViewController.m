//
//  SSFilterViewController.m
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/18/15.
//  Copyright (c) 2015 Oclocksoftware. All rights reserved.
//
#define maxWidth 100.0f
#import "SSFilterViewController.h"
#import "SSCustomNavigation.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSAppDelegate.h"
#import "SSCategoryViewController.h"
@interface SSFilterViewController () {
    
    NSMutableArray *selectedFeatures;
     BOOL shareEnabled;
    NSMutableArray* uniqueValues;
    NSMutableDictionary *filterDict;
}


@property (nonatomic, strong) NSArray *isProductSelectedArray;
@property (nonatomic, strong) NSArray *dataArray;


@end

@implementation SSFilterViewController
@synthesize currentFilterArray,filterCollectionView;


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark - View States

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.filterCollectionView registerClass:[SSFilterCollectionViewCell class] forCellWithReuseIdentifier:@"SSFilterCollectionViewCell"];
    
    [self.filterCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
   
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.headerReferenceSize = CGSizeMake(self.filterCollectionView.bounds.size.width, 50);
    [self.filterCollectionView setCollectionViewLayout:flowLayout];
    self.filterCollectionView.allowsMultipleSelection = YES;
    self.filterCollectionView.allowsSelection = YES; //this
    filtersArr = [[NSMutableArray alloc]init];
    if(IS_IPHONE6_Plus){
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    [self.navigationController.navigationBar addSubview:navBorder];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    uniqueValues= [[NSMutableArray alloc] init];
    filterDict = [NSMutableDictionary new];
    [self loadNavigation];
    [super viewWillAppear:animated];
    [self loadFilters];
    [self.filterCollectionView setAllowsMultipleSelection:YES];
    
}

#pragma mark - Filters

- (void) loadFilters {
    
    [COMMON showLoadingIcon:self];
    [self.filterCollectionView reloadData];
    NSLog(@"FilterCollectionView=%@",self.filterCollectionView);
    [COMMON removeLoadingIcon];
    NSLog(@"self.currentFilterArray =%@",self.currentFilterArray);
}

- (void)loadNavigation
{
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    SSCustomNavigation *navigation = [[SSCustomNavigation alloc] initWithNibName:@"SSCustomNavigation" bundle:nil];
    navigation.view.tag = 1001;
    [self.navigationController.navigationBar addSubview:navigation.view];
    
    [navigation setLeftHidden:NO];
    [navigation setTitleHidden:NO];
    [navigation setCatergoryShopBtnLabelHidden:YES];
    [navigation.titleLbl setText:NSLocalizedString(@"Filtre", @"")];
    [navigation.titleLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(15)]];
    
    [navigation.navigationLeftLbl setText:NSLocalizedString(@"Annuler", @"")];
    [navigation.navigationLeftLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [navigation.navigationLeftBtn addTarget:self action:@selector(annulerBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [navigation.navigationRightLbl setText:NSLocalizedString(@"Valider", @"")];
    [navigation.navigationRightLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [navigation.navigationRightBtn addTarget:self action:@selector(validerBtnAction:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark- Collection View Delegates

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return [self.currentFilterArray count];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSMutableArray *sectionArray = [[self.currentFilterArray objectAtIndex:section] objectForKey:@"values"];
    return [sectionArray count];
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SSFilterCollectionViewCell";
    SSFilterCollectionViewCell *cell = (SSFilterCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell setNeedsDisplay];
    
    cell.colorLabel = nil;
    cell.backgroungColorlabel = nil;
    cell.numberLabel = nil;
    
    cell.borderView.layer.borderColor =[UIColor lightGrayColor].CGColor;
    cell.borderView.layer.borderWidth =1.0;
    
    if (cell != nil)
    {
        cell.colorLabel = (UILabel *)[cell viewWithTag:101];
        cell.backgroungColorlabel = (UILabel *)[cell viewWithTag:201];
        cell.numberLabel = (UILabel *)[cell viewWithTag:301];
    }
    
    cell.numberLabel.hidden = YES;
    cell.colorLabel.hidden = YES;
    cell.backgroungColorlabel.hidden = YES;
    
    NSMutableDictionary *data = [self.currentFilterArray objectAtIndex:indexPath.section];
    NSString *filterName =[data objectForKey:@"name"];
    
    if ([filterName isEqualToString:@"Couleur"])
    {
        cell.borderView.layer.borderWidth = 0.3f;
        cell.borderView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        cell.colorLabel.hidden = NO;
        
        NSString *filterColorCode = [[[data objectForKey:@"values"] objectAtIndex:indexPath.row] objectForKey:@"color_code"];
        
        if (filterColorCode != NULL && ![filterColorCode isKindOfClass:[NSNull class]])
        {
            filterColorCode = [filterColorCode stringByReplacingOccurrencesOfString:@"#" withString:@""];
            UIColor *color1 = [COMMON colorWithHexString:filterColorCode];
            cell.colorLabel.backgroundColor = color1;
        }
        else
        {
            cell.colorLabel.backgroundColor = [UIColor clearColor];
        }
        
        if (isSelected)
        {
            for (NSIndexPath *collectionIndexPath in [filterCollectionView indexPathsForSelectedItems])
            {
                if ([indexPath isEqual:collectionIndexPath])
                {
                    cell.backgroungColorlabel.hidden = NO;
                    cell.backgroungColorlabel.layer.borderColor = [UIColor orangeColor].CGColor;
                    cell.backgroungColorlabel.layer.borderWidth = 2.0;
                    break;
                }
            }
        }
    }
    else
    {
        cell.numberLabel.hidden = NO;
        
        NSString *filterValue = [[[data objectForKey:@"values"] objectAtIndex:indexPath.row] objectForKey:@"name"];
        [cell.numberLabel  setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
        [cell.numberLabel setText:filterValue];
        CGSize labelSize = cell.numberLabel.frame.size;
        [cell.numberLabel sizeThatFits:labelSize];
        
        if (isSelected)
        {
            for (NSIndexPath *collectionIndexPath in [filterCollectionView indexPathsForSelectedItems])
            {
                if ([indexPath isEqual:collectionIndexPath])
                {
                    cell.backgroungColorlabel.hidden = NO;
                    cell.backgroungColorlabel.backgroundColor = [UIColor orangeColor];
                    break;
                }
            }
        }
    }
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionHeader)
    {
        UICollectionReusableView *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        
        if (reusableview==nil)
        {
            reusableview=[[UICollectionReusableView alloc] initWithFrame:CGRectMake(0, 0, 375, 50)];
        }
        
        for (UIView* view in reusableview.subviews)
        {
            if ([view isKindOfClass:[UILabel class]]) {
                [view removeFromSuperview];
            }
        }
        
        UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 375, 50)];
        NSMutableDictionary *data = [self.currentFilterArray objectAtIndex:indexPath.section];
        NSString *cellData = [data objectForKey:@"name"];
        label.text=cellData;
        [reusableview addSubview:label];
        return reusableview;
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *name;
    NSMutableDictionary *dict;
    dict = [self.currentFilterArray objectAtIndex:indexPath.section];
    name =[dict objectForKey:@"name"];
    
    if (IS_IPHONE4 ||IS_IPHONE5 ) {
        
        if ([name isEqualToString:@"Taille" ] || [name isEqualToString:@"Couleur"])
        {
            int numberOfCellInRow = 6;
            int padding = 5;
            CGFloat collectionCellWidth =  [[UIScreen mainScreen] bounds].size.width/numberOfCellInRow;
            CGFloat finalWidthWithPadding = collectionCellWidth - (padding);
            return CGSizeMake(finalWidthWithPadding , finalWidthWithPadding);
            return CGSizeMake(50, 50);
        }
        else
        {
            return CGSizeMake(90,40);
        }
    }
   else if (IS_IPHONE6 ||IS_IPHONE6_Plus)
        {
            if ([name isEqualToString:@"Taille" ] || [name isEqualToString:@"Couleur" ] )
            {
                int numberOfCellInRow = 6;
                int padding = 5;
                CGFloat collectionCellWidth =  [[UIScreen mainScreen] bounds].size.width/numberOfCellInRow;
                CGFloat finalWidthWithPadding = collectionCellWidth - (padding);
                return CGSizeMake(finalWidthWithPadding , finalWidthWithPadding);
                return CGSizeMake(50, 50);
            }
            else {
                return CGSizeMake(110,40);
            }
        }
    
   else {
       if ([name isEqualToString:@"Taille" ] || [name isEqualToString:@"Couleur" ] )
       {
           int numberOfCellInRow = 10;
           int padding = 5;
           CGFloat collectionCellWidth =  [[UIScreen mainScreen] bounds].size.width/numberOfCellInRow;
           CGFloat finalWidthWithPadding = collectionCellWidth - (padding);
           return CGSizeMake(finalWidthWithPadding , finalWidthWithPadding);
           return CGSizeMake(50, 50);
       }
       else {
           return CGSizeMake(140,60);
       }
   }

  return CGSizeMake(90,40);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    isSelected = YES;
    
    SSFilterCollectionViewCell *datasetCell = (SSFilterCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    NSMutableDictionary *data = [self.currentFilterArray objectAtIndex:indexPath.section];
    
    //OLD
    
//    NSString *strNbr = [[[data valueForKey:@"values"]objectAtIndex:indexPath.row]valueForKey:@"attribute_id"];
//    NSString *strSend = [NSString stringWithFormat:@"layered_%@_%@=%@_%@",[data valueForKey:@"type"],strNbr,strNbr,[data valueForKey:@"id_key"]];
    
//    if([filtersArr count] == 0)
//        [filtersArr addObject:strSend];
//    else
//        [filtersArr addObject:[NSString stringWithFormat:@"&%@",strSend]];
//    
//     filterStr = [filtersArr componentsJoinedByString:@""];
   
    //New change raji
    
    NSString *strAttrId = [[[data valueForKey:@"values"]objectAtIndex:indexPath.row]valueForKey:@"attribute_id"];
    NSString *strSend = [NSString stringWithFormat:@"layered_%@_%@=%@_%@",[data valueForKey:@"type"],strAttrId,strAttrId,[data valueForKey:@"id_key"]];
    
    [filterDict setObject:strSend forKey:strSend];
    
    
    NSString *filterName =[data objectForKey:@"name"];
    if ([filterName isEqualToString:@"Couleur" ] )
    {
        datasetCell.backgroungColorlabel.hidden = NO;
        datasetCell.backgroungColorlabel.layer.borderColor = [UIColor orangeColor].CGColor;
        datasetCell.backgroungColorlabel.layer.borderWidth = 2.0;
    }
    
    else if([filterName isEqualToString:@"Taille" ])
    {
        datasetCell.backgroungColorlabel.hidden = NO;
        datasetCell.backgroungColorlabel.backgroundColor = [UIColor orangeColor];
        for (UIView * view in datasetCell.backgroungColorlabel.subviews)
        {
            if ([view isKindOfClass:[UILabel class]])
            {
                [view removeFromSuperview];
            }
        }
    }
    else
    {
        datasetCell.backgroungColorlabel.hidden = NO;
        datasetCell.backgroungColorlabel.backgroundColor = [UIColor orangeColor];
    }
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    SSFilterCollectionViewCell *datadidselCell = (SSFilterCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    NSMutableDictionary *data = [self.currentFilterArray objectAtIndex:indexPath.section];
    
     //old
    //NSString *strNbr = [[[data valueForKey:@"values"]objectAtIndex:indexPath.row]valueForKey:@"nbr"];
     //NSString *strSend = [NSString stringWithFormat:@"layered_%@_%@=%@_%@",[data valueForKey:@"type"],strNbr,strNbr,[data valueForKey:@"id_key"]];
    //    if([filtersArr count] == 1)
    //        [filtersArr removeObject:strSend];
    //    else
    //        [filtersArr removeObject:[NSString stringWithFormat:@"&%@", strSend]];
    
    //      filterStr = [filtersArr componentsJoinedByString:@""];

    //new change raji nbr not using only attribute_id and id_key  layered_id_feature_”attribute_id”=“attribute_id”_”id_key"
    
    NSString *strAttrId = [[[data valueForKey:@"values"]objectAtIndex:indexPath.row]valueForKey:@"attribute_id"];
    NSString *strSend = [NSString stringWithFormat:@"layered_%@_%@=%@_%@",[data valueForKey:@"type"],strAttrId,strAttrId,[data valueForKey:@"id_key"]];
    
    NSLog(@"strSend-->%@",strSend);
    
    [filterDict removeObjectForKey:strSend];
    
    
    NSString *filterName = [data objectForKey:@"name"];
    if ([filterName isEqualToString:@"Couleur" ] )
    {
        datadidselCell.backgroungColorlabel.layer.borderColor = [UIColor clearColor].CGColor;
        datadidselCell.backgroungColorlabel.layer.borderWidth = 2.0;
    }
    
    else if([filterName isEqualToString:@"Taille" ])
    {
        datadidselCell.backgroungColorlabel.backgroundColor = [UIColor whiteColor];
    }
    else
    {
         datadidselCell.backgroungColorlabel.backgroundColor = [UIColor whiteColor];
    }
}

#pragma mark-Uibutton Action

-(void)annulerBtnAction:(id)sender
{
    SSAppDelegate *appDelegate = (SSAppDelegate *)[[UIApplication sharedApplication] delegate];
    [COMMON removeLoadingIcon];
    [[appDelegate categoryViewController] loadProducts];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)validerBtnAction:(id)sender
{
    filtersArr = (NSMutableArray*)[filterDict allValues];
    NSString *tempStr =  [filtersArr componentsJoinedByString:@"\n"];
    filterStr = [tempStr stringByReplacingOccurrencesOfString:@"\n" withString:@"&"];
    
    NSLog(@"filterStr-->%@",filterStr);
    
    [self dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FILTER" object:filterStr];
        SSAppDelegate *appDelegate = (SSAppDelegate *)[[UIApplication sharedApplication] delegate];
        [[appDelegate categoryViewController].productsTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    }];

}

@end
