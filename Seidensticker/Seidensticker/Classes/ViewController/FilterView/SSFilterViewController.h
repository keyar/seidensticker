//
//  SSFilterViewController.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/18/15.
//  Copyright (c) 2015 Oclocksoftware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSFilterCollectionViewCell.h"
#import "SSCategoryViewController.h"

@interface SSFilterViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

{
    IBOutlet SSFilterCollectionViewCell *ssFilterCollectionViewCell;
    
    NSMutableArray *filtersArr;
    NSString *filterStr;
    BOOL isSelected;
    
    
}
@property(nonatomic,retain)id controller;
@property (nonatomic, retain) NSMutableArray *currentFilterArray;
@property (strong, nonatomic) IBOutlet UICollectionView *filterCollectionView;
@property (nonatomic, strong)SSCategoryViewController *ssCategoryView;

- (void)loadNavigation;
@end
