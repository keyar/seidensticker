//
//  SSFilterCollectionViewCell.m
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/28/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSFilterCollectionViewCell.h"

@implementation SSFilterCollectionViewCell
@synthesize numberLabel,colorLabel,backgroungColorlabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"SSFilterCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
    }
    
    return self;
    
}
- (void)prepareForReuse {
    self.backgroungColorlabel = FALSE;
    // add remaining properties
}
@end
