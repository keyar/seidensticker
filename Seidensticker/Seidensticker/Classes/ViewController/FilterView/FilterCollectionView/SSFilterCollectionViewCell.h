//
//  SSFilterCollectionViewCell.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/28/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSFilterCollectionViewCell : UICollectionViewCell


@property (strong, nonatomic) IBOutlet UILabel *numberLabel,*backgroungColorlabel;
@property (strong, nonatomic) IBOutlet UIView *borderView;
@property (strong, nonatomic) IBOutlet UILabel *colorLabel;

@end
