//
//  SSProductPageViewController.m
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/18/15.
//  Copyright (c) 2015 Oclocksoftware. All rights reserved.
//

#import "SSAppDelegate.h"
#import "SSProductPageViewController.h"
#import "SSCustomNavigation.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#import "SSShoppingCart.h"
#import "AsyncImage.h"
#import "SSAddCartInfoView.h"
#import "NMBottomTabBarController.h"
#import "NMBottomTabBar.h"
#import "SelectSizeView.h"
#import "SSShoppingCart.h"

@interface SSProductPageViewController ()<NMBottomTabBarControllerDelegate,UITextViewDelegate,NSLayoutManagerDelegate> {
    SSWebService *webservice;
    NSMutableArray *quantityArray;
    
    BOOL isInfoPushedObj;
    UIWindow *windowInfo;
    
    BOOL isFromAddToCart;
    
    UIButton *previsousButtonSelected;
    
    SSCustomNavigation *navigation;
    UILabel *qtyCount;
    NSString *selectedQuantityValue;
    
    
}

@property (nonatomic,retain)  NSString *checkSize;

@property (nonatomic,retain)  NSMutableArray *infoArray;

@property (nonatomic,retain)  NSTimer *timer;

@property(nonatomic,retain) SSAddCartInfoView *viewAddCartInfo;

@property(nonatomic,retain) SelectSizeView *viewSelectSize;

@property (nonatomic,retain)NSArray *productTaileSize;
@property(strong,nonatomic) NSMutableArray *cartItems;



@end

@implementation SSProductPageViewController
@synthesize currentProductItem , infoArray,infoImage, timer , productView ,txtProductDescription,productWarrantyInfoView,productWarrantyInfoLabel,btnAddToCart,btnSize,productTaileSize,dividedViewThree,dividedViewOne,divideViewTwo,topView,productDetail, isSearchPage;
@synthesize cartItems;
@synthesize quantityButton;

#pragma mark - View States
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setHidesBackButton:YES];
    isQuanttyClicked = NO;
    isMonoView = NO;
    [self initialSetup];

  // Do any additional setup after loading the view from its nib.
    dividedViewThree.hidden=YES;
    sizeString = @"";

    //[quantityButton setUserInteractionEnabled:NO];
    webservice = [[SSWebService alloc] init];
    [quantityButton.titleLabel setFont:FuturaStd_Book(13)];
    [quantityButton setTitle:@"Quantity" forState:UIControlStateNormal];
    [btnSize  setTitle:@"Choisir une taille" forState:UIControlStateNormal];
    [btnSize.titleLabel setFont:FuturaStd_Book(13)];
    [quantityButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [quantityButton setBackgroundColor:[UIColor whiteColor]];
    [quantityButton addTarget:self
                       action:@selector(quantityAction:)
       forControlEvents:UIControlEventTouchUpInside];
    [btnSize addTarget:self
                       action:@selector(btnsizeAction:)
             forControlEvents:UIControlEventTouchUpInside];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     divideViewOne.hidden=YES;
    quantityArray = [[NSMutableArray alloc]init];
    productDetail = [[NSMutableDictionary alloc] init];
    selectedSize = @"";
    selectedQuantityValue= @"";
    [btnSize setTitle:@"Choisir une taille" forState:UIControlStateNormal];
    selectedQuantity = @"";
    [quantityButton setTitle:@"Quantité 1" forState:UIControlStateNormal];
    [self getProductDetail];

    [self loadNavigation];

    self.txtTitlePrice.layoutManager.delegate = self;
    [self Customize];
    [self.txtTitlePrice setFont:FuturaStd_Book(10)];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
//    self.productView.contentSize = CGSizeMake(self.view.frame.size.width,
//                                              self.productView.frame.size.height+self.txtProductDescription.frame.size.height + 150);
}

#pragma
#pragma mark-custom Navigation

- (void)loadNavigation
{
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    navigation = [[SSCustomNavigation alloc] initWithNibName:@"SSCustomNavigation" bundle:nil];
    navigation.view.tag = 1001;
    [self.navigationController.navigationBar addSubview:navigation.view];
    
    [navigation setLeftHidden:NO];
    
    [navigation setTitleHidden:NO];
    
    [navigation setShopBtnHidden:NO];
    [navigation setShopLabelHidden:NO];
    [navigation setCatergoryShopBtnLabelHidden:YES];

    
    [navigation.titleLbl setText:NSLocalizedString(@"Fiche produit", @"")];
    
    [navigation.titleLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    
    [navigation.navigationLeftLbl setText:NSLocalizedString(@"Retour", @"")];
    [navigation.navigationLeftLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [navigation.navigationLeftBtn addTarget:self action:@selector(productRetourBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self setLabel];

}
-(void)setLabel
{
    NSLog(@"cart values %@",[SHOPPINGCART getCartItems]);
    cartItems = [SHOPPINGCART getCartItems];
    
    NSInteger quantity = 0;
    
    
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }
    
    navigation.navigationShopLbl.text = [NSString stringWithFormat:@"%ld",(long)quantity];
    [navigation.navigationShopLbl setBackgroundColor:[UIColor clearColor]];
    navigation.navigationShopLbl.textColor = [UIColor blackColor];
    [navigation.navigationShopLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(9)]];
    [navigation.navigationShopLbl setTextAlignment:NSTextAlignmentCenter];
    
    [navigation.navigationShopBtn addTarget:self action:@selector(cartBtnAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void) Customize {
    [[self.btnSize layer] setBorderWidth:1.0f];
    [[self.btnSize layer] setBorderColor:[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f].CGColor];
    [[self.quantityButton layer] setBorderWidth:1.0f];
    [[self.quantityButton layer] setBorderColor:[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f].CGColor];

}
#pragma mark - Right Menu

-(void)cartBtnAction:(id)sender
{
    SSMonPanierViewController *monpanierViewController;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        isMonoView = NO;

        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonpanierViewControlleriPad" bundle:[NSBundle mainBundle]];
        
        
    }
    else {
        isMonoView = YES;

        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonPanierViewController" bundle:[NSBundle mainBundle]];
    }
    
    [self.navigationController pushViewController:monpanierViewController animated:YES];
}

#pragma mark - Product Detail

- (NSString *) getFormattedDescription {
    NSString *strDescription = [productDetail objectForKey:@"description_short"];
    
    strDescription = [NSString stringWithFormat:@"%@\n",strDescription];
    
    NSMutableArray *featuresArray = [productDetail objectForKey:@"features"];
    
    if (featuresArray != NULL) {
        for (int i=0; i<featuresArray.count; i++) {
            NSString *featuresName = [NSString stringWithFormat:@"%@:",[[featuresArray objectAtIndex:i] objectForKey:@"name"]];
            featuresName = [featuresName stringByPaddingToLength:(24-featuresName.length) withString:@" " startingAtIndex:0];
            
            NSString *featuresValue = [[featuresArray objectAtIndex:i] objectForKey:@"value"];
            strDescription = [NSString stringWithFormat:@"%@\n%@\t%@",strDescription,featuresName,featuresValue];
        }
    }
    
    return strDescription;
    
}

#pragma mark-ProductWarrantyInfoDetail

-(void) productWarrantyInfoDetail
{
    NSMutableArray *productWarrantyInfoArray=[productDetail objectForKey:@"product_warranty_info"];
    
    NSString *checkProductWarrantyCondition=[NSString stringWithFormat:@"%@",productWarrantyInfoArray];

    if([checkProductWarrantyCondition isEqualToString:@""] )
    {
        
        self.productWarrantyInfoView.hidden=YES;
        self.dividedViewOne.hidden=YES;
        self.divideViewTwo.hidden=YES;
        self.btnSize.hidden = YES;
        [self updateHeightConstraints];

            if([ProductTaileString isEqualToString:@"TailleUnique"]) {
                //[quantityButton setUserInteractionEnabled:YES];
                [quantityButton setFrame:btnSize.frame];

            } else {
                self.btnSize.hidden = NO;

            }

    }
    else if(productWarrantyInfoArray != NULL && productWarrantyInfoArray >0)
    {
        
        for(int i=0; i<productWarrantyInfoArray.count;i++)
        {
            UILabel *productWarrantyLabel = [[UILabel alloc] initWithFrame:CGRectMake(40,20*i+5,self.productWarrantyInfoView.frame.size.width,32)];
            
            [productWarrantyLabel setFont:[UIFont fontWithName:@"FuturaStd-Book" size:14]];
            
            NSLog(@"%@",productWarrantyLabel);
            
            productWarrantyLabel.text = [productWarrantyInfoArray objectAtIndex:i];
            
            [self.productWarrantyInfoView addSubview:productWarrantyLabel];
            
            UIImageView *productWarrantyTickImage =[[UIImageView alloc] initWithFrame:CGRectMake(12,20*i+10,20,20)];
            productWarrantyTickImage.image=[UIImage imageNamed:@"tick"];
            
            [self.productWarrantyInfoView addSubview:productWarrantyTickImage];
            
        }
    }
}


#pragma mark-ProductimageDetail

-(void) productimageDeatil
{

    infoArray = [productDetail objectForKey:@"product_image"];
    
    for(int i=0;i<infoArray.count;i++)
    {
        
        NSString *photoURLString =[[productDetail objectForKey:@"product_image"] objectAtIndex:i];
        int yPosition;
        if(IS_IPHONE_4)
        {
              yPosition=0;
        }
        else if(IS_IPHONE5  )
        {
            yPosition=-20;
            
            
        }
        else if(IS_IPHONE6 ||IS_IPHONE6_Plus)
        {
              yPosition=-70;
            
        }
        else{
            yPosition=-70;
        }
        infoImage = [[UIImageView alloc] initWithFrame:CGRectMake(i*(self.view.frame.size.width)+0, yPosition, self.view.frame.size.width, self.view.frame.size.height)];
        infoImage.tag = i+1;
        
        AsyncImage *asyncImage = [[AsyncImage alloc]initWithFrame:CGRectMake(0, 0, self.infoImage.frame.size.width, self.infoImage.frame.size.height)];
        
        [asyncImage setLoadingImage];
        [asyncImage loadImageFromURL:[NSURL URLWithString:photoURLString]
                                type:AsyncImageResizeTypeAspectRatio
                             isCache:YES];
        
        [infoImage addSubview:asyncImage];
        [slideScroll addSubview:infoImage];
        
         NSLog(@"Productcheck = %@",[productDetail objectForKey:@"product_image"]);
        
        
    }
    [slideScroll setContentSize:CGSizeMake((infoArray.count * self.view.frame.size.width), 240)];
   // [slideScroll setContentSize:CGSizeMake((infoArray.count * self.view.frame.size.width), screen_height)];
    xslider=0;
    pgDtView=[[UIView alloc]init];
    pgDtView.backgroundColor=[UIColor clearColor];
    pageImageView =[[UIImageView alloc]init];
    infoPageControl.numberOfPages=infoArray.count;
    
    CGRect sliderScrollFrame = slideScroll.frame;
    //CGFloat pageControlViewYPos = CGRectGetMaxY(sliderScrollFrame);
    
    
    UIView *pageControlView = [[UIView alloc]initWithFrame:CGRectMake(0, (slideScroll.frame.origin.y+slideScroll.frame.size.height)+10, screen_width, 20)];
    [self.productView addSubview:pageControlView];
    
    pageControlView.backgroundColor=[UIColor clearColor];
    for(int i=0;i<infoPageControl.numberOfPages;i++)
    {
        blkdot=[[UIImageView alloc]init];
        [blkdot setFrame:CGRectMake(i*13, 0, 7, 7 )];
        [blkdot setImage:[UIImage imageNamed:@"dot_normal"]];
        [pgDtView addSubview:blkdot];
        [pageImageView setFrame:CGRectMake(0, 0,7, 7)];
        [pageImageView setImage:[UIImage imageNamed:@"dot_active"]];
        [pgDtView addSubview:pageImageView];
        //[topView addSubview:pgDtView];
        
       CGFloat width = infoPageControl.numberOfPages * 7;
        
        CGRect pageControlViewFrame = pageControlView.frame;
        pageControlViewFrame.size.width = width;
        pageControlView.frame = pageControlViewFrame;
         [pageControlView addSubview:pgDtView];
        
        
//        if(IS_IPHONE5||IS_IPHONE4)
//        {
//            [pgDtView setFrame:CGRectMake(100, -35, infoPageControl.numberOfPages*13, 10)];
//
//        }
//        else if(IS_IPHONE6_Plus||IS_IPHONE6)
//        {
//            [pgDtView setFrame:CGRectMake(125, -20, infoPageControl.numberOfPages*13, 10)];
//        }
//        else{
//            [pgDtView setFrame:CGRectMake(320, 50, infoPageControl.numberOfPages*13, 10)];
//        }

    }
   
    [pgDtView setFrame:CGRectMake(0,pageControlView.frame.size.height - 15,pageControlView.frame.size.width,7)];//10
    pageControlView.center = CGPointMake(self.productView.center.x, (slideScroll.frame.origin.y+slideScroll.frame.size.height)+10);
    [pageControlView addSubview:pgDtView];

}


#pragma mark-setProductDetail

- (void) setProductDetail
{
    NSLog(@"Product Detail = %@",productDetail);
    if(isStoreApp){
         self.txtTitlePrice.text = [COMMON getDecodedString:[NSString stringWithFormat:@"%@\n%@",[productDetail objectForKey:@"name"],[productDetail objectForKey:@"price"]]];
        
    }
    else{
        if([productDetail objectForKey:@"special_price"])
        {
            self.txtTitlePrice.text = [COMMON getDecodedString:[NSString stringWithFormat:@"%@",[productDetail objectForKey:@"name"]]];
            int xpositionOriginalPrice;
            int xpositionDiscountPrice;
            int xpositionDiscountPerPrice;
            if(IS_IPHONE5 ||IS_IPHONE4)
            {
                xpositionOriginalPrice=60;
                xpositionDiscountPrice=120;
                xpositionDiscountPerPrice=180;
                
            }
            else if(IS_IPHONE6 )
            {
                xpositionOriginalPrice=100;
                xpositionDiscountPrice=160;
                xpositionDiscountPerPrice=215;
            }
            else if(IS_IPHONE6_Plus)
            {
                xpositionOriginalPrice=120;
                xpositionDiscountPrice=180;
                xpositionDiscountPerPrice=235;
            }
            else
            {
                xpositionOriginalPrice=300;
                xpositionDiscountPrice=360;
                xpositionDiscountPerPrice=415;
            }
            UILabel *productOriginalPrice = [[UILabel alloc] initWithFrame:CGRectMake(xpositionOriginalPrice,28, 60,32)];
            
            [productOriginalPrice setFont:[UIFont fontWithName:@"FuturaStd-Book" size:12]];
            productOriginalPrice.textColor=[UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:2.0f];
            productOriginalPrice.text=[COMMON getDecodedString:[NSString stringWithFormat:@"%@",[productDetail objectForKey:@"price"]]];
            
            UIView *divideview = [[UILabel alloc] initWithFrame:CGRectMake(0,15, 42,1)];
            divideview.backgroundColor=[UIColor blackColor];
            [productOriginalPrice addSubview:divideview];
            
            [self.txtTitlePrice addSubview:productOriginalPrice];
            
            UILabel *productDiscountPrice = [[UILabel alloc] initWithFrame:CGRectMake(xpositionDiscountPrice,28, 60,32)];
            
            [productDiscountPrice setFont:[UIFont fontWithName:@"FuturaStd-Book" size:12]];
            productDiscountPrice.textColor=[UIColor redColor];
            productDiscountPrice.text=[COMMON getDecodedString:[NSString stringWithFormat:@"%@",[productDetail objectForKey:@"special_price"]]];
            [self.txtTitlePrice addSubview:productDiscountPrice];
            
            float originalValue= [productOriginalPrice.text floatValue];
            float discountvalue=[productDiscountPrice.text floatValue];
            float productMinusValue= originalValue-discountvalue;
            int DiscountPerValue= (productMinusValue*100)/originalValue;
            
            UILabel *productDiscountPerPrice = [[UILabel alloc] initWithFrame:CGRectMake(xpositionDiscountPerPrice,33, 45,20)];
            
            [productDiscountPerPrice setFont:[UIFont fontWithName:@"FuturaStd-Book" size:12]];
            productDiscountPerPrice.backgroundColor=[UIColor redColor];
            productDiscountPerPrice.textColor=[UIColor whiteColor];
            productDiscountPerPrice.layer.masksToBounds = YES;
            productDiscountPerPrice.layer.cornerRadius = 8.0;
            productDiscountPerPrice.text=[NSString stringWithFormat:@"  - %d%%",DiscountPerValue];
            [self.txtTitlePrice addSubview:productDiscountPerPrice];
        }
        else{
            self.txtTitlePrice.text = [COMMON getDecodedString:[NSString stringWithFormat:@"%@\n%@",[productDetail objectForKey:@"name"],[productDetail objectForKey:@"price"]]];
        }
 
    }
    
    
    self.txtProductDescription.text = [self getFormattedDescription];
    self.txtProductDescription.editable = NO;
    
    NSString *refStr = [self getFormattedDescription];
    
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]init];
    
    attributedStr = [self getAttributedString:refStr];
    
    CGRect rect = [attributedStr boundingRectWithSize:CGSizeMake(txtProductDescription.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    
    self.txtReference.text = [NSString stringWithFormat:@"Référence : %@",[COMMON getDecodedString:[productDetail objectForKey:@"reference"]]];
    self.txtTitlePrice.textAlignment    = NSTextAlignmentCenter;
    self.txtProductDescription.textAlignment   = NSTextAlignmentJustified;
 
    [self productimageDeatil];
    
    [self productWarrantyInfoDetail];
    
    CGFloat txtReferenceHeight = rect.size.height;
    
    CGRect textDescframe = txtProductDescription.frame;
    
    textDescframe.size.height =  txtReferenceHeight + 30;
    
    [txtProductDescription setFrame:textDescframe];
    
    NSLog(@"txtReferenceHeight--> %f",txtReferenceHeight);
    
    [self updateScrollViewContentSize];
    
    [self.btnAddToCart setEnabled:YES];
    [self.btnAddToCart setUserInteractionEnabled:YES];
    
    
}
- (NSMutableAttributedString *)getAttributedString:(NSString *)productStr {
    
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]init];
    
    NSDictionary *attributes = @{NSFontAttributeName : [COMMON getResizeableFont:FuturaStd_Book(14)],
                                 NSForegroundColorAttributeName : [UIColor blackColor]};
    
    NSAttributedString *firstAttributedString = [[NSAttributedString alloc] initWithString:productStr
                                                                                attributes: attributes];
    
    
    [attributedStr appendAttributedString:[firstAttributedString mutableCopy]];
    
    return attributedStr;
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView2
{
    if (scrollView2==slideScroll)
    {
        pull=@"";
        jslider = scrollView2.contentOffset.x/self.view.frame.size.width;
        [slideScroll setNeedsDisplay];
        infoPageControl.currentPage=jslider;
        [pageImageView setFrame:CGRectMake(jslider*13, 0, 7, 7)];
    }
    
    isTapping=NO;
    scrolldragging=@"YES";
}

#pragma mark- Hide the taile Button

-(void)hideTaileButton
{
    self.productTaileSize=[productDetail objectForKey:@"combinations"];
    
    NSLog(@"productTaileSize%@",self.productTaileSize);
    
    NSSortDescriptor *sortSizeArray = [[NSSortDescriptor alloc] initWithKey:@"attribute_name" ascending:YES];
    self.productTaileSize = [self.productTaileSize sortedArrayUsingDescriptors:@[sortSizeArray]];
    NSLog(@"productTaileSize%@",self.productTaileSize);
//    if(productTaileSize.count == 0){
//      
//        //FOR Combinations Empty
//        ProductTaileString=@"TailleUnique";
//    }
//    else{
//         ProductTaileString=[[productTaileSize valueForKey:@"attribute_name"]objectAtIndex:0];
//    }
    ProductTaileString=[[productTaileSize valueForKey:@"attribute_name"]objectAtIndex:0];

    if(self.productTaileSize != NULL && [ProductTaileString isEqualToString:@"TailleUnique"])
    {
        quantityArray = [[[productTaileSize firstObject] valueForKey:@"quantities"] valueForKey:@"quantity"];
        NSString *quantityString = [[productTaileSize firstObject] valueForKey:@"total_quantity"];
        quantityApiCount = [quantityString integerValue];
        [quantityButton setFrame:btnSize.frame];
//        if (IS_IPHONE5 || IS_IPHONE4 ||IS_IPHONE6) {
//            CGRect quantityFrame = quantityButton.frame;
//            quantityFrame.origin.y = btnSize.frame.origin.y+5;
//            quantityFrame.size.height = 30;
//            self.quantityButton.frame = quantityFrame;
//        }


       // [quantityButton setUserInteractionEnabled:YES];

        self.btnSize.hidden=YES;
        

        NSMutableArray *productWarrantyInfoArray=[productDetail objectForKey:@"product_warranty_info"];
        
        NSString *checkProductWarrantyCondition=[NSString stringWithFormat:@"%@",productWarrantyInfoArray];
        
        if([checkProductWarrantyCondition isEqualToString:@""] )
        {
            
            self.productWarrantyInfoView.hidden=YES;
            self.dividedViewOne.hidden=YES;
            self.divideViewTwo.hidden=YES;
        }
        else if(productWarrantyInfoArray != NULL && productWarrantyInfoArray >0)
        {
            
            for(int i=0; i<productWarrantyInfoArray.count;i++)
            {
                UILabel *productWarrantyLabel = [[UILabel alloc] initWithFrame:CGRectMake(40,20*i+5,self.productWarrantyInfoView.frame.size.width,32)];
                
                [productWarrantyLabel setFont:[UIFont fontWithName:@"FuturaStd-Book" size:14]];
                
                NSLog(@"%@",productWarrantyLabel);
                
                productWarrantyLabel.text = [productWarrantyInfoArray objectAtIndex:i];
                
                [self.productWarrantyInfoView addSubview:productWarrantyLabel];
                
                UIImageView *productWarrantyTickImage =[[UIImageView alloc] initWithFrame:CGRectMake(12,20*i+10,20,20)];
                productWarrantyTickImage.image=[UIImage imageNamed:@"tick"];
                
                [self.productWarrantyInfoView addSubview:productWarrantyTickImage];
                
            }
            
        }
    }
    else
    {
        
        self.btnSize.hidden=NO;
        self.btnAddToCart.hidden=NO;
        self.quantityButton.hidden=NO;

    }
}

#pragma mark-load product Detail

- (void) getProductDetail {
    
    [COMMON showLoadingIcon:self];
    
    NSString *productURL = [currentProductItem objectForKey:@"url"];
    
    [webservice getRequestForURL:productURL success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        productDetail = [[responseObject objectForKey:@"productdetails"] objectForKey: @"product" ];
       if(isMonoView == NO)
         [self hideTaileButton];
        [self setProductDetail];
        [COMMON removeLoadingIcon];
        
    } failure:^(AFHTTPRequestOperation *operation, id error) {
        [COMMON removeLoadingIcon];
    }];
}

#pragma mark - Update ScrollView Content Size

- (void) updateScrollViewContentSize {
//    if(IS_IPHONE5) {
// 
//        self.productView.contentSize = CGSizeMake(self.view.frame.size.width,
//                                                  self.productView.frame.size.height +
//                                                  self.txtProductDescription.frame.size.height +50
//                                                   );
//    }
//    else if(IS_IPHONE6){
//        self.productView.contentSize = CGSizeMake(self.view.frame.size.width,
//                                                  self.productView.frame.size.height +
//                                                  self.txtProductDescription.frame.size.height
//                                                  -40);
//
//    }
//    else if(IS_IPHONE6_Plus){
//        self.productView.contentSize = CGSizeMake(self.view.frame.size.width,
//                                                  self.productView.frame.size.height +
//                                                  self.txtProductDescription.frame.size.height
//                                                  -240);
//        
//    }
//    else if(IS_IPHONE4) {
//        self.productView.contentSize = CGSizeMake(self.view.frame.size.width,
//                                                      self.productView.frame.size.height +
//                                                      self.txtProductDescription.frame.size.height + 150);
//        }
//    else {
//        self.productView.contentSize = CGSizeMake(self.view.frame.size.width,
//                                                           self.productView.frame.size.height+self.txtProductDescription.frame.size.height + 150);
//    }
    
//    CGRect contentRect = CGRectZero;
//    for (UIView *view in self.productView.subviews) {
//        contentRect = CGRectUnion(contentRect, view.frame);
//    }
//    
//    [self.productView setContentSize:contentRect.size];
    
    CGRect textDescframe = txtProductDescription.frame;
    
    CGFloat textDescMaxY = CGRectGetMaxY(textDescframe);
    
    NSLog(@"textDescMaxY--> %f",textDescMaxY);
    
    [self.productView setContentSize:CGSizeMake(screen_width, textDescMaxY)];
    
}

#pragma mark- showSelectedtaileSize

-(void)showTaileView
{
    float buttonWidth=0.0;
    float buttonHeight = 0.0;
    NSString *productSizeValue;
    int sizeTag;
    
    windowInfo = [[[UIApplication sharedApplication] delegate] window];
    
    SelectSizeView *viewSelectSize = [[SelectSizeView alloc] init];
    
    [[[[UIApplication sharedApplication] delegate] window] addSubview:viewSelectSize];
    [self setViewSelectSize:viewSelectSize];
    [viewSelectSize.labelTitle setText:NSLocalizedString(@"Séletionnez une taille", @"")];

    
    NSDictionary *dictView = @{@"_viewSelectSize":_viewSelectSize};
    
    
    [[[[UIApplication sharedApplication] delegate] window] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_viewSelectSize]|"
                                                                                                                  options:0
                                                                                                                  metrics:nil views:dictView]];
    [[[[UIApplication sharedApplication] delegate] window] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_viewSelectSize]|"
                                                                                                                  options:0
                                                                                                                  metrics:nil views:dictView]];
    int yPos = 0;
    int xPos = ((self.view.frame.size.width - (6*20))/8)/2;
    int seperator = 15;
    if(IS_IPHONE6 ||IS_IPHONE6_Plus ||IS_IPHONE4 || IS_IPHONE5)
    {
        buttonWidth = (self.view.frame.size.width - (6*20))/6;
        buttonHeight = (self.view.frame.size.width - (6*20))/6;
    }
    else{
        buttonWidth = (self.view.frame.size.width - (6*20))/6;
        buttonHeight = (self.view.frame.size.width - (6*50))/6;
    }
    for (UIView *subview in [_viewSelectSize.scrollviewButtonContainer subviews]) {
        [subview removeFromSuperview];
    }
    
    self.productTaileSize=[productDetail objectForKey:@"combinations"];
    
    NSLog(@"%@",self.productTaileSize);
    
    NSSortDescriptor *sortSizeArray = [[NSSortDescriptor alloc] initWithKey:@"attribute_name" ascending:YES];
    self.productTaileSize = [self.productTaileSize sortedArrayUsingDescriptors:@[sortSizeArray]];
    NSLog(@"%@",self.productTaileSize);
//    
//    if(productTaileSize.count == 0){
//        //FOR Combinations Empty
//        productSizeValue=@"TailleUnique";
//        
//    }
//    else{
//        productSizeValue=[[productTaileSize valueForKey:@"attribute_name"]objectAtIndex:0];
//    }
    
    productSizeValue=[[productTaileSize valueForKey:@"attribute_name"]objectAtIndex:0];
    NSLog(@"Product Taile String===%@",productSizeValue);
    if(self.productTaileSize != NULL && [productSizeValue isEqualToString:@"TailleUnique"] )
    {
        //[self.viewSelectSize removeFromSuperview];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.viewSelectSize removeFromSuperview];
           if (isFromAddToCart) {
               
                [self lightBoxView];
            }
        });
    }

//    else if(self.productTaileSize != NULL && self.productTaileSize.count ==1){
//        self.btnSize.hidden=NO;
//        int i=0;
//        NSLog(@"%d",i);
//        productSizeValue = [[productTaileSize objectAtIndex:i] objectForKey:@"attribute_name"];
//            NSLog(@"%@",productSizeValue);
//        UIButton *theButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        //[theButton addTarget:self action:@selector(selectSizeButtonAction:) forControlEvents:UIControlEventTouchDown];
//        int value =[productSizeValue intValue];
//        theButton.tag = value;
//        //[self selectSizeButtonAction:nil];
//    }
    else if (self.productTaileSize != NULL)
    {
        self.btnSize.hidden=NO;
        self.quantityButton.hidden=NO;
        self.btnAddToCart.hidden=NO;
        if (self.productTaileSize.count%6 == 0) {
            viewSelectSize.rowCounts = self.productTaileSize.count/6;
        }
        else{
            viewSelectSize.rowCounts = self.productTaileSize.count/6 + 1;
        }
        int indexSize=1;
        for (int i=0; i<self.productTaileSize.count;i++)
        {
            NSLog(@"%d",i);
            productSizeValue = [[productTaileSize objectAtIndex:i] objectForKey:@"attribute_name"];
            NSLog(@"%@",productSizeValue);
            
            NSString *SizeValueTag=[NSString stringWithFormat:@"%@",productSizeValue];
            sizeTag=[SizeValueTag intValue];
            NSLog(@"%d",sizeTag);
            UIButton *buttonSize;
            buttonSize = [UIButton buttonWithType:UIButtonTypeCustom];
            [buttonSize setFrame:CGRectMake(xPos, yPos, buttonWidth, buttonHeight)];
            [buttonSize addTarget:self action:@selector(selectSizeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            //[buttonSize setTag:sizeTag];
            [buttonSize setTitle:[NSString stringWithFormat:@"%@",SizeValueTag] forState:UIControlStateNormal];
            [buttonSize setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
            [buttonSize.titleLabel setFont:FuturaStd_Book(14)];
            [buttonSize.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
            [buttonSize.layer setBorderWidth:1.0];
            NSLog(@"Titel = %@",[self.btnSize currentTitle]);
            NSString *strSize = [NSString stringWithFormat:@"%@",SizeValueTag];
            self.checkSize=[NSString stringWithFormat:@"%@",strSize];
            NSLog(@" size %@",self.checkSize);
            NSLog(@"self.btnSize currentTitle %@",[self.btnSize currentTitle]);
            if ([[self.btnSize currentTitle] rangeOfString:strSize].location != NSNotFound) {
                [buttonSize setBackgroundColor:ORANGE_COLOR];
                previsousButtonSelected = buttonSize;
            }
            [_viewSelectSize.scrollviewButtonContainer addSubview:buttonSize];
            xPos = xPos + buttonWidth + seperator;
            if (indexSize % 6 == 0) {
                yPos = yPos + buttonHeight + seperator;
                xPos = ((self.view.frame.size.width - (6*20))/8)/2;
            }
            
            indexSize++;
        }
    }
    
}

-(void) lightBoxView
{
    if ([selectedSize isEqualToString:@""] || [selectedSizeIPA isEqualToString:@""]) {
        
        if (self.productTaileSize == NULL) {
            self.productTaileSize = [productDetail objectForKey:@"combinations"];
        }
        NSLog(@"%@",self.productTaileSize);
        
        //FOR Combinations those have Only one Attribute
        if (self.productTaileSize != NULL && [self.productTaileSize isKindOfClass:[NSArray class]] && [self.productTaileSize count] == 1) {
            
            selectedSize    = [[self.productTaileSize objectAtIndex:0] valueForKey:@"attribute_name"];
            selectedSizeIPA = [[self.productTaileSize objectAtIndex:0] valueForKey:@"id_product_attribute"];
        }
//         //FOR Combinations Empty
//        if(productTaileSize.count == 0){
//            selectedSize    = @"TailleUnique";
//            selectedSizeIPA = @"0000";
//        }

    }
    if (productDetail != NULL && ![selectedSize isEqualToString:@""]) {
        if (![selectedQuantity isEqualToString:@""]) {
            NSUInteger quantityCount = [selectedQuantity integerValue];
//              NSUInteger quatyCount = [quantityArray count];
            [SHOPPINGCART addToCart:productDetail withSize:selectedSize withSizeIPA:selectedSizeIPA andQuantity:quantityCount quantityCount:quantityApiCount isCartTable:NO];
        }
        else {
            [SHOPPINGCART addToCart:productDetail withSize:selectedSize withSizeIPA:selectedSizeIPA andQuantity:1 quantityCount:1 isCartTable:NO];
        }
    }
    windowInfo = [[[UIApplication sharedApplication] delegate] window];
    
    SSAddCartInfoView *viewAddCartInfo = [[SSAddCartInfoView alloc] init];
    [windowInfo addSubview:viewAddCartInfo];
    [self setViewAddCartInfo:viewAddCartInfo];
    
    [viewAddCartInfo.buttonContinuer addTarget:self action:@selector(continuerButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [viewAddCartInfo.buttonAller     addTarget:self action:@selector(allerButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary *dictView = @{@"_viewAddCartInfo":_viewAddCartInfo};
    [windowInfo addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_viewAddCartInfo]|"
                                                                       options:0
                                                                       metrics:nil views:dictView]];
    [windowInfo addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_viewAddCartInfo]|"
                                                                       options:0
                                                                       metrics:nil views:dictView]];
    if([selectedSize isEqualToString:@""])
    {
        [viewAddCartInfo removeFromSuperview];
        [self showTaileView];
    }
    
    [self setLabel];
}

#pragma mark-Button Action
-(void)productRetourBtnAction:(id)sender
{
    NSArray *array = [self.navigationController viewControllers];
    if(isSearchPage==YES){
        //Go to Search page
        [self.navigationController popToViewController:[array objectAtIndex:0] animated:YES];
        [self.navigationController setNavigationBarHidden:YES];
    }
    else{
        //Go to ProductList page
        if([self.redirectproduct isEqualToString: @"YES"])
        {
            [self.navigationController popToViewController:[array objectAtIndex:0] animated:YES];
            [self.navigationController setNavigationBarHidden:YES];

        }
        else{
        [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];
        }
    }
    
}

- (IBAction)addToCart:(id)sender {
    
    if ([selectedSize isEqualToString:@""] || [selectedSizeIPA isEqualToString:@""]) {
        isFromAddToCart = YES;
        [self showTaileView];
    }
    else {
        if(isQuanttyClicked) {
            //isQuanttyClicked = NO;
            [self lightBoxView];
        } else{
            //[self QuantityView];
            [self.view setUserInteractionEnabled:NO];
            [self performSelector:@selector(QuantityView) withObject:self afterDelay:0.2];

            //isQuanttyClicked = YES;
        }
    }
}

-(IBAction)continuerButtonAction:(id)sender{
    [self.viewAddCartInfo removeFromSuperview];
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
    [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];

    
}

-(IBAction)allerButtonAction:(id)sender{
    [self.viewAddCartInfo removeFromSuperview];
//     [self.navigationController setNavigationBarHidden:YES];
//    SSAppDelegate *appDelegate = (SSAppDelegate *) [UIApplication sharedApplication].delegate;
//    [appDelegate selectTabAtIndex:2];
    SSMonPanierViewController *monpanierViewController;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        isMonoView = NO;
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonpanierViewControlleriPad" bundle:[NSBundle mainBundle]];
        
        
    }
    else {
        isMonoView = YES;
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonPanierViewController" bundle:[NSBundle mainBundle]];
    }
    
    [self.navigationController pushViewController:monpanierViewController animated:YES];

    
}

- (IBAction)lightBox:(id)sender {
}

- (IBAction)btnsizeAction:(id)sender
{
    //[quantityButton setUserInteractionEnabled:YES];
    isFromAddToCart = NO;
    [self showTaileView];
    isQuanttyClicked = NO;

}

- (IBAction)quantityAction:(id)sender {
    isQuanttyClicked = YES;
    isFromAddToCart = NO;
   // [self QuantityView];
    [self.view setUserInteractionEnabled:NO];
    [self performSelector:@selector(QuantityView) withObject:self afterDelay:0.2];


}

- (NSString *) getIPAForSize:(NSString *) currentSize {

    if (self.productTaileSize == NULL) {
        self.productTaileSize = [productDetail objectForKey:@"combinations"];
    }
    
    if (self.productTaileSize != NULL && [self.productTaileSize isKindOfClass:[NSArray class]]) {
        for (int i=0; i<self.productTaileSize.count;i++) {
            if ([[[self.productTaileSize objectAtIndex:i] objectForKey:@"attribute_name"] isEqualToString:currentSize]) {
                return [[self.productTaileSize objectAtIndex:i] objectForKey:@"id_product_attribute"];
            }
        }
    }
    return @"";
}
-(IBAction)selectSizeButtonAction:(id)sender{
    NSLog(@"Selected size =%ld",(long)[sender tag]);
    isQuanttyClicked = NO;
    UIButton *sizeButton=(UIButton *)sender;
    NSString *lastTemp = sizeString;
    sizeString=sizeButton.titleLabel.text;
    
    selectedQuantityValue=@"";

    
    if([lastTemp isEqualToString:sizeString]) {
    } else {
        [quantityButton setTitle:@"Quantité 1" forState:UIControlStateNormal];
    }
    NSUInteger Proindex = 0;
    for (int i=0; i<[self.productTaileSize count]; i++) {
        NSString *strSTr=[[self.productTaileSize valueForKey:@"attribute_name"] objectAtIndex:i];
        if ([sizeString isEqualToString:strSTr]) {
            Proindex=i;
            break;
        }
    }
    
    
    NSArray *quantityAray = [[self.productTaileSize valueForKey:@"quantities"]objectAtIndex:Proindex];
    NSString *quantityString =[[self.productTaileSize valueForKey:@"total_quantity"]objectAtIndex:Proindex];
    quantityApiCount = [quantityString integerValue];

    
    quantityArray = [quantityAray valueForKey:@"quantity"];
    
    NSLog(@"%@",quantityArray);

    if (sender == nil) {
        selectedSize = [[self.productTaileSize objectAtIndex:0]  objectForKey:@"attribute_name"];
    } else {
        selectedSize    = sizeString;
    }
    selectedSizeIPA = [self getIPAForSize:selectedSize];
    [self.btnSize setTitle:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Taille", @""),selectedSize] forState:UIControlStateNormal];
    
    
    if (sender != nil) {
//
        [sender setBackgroundColor:ORANGE_COLOR];

        if (sender != previsousButtonSelected) {
            [previsousButtonSelected setBackgroundColor:[UIColor clearColor]];
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.viewSelectSize removeFromSuperview];
//            if (isFromAddToCart) {
//                [self lightBoxView];
            //}
            //[self QuantityView];
            [self.view setUserInteractionEnabled:NO];
             [self performSelector:@selector(QuantityView) withObject:self afterDelay:0.2];
            

        });
    } else {
        [self.btnSize setUserInteractionEnabled:NO];

    }
    
}

-(void)QuantityView
{
    [self.view setUserInteractionEnabled:YES];
    float buttonWidth=0.0;
    float buttonHeight = 0.0;
    NSString *productSizeValue;
    int sizeTag;
    
    
    windowInfo = [[[UIApplication sharedApplication] delegate] window];
    
    SelectSizeView *viewSelectSize = [[SelectSizeView alloc] init];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:viewSelectSize];
    [self setViewSelectSize:viewSelectSize];
    [viewSelectSize.labelTitle setText:NSLocalizedString(@"Sélectionner une quantité", @"")];  //Séletionnez une Quantité  new->Sélectionner une quantité

    NSDictionary *dictView = @{@"_viewSelectSize":_viewSelectSize};
    
    
    [[[[UIApplication sharedApplication] delegate] window] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_viewSelectSize]|"
                                                                                                                  options:0
                                                                                                                  metrics:nil views:dictView]];
    [[[[UIApplication sharedApplication] delegate] window] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_viewSelectSize]|"
                                                                                                                  options:0
                                                                                                                  metrics:nil views:dictView]];
    int yPos = 0;
    int xPos = ((self.view.frame.size.width - (6*20))/8)/2;
    int seperator = 15;
    if(IS_IPHONE6 ||IS_IPHONE6_Plus ||IS_IPHONE4 || IS_IPHONE5)
    {
        buttonWidth = (self.view.frame.size.width - (6*20))/6;
        buttonHeight = (self.view.frame.size.width - (6*20))/6;
    }
    else{
        buttonWidth = (self.view.frame.size.width - (6*20))/6;
        buttonHeight = (self.view.frame.size.width - (6*50))/6;
    }
    for (UIView *subview in [_viewSelectSize.scrollviewButtonContainer subviews]) {
        [subview removeFromSuperview];
    }
    
    
    NSLog(@"%@",quantityArray);
    
    
    if([quantityArray count]!=0 && quantityArray != NULL)
      productSizeValue=[NSString stringWithFormat:@"%@",[quantityArray firstObject]];//objectAtIndex:0
    NSLog(@"Product Taile String===%@",productSizeValue);
    if(quantityArray != NULL && [productSizeValue isEqualToString:@"TailleUnique"] )
    {
        //[self.viewSelectSize removeFromSuperview];
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [self.viewSelectSize removeFromSuperview];
//            if (isFromAddToCart) {
//                
//                [self lightBoxView];
//            }
//        });
    }
    
    else if (quantityArray != NULL)
    {
//        self.btnSize.hidden=NO;
        self.btnAddToCart.hidden=NO;
        self.quantityButton.hidden=NO;
        if (quantityArray.count%6 == 0) {
            viewSelectSize.rowCounts = quantityArray.count/6;
        }
        else{
            viewSelectSize.rowCounts = quantityArray.count/6 + 1;
        }
        
        NSUInteger quantityArrayCount ;
        quantityArrayCount = 0;
        if([quantityArray count]!=0) {
            if(quantityArray.count <=10){
                quantityArrayCount = [quantityArray count] ;
            }
            else {
                
                quantityArrayCount = [[quantityArray subarrayWithRange:NSMakeRange(0, 9)] count];
            }
        }
        
        int indexSize=1;
        for (int i=0; i<quantityArrayCount;i++)
        {
            NSLog(@"%d",i);
            productSizeValue = [quantityArray objectAtIndex:i] ;
            NSLog(@"%@",productSizeValue);
            
            NSString *SizeValueTag=[NSString stringWithFormat:@"%@",productSizeValue];
            sizeTag=[SizeValueTag intValue];
            NSLog(@"%d",sizeTag);
            UIButton *buttonSize;
            buttonSize = [UIButton buttonWithType:UIButtonTypeCustom];
            [buttonSize setFrame:CGRectMake(xPos, yPos, buttonWidth, buttonHeight)];
            [buttonSize addTarget:self action:@selector(selectQuantityAction:) forControlEvents:UIControlEventTouchUpInside];
            //[buttonSize setTag:sizeTag];
            [buttonSize setTitle:[NSString stringWithFormat:@"%@",SizeValueTag] forState:UIControlStateNormal];
            [buttonSize setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
            [buttonSize.titleLabel setFont:FuturaStd_Book(14)];
            [buttonSize.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
            [buttonSize.layer setBorderWidth:1.0];
            NSLog(@"Titel = %@",[self.quantityButton currentTitle]);
            NSString *strSize = [NSString stringWithFormat:@"%@",SizeValueTag];
            self.checkSize=[NSString stringWithFormat:@"%@",strSize];
            NSLog(@" size %@",self.checkSize);
            NSLog(@"self.btnSize currentTitle %@",[self.quantityButton currentTitle]);
            
//            if ([[self.quantityButton currentTitle] rangeOfString:strSize].location != NSNotFound) {
//                [buttonSize setBackgroundColor:ORANGE_COLOR];
//                previsousButtonSelected = buttonSize;
//            }
            if(![selectedQuantityValue isEqualToString:@""]){
                if([selectedQuantityValue isEqualToString:strSize]){
                    [buttonSize setBackgroundColor:ORANGE_COLOR];
                    previsousButtonSelected = buttonSize;
                }
            }
            else{
//                if(i==0){
//                    [buttonSize setBackgroundColor:ORANGE_COLOR];
//                    previsousButtonSelected = buttonSize;
//                }
                
            }
            
            [_viewSelectSize.scrollviewButtonContainer addSubview:buttonSize];
            xPos = xPos + buttonWidth + seperator;
            if (indexSize % 6 == 0) {
                yPos = yPos + buttonHeight + seperator;
                xPos = ((self.view.frame.size.width - (6*20))/8)/2;
            }
            
            indexSize++;
        }
    }
    
}

-(void)selectQuantityAction:(UIButton *)sender {
    
    isQuanttyClicked = YES;
    
    NSLog(@"Selected size =%ld",(long)[sender tag]);
    UIButton *sizeButton=(UIButton *)sender;
    NSString *sizeStr=sizeButton.titleLabel.text;
    selectedQuantityValue = sizeStr;

    if (sender == nil) {
        selectedQuantity = [quantityArray firstObject]  ;
    } else {
        selectedQuantity    = sizeStr;
    }
    //    selectedSizeIPA = [self getIPAForSize:selectedQuantity];
    [self.quantityButton setTitle:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Quantité", @""),selectedQuantity] forState:UIControlStateNormal];
   // [self lightBoxView];

    if (sender != nil) {
        
        [sender setBackgroundColor:ORANGE_COLOR];
        
        if (sender != previsousButtonSelected) {
            [previsousButtonSelected setBackgroundColor:[UIColor clearColor]];
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.viewSelectSize removeFromSuperview];
            //if (isFromAddToCart) {
                [self lightBoxView];
           // }
        });
    }
//    else {
//       // [self.quantityButton setUserInteractionEnabled:NO];
//    }
    
}

- (void) initialSetup {
    
    slideScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, screen_width, screen_height)];
    [slideScroll setPagingEnabled:YES];
    slideScroll.delegate = self;
    slideScroll.showsHorizontalScrollIndicator = NO;

    [self.productView addSubview:slideScroll];
    
    infoImage = [[UIImageView alloc] init];
    [slideScroll addSubview:infoImage];
    infoPageControl = [[UIPageControl alloc] init];
    [slideScroll addSubview:infoPageControl];
    
    topView  = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(slideScroll.frame)+5, screen_width, 10)];
    [self.productView addSubview:topView];
    
    self.txtTitlePrice = [[UITextView alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(topView.frame)+5, screen_width-20, 50)];
    [self.txtTitlePrice setEditable:NO];
    [self.txtTitlePrice setSelectable:NO];
    [self.txtTitlePrice setUserInteractionEnabled:NO];
    [self.productView addSubview:self.txtTitlePrice];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        CGFloat XPos = screen_width/3;
        btnSize = [[UIButton alloc] initWithFrame:CGRectMake(XPos, CGRectGetMaxY( self.txtTitlePrice.frame), screen_width- (XPos*2), 35)];
        
        quantityButton = [[UIButton alloc]initWithFrame:CGRectMake(XPos , CGRectGetMaxY( btnSize.frame)+5,screen_width- (XPos*2), 35)];
    } else {
        btnSize = [[UIButton alloc] initWithFrame:CGRectMake(35, CGRectGetMaxY( self.txtTitlePrice.frame), screen_width-70, 35)];
        quantityButton = [[UIButton alloc]initWithFrame:CGRectMake(35, CGRectGetMaxY( btnSize.frame)+5, screen_width-70, 35)];
    }

    [btnSize setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.productView addSubview:btnSize];
    
    [self.productView addSubview:quantityButton];
    
    dividedViewOne = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY( quantityButton.frame)+10, screen_width, 1)];
    [divideViewOne setBackgroundColor:[UIColor lightGrayColor]];
    [self.productView addSubview:dividedViewOne];
    
    productWarrantyInfoView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY( dividedViewOne.frame)+3, screen_width, 80)];
    CGFloat borderWidth = 1.0f;
    productWarrantyInfoView.frame = CGRectInset(productWarrantyInfoView.frame, -borderWidth, -borderWidth);
    productWarrantyInfoView.layer.borderColor = [UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f].CGColor;
    productWarrantyInfoView.layer.borderWidth = borderWidth;
    [self.productView addSubview:productWarrantyInfoView];
    
    divideViewTwo = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY( productWarrantyInfoView.frame)+3, screen_width, 1)];
    [divideViewOne setBackgroundColor:[UIColor lightGrayColor]];
    [self.productView addSubview:divideViewTwo];
    self.dividedViewOne.hidden = NO;
    self.divideViewTwo.hidden = NO;
    _txtReference = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY( divideViewTwo.frame)+10, screen_width-20, 25)];
    [self.productView addSubview:_txtReference];
    
    txtProductDescription = [[UITextView alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY( _txtReference.frame)+10, screen_width-20, 220)];
    [txtProductDescription setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    [self.txtProductDescription setEditable:NO];
    [self.txtProductDescription setSelectable:NO];
    [self.txtProductDescription setUserInteractionEnabled:NO];
    [self.productView addSubview:txtProductDescription];

}

- (void)updateHeightConstraints {
      [_txtReference setFrame:CGRectMake(10, CGRectGetMaxY( quantityButton.frame)+10, screen_width-20, 25)];
    
      [txtProductDescription setFrame:CGRectMake(10, CGRectGetMaxY(_txtReference.frame)+10, screen_width-20, 220)];

}


// Delegate method for line spacing
//- (CGFloat)layoutManager:(NSLayoutManager *)layoutManager lineSpacingAfterGlyphAtIndex:(NSUInteger)glyphIndex withProposedLineFragmentRect:(CGRect)rect
//{
//    return 8;
//}

@end
