//
//  SSProductPageViewController.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/18/15.
//  Copyright (c) 2015 Oclocksoftware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSProductPageViewController : UIViewController <UIScrollViewDelegate>
{
    IBOutlet UINavigationBar *navigationBarTitle;
    IBOutlet UIBarButtonItem *barButtonRetourTitle;
    
    BOOL pageControlBeingUsed;
    
    UIImageView            *infoImage;
    UIView                 *pgDtView;
    UIImageView            *pageImageView;
    IBOutlet UIScrollView  *slideScroll;
    IBOutlet UIView        *divideViewOne;
    IBOutlet UIPageControl *infoPageControl;
    UIImageView            *blkdot;
    NSMutableArray         *ImageArray;
    NSString               *pull;
    
    NSString                *selectedSize;
    NSString                *selectedSizeIPA;
    NSString                *selectedQuantity;

    float xslider;
    NSInteger jslider;
    
    BOOL isTapping;
    BOOL isQuanttyClicked;
    NSString *scrolldragging;
    NSString *sizeString;
    NSString *ProductTaileString;
    NSUInteger quantityApiCount;
    BOOL isMonoView;

}

- (IBAction)addToCart:(id)sender;
- (IBAction)lightBox:(id)sender;

@property(nonatomic,retain) NSMutableDictionary *currentProductItem;
@property (strong, nonatomic) IBOutlet UITextView *txtTitlePrice;


@property (strong,nonatomic) IBOutlet  UILabel *productWarrantyInfoLabel;
@property (strong, nonatomic) IBOutlet UIView *productWarrantyInfoView;

@property (strong, nonatomic) IBOutlet UIView *dividedViewOne,*divideViewTwo,*dividedViewThree;

@property (strong, nonatomic) IBOutlet UITextView *txtDescription;

@property (strong, nonatomic) UITextView *txtProductDescription;

@property (strong, nonatomic) IBOutlet UIButton *btnSize;
@property (strong, nonatomic) IBOutlet UIButton *taileButton;
@property (strong, nonatomic) IBOutlet UILabel *txtReference;
@property (weak, nonatomic) IBOutlet UIButton *btnAddToCart;
@property (strong, nonatomic) IBOutlet UIView *topView;
@property(strong, nonatomic) NSMutableDictionary *productDetail;
@property(strong, nonatomic)IBOutlet UIImageView *infoImage;
@property (nonatomic,strong) NSString  * redirectproduct;


@property (strong, nonatomic) IBOutlet UIScrollView *productView;

@property (nonatomic, assign) BOOL isSearchPage;
@property (strong, nonatomic) IBOutlet UIButton *quantityButton;



@end
