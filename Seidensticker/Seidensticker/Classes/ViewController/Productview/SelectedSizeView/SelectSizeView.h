//
//  SelectSizeView.h
//  Seidensticker
//
//  Created by Rajasekar on 03/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectSizeView : UIView

@property(nonatomic,weak) UIImageView *imageviewBackground;
@property(nonatomic,weak) UIView *viewContainer;
@property(nonatomic,weak) UILabel *labelTitle;
@property(nonatomic,weak) UIImageView *imageviewTopLine;
@property(nonatomic,weak) UIScrollView *scrollviewButtonContainer;
@property(nonatomic,retain) NSLayoutConstraint *viewContainerHeightConstraint;
@property(nonatomic,assign) NSInteger rowCounts;


@end
