//
//  SelectSizeView.m
//  Seidensticker
//
//  Created by Rajasekar on 03/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SelectSizeView.h"
#import "SSConfig.h"
#import "SSCustomNavigation.h"
#import "SSAppCommon.h"
@interface SelectSizeView (){
    BOOL isUpdateConstraint;
}
@end

@implementation SelectSizeView

-(instancetype)init{
    self = [super init];
    
    if (self) {
        [self setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self setBackgroundColor:[UIColor clearColor]];
        [self configureElements];
    }
    
    return self;
}

#pragma mark - Configure elements

-(void)singleTapClose:(UITapGestureRecognizer *)gesture
{
    [self removeGestureRecognizer:gesture];
    [gesture setCancelsTouchesInView:NO];
    [self removeFromSuperview];
    
}

-(void)configureElements{
    UIImageView *imagviewContainerBg = [[UIImageView alloc] init];
    [imagviewContainerBg setTranslatesAutoresizingMaskIntoConstraints:NO];
    [imagviewContainerBg setBackgroundColor:[UIColor whiteColor]];
    [imagviewContainerBg setAlpha:0.6];
    [self addSubview:imagviewContainerBg];
    [self setImageviewBackground:imagviewContainerBg];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapClose:)];
    [tapGesture setNumberOfTapsRequired:1];
    [self addGestureRecognizer:tapGesture];
    
    UIView *viewContainer = [[UIView alloc] init];
    [viewContainer setTranslatesAutoresizingMaskIntoConstraints:NO];
    [viewContainer setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:viewContainer];
    [self setViewContainer:viewContainer];
    
    UIImageView *imageviewTopLine = [[UIImageView alloc] init];
    [imageviewTopLine setTranslatesAutoresizingMaskIntoConstraints:NO];
    [imageviewTopLine setBackgroundColor:[UIColor lightGrayColor]];
    [imageviewTopLine setAlpha:0.6];
    [self.viewContainer addSubview:imageviewTopLine];
    [self setImageviewTopLine:imageviewTopLine];
    
    UILabel *labelTitle = [[UILabel alloc] init];
    [labelTitle setTranslatesAutoresizingMaskIntoConstraints:NO];
    [labelTitle setTextColor:BLACK_COLOR];
    [labelTitle setFont:FuturaStd_Book(13)];
    [labelTitle setText:NSLocalizedString(@"Séletionnez une taille", @"")];
    [labelTitle setTextAlignment:NSTextAlignmentCenter];
    [labelTitle setBackgroundColor:[UIColor clearColor]];
    [self.viewContainer addSubview:labelTitle];
    [self setLabelTitle:labelTitle];
    
    UIScrollView *scrollviewButtonContainer = [[UIScrollView alloc] init];
    [scrollviewButtonContainer setTranslatesAutoresizingMaskIntoConstraints:NO];
    [scrollviewButtonContainer setBackgroundColor:[UIColor whiteColor]];
    [self.viewContainer addSubview:scrollviewButtonContainer];
    [self setScrollviewButtonContainer:scrollviewButtonContainer];
    
}

#pragma mark - Setup constraint
-(void)setUpConstraint{
    
    float buttonWidth = (self.window.frame.size.width - (6*15))/5;
    
    NSLog(@"buttonWidth =%f",buttonWidth);
    
    NSDictionary *viewDictionary = @{@"_imagviewContainerBg":_imageviewBackground,
                                     @"_viewContainer":_viewContainer,
                                     @"_imageviewTopLine":_imageviewTopLine,
                                     @"_labelTitle":_labelTitle,
                                     @"_scrollviewButtonContainer":_scrollviewButtonContainer};
    
     [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_imagviewContainerBg]|"
                                                                  options:0
                                                                  metrics:nil
                                                                    views:viewDictionary]];
     
     [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_imagviewContainerBg]|"
                                                                  options:0
                                                                  metrics:nil
                                                                    views:viewDictionary]];

    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_viewContainer]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:viewDictionary]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(>=0)-[_viewContainer]-(0)-|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:viewDictionary]];
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_imageviewTopLine]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_labelTitle]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_scrollviewButtonContainer]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_imageviewTopLine(==1)]-[_labelTitle(==40)]-[_scrollviewButtonContainer]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    self.viewContainerHeightConstraint = [NSLayoutConstraint constraintWithItem:self.viewContainer
                                                                      attribute:NSLayoutAttributeHeight
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1
                                                                       constant:0];
    
    [self.viewContainer addConstraint:self.viewContainerHeightConstraint];
    
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    
        [self.viewContainerHeightConstraint setConstant:((buttonWidth*self.rowCounts) + 40 + ((self.rowCounts+1)*15))];
        
        [self layoutIfNeeded];
        
        NSLog(@"TOTAL HEIGHT =%f",((buttonWidth*self.rowCounts) + 40 + ((self.rowCounts+1)*15)));

//    });

}

-(void)updateConstraints {
    
    [super updateConstraints];

    if (!isUpdateConstraint) {
        [self setUpConstraint];
        isUpdateConstraint = YES;
    }
}

+(BOOL)requiresConstraintBasedLayout{
    return YES;
}

@end
