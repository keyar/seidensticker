//
//  SSAddCartInfoView.m
//  Seidensticker
//
//  Created by Rajasekar on 03/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSAddCartInfoView.h"
#import "SSConfig.h"

@interface SSAddCartInfoView (){
    BOOL isUpdateConstraint;
}

@end

@implementation SSAddCartInfoView

-(instancetype)init{
    self = [super init];
    
    if (self) {
        [self setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self setBackgroundColor:[UIColor clearColor]];
        [self configureElements];
    }
    
    return self;
}


#pragma mark - configure elements

-(void)singleTapClose:(UITapGestureRecognizer *)gesture{
    [self removeGestureRecognizer:gesture];
    [gesture setCancelsTouchesInView:NO];
    [self removeFromSuperview];
    
}

-(void)configureElements{
    UIImageView *imagviewContainerBg = [[UIImageView alloc] init];
    [imagviewContainerBg setTranslatesAutoresizingMaskIntoConstraints:NO];
    [imagviewContainerBg setBackgroundColor:[UIColor whiteColor]];
    [imagviewContainerBg setAlpha:0.6];
    [self addSubview:imagviewContainerBg];
    [self setImageviewBackground:imagviewContainerBg];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapClose:)];
    [tapGesture setNumberOfTapsRequired:1];
    [self addGestureRecognizer:tapGesture];
    
    UIView *viewContainer = [[UIView alloc] init];
    [viewContainer setTranslatesAutoresizingMaskIntoConstraints:NO];
    [viewContainer setBackgroundColor:[UIColor whiteColor]];
    [viewContainer.layer setBorderWidth:1.0];
    [viewContainer.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [self addSubview:viewContainer];
    [self setViewContainer:viewContainer];
    
    UIButton *buttonArticle = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonArticle setTranslatesAutoresizingMaskIntoConstraints:NO];
    [buttonArticle setBackgroundColor:[UIColor clearColor]];
    [buttonArticle setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    [buttonArticle setContentEdgeInsets:UIEdgeInsetsMake(4, 40, 0, 0)];
    [buttonArticle setImage:[UIImage imageNamed:@"tick"] forState:UIControlStateNormal];
    [buttonArticle setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [buttonArticle setAdjustsImageWhenHighlighted:NO];
    [self.viewContainer addSubview:buttonArticle];
    [self setButtonArticle:buttonArticle];
    
    UILabel *labelArticle = [[UILabel alloc] init];
    [labelArticle setTranslatesAutoresizingMaskIntoConstraints:NO];
    [labelArticle setTextColor:BLACK_COLOR];
    [labelArticle setFont:FuturaStd_Book(13)];
    [labelArticle setText:NSLocalizedString(@"Article ajouté au panier", @"")];
    [labelArticle setTextAlignment:NSTextAlignmentCenter];
    [labelArticle setBackgroundColor:[UIColor clearColor]];
    [self.viewContainer addSubview:labelArticle];
    [self setLabelArticle:labelArticle];
    
    UIImageView *imageviewSeperator1 = [[UIImageView alloc] init];
    [imageviewSeperator1 setTranslatesAutoresizingMaskIntoConstraints:NO];
    [imageviewSeperator1 setBackgroundColor:[UIColor lightGrayColor]];
    [imageviewSeperator1 setAlpha:0.6];
    [self.viewContainer addSubview:imageviewSeperator1];
    [self setImageviewSeperator1:imageviewSeperator1];
    
    
    UIButton *buttonContinuer = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonContinuer setTranslatesAutoresizingMaskIntoConstraints:NO];
    [buttonContinuer setBackgroundColor:[UIColor clearColor]];
    [buttonContinuer setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    [buttonContinuer setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 9)];
    [buttonContinuer setAdjustsImageWhenHighlighted:NO];
    [self.viewContainer addSubview:buttonContinuer];
    [self setButtonContinuer:buttonContinuer];
    
    UILabel *labelContinuer = [[UILabel alloc] init];
    [labelContinuer setTranslatesAutoresizingMaskIntoConstraints:NO];
    [labelContinuer setTextColor:ORANGE_COLOR];
    [labelContinuer setFont:FuturaStd_Book(13)];
    [labelContinuer setText:NSLocalizedString(@"Continuer mon shopping", @"")];
    [labelContinuer setTextAlignment:NSTextAlignmentCenter];
    [labelContinuer setBackgroundColor:[UIColor clearColor]];
    [self.viewContainer addSubview:labelContinuer];
    [self setLabelContinuer:labelContinuer];
    
    UIImageView *imageviewSeperator2 = [[UIImageView alloc] init];
    [imageviewSeperator2 setTranslatesAutoresizingMaskIntoConstraints:NO];
    [imageviewSeperator2 setBackgroundColor:[UIColor lightGrayColor]];
    [imageviewSeperator2 setAlpha:0.6];
    [self.viewContainer addSubview:imageviewSeperator2];
    [self setImageviewSeperator2:imageviewSeperator2];
    
    
    UIButton *buttonAller = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonAller setTranslatesAutoresizingMaskIntoConstraints:NO];
    [buttonAller setBackgroundColor:[UIColor clearColor]];
    [buttonAller setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    [buttonAller setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 9)];
    [buttonAller setAdjustsImageWhenHighlighted:NO];
    [self.viewContainer addSubview:buttonAller];
    [self setButtonAller:buttonAller];
    
    UILabel *labelAller = [[UILabel alloc] init];
    [labelAller setTranslatesAutoresizingMaskIntoConstraints:NO];
    [labelAller setTextColor:ORANGE_COLOR];
    [labelAller setFont:FuturaStd_Book(13)];
    [labelAller setText:NSLocalizedString(@"Aller au panier", @"")];
    [labelAller setTextAlignment:NSTextAlignmentCenter];
    [labelAller setBackgroundColor:[UIColor clearColor]];
    [self.viewContainer addSubview:labelAller];
    [self setLabelAller:labelAller];
    
    
    
}

-(void)setUpConstraints{
    NSDictionary *viewDictionary = @{@"_imagviewContainerBg":_imageviewBackground,
                                     @"_viewContainer":_viewContainer,
                                     @"_buttonArticle":_buttonArticle,
                                     @"_labelArticle":_labelArticle,
                                     @"_imageviewSeperator1":_imageviewSeperator1,
                                     @"_buttonContinuer":_buttonContinuer,
                                     @"_labelContinuer":_labelContinuer,
                                     @"_imageviewSeperator2":_imageviewSeperator2,
                                     @"_buttonAller":_buttonAller,
                                     @"_labelAller":_labelAller,};
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_imagviewContainerBg]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:viewDictionary]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_imagviewContainerBg]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:viewDictionary]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.viewContainer
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1
                                                      constant:0]];
    int constantWidth;
    
    if(IS_IPHONE4 ||IS_IPHONE5 ||IS_IPHONE6 ||IS_IPHONE6_Plus)
    {
        constantWidth=300;
    }
    else
    {
        constantWidth=500;
        
    }
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.viewContainer
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                    multiplier:1
                                                      constant:constantWidth]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.viewContainer
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                    multiplier:1
                                                      constant:120]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.viewContainer
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1
                                                      constant:0]];
    
    
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_buttonArticle]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_buttonContinuer]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_buttonAller]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_labelArticle]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_labelContinuer]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_labelAller]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_imageviewSeperator1]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_imageviewSeperator2]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(==0)-[_buttonArticle(==40)]-(==1)-[_buttonContinuer(==40)]-[_buttonAller(==40)]-(==0)-|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];
    
    
    [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(==5)-[_labelArticle(==35)]-[_imageviewSeperator1(==1)]-(==2)-[_labelContinuer(==35)]-(==2)-[_imageviewSeperator2(==1)]-(==5)-[_labelAller(==25)]-|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:viewDictionary]];

}

-(void)updateConstraints{
    [super updateConstraints];
    if (!isUpdateConstraint) {
        [self setUpConstraints];
        isUpdateConstraint = YES;
    }
}

+(BOOL)requiresConstraintBasedLayout{
    return YES;
}


@end
