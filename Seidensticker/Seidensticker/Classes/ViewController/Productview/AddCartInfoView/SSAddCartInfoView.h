//
//  SSAddCartInfoView.h
//  Seidensticker
//
//  Created by Rajasekar on 03/10/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSAddCartInfoView : UIView

@property(nonatomic,weak)  UIImageView *imageviewBackground;
@property(nonatomic,weak)  UIView *viewContainer;
@property(nonatomic,weak)  UIImageView *imageviewSeperator1,*imageviewSeperator2;
@property(nonatomic,weak)  UIButton *buttonArticle;
@property(nonatomic,weak)  UIButton *buttonContinuer;
@property(nonatomic,weak)  UIButton *buttonAller;
@property(nonatomic,weak)  UILabel *labelArticle;
@property(nonatomic,weak)  UILabel *labelContinuer;
@property(nonatomic,weak)  UILabel *labelAller;
@end
