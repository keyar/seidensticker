//
//  SSMenuDetailViewController.h
//  Seidensticker
//
//  Created by OCS iOS Developer on 22/09/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSMenuCustomView.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSCustomNavigation.h"
#import "SSShopViewController.h"
#import "SSAppDelegate.h"

#pragma mark – interface

@interface SSMenuCustomView ()

@property(nonatomic,retain) UIView *view;

@property(nonatomic,readonly,retain) UINavigationController *navigationController;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) UIView *centerView;

@property (nonatomic, strong) NSArray *menuItems;

@end

#pragma mark – implementation

@implementation SSMenuCustomView
@synthesize menuTitle;
+(SSMenuCustomView *)loadView{
    return [[NSBundle mainBundle]loadNibNamed:@"SSMenuCustomView" owner:self options:nil][0];
}

-(void)awakeFromNib{
    _tableView = [[UITableView alloc] init];//WithFrame:CGRectMake(0, 45, 375, 580)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
    _tableView.opaque = NO;
    _tableView.backgroundView = nil;
    _tableView.bounces = NO;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.separatorColor = [UIColor colorWithRed:225 green:0 blue:0 alpha:1.0];//colorWithWhite:0.15f alpha:0.2f];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"MenuCell"];
    [self addSubview:_tableView];
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                            NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(14)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                            
                                                            }];
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    navigationBarTitle.topItem.title = NSLocalizedString (@"Choisir une catégorie",@"");
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(12)]} forState:UIControlStateNormal];
    
    barButtonShowMenuTitle.title=NSLocalizedString(@"Fermer", @"");
    
//    barButtonShowMenuTitle.width=-50.0f;
    
//    [barButtonShowMenuTitle  :5.0f];
    
    //[barButtonShowMenuTitle setBounds:CGRectMake(0.0, 0.0, 95.0, 34.0)];
    
       
    [self setupConstraints];
}

- (void)updateConstraints {
    
    [super updateConstraints];
    
}


-(void) setupConstraints{
        
    // Width constraint
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeWidth
                                                    multiplier:1.0
                                                      constant:0]];
    
    // Height constraint
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:1.0 //0.928
                                                      constant:0]];
    
    // Center horizontally
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0
                                                      constant:0.0]];
    
    if (IS_IPHONE6) {
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.21 //1.105
                                                          constant:0.0]];
        
    }
    else if (IS_IPHONE4) {
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.30  //1.184
                                                          constant:0.0]];
        
    }
    else if (IS_IPHONE5)  {
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.251   //1.21
                                                          constant:0.0]];
        
    }
    
    else if (IS_IPHONE6_Plus) {
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.175  //1.085//1.159
                                                          constant:0.0]];
        
    }
    
    else if (iPad) {
        
        // Center vertically
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.135 //1.112
                                                          constant:0.0]];
        
    }


    
}

#pragma mark – UITableViewDataSource & Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 47;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    return _menuCategories.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"MenuCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //if not need
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    _item = [[self.menuCategories objectAtIndex:indexPath.row] objectForKey:@"title"];
    cell.backgroundColor =[UIColor clearColor];
    [cell.textLabel setText:[COMMON getDecodedString:_item]];
    [cell.textLabel setFont:[COMMON getResizeableFont:(FuturaStd_Book(12))]];
    

    cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"menu_accessory"]];
    
    [cell.accessoryView setFrame:CGRectMake(10, 5, 15, 15)];
    //line_separator
    
    
    if (IS_IPHONE6) {
        UIView * lineSeparator = [[UIView alloc] initWithFrame:CGRectMake(15,cell.frame.size.height + 2.5,cell.frame.size.width + 28,0.5)];
        lineSeparator.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
        [cell addSubview:lineSeparator];
        
        
    }
    else if (IS_IPHONE5)  {
        UIView * lineSeparator = [[UIView alloc] initWithFrame:CGRectMake(15,cell.frame.size.height + 2.5,cell.frame.size.width - 24,0.5)];
        lineSeparator.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
        [cell addSubview:lineSeparator];
    }
    else if (IS_IPHONE4) {
        UIView * lineSeparator = [[UIView alloc] initWithFrame:CGRectMake(15,cell.frame.size.height + 2.5,cell.frame.size.width - 25,0.5)];
        lineSeparator.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
        [cell addSubview:lineSeparator];
        
    }
    
    else if (IS_IPHONE6_Plus) {
        UIView * lineSeparator = [[UIView alloc] initWithFrame:CGRectMake(20,cell.frame.size.height + 2.5,cell.frame.size.width + 60,0.5)];
        lineSeparator.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
        [cell addSubview:lineSeparator];
        
    }
    
    else if (iPad) {
        
        
        UIView * lineSeparator = [[UIView alloc] initWithFrame:CGRectMake(20,cell.frame.size.height + 2.5,cell.frame.size.width + 420,0.5)];
        lineSeparator.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
        [cell addSubview:lineSeparator];
        
    }
    
    // hide the selection of cell
    
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    
    //if cell selection is used below line no need.
    
    UIView *bgColorView = [[UIView alloc] initWithFrame:CGRectMake(0, 23, 24, 1)];
    [bgColorView setBackgroundColor:[UIColor colorWithRed:140.0f/255.0f green:140.0f/255.0f blue:140.0f/255.0f alpha:0.1f]];
    
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
    
    }


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if ([[_menuCategories objectAtIndex:indexPath.row] objectForKey:@"values"] != NULL) {
        menuTitle = ([[_menuCategories objectAtIndex:indexPath.row] objectForKey:@"title"]);
        _itemSelected([[_menuCategories objectAtIndex:indexPath.row] objectForKey:@"values"]);
       
    } else {
        _catSelected([_menuCategories objectAtIndex:indexPath.row]);
        _didcancel();
    }
}


- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar
{
    return UIBarPositionTopAttached;
}

#pragma mark – Button Action (fermer)


- (IBAction)cancelAction:(id)sender {
    
    _didcancel();
}
@end



