//
//  SSMenuDetailViewController.h
//  Seidensticker
//
//  Created by OCS iOS Developer on 23/09/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^CategorySelected)(id selectedCategory);
typedef void (^DidCancel)();

@interface SSMenuDetailViewController : UIViewController
@property (strong, nonatomic) IBOutlet UINavigationBar *navigationBarTitle;

//- (IBAction)backAction:(id)sender;
@property(nonatomic,strong)NSArray *items;
@property(nonatomic,strong)NSString *menuTitleString;

@property(nonatomic,copy)CategorySelected catSelected;

@property(nonatomic,copy)DidCancel didcancel;


@end


