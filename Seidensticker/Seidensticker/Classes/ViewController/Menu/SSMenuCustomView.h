//
//  SSMenuDetailViewController.h
//  Seidensticker
//
//  Created by OCS iOS Developer on 22/09/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//


#import <UIKit/UIKit.h>

typedef void (^MenuSelected)(id menuItemsForSelectedCategory);
typedef void (^CategorySelected)(id selectedCategory);
typedef void (^DidCancel)();

@interface SSMenuCustomView : UIView <UITableViewDataSource ,UITableViewDelegate>

{
    
    IBOutlet UINavigationBar *navigationBarTitle;
    
    IBOutlet UIBarButtonItem *barButtonShowMenuTitle;
    
    
}

@property (nonatomic, strong) NSArray *menuCategories;
@property (nonatomic, strong) NSString * menuTitle;


@property(nonatomic,retain)NSString *item;

@property(nonatomic,copy)MenuSelected itemSelected;
@property(nonatomic,copy)CategorySelected catSelected;

@property(nonatomic,copy)DidCancel didcancel;

+(SSMenuCustomView *)loadView;;
@end