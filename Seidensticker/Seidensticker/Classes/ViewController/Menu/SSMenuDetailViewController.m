//
//  SSMenuDetailViewController.m
//  Seidensticker
//
//  Created by OCS iOS Developer on 23/09/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSMenuDetailViewController.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSCustomNavigation.h"
#import "SSMenuCustomView.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"



#pragma mark – interface

@interface SSMenuDetailViewController ()
{
    SSWebService *webservice;
}
@end

#pragma mark – implementation

@implementation SSMenuDetailViewController
@synthesize menuTitleString;


- (void)viewDidLoad {
    [super viewDidLoad];
    webservice = [[SSWebService alloc] init];
    // Do any additional setup after loading the view from its nib.
  
//    
//    [[UINavigationBar appearance] setTitleTextAttributes: @{
//                                                            NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(14)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
//                                                            
//                                                            }];
//    
//    _navigationBarTitle.topItem.title = NSLocalizedString (  @" Nouveautés",@"");
//
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadNavigation];
}

- (void)loadNavigation
{
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController.navigationBar setTranslucent:NO];
    
    SSCustomNavigation *navigation = [[SSCustomNavigation alloc] initWithNibName:@"SSCustomNavigation" bundle:nil];
    navigation.view.tag = 1001;
    [self.navigationController.navigationBar addSubview:navigation.view];
    
    [navigation setLeftHidden:NO];
    
    [navigation setTitleHidden:NO];
    [navigation setCatergoryShopBtnLabelHidden:YES];

    [navigation.titleLbl setText:[COMMON getDecodedString:menuTitleString]];
     NSLog(@"topmenu%@",menuTitleString);
    [navigation.titleLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    [navigation.titleLbl setTextColor:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]];
    
    [navigation.navigationLeftLbl setText:NSLocalizedString(@"Retour", @"")];
    [navigation.navigationLeftLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [navigation.navigationLeftBtn addTarget:self action:@selector(retour:) forControlEvents:UIControlEventTouchUpInside];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark – UITableViewDataSource & Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 48;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    return _items.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"MenuCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    NSString *item = [COMMON getDecodedString:[[_items objectAtIndex:indexPath.row] objectForKey:@"title"]];
    NSLog(@"title%@",[[_items objectAtIndex:indexPath.row] objectForKey:@"title"]);
    [cell.textLabel setText:item];
    [cell.textLabel setFont:[COMMON getResizeableFont:(FuturaStd_Book(12))]];
    cell.backgroundColor = [ UIColor clearColor];
    cell.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"menu_accessory"]];
    [cell.accessoryView setFrame:CGRectMake(10, 5, 15, 15)];
    
   
    //tableview
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.backgroundColor  = [UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1.0f];
    //line_separator
    
    
    if (IS_IPHONE6) {
        UIView * lineSeparator = [[UIView alloc] initWithFrame:CGRectMake(15,cell.frame.size.height + 2.5,cell.frame.size.width + 28,0.5)];
        lineSeparator.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
        [cell addSubview:lineSeparator];
        
        
    }
    else if (IS_IPHONE5)  {
        UIView * lineSeparator = [[UIView alloc] initWithFrame:CGRectMake(15,cell.frame.size.height + 2.5,cell.frame.size.width - 24,0.5)];
        lineSeparator.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
        [cell addSubview:lineSeparator];
    }
    else if (IS_IPHONE4) {
        UIView * lineSeparator = [[UIView alloc] initWithFrame:CGRectMake(15,cell.frame.size.height + 2.5,cell.frame.size.width - 25,0.5)];
        lineSeparator.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
        [cell addSubview:lineSeparator];
        
    }
    
    else if (IS_IPHONE6_Plus) {
        UIView * lineSeparator = [[UIView alloc] initWithFrame:CGRectMake(20,cell.frame.size.height + 2.5,cell.frame.size.width + 60,0.5)];
        lineSeparator.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
        [cell addSubview:lineSeparator];
        
    }
    
    else if (iPad) {
        UIView * lineSeparator = [[UIView alloc] initWithFrame:CGRectMake(20,cell.frame.size.height + 2.5,cell.frame.size.width + 420,0.5)];
        lineSeparator.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
        [cell addSubview:lineSeparator];
        
    }
    
    // hide the selection of cell
    
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    
    //if cell selection is used below line no need. 
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor colorWithRed:222.0f/255.0f green:222.0f/255.0f blue:222.0f/255.0f alpha:1.0f]];
    [cell setSelectedBackgroundView:bgColorView];
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 
    _catSelected([_items objectAtIndex:indexPath.row]);
}

#pragma mark – Button Action (fermer)


- (IBAction)cancelAction:(id)sender {
    
    _didcancel();
}


#pragma mark - Button Action (retour)

//- (IBAction)backAction:(id)sender {
//    
//    [self.navigationController popViewControllerAnimated:YES];
//        
//}

-(void)retour:(id)sender
{
    
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
    [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
    
    
}
@end


