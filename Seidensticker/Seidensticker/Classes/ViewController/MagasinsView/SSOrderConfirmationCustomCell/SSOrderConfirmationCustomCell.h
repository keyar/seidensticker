//
//  SSOrderConfirmationCustomCell.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 11/23/15.
//  Copyright © 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSOrderConfirmationCustomCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *orderConfirmationImage;

@property (strong, nonatomic) IBOutlet UILabel *orderConfirmationTitleLabel;

@property (strong, nonatomic) IBOutlet UILabel *orderConfirmationSizeLabel;

@property (strong, nonatomic) IBOutlet UILabel *orderConfirmationQuantityLabel;

@property (strong, nonatomic) IBOutlet UILabel *orderConfirmationPriceLabel;


@end
