//
//  SSMagasinsViewController.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/24/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManager.h>

@interface SSMagasinsViewController : UIViewController
{
    //IBOutlet UINavigationBar *newNavBar;
   
    IBOutlet MKMapView *mapViewObj;
    IBOutlet UIView *backgroundWebView;
    IBOutlet UIWebView *webViewObj;
}
@property(strong,nonatomic) NSMutableArray *cartItems;
@property(nonatomic,retain) CLLocation *userLocation;

@end
