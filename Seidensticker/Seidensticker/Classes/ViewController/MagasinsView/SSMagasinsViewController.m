//
//  SSMagasinsViewController.m
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/24/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//;

#import "SSMagasinsViewController.h"
#import "SSAppCommon.h"
#import "SSConfig.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#import "AsyncImage.h"
#import "KPAnnotation.h"
#import "KPGridClusteringAlgorithm.h"
#import "KPClusteringController.h"
#import "MyAnnotation.h"
#import "TestAnnotation.h"
#import "KPGridClusteringAlgorithm_Private.h"
#import "SSFilterViewController.h"
#import "SSShoppingCart.h"
#import "SSMonPanierViewController.h"
#import "SSStoreLoginViewController.h"


@interface SSMagasinsViewController ()<MKMapViewDelegate,CLLocationManagerDelegate,UITextFieldDelegate,KPClusteringControllerDelegate, KPClusteringControllerDelegate>

{
    SSWebService *webservice;
    AsyncImage *asyncImg;
    UILabel *lblDate;
    double gpsLatitude;
    double gpsLongitude;
    NSArray *arraySearchEventList;
    CLLocationManager *_locationManager;
    UILabel *qtyCount;
}

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (strong, nonatomic) KPClusteringController *clusteringController;

@end

@implementation SSMagasinsViewController

@synthesize userLocation,coordinate;
@synthesize cartItems;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    webservice = [[SSWebService alloc]init];
    arraySearchEventList     = [[NSArray alloc] init];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(isStoreApp){
        if ([[NSUserDefaults standardUserDefaults] boolForKey:IsStoreUserLoggedIn]==NO) {
            [self loadStoreLoginPage];
        }
        
    }
    [COMMON TrackerWithName:@"Magasins View"];
    [self styleNavBar];
    [self loadStores];
    
}
-(void)loadStoreLoginPage{
    
    SSStoreLoginViewController *storeLoginViewController=nil;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        storeLoginViewController = [[SSStoreLoginViewController alloc]initWithNibName:@"SSStoreLoginViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        storeLoginViewController = [[SSStoreLoginViewController alloc]initWithNibName:@"SSStoreLoginViewController" bundle:[NSBundle mainBundle]];
    }
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:storeLoginViewController];
    
    [self.navigationController presentViewController:navigationController animated:NO completion:nil];
    
}

#pragma mark - Load Stores

- (void) loadStores {
    [COMMON showLoadingIcon:self];
    [webservice getListStores:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject objectForKey:@"liststores"] != NULL && [[responseObject objectForKey:@"liststores"] isKindOfClass:[NSArray class]]) {
            [self loadMapData:[responseObject objectForKey:@"liststores"]];
             arraySearchEventList=[responseObject objectForKey:@"liststores"];
            [self loadLocationsInMapView];
        }
        [COMMON removeLoadingIcon];
    } failure:^(AFHTTPRequestOperation *operation, id error) {
        [SSAppCommon showSimpleAlertWithMessage:@"No Internet Connection available. Kindly check your Internet"];
        NSLog(@"Error = %@",error);
        [COMMON removeLoadingIcon];
    }];
}

#pragma mark - Load Map Data

- (void) loadMapData:(NSArray *)mapDataArray
{
    NSLog(@"Count = %lu",(unsigned long)[mapDataArray count]);
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)styleNavBar
{
//    [self.navigationController setNavigationBarHidden:YES animated:NO];
//    newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 64.0)];
//    [newNavBar setBackgroundColor:[UIColor clearColor]];
//    [newNavBar setBarTintColor:[UIColor whiteColor]];
//    
//    UINavigationItem *newItem = [[UINavigationItem alloc] init];
//    
//    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 33)];
//    newItem.titleView = customView;
//    
//    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, customView.frame.size.width, customView.frame.size.height)];
//    titleLabel.text = @"Magasins";
//    [titleLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(15)]];
//    titleLabel.textAlignment = NSTextAlignmentCenter;
//    [customView addSubview:titleLabel];
//    
//    [[UINavigationBar appearance] setTitleTextAttributes: @{
//                                                            NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)]}];
//    
//    [newNavBar setItems:@[newItem]];
//    [self.view addSubview:newNavBar];
    //[self setButton:newNavBar];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    UINavigationBar *newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, screen_width, 64.0)];
    [newNavBar setBackgroundColor:[UIColor clearColor]];
    
    [newNavBar setBarTintColor:[UIColor whiteColor]];
    newNavBar.tag=1001;
    UINavigationItem *newItem = [[UINavigationItem alloc] init];
    newItem.title = @"Magasins";
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                            NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)]}];
    [newNavBar pushNavigationItem:newItem animated:false];
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,64,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];

    [newNavBar addSubview:navBorder];
    
    
    cartItems = [SHOPPINGCART getCartItems];
    NSInteger quantity = 0;
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"shop_active_icon"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(rigtSideBtnAction:)forControlEvents:UIControlEventTouchUpInside];
    
    
    [button setFrame:CGRectMake(0, 0, 38, 38)];
    [button setBackgroundColor:[UIColor clearColor]];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(11, 15, 15,15)];
    [label setFont:[COMMON getResizeableFont:FuturaStd_Book(8)]];
    [label setText:[NSString stringWithFormat:@"%ld",(long)quantity]];
    [label setBackgroundColor:[UIColor clearColor]];
    label.textAlignment = NSTextAlignmentCenter;
    [label setTextColor:[UIColor blackColor]];
    
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(newNavBar.frame.size.width-55,(newNavBar.frame.size.height/2) -10 , 40, 40)];
    [backButtonView setBackgroundColor:[UIColor clearColor]];
    [button addSubview:label];
    [backButtonView addSubview:button];
    
    [newNavBar addSubview:backButtonView];
    [self.view addSubview:newNavBar];

    
}


#pragma mark-custom Navigation For WebView

- (void)magasinsNavBar
{
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    UINavigationBar *newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 64.0)];
    [newNavBar setBackgroundColor:[UIColor clearColor]];
    [newNavBar setBarTintColor:[UIColor whiteColor]];
    
    UINavigationItem *newItem = [[UINavigationItem alloc] init];
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 33)];
    newItem.titleView = customView;
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, customView.frame.size.width, customView.frame.size.height)];
    titleLabel.text = @"Magasins";
    [titleLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(15)]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [customView addSubview:titleLabel];
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                            NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)]}];
    
    [newNavBar setItems:@[newItem]];
    
    UIBarButtonItem *shopBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Rétour" style:UIBarButtonItemStylePlain target:self action:@selector(showBackButton:)];
    
    [shopBarButtonItem setTintColor:[UIColor orangeColor]];
    
    newItem.leftBarButtonItem = shopBarButtonItem;
    
    [shopBarButtonItem setTitleTextAttributes:@{
                                                NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(12)]
                                                } forState:UIControlStateNormal];
    [backgroundWebView addSubview:newNavBar];
    
    
}
-(void)setButton :(UINavigationBar *) newNavBar
{
    UIView *rightSideNaviView = [UIView new];
    [rightSideNaviView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [rightSideNaviView setBackgroundColor:[UIColor clearColor]];
    [newNavBar addSubview:rightSideNaviView];
    
    CGFloat lblConstant;
    
    // Width constraint, half of parent view width
    [newNavBar addConstraint:[NSLayoutConstraint constraintWithItem:rightSideNaviView
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:newNavBar
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:0.2
                                                           constant:0]];
    
    // Height constraint, half of parent view height
    [newNavBar addConstraint:[NSLayoutConstraint constraintWithItem:rightSideNaviView
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:newNavBar
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:0.6
                                                           constant:0]];
    
    if(IS_IPHONE5 || IS_IPHONE4 || IS_IPHONE6)
        lblConstant = 8;
    else if(IS_IPHONE6_Plus)
        lblConstant = 11;
    else
        lblConstant = 9;
    
    [newNavBar addConstraint:[NSLayoutConstraint constraintWithItem:rightSideNaviView
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:rightSideNaviView.superview
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1.0
                                                           constant:lblConstant]];
    
    // Center vertically
    [newNavBar addConstraint:[NSLayoutConstraint constraintWithItem:rightSideNaviView
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:rightSideNaviView.superview
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0
                                                           constant:10.0]];
    
    
    
    UIImageView *image =[UIImageView new];
    [image setTranslatesAutoresizingMaskIntoConstraints:NO];
    image.image=[UIImage imageNamed:@"shop_active_icon"];
    image.contentMode = UIViewContentModeScaleAspectFit;
    [rightSideNaviView addSubview:image];
    
    NSDictionary *constraintsDictionary = @{@"_image":image};
    
    
    NSDictionary *metricsDict;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        [rightSideNaviView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[_image]|"
                                                                                  options:0
                                                                                  metrics:  metricsDict
                                                                                    views:  constraintsDictionary]];
        [rightSideNaviView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_image]|"
                                                                                  options:0
                                                                                  metrics:metricsDict
                                                                                    views:constraintsDictionary]];
    }
    else {
        
        [rightSideNaviView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_image]|"
                                                                                  options:0
                                                                                  metrics:  metricsDict
                                                                                    views:  constraintsDictionary]];
        [rightSideNaviView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_image]|"
                                                                                  options:0
                                                                                  metrics:metricsDict
                                                                                    views:constraintsDictionary]];
    }
    
    
    qtyCount = [UILabel new];
    [qtyCount setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSLog(@"cart values %@",[SHOPPINGCART getCartItems]);
    cartItems = [SHOPPINGCART getCartItems];
    
    NSInteger quantity = 0;
    
    
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }
    
    qtyCount.text = [NSString stringWithFormat:@"%ld",(long)quantity];
    [qtyCount setBackgroundColor:[UIColor clearColor]];
    qtyCount.textColor = [UIColor blackColor];
    [qtyCount setFont:[COMMON getResizeableFont:FuturaStd_Book(8)]];
    [qtyCount setTextAlignment:NSTextAlignmentCenter];
    qtyCount.userInteractionEnabled = NO;
    [image addSubview:qtyCount];
    
    
    NSDictionary *viewDictionary = @{@"_qtyCount":qtyCount};
    
    
    NSDictionary *metricsDictionary;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        
        [image addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_qtyCount]-43-|"
                                                                      options:0
                                                                      metrics:metricsDictionary
                                                                        views:viewDictionary]];
        [image addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[_qtyCount]|"
                                                                      options:0
                                                                      metrics:metricsDictionary
                                                                        views:viewDictionary]];
    }
    else {
        
        if (IS_IPHONE4 || IS_IPHONE5) {
            
            [image addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_qtyCount]-28-|"
                                                                          options:0
                                                                          metrics:metricsDictionary
                                                                            views:viewDictionary]];
            [image addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[_qtyCount]|"
                                                                          options:0
                                                                          metrics:metricsDictionary
                                                                            views:viewDictionary]];
        }
        else if (IS_IPHONE6_Plus){
            
            
            if(quantity<=9){
                [image addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_qtyCount]-38-|"
                                                                              options:0
                                                                              metrics:metricsDictionary
                                                                                views:viewDictionary]];
                
            }
            else{
                [image addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_qtyCount]-36-|"
                                                                              options:0
                                                                              metrics:metricsDictionary
                                                                                views:viewDictionary]];
            }

            [image addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[_qtyCount]|"
                                                                          options:0
                                                                          metrics:metricsDictionary
                                                                            views:viewDictionary]];
        }
        else{
            
            [image addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_qtyCount]-34-|"
                                                                          options:0
                                                                          metrics:metricsDictionary
                                                                            views:viewDictionary]];
            [image addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[_qtyCount]|"
                                                                          options:0
                                                                          metrics:metricsDictionary
                                                                            views:viewDictionary]];
        }
        
    }
    
    UIButton *rightSideBtn = [UIButton new];
    [rightSideBtn setTranslatesAutoresizingMaskIntoConstraints:NO];
    rightSideBtn.userInteractionEnabled = YES;
    [rightSideBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rightSideBtn setBackgroundColor:[UIColor clearColor]];
    [rightSideNaviView addSubview:rightSideBtn];
    
    [rightSideBtn addTarget:self action:@selector(rigtSideBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary *viewDictiory = @{@"_rightSideBtn":rightSideBtn};
    
    
    NSDictionary *metricsDictiony;
    
    [rightSideNaviView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_rightSideBtn]|"
                                                                              options:0
                                                                              metrics:metricsDictiony
                                                                                views:viewDictiory]];
    [rightSideNaviView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_rightSideBtn]|"
                                                                              options:0
                                                                              metrics:metricsDictiony
                                                                                views:viewDictiory]];
    
    
}

#pragma mark - Right Menu

-(void)rigtSideBtnAction:(id)sender
{
    SSMonPanierViewController *monpanierViewController;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonpanierViewControlleriPad" bundle:[NSBundle mainBundle]];
        
        
    }
    else {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonPanierViewController" bundle:[NSBundle mainBundle]];
    }
    
    [self.navigationController pushViewController:monpanierViewController animated:YES];
}

#pragma mark- Load Location View

-(void)loadLocationsInMapView
{
    double LatiVal = [FranceLatitude doubleValue];
    
    double LongiVal =[FranceLongitude doubleValue];
    
    gpsLatitude     =   LatiVal;
    
    gpsLongitude    =   LongiVal;
    
    CLLocation  *newLocation;
    
    CLLocationDegrees startlatitude  = 0.0;
    
    CLLocationDegrees startlongitude = 0.0;
    
    startlatitude  = LatiVal;
    
    startlongitude = LongiVal;
    
    newLocation = [[CLLocation alloc] initWithLatitude:startlatitude longitude:startlongitude];
    
    userLocation = newLocation;
    
    [self.clusteringController setAnnotations:[self reSetannotations]];
    
    [mapViewObj setDelegate:self];

    [mapViewObj setZoomEnabled:YES];
        
    [mapViewObj setCenterCoordinate:mapViewObj.userLocation.coordinate];
        
    [mapViewObj setUserTrackingMode:MKUserTrackingModeFollow];
        
    [self.view setUserInteractionEnabled:YES];
 
    KPGridClusteringAlgorithm *algorithm = [KPGridClusteringAlgorithm new];
    
    algorithm.annotationSize = CGSizeMake(25, 50);
    
    algorithm.clusteringStrategy = KPGridClusteringAlgorithmStrategyTwoPhase;
    
    self.clusteringController = [[KPClusteringController alloc] initWithMapView:mapViewObj
                                                            clusteringAlgorithm:algorithm];
    self.clusteringController.delegate = self;
    
    self.clusteringController.animationOptions = UIViewAnimationOptionCurveEaseOut;
    
    [self.clusteringController setAnnotations:[self annotations]];
    
    NSLog(@"arraySearchEventList %@",[arraySearchEventList lastObject]);
    
    NSString * lastobjlat;
    
    double miles;
    
    lastobjlat = [[arraySearchEventList lastObject] objectForKey:@"latitude"];
        miles = 500.0;
    double scalingFactor = ABS( (cos(2 * M_PI * [lastobjlat floatValue] / 360.0) ));
    MKCoordinateSpan span;
    span.latitudeDelta = miles/69.0;
    span.longitudeDelta = miles/(scalingFactor * 69.0);
    MKCoordinateRegion region;
    region.span = span;
    region.center = userLocation.coordinate;
    [mapViewObj setRegion:region animated:YES];
    mapViewObj.showsUserLocation = YES;
}


- (NSArray *)reSetannotations {
    // build an NYC and SF cluster
    
    NSMutableArray *annotations = [NSMutableArray array];
    
    return annotations;
}

- (NSArray *)annotations {

    NSMutableArray *annotations = [NSMutableArray array];
    
    double LatiVal = 0.0;
    double LongiVal = 0.0;
    if ([arraySearchEventList count] > 0) {

        for (int i = 0; i <[arraySearchEventList count]; i++)
        {
            LatiVal = [[[arraySearchEventList objectAtIndex:i] valueForKey:@"latitude"]  doubleValue];
                
            LongiVal = [[[arraySearchEventList objectAtIndex:i] valueForKey:@"longitude"]  doubleValue];
                
            NSString *strName = [NSString stringWithFormat:@"%@",[[arraySearchEventList objectAtIndex:i] objectForKey:@"name"]];
            NSString *strAddress = [NSString stringWithFormat:@"%@,%@,%@ %@",[[arraySearchEventList objectAtIndex:i] objectForKey:@"address1"],[[arraySearchEventList objectAtIndex:i] objectForKey:@"postcode"],[[arraySearchEventList objectAtIndex:i] objectForKey:@"city"],[[arraySearchEventList objectAtIndex:i] objectForKey:@"country"]];
            
            TestAnnotation *a1 = [[TestAnnotation alloc] init];
            a1.index=i;
            a1.name = strName;
            a1.address = strAddress;
            a1.coordinate = CLLocationCoordinate2DMake(LatiVal,
                                                           LongiVal);
            [annotations addObject:a1];
                
            }
        }

    return annotations;
}


#pragma mark - <MKMapViewDelegate>

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
    [self.clusteringController refresh:YES];
    
}


- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if ([view.annotation isKindOfClass:[KPAnnotation class]]) {
        KPAnnotation *cluster = (KPAnnotation *)view.annotation;
        
        if (cluster.annotations.count > 1){
            [mapViewObj setRegion:MKCoordinateRegionMakeWithDistance(cluster.coordinate,cluster.radius * 2.5f,cluster.radius * 2.5f)
                                 animated:YES];
        }
        else{
            
            view.canShowCallout = YES;
            
        }
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    MKAnnotationView *annotationView = nil;
    if ([annotation isKindOfClass:[KPAnnotation class]]) {
        
        KPAnnotation *a = (KPAnnotation *)annotation;
        
        if ([annotation isKindOfClass:[MKUserLocation class]]){
            return nil;
        }
        
        if ([a.annotations count] > 1) {
            annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"cluster"];
            
            if (annotationView == nil) {
                annotationView = [[MKAnnotationView alloc] initWithAnnotation:a reuseIdentifier:@"cluster"];
                
            }
            annotationView.tag = [[a.annotations anyObject] index];

            UIImageView *imagenew = [[UIImageView alloc]initWithFrame:CGRectMake(-10,-10, 25, 50)];
            imagenew = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"map_icon1"]];
            [annotationView addSubview:imagenew];
            
            annotationView.image = imagenew.image;
            
            annotationView.centerOffset = CGPointMake(0, -imagenew.frame.size.height/2);

            annotationView.canShowCallout = YES;
            [lblDate clearsContextBeforeDrawing];
            
            for (UIView* subView in [annotationView subviews])
            {
                if ([subView isKindOfClass:[UILabel class]]) {
                    
                    [subView removeFromSuperview];
                }
            }
            
           lblDate                     = [[UILabel alloc]initWithFrame:CGRectMake(9,10 ,15 ,15)];// 66 59
            lblDate.textAlignment       = NSTextAlignmentCenter;
           lblDate.font                = FuturaStd_Book(11);
           lblDate.backgroundColor     = [UIColor clearColor];
           lblDate.textColor           = [UIColor blackColor];
            [annotationView addSubview:lblDate];
            
        NSString *strDate = [NSString stringWithFormat:@"%lu",(unsigned long)[a.annotations count]];
           
        if ([a.annotations count] < 100)
               lblDate.text = strDate;
        else
               lblDate.text = @"...";
        }
        
        else {
            
            annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"map_icon1"];
            
            [lblDate clearsContextBeforeDrawing];
            
            if (annotationView == nil) {
                
                annotationView = [[MKAnnotationView alloc] initWithAnnotation:[a.annotations anyObject]reuseIdentifier:@"map_icon1"];
                
            }
            
            for (UIView* subView in [annotationView subviews])
            {
                if ([subView isKindOfClass:[UILabel class]]) {
                    
                    [subView removeFromSuperview];
                }
                
                if ([subView isKindOfClass:[AsyncImage class]]) {
                    
                    [subView removeFromSuperview];
                }
            }
            
            annotationView.tag = [[a.annotations anyObject] index];
            UIImageView *imagenew = [[UIImageView alloc]initWithFrame:CGRectMake(-10,-10, 25, 40)];
            imagenew = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"map_icon1"]];
            
            [annotationView addSubview:imagenew];
            
            annotationView.image = imagenew.image;
            
            annotationView.centerOffset = CGPointMake(0, -imagenew.frame.size.height/2);
            
            asyncImg = [[AsyncImage alloc] initWithFrame:CGRectMake(-7, -7, 29, 29)];
            
            NSString *strURL = @"";
            
                strURL = [NSString stringWithFormat:@"%@",[[arraySearchEventList objectAtIndex:annotationView.tag] valueForKey:@"API_KEY_STORES"]];
           
            if ([strURL length]) {
                NSURL *strNsurl = [NSURL URLWithString:strURL];
                [asyncImg loadImageFromURL:strNsurl type:AsyncImageResizeTypeCrop isCache:YES];
            }
            if (asyncImg)
            [annotationView addSubview:asyncImg];
            annotationView.canShowCallout = YES;
        }
        
        annotationView.canShowCallout = YES;
    }
    
    else if ([annotation isKindOfClass:[MyAnnotation class]]) {
        
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"nocluster"];
        annotationView.image = [UIImage imageNamed:@"map_icon1"];
    }
    
     // IMP LINE
    
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    annotationView.rightCalloutAccessoryView = infoButton;
    
    return annotationView;
}


- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
        NSUInteger currentTag = [view tag];
        NSArray *latitudeValue = [[NSArray alloc]init];
        NSArray *longitudeValue = [[NSArray alloc]init];
        if ([arraySearchEventList count] >= currentTag)
        {
            [COMMON showLoadingIcon:self];
            NSLog(@"Current Value = %@",[arraySearchEventList objectAtIndex:currentTag]);
            latitudeValue = [[arraySearchEventList objectAtIndex:currentTag] objectForKey:@"latitude"];
            longitudeValue = [[arraySearchEventList objectAtIndex:currentTag] objectForKey:@"longitude"];
            NSString *latitudeLongitudeString=[NSString stringWithFormat:@"%@,%@",latitudeValue,longitudeValue];
            NSString *google = @"http://maps.google.com/?q=";
            NSString *URL = [google stringByAppendingString:latitudeLongitudeString];
            NSURL *urlDisplay = [NSURL URLWithString:URL];
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:urlDisplay];
            [webViewObj loadRequest:urlRequest];
            [self magasinsNavBar];
            [self.view addSubview:backgroundWebView];
            [COMMON removeLoadingIcon];
        }
}

#pragma mark - <KPClusteringControllerDelegate>

- (void)clusteringController:(KPClusteringController *)clusteringController configureAnnotationForDisplay:(KPAnnotation *)annotation {
    
    if(annotation.annotations.count > 1){
        annotation.title = [NSString stringWithFormat:@"%lu Evènements", (unsigned long)annotation.annotations.count];
        return;
    }
    
    NSString *strName;
    NSString *strAddress;
        strName = [NSString stringWithFormat:@"%@",[[arraySearchEventList objectAtIndex:[[annotation.annotations anyObject] index]] objectForKey:@"name"]];
        strAddress = [NSString stringWithFormat:@"%@,%@,%@,%@",[[arraySearchEventList objectAtIndex:[[annotation.annotations anyObject] index]] objectForKey:@"address1"],[[arraySearchEventList objectAtIndex:[[annotation.annotations anyObject] index]] objectForKey:@"postcode"],[[arraySearchEventList objectAtIndex:[[annotation.annotations anyObject] index]] objectForKey:@"city"],[[arraySearchEventList objectAtIndex:[[annotation.annotations anyObject] index]] objectForKey:@"country"]];
    annotation.title = [NSString stringWithFormat:@"%@",strName];
    if ([strAddress isEqualToString:@""] || [strAddress isEqualToString:@"(null)"] || strAddress ==nil  ) {
        
    }
    else{
        annotation.subtitle = [NSString stringWithFormat:@"%@ \n",strAddress];
    }
    
}

- (void)clusteringController:(KPClusteringController *)clusteringController performAnimations:(void (^)())animations withCompletionHandler:(void (^)(BOOL))completion {
    [UIView animateWithDuration:0.5
                          delay:0
         usingSpringWithDamping:0.8
          initialSpringVelocity:0.6
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:animations
                     completion:completion];
}


#pragma mark-Button Action
-(void)showBackButton:(id)sender
{
    [backgroundWebView removeFromSuperview];
    
}

@end
