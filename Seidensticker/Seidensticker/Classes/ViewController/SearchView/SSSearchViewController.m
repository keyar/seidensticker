//
//  SSSearchViewController.m
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/24/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSSearchViewController.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSCustomNavigation.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#import "SSProductPageViewController.h"

@interface SSSearchViewController ()

{
    SSWebService *webservice;
    NSUInteger textCount;
    
    
}

@end

@implementation SSSearchViewController

@synthesize searchController;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    webservice = [[SSWebService alloc]init];
    _searchArray = [[NSMutableArray alloc]init];
    
    id barButtonAppearanceInSearchBar = [UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil];
    
    [barButtonAppearanceInSearchBar setTitleTextAttributes:@{
                                      NSFontAttributeName : [UIFont fontWithName:@"FuturaStd-Book" size:14],
                             NSForegroundColorAttributeName : [UIColor orangeColor]
                                                             } forState:UIControlStateNormal];
   [barButtonAppearanceInSearchBar setTitle:@"Annuler"];
    
// Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
//    [super viewWillAppear:animated];
    [COMMON TrackerWithName:@"Search View"];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark- tableview delegate Method

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_searchArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if(textCount > 2)
        cell.textLabel.text = [[_searchArray objectAtIndex:indexPath.row]objectForKey:@"name"];
        [cell.textLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SSProductPageViewController *productVC = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        productVC = [[SSProductPageViewController alloc]initWithNibName:@"SSProductpageViewControlleriPad" bundle:[NSBundle mainBundle]];
        productVC.currentProductItem = [_searchArray objectAtIndex:indexPath.row];
        UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,768,0.8)];
        [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
        productVC.isSearchPage = YES;
        [self.navigationController.navigationBar addSubview:navBorder];
        [self.navigationController pushViewController:productVC animated:YES];
    }
    else {
        productVC = [[SSProductPageViewController alloc]
                     initWithNibName:@"SSProductPageViewController"
                     bundle:nil];
        productVC.currentProductItem = [_searchArray objectAtIndex:indexPath.row];
        productVC.isSearchPage = YES;
        UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,375,0.8)];
        [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
        [self.navigationController.navigationBar addSubview:navBorder];
        [self.navigationController pushViewController:productVC animated:YES];
        
    }
}

#pragma mark-search bar delegate function

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
      textCount = text.length;
    
      [webservice searchProductsForKeyword:text success:^(AFHTTPRequestOperation *operation, id responseObject){
            
            if ([responseObject objectForKey:API_KEY_SEARCH] !=NULL)
            {
                [self loadTableWithProducts:responseObject];
            }

        } failure:^(AFHTTPRequestOperation *operation, id error){
        [SSAppCommon showSimpleAlertWithMessage:@"No Internet Connection available. Kindly check your Internet"];
            
        }];
}

- (void) loadTableWithProducts:(NSMutableDictionary *)dataArray {
    
    if (([dataArray objectForKey:@"search"] != NULL) &&
        [[dataArray objectForKey:@"search"] isKindOfClass:[NSDictionary class]] &&
        [[dataArray objectForKey:@"search"] objectForKey:@"products"] != NULL &&
        [[[dataArray objectForKey:@"search"] objectForKey:@"products"] isKindOfClass:[NSArray class]]
        ) {
        
        _searchArray = [[dataArray objectForKey:@"search"] objectForKey:@"products"];
        [_searchTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    }
    
    [_searchTableView reloadData];
}

- (void)searchDisplayControllerWillBeginSearch:(UISearchBar *)controller
{
    [self performSelector:@selector(removeOverlay) withObject:nil afterDelay:.01f];
}

- (void)removeOverlay
{
    [[self.view.subviews lastObject] removeFromSuperview];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    _searchTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;

}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.searchArray = nil;
    [self.searchTableView reloadData];

}

@end
