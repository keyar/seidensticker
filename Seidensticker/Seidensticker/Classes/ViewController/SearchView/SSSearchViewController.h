//
//  SSSearchViewController.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 9/24/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSSearchViewController : UIViewController <UISearchControllerDelegate, UISearchDisplayDelegate, UISearchBarDelegate>
{
    BOOL *isFilter;
}

@property (nonatomic, strong) NSMutableArray *searchArray;

@property (nonatomic, strong) NSArray *searchtableData;

@property (strong, nonatomic)IBOutlet UITableView *searchTableView;

@property (strong, nonatomic) IBOutlet UISearchBar *searchController;

@property (nonatomic, strong) IBOutlet UIView *divideViewOne;


@end
