//
//  SSOrderConfirmationViewController.m
//  Seidensticker
//
//  Created by ocs-mini-7 on 10/9/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import "SSOrderConfirmationViewController.h"
#import "SSConfig.h"
#import "SSCustomNavigation.h"
#import "SSAppCommon.h"
#import "SSOrderConfirmationCustomCell.h"
#import "SSMonPanierViewController.h"
#import "SSAppDelegate.h"
#import "SSShoppingCart.h"
#import "AsyncImage.h"
#import "SSPaymentWebViewController.h"
#import "SSShopViewController.h"
#import "NMBottomTabBarController.h"

#pragma mark-Interfaces

@interface SSOrderConfirmationViewController ()
{
    NMBottomTabBarController    *bottomTabBar;
       SSMonPanierViewController *monPanierViewController;
    
 
}
@end

#pragma mark-Implementations

@implementation SSOrderConfirmationViewController

@synthesize isPush,addressDisplayOrderNumber;

#pragma mark-view states
- (void)viewDidLoad {
    [super viewDidLoad];
    [COMMON setTabBarSelection:YES];
    
    [orderConfirmationReferenceNumberTextField setEnabled:NO];
    
    int textViewFontSize =15;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        textViewFontSize=20;
    }

    [orderConfirmationTextView setFont:FuturaStd_Book(textViewFontSize)];
    
     //[COMMON getResizeableFont:FuturaStd_Book(textViewFontSize)]];
    orderConfirmationTextView.text = NSLocalizedString(@"Votre commande sur Seidensticker est terminée.Pour toute question ou information complémentaire merci de contacter notre support client.", @"");
    
    SSAppDelegate *appDelegate = (SSAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.orderConfirmationViewController=self;
//    [self updateScrollViewContentSize];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [COMMON TrackerWithName:@"OrderConfirmation View"];
    monPanierViewController=[[SSMonPanierViewController alloc]init];
    monPanierViewController.cartItems=[SHOPPINGCART getCartItems];
    NSLog(@"Cart Items=%@",monPanierViewController.cartItems);
    SSAppDelegate *appDelegate = (SSAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    orderConfirmationReferenceNumberTextField.text=[[appDelegate paymentWebViewController]orderNumber];
    if(isStoreApp){
        orderConfirmationReferenceNumberTextField.text= addressDisplayOrderNumber;
    }
    NSLog(@"orderConfirmationReferenceNumberTextField==%@",orderConfirmationReferenceNumberTextField.text);
    [self loadNavigation];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [SHOPPINGCART clearCart];
    [SHOPPINGCART removeCouponFromCart];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Custom Navigation

- (void)loadNavigation
{
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController.navigationBar setTranslucent:NO];
    SSCustomNavigation *navigation = [[SSCustomNavigation alloc] initWithNibName:@"SSCustomNavigation" bundle:nil];
    navigation.view.tag = 1001;
    [self.navigationController.navigationBar addSubview:navigation.view];
    
    [navigation setLeftHidden:NO];
    
    [navigation setTitleHidden:NO];
    
    [navigation setShopBtnHidden:YES];
    [navigation setShopLabelHidden:YES];
    [navigation setCatergoryShopBtnLabelHidden:YES];

    
    [navigation.titleLbl setText:NSLocalizedString(@"Confirmation de votre commande", @"")];
    
    [navigation.titleLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
  
}

#pragma mark-uiTableview delegate method

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPHONE6_Plus ||IS_IPHONE6 ||IS_IPHONE5 ||IS_IPHONE4)
    {
        return 150;
    }
    else
    {
        return 320;
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [monPanierViewController.cartItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"SSOrderConfirmationCustomCell";
    SSOrderConfirmationCustomCell *cell  = (SSOrderConfirmationCustomCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

    if (cell == nil)
    {
        NSArray *nib;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"SSOrderConfirmationCustomCell" owner:self options:nil];
        }
        else
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"SSOrderConfirmationCustomCelliPad" owner:self options:nil];
        }
        cell         = [nib objectAtIndex:0];
    }
    //monPanierViewController=[[SSMonPanierViewController alloc]init];
    
    NSDictionary *currentCartItem = [monPanierViewController.cartItems objectAtIndex:indexPath.row];
    NSString *photoURLString = [currentCartItem objectForKey:@"productImage"];
    AsyncImage *asyncImage = [[AsyncImage alloc]initWithFrame:CGRectMake(0, 0, cell.orderConfirmationImage.frame.size.width, cell.orderConfirmationImage.frame.size.height)];
    [asyncImage setLoadingImage];
    [asyncImage loadImageFromURL:[NSURL URLWithString:photoURLString]
                            type:AsyncImageResizeTypeCrop
                         isCache:YES];
    
    [cell.orderConfirmationImage addSubview:asyncImage];
    cell.orderConfirmationTitleLabel.text       = [COMMON getDecodedString:[currentCartItem objectForKey:@"productName"]];
    if([[currentCartItem objectForKey:@"productSize"] isEqualToString:@"TailleUnique"])
    {
        cell.orderConfirmationSizeLabel.hidden     =YES;
        cell.orderConfirmationQuantityLabel.translatesAutoresizingMaskIntoConstraints = YES;
        int yposition;
        if(IS_IPHONE4 ||IS_IPHONE5 ||IS_IPHONE6 ||IS_IPHONE6_Plus)
        {
            yposition=25;
            
        }
        else{
            yposition=50;
        }
        [cell.orderConfirmationQuantityLabel setFrame:CGRectMake(cell.orderConfirmationQuantityLabel.frame.origin.x+5, cell.orderConfirmationQuantityLabel.frame.origin.y-yposition, cell.orderConfirmationQuantityLabel.frame.size.width, cell.orderConfirmationQuantityLabel.frame.size.height)];
    }
    else
    {
        cell.orderConfirmationSizeLabel.text        = [NSString stringWithFormat:@"Taille: %@",[COMMON getDecodedString:[currentCartItem objectForKey:@"productSize"]]];
    }
    
    NSLog(@"size==%@",cell.orderConfirmationSizeLabel);
    
    cell.orderConfirmationQuantityLabel.text    = [NSString stringWithFormat:@"Quantité: %@",[COMMON getDecodedString:[currentCartItem objectForKey:@"quantity"]]];
    
    NSLog(@"%@",cell.orderConfirmationQuantityLabel);
    
    cell.orderConfirmationPriceLabel.text       = [COMMON getDecodedString:[currentCartItem objectForKey:@"productPrice"]];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Remove seperator inset
    
    orderConfirmationTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    if ([orderConfirmationTableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [orderConfirmationTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        
        [cell setPreservesSuperviewLayoutMargins:NO];
        
    }
    
    // Explictly set your cell's layout margins
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        
        [cell setLayoutMargins:UIEdgeInsetsZero];
        
    }
    
}

#pragma mark-Button Action
-(IBAction)retourAccueil:(id)sender
{
    [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
    
    [SHOPPINGCART clearCart];
    [SHOPPINGCART removeCouponFromCart];
    SSAppDelegate *appDelegate = (SSAppDelegate *)[[UIApplication sharedApplication] delegate] ;
    appDelegate.monPanierViewController.isPushView=YES;
    
    //OLD CODE
//    [self.navigationController dismissViewControllerAnimated:YES completion:^{
//          [appDelegate selectTabAtIndex:0];
//    }];
    
    //new change raji
    [appDelegate selectTabAtIndex:0];
  
    
//   [self.navigationController dismissViewControllerAnimated:NO completion:nil];
  
//    NMBottomTabBarController *tabBarController;
//    tabBarController = (NMBottomTabBarController *)self.window.rootViewController;
//    tabBarController.delegate = self;
//    
//    [tabBarController.tabBar configureTabAtIndex:0
//                            andTitleOrientation :kTItleToBottomOfIcon
//                   withUnselectedBackgroundImage:nil
//                         selectedBackgroundImage:nil
//                                       iconImage:[UIImage imageNamed:@"shop_active_icon"]
//                                         andText:@"SHOP"
//                                     andTextFont:FuturaStd_Book(10)
//                                   andFontColour:[UIColor blackColor]];
//
   
}

@end
