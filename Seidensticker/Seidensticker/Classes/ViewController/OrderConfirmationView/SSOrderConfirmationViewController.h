//
//  SSOrderConfirmationViewController.h
//  Seidensticker
//
//  Created by ocs-mini-7 on 10/9/15.
//  Copyright (c) 2015 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSOrderConfirmationViewController : UIViewController

{
    IBOutlet UITextField *orderConfirmationReferenceNumberTextField;
    IBOutlet UITextView  *orderConfirmationTextView;
    IBOutlet UITableView *orderConfirmationTableView;
    IBOutlet UIView *divideViewOne,*divideViewTwo;
    IBOutlet UIScrollView *orderConfirmationBackgroundScrollView;
    
}

@property(nonatomic,retain) NSString *addressDisplayOrderNumber;


@property(nonatomic,assign)BOOL isPush;
-(IBAction)retourAccueil:(id)sender;
@end
