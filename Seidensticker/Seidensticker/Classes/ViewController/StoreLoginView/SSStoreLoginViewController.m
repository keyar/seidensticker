//
//  SSStoreLoginViewController.m
//  Seidensticker
//
//  Created by OCS iOS Developer Raji on 24/11/16.
//  Copyright © 2016 Nandha Kumar. All rights reserved.
//

#import "SSStoreLoginViewController.h"
#import "SSAppDelegate.h"
#import "SSConfig.h"
#import "SSAppCommon.h"
#import "SSCustomNavigation.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#import "NSString+validations.h"
#import "NMBottomTabBarController.h"
#import "BSKeyboardControls.h"
#import "SSShoppingCart.h"

@interface SSStoreLoginViewController ()<NMBottomTabBarDelegate,NMBottomTabBarControllerDelegate,UITextFieldDelegate>
{
    SSWebService                *webservice;
    
    SSCustomNavigation          *navigation;
    NMBottomTabBarController *tabBarController;

    BOOL            iskeyboard;
    //Keyboard return elements
    UIBarButtonItem         *doneBarItem;
    UIToolbar               *keyboardToolbar;
    UIBarButtonItem         *spaceBarItem;
    UIBarButtonItem         *previousBarItem;
    UIBarButtonItem         *nextBarItem;
    NSInteger               URLTag;
    UILabel                 *qtyCount;
    SSAppDelegate *appDelegate;
    
    UITextField *emailTextField;
    UITextField *passwordTextField;
    
    //NSString *strEmailAddress,*strPassword;
    
    CGFloat commonHeight;
    
    NSString    *deviceUDID;
    NSString    *deviceToken;
}
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@property (strong, nonatomic) UIButton *customButton;
@end

@implementation SSStoreLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    webservice = [[SSWebService alloc] init];
    appDelegate = (SSAppDelegate *) [UIApplication sharedApplication].delegate;

    [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
    [self.navigationController setNavigationBarHidden:NO];
    
    // [self styleNavBar];
    [self.view setUserInteractionEnabled:YES];

}- (void)viewDidAppear:(BOOL)animated
{
    [self initializeKeyboardToolBar];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [self.view setUserInteractionEnabled:YES];
    [super viewWillAppear:animated];
    
    deviceUDID =[[NSUserDefaults standardUserDefaults]valueForKey:ID_DEVICE];
    deviceToken=[[NSUserDefaults standardUserDefaults]valueForKey:DEVICETOKEN];

    
    [_loginBtnAction addTarget:self action:@selector(checkForValidations) forControlEvents:UIControlEventTouchUpInside];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        commonHeight =65;
    }
    else{
        commonHeight =90;
    }

    [self styleNavBar];
    

    
    [COMMON TrackerWithName:@"Login View"];
    
}

-(void) initializeKeyboardToolBar
{
    if (keyboardToolbar == nil)
    {
        keyboardToolbar            = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 38.0f)];
        keyboardToolbar.barStyle   = UIBarStyleBlackTranslucent;
        spaceBarItem    = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                         target:nil
                                                                         action:nil];
        
        doneBarItem     = [[UIBarButtonItem alloc]  initWithTitle:NSLocalizedString(@"Suivant", @"")
                                                            style:UIBarButtonItemStyleDone
                                                           target:self
                                                           action:@selector(gotoLogin:)];
        
        [keyboardToolbar setItems:[NSArray     arrayWithObjects:
                                   spaceBarItem,
                                   doneBarItem, nil]];
        
    }
}

#pragma mark - BSKeyboard Control Delegate

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls{
}
#pragma mark-navigation bar creation

- (void)styleNavBar
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    UINavigationBar *newNavBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, screen_width, 64.0)];
    [newNavBar setBackgroundColor:[UIColor clearColor]];
    
    [newNavBar setBarTintColor:[UIColor whiteColor]];
    newNavBar.tag=1001;
    UINavigationItem *newItem = [[UINavigationItem alloc] init];
    newItem.title = @"Connexion";
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                            NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)]}];
    [newNavBar pushNavigationItem:newItem animated:false];
    
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,64,self.view.frame.size.width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    
    [newNavBar addSubview:navBorder];
    [self.view addSubview:newNavBar];
    
}


#pragma mark- UITableview Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(IS_IPHONE4 ||IS_IPHONE5 ||IS_IPHONE6 ||IS_IPHONE6_Plus)
    {
        return 65;
    }
    else{
        return 90;
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(tableView == _firstloginTable)
    {
        return 1;
    }
    else
    {
        return 1;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == _firstloginTable)
    {
        return 2;
    }
    else
    {
        return 1;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier=@"Cell";
    SSMonPanierLoginViewCustomCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib;
        nib = [[NSBundle mainBundle] loadNibNamed:@"SSMonPanierLoginViewCustomCell" owner:self options:nil];
        
        cell= [nib objectAtIndex:0];
    }
    

        cell.loginTextField.delegate = self;
        if (indexPath.row == 0){
            cell.loginTextField.tag = 101;
            [cell.loginTextField setKeyboardType:UIKeyboardTypeEmailAddress];
            cell.loginTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                         initWithString:@"Adresse email..."
                                                         attributes:@{
                                                                      NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                      }];
        }
        if (indexPath.row == 1){
            cell.loginTextField.tag = 102;
            cell.loginTextField.secureTextEntry =YES;
            cell.loginTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                         initWithString:@"Mot de passe..."
                                                         attributes:@{
                                                                      NSFontAttributeName: [COMMON getResizeableFont:FuturaStd_Book(15)],NSForegroundColorAttributeName:[UIColor colorWithRed:113.0f/255.0f green:113.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
                                                                      }];
        }
        loginTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
        [self addBorderToCellBorder:cell];
        return cell;
    
}

#pragma mark - addBorderToCell
-(void)addBorderToCellBorder:(UITableViewCell*)cell{
    UIView *leftBorder;
    UIView *rightBorder;
    UIView *topBorder;
    UIView *bottomBorder;
    leftBorder = [[UIView alloc] initWithFrame:CGRectMake(1, 0, 1.5, cell.frame.size.height)];
    rightBorder = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.size.width-2.0f, 0, 1.5, cell.frame.size.height)];
    topBorder = [[UIView alloc] initWithFrame:CGRectMake(1, 0,cell.frame.size.width,1.5)];
    bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height-2.0f, cell.frame.size.width, 1.5)];
    [cell addSubview:leftBorder];
    [cell addSubview:topBorder];
    [cell addSubview:rightBorder];
    [cell addSubview:bottomBorder];
    [leftBorder setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin];
    [rightBorder setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin];
    [topBorder setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
    [bottomBorder setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
    leftBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
    rightBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
    topBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
    bottomBorder.backgroundColor = [UIColor colorWithRed:151.0f/255.0f green:151.0f/255.0f blue:151.0f/255.0f alpha:1.0f];
    
}

#pragma mark- hide keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //hides keyboard when another part of layout was touched
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
}
#pragma mark - checkForValidations
-(void)checkForValidations{
    
    usernameCell=(SSMonPanierLoginViewCustomCell *)[_firstloginTable visibleCells][0];
    passwordCell=(SSMonPanierLoginViewCustomCell *)[_firstloginTable visibleCells][1];
    
    strEmailAddress = usernameCell.loginTextField.text;
    strPassword     = passwordCell.loginTextField.text;
    
    [self.view setUserInteractionEnabled:NO];
    
    if([NSString isEmpty:strEmailAddress] && [NSString isEmpty:strPassword]){
        [SSAppCommon showSimpleAlertWithMessage:EMAIL_REQUIRED];
        [COMMON removeLoadingIcon];
        [self.view setUserInteractionEnabled:YES];
        return;
    }
    if(![NSString isEmpty:strEmailAddress] && [NSString isEmpty:strPassword]){
        [SSAppCommon showSimpleAlertWithMessage:INVALID_PASSWORD];
        [COMMON removeLoadingIcon];
        [self.view setUserInteractionEnabled:YES];
        return;
    }
    if([NSString isEmpty:strEmailAddress] && ![NSString isEmpty:strPassword]){
        [SSAppCommon showSimpleAlertWithMessage:EMAIL_REQUIRED];
        [COMMON removeLoadingIcon];
        [self.view setUserInteractionEnabled:YES];
        return;
    }
    if(![NSString isEmpty:strEmailAddress] && ![NSString isEmpty:strPassword]){
        if(![NSString validateEmail:strEmailAddress]){
            [SSAppCommon showSimpleAlertWithMessage:INVALID_EMAIL];
            [COMMON removeLoadingIcon];
            [self.view setUserInteractionEnabled:YES];
            return;
        }
        [self loginEmailAPI];
    }

}
#pragma mark - loginEmailAPI
-(void) loginEmailAPI{
    [COMMON showLoadingIcon:self];
    [webservice userLoginWithEmailAddress:strEmailAddress
                              andPassword:strPassword
                              deviceToken:deviceToken
                                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                      if ([responseObject objectForKey:@"customer"] != NULL) {
                                          [self.view setUserInteractionEnabled:YES];
                                          
                                          [COMMON storeUserDetails:[responseObject objectForKey:@"customer"]];
                                          
                                          [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IsStoreUserLoggedIn];
                                          [[NSUserDefaults standardUserDefaults] synchronize];
                                          
                                          
                                          [self goToHomeScreen];
                                          [COMMON removeLoadingIcon];
                                          
                                          
                                      }
                                      else if([responseObject objectForKey:@"error"] != NULL){
                                          [SSAppCommon showSimpleAlertWithMessage:[responseObject objectForKey:@"error"]];
                                          [COMMON removeLoadingIcon];
                                          [self.view setUserInteractionEnabled:YES];
                                      }
                                      else {
                                          [SSAppCommon showSimpleAlertWithMessage:INVALID_AUTHENTICATION];
                                          [COMMON removeLoadingIcon];
                                      }
                                      
                                      [self.view setUserInteractionEnabled:YES];
                                      
                                  }
                                  failure:^(AFHTTPRequestOperation *operation, id error) {
                                      [COMMON removeLoadingIcon];
                                      [self.view setUserInteractionEnabled:YES];
                                  }];
}

-(void)goToHomeScreen{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IsStoreUserLoggedIn];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
   
//   // [appDelegate loadHomewView];
//
//    UIWindow *window = [[UIWindow alloc]initWithFrame:self.view.bounds];
//    
//    tabBarController = ([[NMBottomTabBarController alloc]init];
//    
//    [tabBarController selectTabAtIndex:0];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}





@end
