//
//  SSCommandsTableViewCell.m
//  Seidensticker
//
//  Created by Admin on 29/07/16.
//  Copyright © 2016 Nandha Kumar. All rights reserved.
//

#import "SSCommandsTableViewCell.h"
#import "SSAppCommon.h"
#import "SSConfig.h"

@implementation SSCommandsTableViewCell
@synthesize mainView,titleLabel,sepLabel,firstTextView,sepTwoLabel,secondTextView;

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self setUpIntial];
}

- (void)setUpIntial {
    
    self.mainView.layer.borderWidth = 1.0f;
    self.mainView.layer.borderColor = [UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f blue:182.0f/255.0f alpha:1.0f].CGColor;
    
    [titleLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    [titleLabel setTextColor:[UIColor blackColor]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
    
    self.sepLabel.backgroundColor = [UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f blue:182.0f/255.0f alpha:1.0f];
    
    self.sepTwoLabel.backgroundColor = [UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f blue:182.0f/255.0f alpha:1.0f];
}

@end
