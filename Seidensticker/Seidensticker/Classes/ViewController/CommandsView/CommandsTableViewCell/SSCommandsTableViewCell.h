//
//  SSCommandsTableViewCell.h
//  Seidensticker
//
//  Created by Admin on 29/07/16.
//  Copyright © 2016 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSCommandsTableViewCell : UITableViewCell<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *mainView;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) IBOutlet UILabel *sepLabel;

@property (strong, nonatomic) IBOutlet UITextView *firstTextView;

@property (strong, nonatomic) IBOutlet UILabel *sepTwoLabel;

@property (strong, nonatomic) IBOutlet UITextView *secondTextView;

@end
