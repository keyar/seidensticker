//
//  SSCommandsViewController.m
//  Seidensticker
//
//  Created by Admin on 28/07/16.
//  Copyright © 2016 Nandha Kumar. All rights reserved.
//

#import "SSCommandsViewController.h"
#import "SSAppDelegate.h"
#import "SSMonPanierAddressViewController.h"
#import "SSMonPanierAddressModifierViewController.h"
#import "SSAppCommon.h"
#import "SSConfig.h"
#import "SSCustomNavigation.h"
#import "SSPaymentDetailViewController.h"
#import "SSWebService.h"
#import "SSWebServiceConfig.h"
#import "NSString+validations.h"
#import "NMBottomTabBarController.h"
#import "SSCommandsTableViewCell.h"
#import "SSShoppingCart.h"

//#import "SSPaymentViewController.h"

@interface SSCommandsViewController ()<UITextViewDelegate, UITableViewDelegate, UITableViewDataSource>
{
    SSWebService                *webService;
    NMBottomTabBarController    *bottomTabBar;
    NSMutableArray              *userArray;
    SSAppDelegate               *appDelegate;
    UIRefreshControl *refreshControl;
}

@property (strong, nonatomic) IBOutlet UITableView *commandsTableView;

@property (strong, nonatomic) IBOutlet UILabel *noRecordsLabel;

@property(nonatomic, strong) NSDictionary *commandsDictionary;

@end

@implementation SSCommandsViewController
@synthesize commandsDictionary,commandsArray;
@synthesize cartItems;
@synthesize isPushFromAccountPage;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    appDelegate = (SSAppDelegate *) [UIApplication sharedApplication].delegate;
    
    webService = [[SSWebService alloc] init];
    
    commandsArray = [NSArray new];
    // Initialize the refresh control.
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor whiteColor];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self
                       action:@selector(commandsAPI)
             forControlEvents:UIControlEventValueChanged];
    [_commandsTableView addSubview:refreshControl];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self showTabBar];
    
    
    [COMMON TrackerWithName:@"Mon Account View"];
   
    _noRecordsLabel.hidden = YES;
    
    [self commandsAPI];
    
    [self loadNavigation];
    
    [self setUpInitialValues];
    
}

- (void) setUpInitialValues {
    
    userArray = [[NSMutableArray alloc]init];
    
    NSDictionary *userDict = [[NSDictionary alloc]init];
    
    userDict = [COMMON getUserDetails];

    [_commandsTableView registerNib:[UINib nibWithNibName:@"SSCommandsTableViewCell" bundle:nil] forCellReuseIdentifier:@"SSCommandsTableViewCell"];
}

#pragma mark - Commands API

- (void) commandsAPI {
    [COMMON showLoadingIcon:self];
    [webService getCommandsData:[COMMON getCustomerSecureKey]
                        success:^(AFHTTPRequestOperation *operation, id responseObject){
                            [refreshControl endRefreshing];
                            NSLog(@"Response = %@",responseObject);
                            
                            [COMMON removeLoadingIcon];
                            
                            if ([responseObject count] == 0) {
                                
                                [COMMON removeLoadingIcon];
                                [self loadEmptyLabel];
                                
                            }
                            
                            

                            if([responseObject objectForKey:@"error"] != NULL){
                                
                                //[COMMON removeLoadingIcon];
                                //[COMMON showAlert:[responseObject objectForKey:@"error"]];
                                [COMMON removeLoadingIcon];
                                [self loadEmptyLabel];

                                
                            }

                            else if ([responseObject objectForKey:@"ordershistory"] != NULL) {
                                
                                NSDictionary *addressDict;
                                
                                addressDict = [responseObject mutableCopy];
                                
                                commandsArray = [[addressDict objectForKey:@"ordershistory"] objectForKey:@"orders_list"];
                                
                                [_commandsTableView reloadData];
                            }
                        }
                        failure:^(AFHTTPRequestOperation *operation, id error) {
                            [refreshControl endRefreshing];
                            //[SSAppCommon showSimpleAlertWithMessage:@"No Internet Connection available. Kindly check your Internet"];
                            [self loadEmptyLabel];
                            [COMMON removeLoadingIcon];
                            
                        }];
}
-(void)loadEmptyLabel{
    UILabel *noItemsLabel=[UILabel new];
    CGFloat XPos = 20;
    CGFloat YPos = screen_height/2;
    noItemsLabel.frame=CGRectMake(XPos ,(YPos-40) ,screen_width-(XPos*2),40);
    
    noItemsLabel.textAlignment=NSTextAlignmentCenter;
    [noItemsLabel setFont:[COMMON getResizeableFont:FuturaStd_Book(18)]];
    noItemsLabel.tag=EMPTYCARTLABEL_TAG;
    noItemsLabel.text=@"Votre commands est vide";
    [noItemsLabel setBackgroundColor:[UIColor clearColor]];
    _commandsTableView.hidden=YES;
    [self.view addSubview:noItemsLabel];
}


- (void) redirectToCommandsViewWithDictionary:(id) addressDictionary {
    
    SSCommandsViewController *commandesVC =  nil;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        commandesVC = [[SSCommandsViewController alloc]initWithNibName:@"SSCommandsViewControlleriPad" bundle:[NSBundle mainBundle]];
    }
    else {
        commandesVC = [[SSCommandsViewController alloc]initWithNibName:@"SSCommandsViewController" bundle:[NSBundle mainBundle]];
    }
    
    if([addressDictionary isKindOfClass:[NSArray class]])
        addressDictionary = [addressDictionary firstObject];
    
    commandesVC.commandsArray = [addressDictionary objectForKey:@"orders_list"];
    
    [self.navigationController pushViewController:commandesVC animated:YES];
}

#pragma mark- show and hide TabBar

- (void) showTabBar
{
    [appDelegate showTabBar];
}
- (void) hideTabbar {
    
    [appDelegate hideTabBar];
}


#pragma mark- loadNavigation

- (void)loadNavigation
{
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController.navigationBar setTranslucent:NO];
    SSCustomNavigation *navigation = [[SSCustomNavigation alloc] initWithNibName:@"SSCustomNavigation" bundle:nil];
    navigation.view.tag = 1001;
    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,44,screen_width,0.8)];
    [navBorder setBackgroundColor:[UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1.0f]];
    
    [navigation.view addSubview:navBorder];
    [self.navigationController.navigationBar addSubview:navigation.view];
    [navigation setLeftHidden:NO];
    [navigation setTitleHidden:NO];
    [navigation setShopBtnHidden:NO];
    [navigation setShopLabelHidden:NO];
    [navigation setCatergoryShopBtnLabelHidden:YES];
    [navigation.titleLbl setText:NSLocalizedString(@"Mes commandes", @"")];
    [navigation.titleLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(14)]];
    
    [navigation.navigationLeftLbl setText:NSLocalizedString(@"Retour", @"")];
    [navigation.navigationLeftLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(12)]];
    [navigation.navigationLeftBtn addTarget:self
                                     action:@selector(retourBack:)
                           forControlEvents:UIControlEventTouchUpInside];
    [self setLabel:navigation];
    
}
-(void)setLabel:(SSCustomNavigation*)navigation
{
    NSLog(@"cart values %@",[SHOPPINGCART getCartItems]);
    cartItems = [SHOPPINGCART getCartItems];
    
    NSInteger quantity = 0;
    
    
    for (int i = 0; i < [cartItems count]; i++) {
        
        quantity += [[[cartItems objectAtIndex:i] valueForKey:@"quantity"] integerValue];
    }
    
    navigation.navigationShopLbl.text = [NSString stringWithFormat:@"%ld",(long)quantity];
    [navigation.navigationShopLbl setBackgroundColor:[UIColor clearColor]];
    navigation.navigationShopLbl.textColor = [UIColor blackColor];
    [navigation.navigationShopLbl setFont:[COMMON getResizeableFont:FuturaStd_Book(8)]];
    [navigation.navigationShopLbl setTextAlignment:NSTextAlignmentCenter];
    
    [navigation.navigationShopBtn addTarget:self action:@selector(commandsCartBtnAction:) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark - Right Menu

-(void)commandsCartBtnAction:(id)sender
{
    SSMonPanierViewController *monpanierViewController;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonpanierViewControlleriPad" bundle:[NSBundle mainBundle]];
        
        
    }
    else {
        monpanierViewController = [[SSMonPanierViewController alloc]initWithNibName:@"SSMonPanierViewController" bundle:[NSBundle mainBundle]];
    }
    
    [self.navigationController pushViewController:monpanierViewController animated:YES];
}



-(void)retourBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    if(isPushFromAccountPage==YES){
        [[self.navigationController.navigationBar viewWithTag:1001] removeFromSuperview];
        [self.navigationController setNavigationBarHidden:NO];
    }
    else{
        [self.navigationController setNavigationBarHidden:YES];
    }
   
}


- (NSString *) setUpOrderValues
{
    NSString *strAddress = @"";
    
    NSString *orderId = [commandsDictionary valueForKey:@"id_order"];
    
    NSString *orderState = [commandsDictionary valueForKey:@"order_state"];
    
    NSString *totalPaid = [commandsDictionary valueForKey:@"total_paid"];
    
    
    if (orderId != NULL && ![orderId isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"COMMANDE %@ %@\n",strAddress,orderId];
    }
    if (totalPaid != NULL && ![totalPaid isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@MONTANT : %@\n",strAddress,totalPaid];
    }
    if ((NSString *)[NSNull null] == orderState||orderState == nil) {
        orderState=@"";
    }
    if (orderState != NULL && ![orderState isEqualToString:@""]) {
        strAddress = [NSString stringWithFormat:@"%@ÉTAT : %@\n",strAddress,orderState];
    }
    
    return strAddress;
}


- (NSMutableAttributedString *)getAttributedString:(NSString *)productStr {
    
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]init];
    
    NSDictionary *attributes = @{NSFontAttributeName : [COMMON getResizeableFont:FuturaStd_Book(15)],
                                 NSForegroundColorAttributeName : [UIColor blackColor]};
    
    NSAttributedString *firstAttributedString = [[NSAttributedString alloc] initWithString: [productStr stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"]
                                                                                attributes: attributes];
    
    
    
    [attributedStr appendAttributedString:[firstAttributedString mutableCopy]];
    
    return attributedStr;
}


#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return commandsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
     
    CGFloat cellHeight;
    
    commandsDictionary = [commandsArray objectAtIndex:indexPath.row];
    
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc]init];
    
    NSString *productDetailsStr = [commandsDictionary valueForKey:@"product_details"];
    
    [productDetailsStr stringByReplacingOccurrencesOfString:@"\\n\\n" withString:@"\n"];

    attributedStr = [self getAttributedString:productDetailsStr];
    
    CGRect rect = [attributedStr boundingRectWithSize:CGSizeMake(screen_width - 20, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    
    CGSize dataSize = CGSizeMake(rect.size.width , rect.size.height);
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        cellHeight = dataSize.height + 220;
    }
    else{
        cellHeight = dataSize.height + 280;
    }
    return cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    static NSString *simpleTableIdentifier = @"SSCommandsTableViewCell";
    SSCommandsTableViewCell *cell  = (SSCommandsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (indexPath.section == 0) {
        
        if (cell == nil)
        {
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            {
                NSArray *nib;
                nib     = [[NSBundle mainBundle] loadNibNamed:@"SSCommandsTableViewCell" owner:self options:nil];
                
            }
            else
            {
                NSArray *nib;
                nib     = [[NSBundle mainBundle] loadNibNamed:@"SSCommandsTableViewCell" owner:self options:nil];
               
                
            }
        }
    }
    commandsDictionary = [commandsArray objectAtIndex:indexPath.row];

    NSString *daterString;
    
    daterString = [commandsDictionary valueForKey:@"date_add"];
    
    cell.titleLabel.text = daterString;
    
    cell.firstTextView.text = [self setUpOrderValues];
    
    [cell.firstTextView setFont:[COMMON getResizeableFont:FuturaStd_Book(15)]];
    
    NSString *productDetailsStr = [commandsDictionary valueForKey:@"product_details"];
    
    cell.secondTextView.attributedText = [self getAttributedString:productDetailsStr];
    
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}


@end
