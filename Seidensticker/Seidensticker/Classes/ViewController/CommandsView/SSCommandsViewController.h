//
//  SSCommandsViewController.h
//  Seidensticker
//
//  Created by Admin on 28/07/16.
//  Copyright © 2016 Nandha Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSCommandsViewController : UIViewController

@property(nonatomic, strong) NSArray *commandsArray;
@property(strong,nonatomic) NSMutableArray *cartItems;
@property(nonatomic,assign) BOOL isPushFromAccountPage;
@end
